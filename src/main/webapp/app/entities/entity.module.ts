import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'credit',
        loadChildren: () => import('./credit/credit.module').then(m => m.MmdbCreditModule),
      },
      {
        path: 'genre',
        loadChildren: () => import('./genre/genre.module').then(m => m.MmdbGenreModule),
      },
      {
        path: 'image',
        loadChildren: () => import('./image/image.module').then(m => m.MmdbImageModule),
      },
      {
        path: 'image-data',
        loadChildren: () => import('./image-data/image-data.module').then(m => m.MmdbImageDataModule),
      },
      {
        path: 'movie',
        loadChildren: () => import('./movie/movie.module').then(m => m.MmdbMovieModule),
      },
      {
        path: 'my-movie',
        loadChildren: () => import('./my-movie/my-movie.module').then(m => m.MmdbMyMovieModule),
      },
      {
        path: 'person',
        loadChildren: () => import('./person/person.module').then(m => m.MmdbPersonModule),
      },
      /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
    ]),
  ],
})
export class MmdbEntityModule {}
