import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { MmdbSharedModule } from 'app/shared/shared.module';
import { CreditComponent } from './credit.component';
import { CreditDetailComponent } from './credit-detail.component';
import { CreditUpdateComponent } from './credit-update.component';
import { CreditDeleteDialogComponent } from './credit-delete-dialog.component';
import { creditRoute } from './credit.route';

@NgModule({
  imports: [MmdbSharedModule, RouterModule.forChild(creditRoute)],
  declarations: [CreditComponent, CreditDetailComponent, CreditUpdateComponent, CreditDeleteDialogComponent],
  entryComponents: [CreditDeleteDialogComponent],
})
export class MmdbCreditModule {}
