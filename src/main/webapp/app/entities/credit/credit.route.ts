import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { ICredit, Credit } from 'app/shared/model/credit.model';
import { CreditService } from './credit.service';
import { CreditComponent } from './credit.component';
import { CreditDetailComponent } from './credit-detail.component';
import { CreditUpdateComponent } from './credit-update.component';

@Injectable({ providedIn: 'root' })
export class CreditResolve implements Resolve<ICredit> {
  constructor(private service: CreditService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<ICredit> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((credit: HttpResponse<Credit>) => {
          if (credit.body) {
            return of(credit.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new Credit());
  }
}

export const creditRoute: Routes = [
  {
    path: '',
    component: CreditComponent,
    data: {
      authorities: [Authority.USER],
      pageTitle: 'mmdbApp.credit.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: CreditDetailComponent,
    resolve: {
      credit: CreditResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'mmdbApp.credit.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: CreditUpdateComponent,
    resolve: {
      credit: CreditResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'mmdbApp.credit.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: CreditUpdateComponent,
    resolve: {
      credit: CreditResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'mmdbApp.credit.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
];
