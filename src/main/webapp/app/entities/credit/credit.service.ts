import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as moment from 'moment';

import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { ICredit } from 'app/shared/model/credit.model';

type EntityResponseType = HttpResponse<ICredit>;
type EntityArrayResponseType = HttpResponse<ICredit[]>;

@Injectable({ providedIn: 'root' })
export class CreditService {
  public resourceUrl = SERVER_API_URL + 'api/credits';

  constructor(protected http: HttpClient) {}

  create(credit: ICredit): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(credit);
    return this.http
      .post<ICredit>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(credit: ICredit): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(credit);
    return this.http
      .put<ICredit>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<ICredit>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<ICredit[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  protected convertDateFromClient(credit: ICredit): ICredit {
    const copy: ICredit = Object.assign({}, credit, {
      lastTMDBUpdate: credit.lastTMDBUpdate && credit.lastTMDBUpdate.isValid() ? credit.lastTMDBUpdate.format(DATE_FORMAT) : undefined,
    });
    return copy;
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.lastTMDBUpdate = res.body.lastTMDBUpdate ? moment(res.body.lastTMDBUpdate) : undefined;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((credit: ICredit) => {
        credit.lastTMDBUpdate = credit.lastTMDBUpdate ? moment(credit.lastTMDBUpdate) : undefined;
      });
    }
    return res;
  }
}
