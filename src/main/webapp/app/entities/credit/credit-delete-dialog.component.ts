import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ICredit } from 'app/shared/model/credit.model';
import { CreditService } from './credit.service';

@Component({
  templateUrl: './credit-delete-dialog.component.html',
})
export class CreditDeleteDialogComponent {
  credit?: ICredit;

  constructor(protected creditService: CreditService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.creditService.delete(id).subscribe(() => {
      this.eventManager.broadcast('creditListModification');
      this.activeModal.close();
    });
  }
}
