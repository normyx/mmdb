import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager, JhiParseLinks } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { ICredit } from 'app/shared/model/credit.model';

import { ITEMS_PER_PAGE } from 'app/shared/constants/pagination.constants';
import { CreditService } from './credit.service';
import { CreditDeleteDialogComponent } from './credit-delete-dialog.component';

@Component({
  selector: 'jhi-credit',
  templateUrl: './credit.component.html',
})
export class CreditComponent implements OnInit, OnDestroy {
  credits: ICredit[];
  eventSubscriber?: Subscription;
  itemsPerPage: number;
  links: any;
  page: number;
  predicate: string;
  ascending: boolean;

  constructor(
    protected creditService: CreditService,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal,
    protected parseLinks: JhiParseLinks
  ) {
    this.credits = [];
    this.itemsPerPage = ITEMS_PER_PAGE;
    this.page = 0;
    this.links = {
      last: 0,
    };
    this.predicate = 'id';
    this.ascending = true;
  }

  loadAll(): void {
    this.creditService
      .query({
        page: this.page,
        size: this.itemsPerPage,
        sort: this.sort(),
      })
      .subscribe((res: HttpResponse<ICredit[]>) => this.paginateCredits(res.body, res.headers));
  }

  reset(): void {
    this.page = 0;
    this.credits = [];
    this.loadAll();
  }

  loadPage(page: number): void {
    this.page = page;
    this.loadAll();
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInCredits();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: ICredit): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInCredits(): void {
    this.eventSubscriber = this.eventManager.subscribe('creditListModification', () => this.reset());
  }

  delete(credit: ICredit): void {
    const modalRef = this.modalService.open(CreditDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.credit = credit;
  }

  sort(): string[] {
    const result = [this.predicate + ',' + (this.ascending ? 'asc' : 'desc')];
    if (this.predicate !== 'id') {
      result.push('id');
    }
    return result;
  }

  protected paginateCredits(data: ICredit[] | null, headers: HttpHeaders): void {
    const headersLink = headers.get('link');
    this.links = this.parseLinks.parse(headersLink ? headersLink : '');
    if (data) {
      for (let i = 0; i < data.length; i++) {
        this.credits.push(data[i]);
      }
    }
  }
}
