import { Component, OnInit, ElementRef } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { JhiDataUtils, JhiFileLoadError, JhiEventManager, JhiEventWithContent } from 'ng-jhipster';

import { IImageData, ImageData } from 'app/shared/model/image-data.model';
import { ImageDataService } from './image-data.service';
import { AlertError } from 'app/shared/alert/alert-error.model';
import { IImage } from 'app/shared/model/image.model';
import { ImageService } from 'app/entities/image/image.service';

@Component({
  selector: 'jhi-image-data-update',
  templateUrl: './image-data-update.component.html',
})
export class ImageDataUpdateComponent implements OnInit {
  isSaving = false;
  images: IImage[] = [];

  editForm = this.fb.group({
    id: [],
    imageSize: [null, [Validators.required, Validators.maxLength(40)]],
    imageBytes: [null, [Validators.required]],
    imageBytesContentType: [],
    imageId: [null, Validators.required],
  });

  constructor(
    protected dataUtils: JhiDataUtils,
    protected eventManager: JhiEventManager,
    protected imageDataService: ImageDataService,
    protected imageService: ImageService,
    protected elementRef: ElementRef,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ imageData }) => {
      this.updateForm(imageData);

      this.imageService.query().subscribe((res: HttpResponse<IImage[]>) => (this.images = res.body || []));
    });
  }

  updateForm(imageData: IImageData): void {
    this.editForm.patchValue({
      id: imageData.id,
      imageSize: imageData.imageSize,
      imageBytes: imageData.imageBytes,
      imageBytesContentType: imageData.imageBytesContentType,
      imageId: imageData.imageId,
    });
  }

  byteSize(base64String: string): string {
    return this.dataUtils.byteSize(base64String);
  }

  openFile(contentType: string, base64String: string): void {
    this.dataUtils.openFile(contentType, base64String);
  }

  setFileData(event: any, field: string, isImage: boolean): void {
    this.dataUtils.loadFileToForm(event, this.editForm, field, isImage).subscribe(null, (err: JhiFileLoadError) => {
      this.eventManager.broadcast(
        new JhiEventWithContent<AlertError>('mmdbApp.error', { ...err, key: 'error.file.' + err.key })
      );
    });
  }

  clearInputImage(field: string, fieldContentType: string, idInput: string): void {
    this.editForm.patchValue({
      [field]: null,
      [fieldContentType]: null,
    });
    if (this.elementRef && idInput && this.elementRef.nativeElement.querySelector('#' + idInput)) {
      this.elementRef.nativeElement.querySelector('#' + idInput).value = null;
    }
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const imageData = this.createFromForm();
    if (imageData.id !== undefined) {
      this.subscribeToSaveResponse(this.imageDataService.update(imageData));
    } else {
      this.subscribeToSaveResponse(this.imageDataService.create(imageData));
    }
  }

  private createFromForm(): IImageData {
    return {
      ...new ImageData(),
      id: this.editForm.get(['id'])!.value,
      imageSize: this.editForm.get(['imageSize'])!.value,
      imageBytesContentType: this.editForm.get(['imageBytesContentType'])!.value,
      imageBytes: this.editForm.get(['imageBytes'])!.value,
      imageId: this.editForm.get(['imageId'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IImageData>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: IImage): any {
    return item.id;
  }
}
