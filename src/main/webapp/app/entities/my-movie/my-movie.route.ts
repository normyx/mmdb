import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IMyMovie, MyMovie } from 'app/shared/model/my-movie.model';
import { MyMovieService } from './my-movie.service';
import { MyMovieComponent } from './my-movie.component';
import { MyMovieDetailComponent } from './my-movie-detail.component';
import { MyMovieUpdateComponent } from './my-movie-update.component';

@Injectable({ providedIn: 'root' })
export class MyMovieResolve implements Resolve<IMyMovie> {
  constructor(private service: MyMovieService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IMyMovie> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((myMovie: HttpResponse<MyMovie>) => {
          if (myMovie.body) {
            return of(myMovie.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new MyMovie());
  }
}

export const myMovieRoute: Routes = [
  {
    path: '',
    component: MyMovieComponent,
    data: {
      authorities: [Authority.USER],
      pageTitle: 'mmdbApp.myMovie.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: MyMovieDetailComponent,
    resolve: {
      myMovie: MyMovieResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'mmdbApp.myMovie.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: MyMovieUpdateComponent,
    resolve: {
      myMovie: MyMovieResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'mmdbApp.myMovie.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: MyMovieUpdateComponent,
    resolve: {
      myMovie: MyMovieResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'mmdbApp.myMovie.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
];
