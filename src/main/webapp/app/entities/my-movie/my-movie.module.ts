import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { MmdbSharedModule } from 'app/shared/shared.module';
import { MyMovieComponent } from './my-movie.component';
import { MyMovieDetailComponent } from './my-movie-detail.component';
import { MyMovieUpdateComponent } from './my-movie-update.component';
import { MyMovieDeleteDialogComponent } from './my-movie-delete-dialog.component';
import { myMovieRoute } from './my-movie.route';

@NgModule({
  imports: [MmdbSharedModule, RouterModule.forChild(myMovieRoute)],
  declarations: [MyMovieComponent, MyMovieDetailComponent, MyMovieUpdateComponent, MyMovieDeleteDialogComponent],
  entryComponents: [MyMovieDeleteDialogComponent],
})
export class MmdbMyMovieModule {}
