import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager, JhiParseLinks } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IMyMovie } from 'app/shared/model/my-movie.model';

import { ITEMS_PER_PAGE } from 'app/shared/constants/pagination.constants';
import { MyMovieService } from './my-movie.service';
import { MyMovieDeleteDialogComponent } from './my-movie-delete-dialog.component';

@Component({
  selector: 'jhi-my-movie',
  templateUrl: './my-movie.component.html',
})
export class MyMovieComponent implements OnInit, OnDestroy {
  myMovies: IMyMovie[];
  eventSubscriber?: Subscription;
  itemsPerPage: number;
  links: any;
  page: number;
  predicate: string;
  ascending: boolean;

  constructor(
    protected myMovieService: MyMovieService,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal,
    protected parseLinks: JhiParseLinks
  ) {
    this.myMovies = [];
    this.itemsPerPage = ITEMS_PER_PAGE;
    this.page = 0;
    this.links = {
      last: 0,
    };
    this.predicate = 'id';
    this.ascending = true;
  }

  loadAll(): void {
    this.myMovieService
      .query({
        page: this.page,
        size: this.itemsPerPage,
        sort: this.sort(),
      })
      .subscribe((res: HttpResponse<IMyMovie[]>) => this.paginateMyMovies(res.body, res.headers));
  }

  reset(): void {
    this.page = 0;
    this.myMovies = [];
    this.loadAll();
  }

  loadPage(page: number): void {
    this.page = page;
    this.loadAll();
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInMyMovies();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IMyMovie): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInMyMovies(): void {
    this.eventSubscriber = this.eventManager.subscribe('myMovieListModification', () => this.reset());
  }

  delete(myMovie: IMyMovie): void {
    const modalRef = this.modalService.open(MyMovieDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.myMovie = myMovie;
  }

  sort(): string[] {
    const result = [this.predicate + ',' + (this.ascending ? 'asc' : 'desc')];
    if (this.predicate !== 'id') {
      result.push('id');
    }
    return result;
  }

  protected paginateMyMovies(data: IMyMovie[] | null, headers: HttpHeaders): void {
    const headersLink = headers.get('link');
    this.links = this.parseLinks.parse(headersLink ? headersLink : '');
    if (data) {
      for (let i = 0; i < data.length; i++) {
        this.myMovies.push(data[i]);
      }
    }
  }
}
