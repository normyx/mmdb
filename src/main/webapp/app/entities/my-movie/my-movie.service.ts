import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as moment from 'moment';

import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IMyMovie } from 'app/shared/model/my-movie.model';

type EntityResponseType = HttpResponse<IMyMovie>;
type EntityArrayResponseType = HttpResponse<IMyMovie[]>;

@Injectable({ providedIn: 'root' })
export class MyMovieService {
  public resourceUrl = SERVER_API_URL + 'api/my-movies';

  constructor(protected http: HttpClient) {}

  create(myMovie: IMyMovie): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(myMovie);
    return this.http
      .post<IMyMovie>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(myMovie: IMyMovie): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(myMovie);
    return this.http
      .put<IMyMovie>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IMyMovie>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IMyMovie[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  protected convertDateFromClient(myMovie: IMyMovie): IMyMovie {
    const copy: IMyMovie = Object.assign({}, myMovie, {
      viewedDate: myMovie.viewedDate && myMovie.viewedDate.isValid() ? myMovie.viewedDate.format(DATE_FORMAT) : undefined,
    });
    return copy;
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.viewedDate = res.body.viewedDate ? moment(res.body.viewedDate) : undefined;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((myMovie: IMyMovie) => {
        myMovie.viewedDate = myMovie.viewedDate ? moment(myMovie.viewedDate) : undefined;
      });
    }
    return res;
  }
}
