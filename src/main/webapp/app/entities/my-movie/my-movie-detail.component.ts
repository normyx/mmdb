import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IMyMovie } from 'app/shared/model/my-movie.model';

@Component({
  selector: 'jhi-my-movie-detail',
  templateUrl: './my-movie-detail.component.html',
})
export class MyMovieDetailComponent implements OnInit {
  myMovie: IMyMovie | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ myMovie }) => (this.myMovie = myMovie));
  }

  previousState(): void {
    window.history.back();
  }
}
