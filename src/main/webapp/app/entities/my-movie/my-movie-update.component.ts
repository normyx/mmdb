import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IMyMovie, MyMovie } from 'app/shared/model/my-movie.model';
import { MyMovieService } from './my-movie.service';
import { IMovie } from 'app/shared/model/movie.model';
import { MovieService } from 'app/entities/movie/movie.service';
import { IUser } from 'app/core/user/user.model';
import { UserService } from 'app/core/user/user.service';

type SelectableEntity = IMovie | IUser;

@Component({
  selector: 'jhi-my-movie-update',
  templateUrl: './my-movie-update.component.html',
})
export class MyMovieUpdateComponent implements OnInit {
  isSaving = false;
  movies: IMovie[] = [];
  users: IUser[] = [];
  viewedDateDp: any;

  editForm = this.fb.group({
    id: [],
    comments: [null, [Validators.maxLength(4000)]],
    vote: [null, [Validators.min(0), Validators.max(5)]],
    viewedDate: [],
    movieId: [],
    userId: [],
  });

  constructor(
    protected myMovieService: MyMovieService,
    protected movieService: MovieService,
    protected userService: UserService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ myMovie }) => {
      this.updateForm(myMovie);

      this.movieService.query().subscribe((res: HttpResponse<IMovie[]>) => (this.movies = res.body || []));

      this.userService.query().subscribe((res: HttpResponse<IUser[]>) => (this.users = res.body || []));
    });
  }

  updateForm(myMovie: IMyMovie): void {
    this.editForm.patchValue({
      id: myMovie.id,
      comments: myMovie.comments,
      vote: myMovie.vote,
      viewedDate: myMovie.viewedDate,
      movieId: myMovie.movieId,
      userId: myMovie.userId,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const myMovie = this.createFromForm();
    if (myMovie.id !== undefined) {
      this.subscribeToSaveResponse(this.myMovieService.update(myMovie));
    } else {
      this.subscribeToSaveResponse(this.myMovieService.create(myMovie));
    }
  }

  private createFromForm(): IMyMovie {
    return {
      ...new MyMovie(),
      id: this.editForm.get(['id'])!.value,
      comments: this.editForm.get(['comments'])!.value,
      vote: this.editForm.get(['vote'])!.value,
      viewedDate: this.editForm.get(['viewedDate'])!.value,
      movieId: this.editForm.get(['movieId'])!.value,
      userId: this.editForm.get(['userId'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IMyMovie>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: SelectableEntity): any {
    return item.id;
  }
}
