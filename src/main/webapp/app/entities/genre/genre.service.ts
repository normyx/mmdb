import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as moment from 'moment';

import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IGenre } from 'app/shared/model/genre.model';

type EntityResponseType = HttpResponse<IGenre>;
type EntityArrayResponseType = HttpResponse<IGenre[]>;

@Injectable({ providedIn: 'root' })
export class GenreService {
  public resourceUrl = SERVER_API_URL + 'api/genres';

  constructor(protected http: HttpClient) {}

  create(genre: IGenre): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(genre);
    return this.http
      .post<IGenre>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(genre: IGenre): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(genre);
    return this.http
      .put<IGenre>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IGenre>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IGenre[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  protected convertDateFromClient(genre: IGenre): IGenre {
    const copy: IGenre = Object.assign({}, genre, {
      lastTMDBUpdate: genre.lastTMDBUpdate && genre.lastTMDBUpdate.isValid() ? genre.lastTMDBUpdate.format(DATE_FORMAT) : undefined,
    });
    return copy;
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.lastTMDBUpdate = res.body.lastTMDBUpdate ? moment(res.body.lastTMDBUpdate) : undefined;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((genre: IGenre) => {
        genre.lastTMDBUpdate = genre.lastTMDBUpdate ? moment(genre.lastTMDBUpdate) : undefined;
      });
    }
    return res;
  }
}
