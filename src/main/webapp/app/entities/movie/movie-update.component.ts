import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IMovie, Movie } from 'app/shared/model/movie.model';
import { MovieService } from './movie.service';
import { IGenre } from 'app/shared/model/genre.model';
import { GenreService } from 'app/entities/genre/genre.service';

@Component({
  selector: 'jhi-movie-update',
  templateUrl: './movie-update.component.html',
})
export class MovieUpdateComponent implements OnInit {
  isSaving = false;
  genres: IGenre[] = [];
  releaseDateDp: any;
  lastTMDBUpdateDp: any;

  editForm = this.fb.group({
    id: [],
    title: [null, [Validators.maxLength(200)]],
    forAdult: [],
    homepage: [],
    originalLanguage: [],
    originalTitle: [],
    overview: [null, [Validators.maxLength(4000)]],
    tagline: [],
    status: [],
    voteAverage: [null, [Validators.min(0), Validators.max(10)]],
    voteCount: [],
    releaseDate: [],
    lastTMDBUpdate: [null, [Validators.required]],
    tmdbId: [null, [Validators.required]],
    runtime: [],
    genres: [],
  });

  constructor(
    protected movieService: MovieService,
    protected genreService: GenreService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ movie }) => {
      this.updateForm(movie);

      this.genreService.query().subscribe((res: HttpResponse<IGenre[]>) => (this.genres = res.body || []));
    });
  }

  updateForm(movie: IMovie): void {
    this.editForm.patchValue({
      id: movie.id,
      title: movie.title,
      forAdult: movie.forAdult,
      homepage: movie.homepage,
      originalLanguage: movie.originalLanguage,
      originalTitle: movie.originalTitle,
      overview: movie.overview,
      tagline: movie.tagline,
      status: movie.status,
      voteAverage: movie.voteAverage,
      voteCount: movie.voteCount,
      releaseDate: movie.releaseDate,
      lastTMDBUpdate: movie.lastTMDBUpdate,
      tmdbId: movie.tmdbId,
      runtime: movie.runtime,
      genres: movie.genres,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const movie = this.createFromForm();
    if (movie.id !== undefined) {
      this.subscribeToSaveResponse(this.movieService.update(movie));
    } else {
      this.subscribeToSaveResponse(this.movieService.create(movie));
    }
  }

  private createFromForm(): IMovie {
    return {
      ...new Movie(),
      id: this.editForm.get(['id'])!.value,
      title: this.editForm.get(['title'])!.value,
      forAdult: this.editForm.get(['forAdult'])!.value,
      homepage: this.editForm.get(['homepage'])!.value,
      originalLanguage: this.editForm.get(['originalLanguage'])!.value,
      originalTitle: this.editForm.get(['originalTitle'])!.value,
      overview: this.editForm.get(['overview'])!.value,
      tagline: this.editForm.get(['tagline'])!.value,
      status: this.editForm.get(['status'])!.value,
      voteAverage: this.editForm.get(['voteAverage'])!.value,
      voteCount: this.editForm.get(['voteCount'])!.value,
      releaseDate: this.editForm.get(['releaseDate'])!.value,
      lastTMDBUpdate: this.editForm.get(['lastTMDBUpdate'])!.value,
      tmdbId: this.editForm.get(['tmdbId'])!.value,
      runtime: this.editForm.get(['runtime'])!.value,
      genres: this.editForm.get(['genres'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IMovie>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: IGenre): any {
    return item.id;
  }

  getSelected(selectedVals: IGenre[], option: IGenre): IGenre {
    if (selectedVals) {
      for (let i = 0; i < selectedVals.length; i++) {
        if (option.id === selectedVals[i].id) {
          return selectedVals[i];
        }
      }
    }
    return option;
  }
}
