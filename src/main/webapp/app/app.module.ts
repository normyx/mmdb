import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import './vendor';
import { MmdbSharedModule } from 'app/shared/shared.module';
import { MmdbCoreModule } from 'app/core/core.module';
import { MmdbAppRoutingModule } from './app-routing.module';
import { MmdbHomeModule } from './home/home.module';
import { MmdbEntityModule } from './entities/entity.module';
// jhipster-needle-angular-add-module-import JHipster will add new module here
import { MainComponent } from './layouts/main/main.component';
import { NavbarComponent } from './layouts/navbar/navbar.component';
import { FooterComponent } from './layouts/footer/footer.component';
import { PageRibbonComponent } from './layouts/profiles/page-ribbon.component';
import { ActiveMenuDirective } from './layouts/navbar/active-menu.directive';
import { ErrorComponent } from './layouts/error/error.component';
import { MymoviesdbModule } from './mymoviesdb/mymoviesdb.module';

@NgModule({
  imports: [
    BrowserModule,
    MmdbSharedModule,
    MmdbCoreModule,
    MmdbHomeModule,
    // jhipster-needle-angular-add-module JHipster will add new module here
    MymoviesdbModule,
    MmdbEntityModule,
    MmdbAppRoutingModule,
  ],
  declarations: [MainComponent, NavbarComponent, ErrorComponent, PageRibbonComponent, ActiveMenuDirective, FooterComponent],
  bootstrap: [MainComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class MmdbAppModule {}
