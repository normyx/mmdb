import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'duration' })
export class MMDBDurationPipe implements PipeTransform {
  transform(value: number | null = null): string {
    if (value) {
      const hours: number = Math.floor(value / 60);
      return hours + 'h' + (value - hours * 60) + 'm';
    } else {
      return '';
    }
  }
}
