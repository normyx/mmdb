import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { AvatarModule } from 'ngx-avatar';
import { MMDBMovieCreditComponent } from './mmdb-movie-credit.component';

@NgModule({
  declarations: [MMDBMovieCreditComponent],
  exports: [MMDBMovieCreditComponent],
  entryComponents: [MMDBMovieCreditComponent],
  // schemas: [ CUSTOM_ELEMENTS_SCHEMA ],
  imports: [
    // import AvatarModule in your app
    AvatarModule,
    CommonModule,
  ],
})
export class MMDBMovieCreditComponentModule {}
