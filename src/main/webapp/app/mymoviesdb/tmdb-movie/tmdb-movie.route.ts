import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IMovie, Movie } from 'app/shared/model/movie.model';
import { TMDBMovieService } from './tmdb-movie.service';
import { TMDBMovieComponent } from './tmdb-movie.component';
import { TMDBMovieDetailComponent } from './tmdb-movie-detail.component';

@Injectable({ providedIn: 'root' })
export class TMDBMovieResolve implements Resolve<IMovie> {
  constructor(private service: TMDBMovieService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IMovie> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((tMDBMovie: HttpResponse<Movie>) => {
          if (tMDBMovie.body) {
            return of(tMDBMovie.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new Movie());
  }
}

export const tMDBMovieExtRoute: Routes = [
  {
    path: '',
    component: TMDBMovieComponent,
    data: {
      authorities: [Authority.USER],
      pageTitle: 'mymoviesApp.tMDBMovie.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: TMDBMovieDetailComponent,
    resolve: {
      movie: TMDBMovieResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'mymoviesApp.tMDBMovie.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
];
