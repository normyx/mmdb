import { HttpResponse } from '@angular/common/http';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { IMovie } from 'app/shared/model/movie.model';
import { JhiEventManager, JhiParseLinks } from 'ng-jhipster';
import { Subscription } from 'rxjs';
import { TMDBMovieService } from './tmdb-movie.service';

@Component({
  selector: 'jhi-tmdb-movie',
  templateUrl: './tmdb-movie.component.html',
})
export class TMDBMovieComponent implements OnInit, OnDestroy {
  movies: IMovie[];
  eventSubscriber?: Subscription;
  currentSearch: string;

  constructor(
    protected tMDBMovieService: TMDBMovieService,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal,
    protected parseLinks: JhiParseLinks,
    protected activatedRoute: ActivatedRoute
  ) {
    this.movies = [];
    this.currentSearch =
      this.activatedRoute.snapshot && this.activatedRoute.snapshot.queryParams['search']
        ? this.activatedRoute.snapshot.queryParams['search']
        : '';
  }

  loadAll(): void {
    if (this.currentSearch) {
      this.tMDBMovieService
        .search({
          query: this.currentSearch,
        })
        .subscribe((res: HttpResponse<IMovie[]>) => (this.movies = res.body || []));
      return;
    }
  }

  reset(): void {
    this.movies = [];
    this.loadAll();
  }

  search(query: string): void {
    this.movies = [];

    this.currentSearch = query;
    this.loadAll();
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInTMDBMovies();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IMovie): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInTMDBMovies(): void {
    this.eventSubscriber = this.eventManager.subscribe('tMDBMovieListModification', () => this.reset());
  }
}
