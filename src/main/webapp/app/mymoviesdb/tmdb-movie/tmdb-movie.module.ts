import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { MmdbSharedModule } from 'app/shared/shared.module';
import { TMDBMovieDetailComponent } from './tmdb-movie-detail.component';
import { TMDBMovieComponent } from './tmdb-movie.component';
import { tMDBMovieExtRoute } from './tmdb-movie.route';

@NgModule({
  imports: [MmdbSharedModule, RouterModule.forChild(tMDBMovieExtRoute)],
  declarations: [TMDBMovieComponent, TMDBMovieDetailComponent],
  entryComponents: [],
})
export class MymoviesTMDBMovieModule {}
