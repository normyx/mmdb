import { HttpResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PersonService } from 'app/entities/person/person.service';
import { ICredit } from 'app/shared/model/credit.model';
import { IMovie } from 'app/shared/model/movie.model';
import { ITMDBImage } from '../model/tmdb-image.model';
import { MMDBMovieService } from './mmdb-movie.service';

@Component({
  selector: 'jhi-movie-detail',
  templateUrl: './mmdb-movie-detail.component.html',
  styleUrls: ['senscritique.scss'],
})
export class MMDBMovieDetailComponent implements OnInit {
  movie: IMovie | null = null;
  poster: ITMDBImage | null = null;
  backdrop: ITMDBImage | null = null;
  casts: ICredit[] | null = null;
  crew: ICredit[] | null = null;
  /*gaugeType: NgxGaugeType = 'full';
  gaugeMin = 0.0;
  gaugeMax = 10;
  gaugeCap: NgxGaugeCap = 'round';
  gaugeSize = 80;
  gaugeThick = 8;
  gaugeForegroundColor = 'rgba(255, 255, 255, 1)';*/

  constructor(protected activatedRoute: ActivatedRoute, protected movieService: MMDBMovieService, protected personService: PersonService) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ movie }) => (this.movie = movie));

    if (this.movie) {
      this.movieService.findMoviePoster(this.movie.id).subscribe((res: HttpResponse<ITMDBImage>) => {
        this.poster = res.body;
      });
      this.movieService.findMovieBackdrop(this.movie.id).subscribe((res: HttpResponse<ITMDBImage>) => {
        this.backdrop = res.body;
      });
      this.movieService.findCreditCastFromMovie(this.movie.id).subscribe((res: HttpResponse<ICredit[]>) => {
        this.casts = res.body;
      });
      this.movieService.findCreditCrewFromMovie(this.movie.id).subscribe((res: HttpResponse<ICredit[]>) => {
        this.crew = res.body;
      });
    }
  }

  previousState(): void {
    window.history.back();
  }

  getDirectors(): ICredit[] | null {
    const directors: ICredit[] = [];
    if (this.crew) {
      for (const crewMember of this.crew) {
        if (crewMember.job === 'Director') {
          directors.push(crewMember);
        }
      }
    }
    return directors;
  }

  getNovels(): ICredit[] | null {
    const novels: ICredit[] = [];
    if (this.crew) {
      for (const crewMember of this.crew) {
        if (crewMember.job === 'Novel') {
          novels.push(crewMember);
        }
      }
    }
    return novels;
  }

  getScreenplays(): ICredit[] | null {
    const screenplays: ICredit[] = [];
    if (this.crew) {
      for (const crewMember of this.crew) {
        if (crewMember.job === 'Screenplay') {
          screenplays.push(crewMember);
        }
      }
    }
    return screenplays;
  }
}
