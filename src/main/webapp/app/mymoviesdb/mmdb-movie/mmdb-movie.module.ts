import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { MmdbSharedModule } from 'app/shared/shared.module';
import { MMDBDurationPipe } from '../mmdb-pipe/mmdb-duration-pipe';
import { MMDBMovieDetailComponent } from './mmdb-movie-detail.component';
import { MMDBMovieComponent } from './mmdb-movie.component';
import { mmdbMovieRoute } from './mmdb-movie.route';
import { MMDBMovieCreditComponentModule } from 'app/mymoviesdb/mmdb-component/mmdb-movie-credit-component/mmdb-movie-credit.module';
import { MMDBMovieDetail2Component } from './mmdb-movie-detail2.component';
import { CommonModule } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  imports: [MmdbSharedModule, RouterModule.forChild(mmdbMovieRoute), MMDBMovieCreditComponentModule, CommonModule, NgbModule],
  declarations: [MMDBMovieComponent, MMDBMovieDetailComponent, MMDBMovieDetail2Component, MMDBDurationPipe],
  entryComponents: [],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class MymoviesdbMMDBMovieModule {}
