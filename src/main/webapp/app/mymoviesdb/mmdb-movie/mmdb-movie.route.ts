import { HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, Router, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { Authority } from 'app/shared/constants/authority.constants';
import { IMovie, Movie } from 'app/shared/model/movie.model';
import { EMPTY, Observable, of } from 'rxjs';
import { flatMap } from 'rxjs/operators';
import { MMDBMovieDetailComponent } from './mmdb-movie-detail.component';
import { MMDBMovieDetail2Component } from './mmdb-movie-detail2.component';
import { MMDBMovieComponent } from './mmdb-movie.component';
import { MMDBMovieService } from './mmdb-movie.service';

@Injectable({ providedIn: 'root' })
export class MMDBMovieResolve implements Resolve<IMovie> {
  constructor(private service: MMDBMovieService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IMovie> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((movie: HttpResponse<Movie>) => {
          if (movie.body) {
            return of(movie.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new Movie());
  }
}

export const mmdbMovieRoute: Routes = [
  {
    path: '',
    component: MMDBMovieComponent,
    data: {
      authorities: [Authority.USER],
      pageTitle: 'mymoviesdbApp.movie.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: MMDBMovieDetailComponent,
    resolve: {
      movie: MMDBMovieResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'mymoviesdbApp.movie.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view2',
    component: MMDBMovieDetail2Component,
    resolve: {
      movie: MMDBMovieResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'mymoviesdbApp.movie.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
];
