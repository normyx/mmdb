export interface IImageData {
  id?: number;
  imageSize?: string;
  imageBytesContentType?: string;
  imageBytes?: any;
  imageId?: number;
}

export class ImageData implements IImageData {
  constructor(
    public id?: number,
    public imageSize?: string,
    public imageBytesContentType?: string,
    public imageBytes?: any,
    public imageId?: number
  ) {}
}
