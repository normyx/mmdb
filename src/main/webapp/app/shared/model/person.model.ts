import { Moment } from 'moment';
import { IImage } from 'app/shared/model/image.model';

export interface IPerson {
  id?: number;
  birthday?: Moment;
  deathday?: Moment;
  name?: string;
  aka?: string;
  gender?: number;
  biography?: string;
  placeOfBirth?: string;
  homepage?: string;
  lastTMDBUpdate?: Moment;
  tmdbId?: string;
  profiles?: IImage[];
}

export class Person implements IPerson {
  constructor(
    public id?: number,
    public birthday?: Moment,
    public deathday?: Moment,
    public name?: string,
    public aka?: string,
    public gender?: number,
    public biography?: string,
    public placeOfBirth?: string,
    public homepage?: string,
    public lastTMDBUpdate?: Moment,
    public tmdbId?: string,
    public profiles?: IImage[]
  ) {}
}
