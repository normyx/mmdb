import { Moment } from 'moment';
import { ICredit } from 'app/shared/model/credit.model';
import { IImage } from 'app/shared/model/image.model';
import { IGenre } from 'app/shared/model/genre.model';
import { MovieStatus } from 'app/shared/model/enumerations/movie-status.model';

export interface IMovie {
  id?: number;
  title?: string;
  forAdult?: boolean;
  homepage?: string;
  originalLanguage?: string;
  originalTitle?: string;
  overview?: string;
  tagline?: string;
  status?: MovieStatus;
  voteAverage?: number;
  voteCount?: number;
  releaseDate?: Moment;
  lastTMDBUpdate?: Moment;
  tmdbId?: string;
  runtime?: number;
  credits?: ICredit[];
  posters?: IImage[];
  backdrops?: IImage[];
  genres?: IGenre[];
}

export class Movie implements IMovie {
  constructor(
    public id?: number,
    public title?: string,
    public forAdult?: boolean,
    public homepage?: string,
    public originalLanguage?: string,
    public originalTitle?: string,
    public overview?: string,
    public tagline?: string,
    public status?: MovieStatus,
    public voteAverage?: number,
    public voteCount?: number,
    public releaseDate?: Moment,
    public lastTMDBUpdate?: Moment,
    public tmdbId?: string,
    public runtime?: number,
    public credits?: ICredit[],
    public posters?: IImage[],
    public backdrops?: IImage[],
    public genres?: IGenre[]
  ) {
    this.forAdult = this.forAdult || false;
  }
}
