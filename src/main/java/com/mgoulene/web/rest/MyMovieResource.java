package com.mgoulene.web.rest;

import com.mgoulene.service.MyMovieService;
import com.mgoulene.web.rest.errors.BadRequestAlertException;
import com.mgoulene.service.dto.MyMovieDTO;
import com.mgoulene.service.dto.MyMovieCriteria;
import com.mgoulene.service.MyMovieQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.mgoulene.domain.MyMovie}.
 */
@RestController
@RequestMapping("/api")
public class MyMovieResource {

    private final Logger log = LoggerFactory.getLogger(MyMovieResource.class);

    private static final String ENTITY_NAME = "myMovie";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final MyMovieService myMovieService;

    private final MyMovieQueryService myMovieQueryService;

    public MyMovieResource(MyMovieService myMovieService, MyMovieQueryService myMovieQueryService) {
        this.myMovieService = myMovieService;
        this.myMovieQueryService = myMovieQueryService;
    }

    /**
     * {@code POST  /my-movies} : Create a new myMovie.
     *
     * @param myMovieDTO the myMovieDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new myMovieDTO, or with status {@code 400 (Bad Request)} if the myMovie has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/my-movies")
    public ResponseEntity<MyMovieDTO> createMyMovie(@Valid @RequestBody MyMovieDTO myMovieDTO) throws URISyntaxException {
        log.debug("REST request to save MyMovie : {}", myMovieDTO);
        if (myMovieDTO.getId() != null) {
            throw new BadRequestAlertException("A new myMovie cannot already have an ID", ENTITY_NAME, "idexists");
        }
        MyMovieDTO result = myMovieService.save(myMovieDTO);
        return ResponseEntity.created(new URI("/api/my-movies/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /my-movies} : Updates an existing myMovie.
     *
     * @param myMovieDTO the myMovieDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated myMovieDTO,
     * or with status {@code 400 (Bad Request)} if the myMovieDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the myMovieDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/my-movies")
    public ResponseEntity<MyMovieDTO> updateMyMovie(@Valid @RequestBody MyMovieDTO myMovieDTO) throws URISyntaxException {
        log.debug("REST request to update MyMovie : {}", myMovieDTO);
        if (myMovieDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        MyMovieDTO result = myMovieService.save(myMovieDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, myMovieDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /my-movies} : get all the myMovies.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of myMovies in body.
     */
    @GetMapping("/my-movies")
    public ResponseEntity<List<MyMovieDTO>> getAllMyMovies(MyMovieCriteria criteria, Pageable pageable) {
        log.debug("REST request to get MyMovies by criteria: {}", criteria);
        Page<MyMovieDTO> page = myMovieQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /my-movies/count} : count all the myMovies.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/my-movies/count")
    public ResponseEntity<Long> countMyMovies(MyMovieCriteria criteria) {
        log.debug("REST request to count MyMovies by criteria: {}", criteria);
        return ResponseEntity.ok().body(myMovieQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /my-movies/:id} : get the "id" myMovie.
     *
     * @param id the id of the myMovieDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the myMovieDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/my-movies/{id}")
    public ResponseEntity<MyMovieDTO> getMyMovie(@PathVariable Long id) {
        log.debug("REST request to get MyMovie : {}", id);
        Optional<MyMovieDTO> myMovieDTO = myMovieService.findOne(id);
        return ResponseUtil.wrapOrNotFound(myMovieDTO);
    }

    /**
     * {@code DELETE  /my-movies/:id} : delete the "id" myMovie.
     *
     * @param id the id of the myMovieDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/my-movies/{id}")
    public ResponseEntity<Void> deleteMyMovie(@PathVariable Long id) {
        log.debug("REST request to delete MyMovie : {}", id);
        myMovieService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
