package com.mgoulene.web.rest;

import com.mgoulene.service.ImageDataService;
import com.mgoulene.web.rest.errors.BadRequestAlertException;
import com.mgoulene.service.dto.ImageDataDTO;
import com.mgoulene.service.dto.ImageDataCriteria;
import com.mgoulene.service.ImageDataQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.mgoulene.domain.ImageData}.
 */
@RestController
@RequestMapping("/api")
public class ImageDataResource {

    private final Logger log = LoggerFactory.getLogger(ImageDataResource.class);

    private static final String ENTITY_NAME = "imageData";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ImageDataService imageDataService;

    private final ImageDataQueryService imageDataQueryService;

    public ImageDataResource(ImageDataService imageDataService, ImageDataQueryService imageDataQueryService) {
        this.imageDataService = imageDataService;
        this.imageDataQueryService = imageDataQueryService;
    }

    /**
     * {@code POST  /image-data} : Create a new imageData.
     *
     * @param imageDataDTO the imageDataDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new imageDataDTO, or with status {@code 400 (Bad Request)} if the imageData has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/image-data")
    public ResponseEntity<ImageDataDTO> createImageData(@Valid @RequestBody ImageDataDTO imageDataDTO) throws URISyntaxException {
        log.debug("REST request to save ImageData : {}", imageDataDTO);
        if (imageDataDTO.getId() != null) {
            throw new BadRequestAlertException("A new imageData cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ImageDataDTO result = imageDataService.save(imageDataDTO);
        return ResponseEntity.created(new URI("/api/image-data/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /image-data} : Updates an existing imageData.
     *
     * @param imageDataDTO the imageDataDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated imageDataDTO,
     * or with status {@code 400 (Bad Request)} if the imageDataDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the imageDataDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/image-data")
    public ResponseEntity<ImageDataDTO> updateImageData(@Valid @RequestBody ImageDataDTO imageDataDTO) throws URISyntaxException {
        log.debug("REST request to update ImageData : {}", imageDataDTO);
        if (imageDataDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ImageDataDTO result = imageDataService.save(imageDataDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, imageDataDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /image-data} : get all the imageData.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of imageData in body.
     */
    @GetMapping("/image-data")
    public ResponseEntity<List<ImageDataDTO>> getAllImageData(ImageDataCriteria criteria, Pageable pageable) {
        log.debug("REST request to get ImageData by criteria: {}", criteria);
        Page<ImageDataDTO> page = imageDataQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /image-data/count} : count all the imageData.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/image-data/count")
    public ResponseEntity<Long> countImageData(ImageDataCriteria criteria) {
        log.debug("REST request to count ImageData by criteria: {}", criteria);
        return ResponseEntity.ok().body(imageDataQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /image-data/:id} : get the "id" imageData.
     *
     * @param id the id of the imageDataDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the imageDataDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/image-data/{id}")
    public ResponseEntity<ImageDataDTO> getImageData(@PathVariable Long id) {
        log.debug("REST request to get ImageData : {}", id);
        Optional<ImageDataDTO> imageDataDTO = imageDataService.findOne(id);
        return ResponseUtil.wrapOrNotFound(imageDataDTO);
    }

    /**
     * {@code DELETE  /image-data/:id} : delete the "id" imageData.
     *
     * @param id the id of the imageDataDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/image-data/{id}")
    public ResponseEntity<Void> deleteImageData(@PathVariable Long id) {
        log.debug("REST request to delete ImageData : {}", id);
        imageDataService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
