package com.mgoulene.mmdb.web.rest;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.Optional;

import javax.servlet.http.HttpServletResponse;

import com.mgoulene.config.ApplicationProperties;
import com.mgoulene.mmdb.service.MMDBImageDataService;
import com.mgoulene.mmdb.service.MMDBImageService;
import com.mgoulene.mmdb.tmdb.service.TMDBAPIService;
import com.mgoulene.service.dto.ImageDTO;
import com.mgoulene.service.dto.ImageDataDTO;

import org.apache.commons.compress.utils.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * REST controller for managing {@link com.mgoulene.domain.TMDBMovie}.
 */
@RestController
@RequestMapping("/api")
public class MMDBPublicImageResource {

    private final Logger log = LoggerFactory.getLogger(MMDBResource.class);

    private final MMDBImageService mmdbImageService;

    private final MMDBImageDataService mmdbImageDataService;

    private final TMDBAPIService tmbAPIService;

    private final ApplicationProperties.TMDBProperties tmdbProps;

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    public MMDBPublicImageResource(MMDBImageService mmdbImageService, TMDBAPIService tmbAPIService,
            ApplicationProperties applicationProperties, MMDBImageDataService mmdbImageDataService) {
        this.mmdbImageService = mmdbImageService;
        this.tmbAPIService = tmbAPIService;
        this.tmdbProps = applicationProperties.getTMDB();
        this.mmdbImageDataService = mmdbImageDataService;
    }

    @GetMapping("/public/tmdb-image-from-tmdb/{imageSize}/{imageTmdbId}")
    public void getImageFromTMDB(@PathVariable(name = "imageSize") String imageSize,
            @PathVariable(name = "imageTmdbId") String imageTmdbId, HttpServletResponse response) throws IOException {
        boolean useCache = tmdbProps.isUsePersistentCache();
        imageTmdbId = "/" + imageTmdbId;
        ImageDataDTO imageDataDTO;
        if (useCache) {
            // try to find Image
            Optional<ImageDTO> imageDTOOpt = mmdbImageService.findOneWhereTMDBId(imageTmdbId);
            if (imageDTOOpt.isPresent()) {
                // try to find ImageData
                Optional<ImageDataDTO> imageDataDTOOpt = mmdbImageDataService.findOneWhereTMDBIdAndSize(imageTmdbId,
                        imageSize);
                if (imageDataDTOOpt.isPresent()) {
                    // got it in the cache
                    log.debug("ImageData {} found with size {}", imageTmdbId, imageSize);
                    imageDataDTO = imageDataDTOOpt.get();
                } else {
                    // need to find and save it
                    log.debug("ImageData {} not found with size {}, get it from TMDB", imageTmdbId, imageSize);
                    imageDataDTO = tmbAPIService.findOneTMDBImageData(imageTmdbId, imageSize);
                    imageDataDTO.setImageId(imageDTOOpt.get().getId());
                    imageDataDTO = mmdbImageDataService.save(imageDataDTO);
                }
            } else {
                // Error, the imageDTO is not found
                log.error("The Image with tmdbId {} is not found", imageTmdbId);
                return;
            }
        } else {
            imageDataDTO = tmbAPIService.findOneTMDBImageData(imageTmdbId, imageSize);
        }
        response.setContentType(imageDataDTO.getImageBytesContentType());
        ByteArrayInputStream is = new ByteArrayInputStream(imageDataDTO.getImageBytes());
        IOUtils.copy(is, response.getOutputStream());

    }

}
