package com.mgoulene.mmdb.web.rest;

import java.util.List;

import com.mgoulene.mmdb.service.MMDBCreditService;
import com.mgoulene.service.dto.CreditDTO;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * REST controller for managing {@link com.mgoulene.domain.TMDBMovie}.
 */
@RestController
@RequestMapping("/api")
public class MMDBCreditResource {

    private final Logger log = LoggerFactory.getLogger(MMDBCreditResource.class);

    private final MMDBCreditService mmdbCreditService;

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    public MMDBCreditResource(MMDBCreditService mmdbCreditService) {
        this.mmdbCreditService = mmdbCreditService;
    }

    @GetMapping("/mmdb-credit-cast-from-movie/{movieId}")
    public ResponseEntity<List<CreditDTO>> getCreditCastFromMovie(@PathVariable Long movieId) {
        log.debug("REST request to get all Credit from Movie : {}", movieId);
        List<CreditDTO> CreditDTOs = mmdbCreditService.findCastFromMovieId(movieId, 10);
        return ResponseEntity.ok().body(CreditDTOs);
    }

    @GetMapping("/mmdb-credit-crew-from-movie/{movieId}")
    public ResponseEntity<List<CreditDTO>> getCreditCrewFromMovie(@PathVariable Long movieId) {
        log.debug("REST request to get all Credit from Movie : {}", movieId);
        List<CreditDTO> CreditDTOs = mmdbCreditService.findCrewFromMovieId(movieId, 1000);
        return ResponseEntity.ok().body(CreditDTOs);
    }

}
