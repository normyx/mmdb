package com.mgoulene.mmdb.web.rest;

import java.util.Optional;

import com.mgoulene.mmdb.service.MMDBImageService;
import com.mgoulene.mmdb.web.dto.TMDBImageRestDTO;
import com.mgoulene.service.dto.ImageDTO;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.github.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.mgoulene.domain.TMDBMovie}.
 */
@RestController
@RequestMapping("/api")
public class MMDBResource {

    private final Logger log = LoggerFactory.getLogger(MMDBResource.class);

    private final MMDBImageService mmdbImageService;

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    public MMDBResource(MMDBImageService mmdbImageService) {
        this.mmdbImageService = mmdbImageService;
    }

    @GetMapping("/movie-poster/{movieId}")
    public ResponseEntity<TMDBImageRestDTO> getMoviePoster(@PathVariable Long movieId) {
        log.debug("REST request to get Movie Poster : {}", movieId);
        String tmdbId = mmdbImageService.findPreferedMoviePoster(movieId).get().getTmdbId();
        ImageDTO imageDTO = new ImageDTO();
        imageDTO.setTmdbId(tmdbId);
        return ResponseUtil.wrapOrNotFound(Optional.of(new TMDBImageRestDTO(imageDTO.getTmdbId())));
    }

    @GetMapping("/movie-backdrop/{movieId}")
    public ResponseEntity<TMDBImageRestDTO> getMovieBackdrop(@PathVariable Long movieId) {
        log.debug("REST request to get Movie Poster : {}", movieId);
        Optional<ImageDTO> imageDTOOpt = mmdbImageService.findPreferedMovieBackdrop(movieId);
        if (imageDTOOpt.isPresent()) {
            String tmdbId = imageDTOOpt.get().getTmdbId();
            ImageDTO imageDTO = new ImageDTO();
            imageDTO.setTmdbId(tmdbId);
            return ResponseUtil.wrapOrNotFound(Optional.of(new TMDBImageRestDTO(imageDTO.getTmdbId())));
        } else {
            return null;
        }
    }

    @GetMapping("/person-image/{personId}")
    public ResponseEntity<TMDBImageRestDTO> getPersonImage(@PathVariable Long personId) {
        log.debug("REST request to get PErson  Image : {}", personId);
        Optional<ImageDTO> imageDTOOpt = mmdbImageService.findPreferedPersonImage(personId);
        if (imageDTOOpt.isPresent()) {
            String tmdbId = imageDTOOpt.get().getTmdbId();
            ImageDTO imageDTO = new ImageDTO();
            imageDTO.setTmdbId(tmdbId);
            return ResponseUtil.wrapOrNotFound(Optional.of(new TMDBImageRestDTO(imageDTO.getTmdbId())));
        } else {
            return null;
        }
    }

}
