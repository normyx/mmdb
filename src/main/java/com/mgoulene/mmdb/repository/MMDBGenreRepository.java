package com.mgoulene.mmdb.repository;

import java.util.Optional;

import com.mgoulene.domain.Genre;
import com.mgoulene.repository.GenreRepository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;


@SuppressWarnings("unused")
@Repository
public interface MMDBGenreRepository extends GenreRepository {

    @Query("select genre from Genre genre where genre.tmdbId =:tmdbId")
    Optional<Genre> findOneWhereTMDBId(@Param("tmdbId") String tmdbId);
}
