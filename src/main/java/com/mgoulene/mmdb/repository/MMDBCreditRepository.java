package com.mgoulene.mmdb.repository;

import java.util.List;
import java.util.Optional;

import com.mgoulene.domain.Credit;
import com.mgoulene.repository.CreditRepository;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
/**
 * Spring Data  repository for the Credit entity.
 */
@SuppressWarnings("unused")
@Repository
public interface MMDBCreditRepository extends CreditRepository {

    @Query("select credit from Credit credit where credit.tmdbId =:tmdbId")
    Optional<Credit> findOneWhereTMDBId(@Param("tmdbId") String tmdbId);

    @Query("select credit from Credit credit where credit.movie.id =:movieId and credit.creditType = 'Cast' order by credit.order asc")
    List<Credit> findAllCastFromMovieId(@Param("movieId") Long movieId);

    @Query("select credit from Credit credit where credit.movie.id =:movieId and credit.creditType = 'Cast' order by credit.order asc")
    List<Credit> findFirstCastFromMovieId(@Param("movieId") Long movieId, Pageable pageable);

    @Query("select credit from Credit credit where credit.movie.id =:movieId and credit.creditType = 'Crew' order by credit.order asc")
    List<Credit> findAllCrewFromMovieId(@Param("movieId") Long movieId);

    @Query("select credit from Credit credit where credit.movie.id =:movieId and credit.creditType = 'Crew' order by credit.order asc")
    List<Credit> findFirstCrewFromMovieId(@Param("movieId") Long movieId, Pageable pageable);
}
