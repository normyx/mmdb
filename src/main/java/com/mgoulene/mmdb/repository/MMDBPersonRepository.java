package com.mgoulene.mmdb.repository;

import java.util.Optional;

import com.mgoulene.domain.Person;
import com.mgoulene.repository.PersonRepository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the Person entity.
 */
@SuppressWarnings("unused")
@Repository
public interface MMDBPersonRepository extends PersonRepository {

    @Query("select person from Person person where person.tmdbId =:tmdbId")
    Optional<Person> findOneWhereTMDBId(@Param("tmdbId") String tmdbId);
}
