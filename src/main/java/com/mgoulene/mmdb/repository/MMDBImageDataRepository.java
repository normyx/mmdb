package com.mgoulene.mmdb.repository;

import java.util.Optional;

import com.mgoulene.domain.ImageData;
import com.mgoulene.repository.ImageDataRepository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the Image entity.
 */
@SuppressWarnings("unused")
@Repository
public interface MMDBImageDataRepository extends ImageDataRepository {


    @Query("select imageData from ImageData imageData where imageData.image.tmdbId =:tmdbId and imageData.imageSize =:imageSize")
    Optional<ImageData> findOneWhereTMDBIdAndSize(@Param("tmdbId") String tmdbId, @Param("imageSize") String imageSize);
    
    

    

}
