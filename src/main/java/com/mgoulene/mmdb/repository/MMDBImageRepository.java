package com.mgoulene.mmdb.repository;

import java.util.List;
import java.util.Optional;

import com.mgoulene.domain.Image;
import com.mgoulene.repository.ImageRepository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the Image entity.
 */
@SuppressWarnings("unused")
@Repository
public interface MMDBImageRepository extends ImageRepository {


    @Query("select image from Image image where image.tmdbId =:tmdbId")
    Optional<Image> findOneWhereTMDBId(@Param("tmdbId") String id);
    
    @Query("select image from Image image where image.posterMovie.id =:movieId order by voteAverage desc")
    List<Image> findMoviePosters(@Param("movieId") Long movieId);

    @Query("select image from Image image where image.backdropMovie.id =:movieId order by voteAverage desc")
    List<Image> findMovieBacksrops(@Param("movieId") Long movieId);

    @Query("select image from Image image where image.person.id =:personId order by voteAverage desc")
    List<Image> findPersonImages(@Param("personId") Long personId);

    

}
