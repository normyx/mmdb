package com.mgoulene.mmdb.tmdb.service.kafka.producer;

import javax.annotation.PostConstruct;

import com.mgoulene.config.KafkaProperties;
import com.mgoulene.mmdb.tmdb.service.kafka.event.KafkaMovieEvent;
import com.mgoulene.service.dto.MovieDTO;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class TMDBMovieProducer {

    private final Logger log = LoggerFactory.getLogger(TMDBMovieProducer.class);

    private final KafkaProducer<String, KafkaMovieEvent> kafkaProducer;

    private final String topicName;

    public TMDBMovieProducer(@Value("${kafka.producer.tmdbmovie.name}") final String topicName,final KafkaProperties kafkaProperties) {
        this.topicName = topicName;
        this.kafkaProducer = new KafkaProducer<>(kafkaProperties.getProducer().get("tmdbmovie"));
    }

    @PostConstruct
    public void init() {
        Runtime.getRuntime().addShutdownHook(new Thread(this::shutdown));
    }

    public void send(final KafkaMovieEvent movieEvent) {
        final ProducerRecord<String, KafkaMovieEvent> record = new ProducerRecord<>(topicName,  movieEvent);
        try {
            log.info("Sending asynchronously a Movie record to topic: '" + topicName + "':"+movieEvent);
            kafkaProducer.send(record);
        } catch (final Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    public void shutdown() {
        log.info("Shutdown Kafka producer");
        kafkaProducer.close();
    }
}
