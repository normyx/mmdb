package com.mgoulene.mmdb.tmdb.service.kafka.producer;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import javax.annotation.PostConstruct;

import com.mgoulene.config.KafkaProperties;
import com.mgoulene.mmdb.tmdb.service.kafka.event.KafkaImageEvent;



@Service
public class TMDBImageProducer {

    private final Logger log = LoggerFactory.getLogger(TMDBImageProducer.class);

    private final KafkaProducer<String, KafkaImageEvent> kafkaProducer;

    private final String topicName;

    public TMDBImageProducer(@Value("${kafka.producer.tmdbimage.name}") final String topicName, final KafkaProperties kafkaProperties) {
        this.topicName = topicName;
        this.kafkaProducer = new KafkaProducer<>(kafkaProperties.getProducer().get("tmdbimage"));
    }

    @PostConstruct
    public void init() {
        Runtime.getRuntime().addShutdownHook(new Thread(this::shutdown));
    }

    public void send(final KafkaImageEvent message) {
        final ProducerRecord<String, KafkaImageEvent> record = new ProducerRecord<>(topicName,  message);
        try {
            log.info("Sending asynchronously a Image record to topic: '" + topicName + "':" +message);
            kafkaProducer.send(record);
        } catch (final Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    public void shutdown() {
        log.info("Shutdown Kafka producer");
        kafkaProducer.close();
    }
}
