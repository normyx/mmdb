package com.mgoulene.mmdb.tmdb.service.kafka.consumer;

import com.mgoulene.config.KafkaProperties;
import com.mgoulene.mmdb.service.kafka.consumer.MMDBGenericConsumer;
import com.mgoulene.mmdb.service.kafka.deserializer.MMDBDeserializationError;
import com.mgoulene.mmdb.tmdb.service.TMDBService;
import com.mgoulene.mmdb.tmdb.service.dto.TMDBImageDTO;
import com.mgoulene.mmdb.tmdb.service.kafka.event.KafkaImageEvent;
import com.mgoulene.mmdb.tmdb.service.kafka.event.KafkaImageEvent.KafkaImageAction;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.core.task.SimpleAsyncTaskExecutor;

import io.vavr.control.Either;


public class TMDBImageConsumer extends MMDBGenericConsumer<KafkaImageEvent> {

    private final Logger log = LoggerFactory.getLogger(TMDBImageConsumer.class);

    private TMDBService tmdbService;

    public TMDBImageConsumer(final String topicName,
            final KafkaProperties kafkaProperties, TMDBService tmdbService) {
        super(topicName, kafkaProperties.getConsumer().get("tmdbimage"), kafkaProperties.getPollingTimeout());

        this.tmdbService = tmdbService;

    }

    @Override
    protected void handleMessage(final ConsumerRecord<String, Either<MMDBDeserializationError, KafkaImageEvent>> record) {
        final Either<MMDBDeserializationError, KafkaImageEvent> value = record.value();

        if (value.isLeft()) {
            log.error("Deserialization record failure: {}", value.getLeft());
        } else {
            log.debug("topic = {}, partition = {}, offset = {},id = {}", record.topic(), record.partition(),
                    record.offset(), this, record.key(), record.value());
            // log.debug("Handling record: {}", value.get());
        }

        // TODO : Checks if Credit Exists

        KafkaImageEvent imageEvent = value.get();
        String relatedTmdbId = imageEvent.getRelatedTmdbId();
        TMDBImageDTO tmdbImageDTO = imageEvent.getImage();
        if (imageEvent.getAction() == KafkaImageAction.INSERT_OR_UPDATE_MOVIE_IMAGES) {
            tmdbService.createOrUpdateMovieImagesFromTMDB(relatedTmdbId);
        } else if (imageEvent.getAction() == KafkaImageAction.INSERT_OR_UPDATE_MOVIE_BACKDROP) {
            tmdbService.createOrUpdateMovieBackdrop(relatedTmdbId, tmdbImageDTO);
        } else if (imageEvent.getAction() == KafkaImageAction.INSERT_OR_UPDATE_MOVIE_POSTER) {
            tmdbService.createOrUpdateMoviePoster(relatedTmdbId, tmdbImageDTO);
        } else if (imageEvent.getAction() == KafkaImageAction.INSERT_OR_UPDATE_PERSON_ALL_PROFILES) {
            tmdbService.createOrUpdatePersonImagesFromTMDB(relatedTmdbId);
        } else if (imageEvent.getAction() == KafkaImageAction.INSERT_OR_UPDATE_PERSON_PROFILE) {
            tmdbService.createOrUpdatePersonProfile(relatedTmdbId, tmdbImageDTO);
        } else {
            log.error("Action not recognize: {}", imageEvent);

        }

    }

    @Bean
    public void executeKafkaTMDBImageRunner() {
        new SimpleAsyncTaskExecutor().execute(this);
    }
}
