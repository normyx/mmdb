package com.mgoulene.mmdb.tmdb.service.mapper;

import com.mgoulene.mmdb.tmdb.service.dto.TMDBGenreDTO;
import com.mgoulene.service.dto.GenreDTO;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;


@Mapper(componentModel = "spring", uses = {})
public interface TMDBGenreMapper extends TMDBMapper<TMDBGenreDTO, GenreDTO> {

    
    @Mapping(target = "id", ignore = true)
    @Mapping(target = "tmdbId", source = "id")
    GenreDTO toDTO(TMDBGenreDTO tmdbGenreDTO);

    
    @Mapping(target = "id", ignore = true)
    @Mapping(target = "tmdbId", source = "id")
    void updateDTO(TMDBGenreDTO tmdbGenreDTO,  @MappingTarget GenreDTO genreDTO);

}
