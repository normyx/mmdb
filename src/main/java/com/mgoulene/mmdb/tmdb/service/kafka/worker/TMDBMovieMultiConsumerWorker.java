package com.mgoulene.mmdb.tmdb.service.kafka.worker;

import com.mgoulene.config.KafkaProperties;
import com.mgoulene.mmdb.service.kafka.consumer.MMDBConsumerInterface;
import com.mgoulene.mmdb.service.kafka.worker.MMDBKafkaMultiConsumerWorker;
import com.mgoulene.mmdb.tmdb.service.TMDBService;
import com.mgoulene.mmdb.tmdb.service.kafka.consumer.TMDBMovieConsumer;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class TMDBMovieMultiConsumerWorker extends MMDBKafkaMultiConsumerWorker {

    private final TMDBService tmdbService;

    public TMDBMovieMultiConsumerWorker(@Value("${kafka.consumer.tmdbmovie.name}") final String topicName,
            final KafkaProperties kafkaProperties, TMDBService tmdbService) {
        super(topicName, kafkaProperties, 5);
        this.tmdbService = tmdbService;
    }

    @Override
    public MMDBConsumerInterface createConsumer() {
        return new TMDBMovieConsumer(topicName, kafkaProperties, tmdbService);
    }

}
