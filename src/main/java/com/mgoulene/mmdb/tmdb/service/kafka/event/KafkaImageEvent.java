package com.mgoulene.mmdb.tmdb.service.kafka.event;

import java.io.Serializable;

import com.mgoulene.mmdb.tmdb.service.dto.TMDBImageDTO;

public class KafkaImageEvent implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = 1L;

    public enum KafkaImageAction {
        INSERT_OR_UPDATE_MOVIE_IMAGES, INSERT_OR_UPDATE_MOVIE_POSTER, INSERT_OR_UPDATE_MOVIE_BACKDROP, INSERT_OR_UPDATE_PERSON_ALL_PROFILES, INSERT_OR_UPDATE_PERSON_PROFILE
    } 

    private String relatedTmdbId;

    private TMDBImageDTO image;

    private KafkaImageAction action;

    public KafkaImageEvent() {
    }

    public KafkaImageEvent(KafkaImageAction action) {
        this.action = action;
    }

    public String getRelatedTmdbId() {
        return relatedTmdbId;
    }

    public void setRelatedTmdbId(String relatedTmdbId) {
        this.relatedTmdbId = relatedTmdbId;
    }

    public TMDBImageDTO getImage() {
        return image;
    }

    public void setImage(TMDBImageDTO image) {
        this.image = image;
    }

    public KafkaImageAction getAction() {
        return action;
    }

    public void setAction(KafkaImageAction action) {
        this.action = action;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((action == null) ? 0 : action.hashCode());
        result = prime * result + ((image == null) ? 0 : image.hashCode());
        result = prime * result + ((relatedTmdbId == null) ? 0 : relatedTmdbId.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        KafkaImageEvent other = (KafkaImageEvent) obj;
        if (action != other.action)
            return false;
        if (image == null) {
            if (other.image != null)
                return false;
        } else if (!image.equals(other.image))
            return false;
        if (relatedTmdbId == null) {
            if (other.relatedTmdbId != null)
                return false;
        } else if (!relatedTmdbId.equals(other.relatedTmdbId))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "KafkaImageEvent [action=" + action + ", image=" + image + ", relatedTmdbId=" + relatedTmdbId + "]";
    }

    

    
}
