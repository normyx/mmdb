package com.mgoulene.mmdb.tmdb.service.kafka.event;

import java.io.Serializable;

import com.mgoulene.mmdb.tmdb.service.dto.TMDBCreditDTO;

public class KafkaCreditEvent implements Serializable {

    private static final long serialVersionUID = 1L;

    public enum KafkaCreditAction {
        INSERT_OR_UPDATE_ALL_FROM_MOVIE, INSERT_OR_UPDATE
    }

    private String tmdbMovieId;
    private String tmdbCreditId;
    private TMDBCreditDTO tmdbCreditDTO;

    private KafkaCreditAction action;

    public KafkaCreditEvent() {
    }

    public KafkaCreditEvent(KafkaCreditAction action) {

        this.action = action;
    }

    public String getTmdbMovieId() {
        return tmdbMovieId;
    }

    public void setTmdbMovieId(String tmdbMovieId) {
        this.tmdbMovieId = tmdbMovieId;
    }

    public KafkaCreditAction getAction() {
        return action;
    }

    public void setAction(KafkaCreditAction action) {
        this.action = action;
    }

    public String getTmdbCreditId() {
        return tmdbCreditId;
    }

    public void setTmdbCreditId(String tmdbCreditId) {
        this.tmdbCreditId = tmdbCreditId;
    }

    
    public TMDBCreditDTO getTmdbCreditDTO() {
        return tmdbCreditDTO;
    }

    public void setTmdbCreditDTO(TMDBCreditDTO tmdbCreditDTO) {
        this.tmdbCreditDTO = tmdbCreditDTO;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((action == null) ? 0 : action.hashCode());
        result = prime * result + ((tmdbCreditDTO == null) ? 0 : tmdbCreditDTO.hashCode());
        result = prime * result + ((tmdbCreditId == null) ? 0 : tmdbCreditId.hashCode());
        result = prime * result + ((tmdbMovieId == null) ? 0 : tmdbMovieId.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        KafkaCreditEvent other = (KafkaCreditEvent) obj;
        if (action != other.action)
            return false;
        if (tmdbCreditDTO == null) {
            if (other.tmdbCreditDTO != null)
                return false;
        } else if (!tmdbCreditDTO.equals(other.tmdbCreditDTO))
            return false;
        if (tmdbCreditId == null) {
            if (other.tmdbCreditId != null)
                return false;
        } else if (!tmdbCreditId.equals(other.tmdbCreditId))
            return false;
        if (tmdbMovieId == null) {
            if (other.tmdbMovieId != null)
                return false;
        } else if (!tmdbMovieId.equals(other.tmdbMovieId))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "KafkaCreditEvent [action=" + action + ", tmdbCreditDTO=" + tmdbCreditDTO + ", tmdbCreditId="
                + tmdbCreditId + ", tmdbMovieId=" + tmdbMovieId + "]";
    }

   

}
