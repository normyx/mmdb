package com.mgoulene.mmdb.tmdb.service.kafka.event;

import java.io.Serializable;



public class KafkaPictureEvent implements Serializable {

    private static final long serialVersionUID = 1L;

    public enum KafkaPictureAction {
        INSERT_OR_UPDATE_MOVIE_COVER, INSERT_OR_UPDATE_MOVIE_BACKDROP, INSERT_OR_UPDATE_PEOPLE_PICTURE
    }

    private String dependentTmdbId;
    private String tmdbId;
    

    private KafkaPictureAction action;

    public KafkaPictureEvent() {
    }

    public KafkaPictureEvent(KafkaPictureAction action) {

        this.action = action;
    }

    public String getDependentTmdbId() {
        return dependentTmdbId;
    }

    public void setDependentTmdbId(String dependentTmdbId) {
        this.dependentTmdbId = dependentTmdbId;
    }

    public String getTmdbId() {
        return tmdbId;
    }

    public void setTmdbId(String tmdbId) {
        this.tmdbId = tmdbId;
    }

    public KafkaPictureAction getAction() {
        return action;
    }

    public void setAction(KafkaPictureAction action) {
        this.action = action;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((action == null) ? 0 : action.hashCode());
        result = prime * result + ((dependentTmdbId == null) ? 0 : dependentTmdbId.hashCode());
        result = prime * result + ((tmdbId == null) ? 0 : tmdbId.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        KafkaPictureEvent other = (KafkaPictureEvent) obj;
        if (action != other.action)
            return false;
        if (dependentTmdbId == null) {
            if (other.dependentTmdbId != null)
                return false;
        } else if (!dependentTmdbId.equals(other.dependentTmdbId))
            return false;
        if (tmdbId == null) {
            if (other.tmdbId != null)
                return false;
        } else if (!tmdbId.equals(other.tmdbId))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "KafkaPictureEvent [action=" + action + ", dependentTmdbId=" + dependentTmdbId + ", tmdbId=" + tmdbId
                + "]";
    }

    

   

}
