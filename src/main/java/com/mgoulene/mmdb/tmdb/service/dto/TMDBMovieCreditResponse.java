package com.mgoulene.mmdb.tmdb.service.dto;

import java.util.Arrays;

import com.fasterxml.jackson.annotation.JsonProperty;

public class TMDBMovieCreditResponse {

  @JsonProperty
  private TMDBCreditDTO[] cast;
  @JsonProperty
  private TMDBCreditDTO[] crew;

  public TMDBMovieCreditResponse() {

  }

  public TMDBCreditDTO[] getCast() {
    return cast;
  }

  public void setCast(TMDBCreditDTO[] cast) {
    this.cast = cast;
  }

  public TMDBCreditDTO[] getCrew() {
    return crew;
  }

  public void setCrew(TMDBCreditDTO[] crew) {
    this.crew = crew;
  }

  @Override
  public String toString() {
    return "TMDBMovieCreditResponse [cast=" + Arrays.toString(cast) + ", crew=" + Arrays.toString(crew) + "]";
  }

  

}