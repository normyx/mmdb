package com.mgoulene.mmdb.tmdb.web.rest;

import java.io.IOException;
import java.util.Optional;

import com.mgoulene.mmdb.tmdb.service.TMDBAPIService;
import com.mgoulene.mmdb.tmdb.service.TMDBService;
import com.mgoulene.mmdb.tmdb.service.dto.TMDBMovieImageDTO;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.github.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.mgoulene.domain.TMDBMovie}.
 */
@RestController
@RequestMapping("/api")
public class TMDBPictureResource {

    private final Logger log = LoggerFactory.getLogger(TMDBPictureResource.class);

    private final TMDBService tmdbService;
    private final TMDBAPIService tmdbApiService;

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    public TMDBPictureResource(TMDBService tmdbService, TMDBAPIService tmdbApiService) {
        this.tmdbService = tmdbService;
        this.tmdbApiService = tmdbApiService;
    }

    /**
     * {@code GET  /movies/:id} : get the "id" movie.
     *
     * @param id the id of the movieDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body
     *         the movieDTO, or with status {@code 404 (Not Found)}.
     * @throws IOException
     */
    /*@GetMapping("/tmdb-picture/{id}")
    public ResponseEntity<PictureDTO> getPicture(@PathVariable String id) throws IOException {
        log.debug("REST request to get TMDBPictureDTO : {}", id);
        PictureDTO pictureDTO = tmdbApiService.findOneTMDBPicture(id);
        return ResponseUtil.wrapOrNotFound(Optional.of(pictureDTO));
    }*/

     /**
     * {@code GET  /movies/:id} : get the "id" movie.
     *
     * @param id the id of the movieDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body
     *         the movieDTO, or with status {@code 404 (Not Found)}.
     * @throws IOException
     */
    @GetMapping("/tmdb-movie-images/{id}")
    public ResponseEntity<TMDBMovieImageDTO> getMovieImages(@PathVariable String id) throws IOException {
        log.debug("REST request to get TMDBPictureDTO : {}", id);
        TMDBMovieImageDTO movieImageDTO = tmdbApiService.findTMDBMovieImages(id);
        return ResponseUtil.wrapOrNotFound(Optional.of(movieImageDTO));
    }


}
