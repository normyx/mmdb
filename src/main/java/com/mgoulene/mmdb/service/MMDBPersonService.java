package com.mgoulene.mmdb.service;

import java.time.LocalDate;
import java.util.Optional;

import com.mgoulene.mmdb.repository.MMDBPersonRepository;
import com.mgoulene.service.PersonService;
import com.mgoulene.service.dto.PersonDTO;
import com.mgoulene.service.mapper.PersonMapper;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class MMDBPersonService extends PersonService {

    private final MMDBPersonRepository mmdbPersonRepository;

    private final Logger log = LoggerFactory.getLogger(MMDBPersonService.class);

    public MMDBPersonService(PersonMapper personMapper, MMDBPersonRepository mmdbPersonRepository) {
        super(mmdbPersonRepository, personMapper);
        this.mmdbPersonRepository = mmdbPersonRepository;
    }

    /**
     * Get one movie by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<PersonDTO> findOneWhereTMDBId(String tmdbId) {
        log.debug("Request to get Movie : {}", tmdbId);
        return mmdbPersonRepository.findOneWhereTMDBId(tmdbId).map(personMapper::toDto);
    }

    @Override
    public PersonDTO save(PersonDTO personDTO) {
        personDTO.setLastTMDBUpdate(LocalDate.now());
        return super.save(personDTO);
    }

}
