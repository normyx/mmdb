package com.mgoulene.mmdb.service;

import java.time.LocalDate;
import java.util.Optional;

import com.mgoulene.mmdb.repository.MMDBGenreRepository;
import com.mgoulene.service.GenreService;
import com.mgoulene.service.dto.GenreDTO;
import com.mgoulene.service.mapper.GenreMapper;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class MMDBGenreService extends GenreService {

    private final MMDBGenreRepository mmdbGenreRepository;

    private final Logger log = LoggerFactory.getLogger(MMDBGenreService.class);

    public MMDBGenreService(GenreMapper genreMapper, MMDBGenreRepository mmdbGenreRepository) {
        super(mmdbGenreRepository, genreMapper);
        this.mmdbGenreRepository = mmdbGenreRepository;
    }

    /**
     * Get one movie by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<GenreDTO> findOneWhereTMDBId(String tmdbId) {
        log.debug("Request to get Movie : {}", tmdbId);
        return mmdbGenreRepository.findOneWhereTMDBId(tmdbId).map(genreMapper::toDto);
    }

    @Override
    public GenreDTO save(GenreDTO genreDTO) {
        genreDTO.setLastTMDBUpdate(LocalDate.now());
        return super.save(genreDTO);
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public GenreDTO saveAndCommit(GenreDTO genreDTO) {
        return save(genreDTO);
    }

}
