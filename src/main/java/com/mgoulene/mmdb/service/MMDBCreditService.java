package com.mgoulene.mmdb.service;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import com.mgoulene.mmdb.repository.MMDBCreditRepository;
import com.mgoulene.service.CreditService;
import com.mgoulene.service.dto.CreditDTO;
import com.mgoulene.service.mapper.CreditMapper;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class MMDBCreditService extends CreditService {

    private final MMDBCreditRepository mmdbCreditRepository;

    private final Logger log = LoggerFactory.getLogger(MMDBCreditService.class);

    public MMDBCreditService(CreditMapper creditMapper, MMDBCreditRepository mmdbCreditRepository) {
        super(mmdbCreditRepository, creditMapper);
        this.mmdbCreditRepository = mmdbCreditRepository;

    }

    @Transactional(readOnly = true)
    public Optional<CreditDTO> findOneWhereTMDBId(String tmdbId) {
        log.debug("Request to get Credit : {}", tmdbId);
        return mmdbCreditRepository.findOneWhereTMDBId(tmdbId).map(creditMapper::toDto);
    }

    @Override
    public CreditDTO save(CreditDTO creditDTO) {
        creditDTO.setLastTMDBUpdate(LocalDate.now());
        return super.save(creditDTO);
    }

    @Transactional(readOnly = true)
    public List<CreditDTO> findCastFromMovieId(Long movieId, Integer limit) {
        log.debug("Request to get all Credit Cast from MovieId : {} with limit {}", movieId, limit);
        if (limit == null) {
            return creditMapper.toDto(mmdbCreditRepository.findAllCastFromMovieId(movieId));
        } else {
            return creditMapper.toDto(mmdbCreditRepository.findFirstCastFromMovieId(movieId, PageRequest.of(0, limit)));
        }
    }

    @Transactional(readOnly = true)
    public List<CreditDTO> findCrewFromMovieId(Long movieId, Integer limit) {
        log.debug("Request to get all Credit Crew from MovieId : {} with limit {}", movieId, limit);
        if (limit == null) {
            return creditMapper.toDto(mmdbCreditRepository.findAllCrewFromMovieId(movieId));
        } else {
            return creditMapper.toDto(mmdbCreditRepository.findFirstCrewFromMovieId(movieId, PageRequest.of(0, limit)));
        }
    }

}
