package com.mgoulene.mmdb.service;

import java.util.Optional;

import com.mgoulene.mmdb.repository.MMDBImageDataRepository;
import com.mgoulene.service.ImageDataService;
import com.mgoulene.service.dto.ImageDataDTO;
import com.mgoulene.service.mapper.ImageDataMapper;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class MMDBImageDataService extends ImageDataService {

    private final Logger log = LoggerFactory.getLogger(MMDBImageDataService.class);

    private final MMDBImageDataRepository mmdbImageDataRepository;

    public MMDBImageDataService(MMDBImageDataRepository mmdbImageDataRepository, ImageDataMapper imageDataMapper) {
        super(mmdbImageDataRepository, imageDataMapper);
        this.mmdbImageDataRepository = mmdbImageDataRepository;
    }

    public Optional<ImageDataDTO> findOneWhereTMDBIdAndSize(String tmdbId, String imageSize) {
        log.debug("Request to get ImageData : {}, size {}", tmdbId, imageSize);
        return mmdbImageDataRepository.findOneWhereTMDBIdAndSize(tmdbId, imageSize).map(imageDataMapper::toDto);

    }

}
