package com.mgoulene.mmdb.service.kafka.consumer;

import io.vavr.control.Either;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.errors.WakeupException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import com.mgoulene.mmdb.service.kafka.deserializer.MMDBDeserializationError;

import java.time.Duration;
import java.util.Collections;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;

public abstract class MMDBGenericConsumer<T> implements MMDBConsumerInterface {

    private final Logger log = LoggerFactory.getLogger(MMDBGenericConsumer.class);

    private final AtomicBoolean closed = new AtomicBoolean(false);

    private final KafkaConsumer<String, Either<MMDBDeserializationError, T>> consumer;
    private final String topicName;
    private final int pollingTimeout;

    public MMDBGenericConsumer(final String topicName, final Map<String, Object> properties, final int pollingTimeout) {
        this.topicName = topicName;
        this.consumer = new KafkaConsumer<>(properties);
        this.pollingTimeout = pollingTimeout;
    }

    @PostConstruct
    public void init() {
        Runtime.getRuntime().addShutdownHook(new Thread(this::shutdown));
    }

    @PreDestroy
    public void destroy() {
        shutdown();
    }

    @Override
    public void run() {
        try {
            consumer.subscribe(Collections.singleton(topicName));
            while (!closed.get()) {
                final ConsumerRecords<String, Either<MMDBDeserializationError, T>> records = consumer.poll(Duration.ofMillis(pollingTimeout));
                for (final ConsumerRecord<String, Either<MMDBDeserializationError, T>> record : records) {
                    //log.debug("handleMessage {}", record);
                    handleMessage(record);
                    
                }
                consumer.commitSync();
            }
        } catch (final WakeupException e) {
            // Ignore exception if closing
            if (!closed.get()) throw e;
        } catch (final Exception e) {
            log.error("An error occurred while trying to poll records from topic!", e);
        } finally {
            consumer.close();
        }
    }

    // Shutdown hook which can be called from a separate thread
    public void shutdown() {
        log.debug("Shutting Down");
        closed.set(true);
        consumer.wakeup();
    }

    protected abstract void handleMessage(ConsumerRecord<String, Either<MMDBDeserializationError, T>> record);
}
