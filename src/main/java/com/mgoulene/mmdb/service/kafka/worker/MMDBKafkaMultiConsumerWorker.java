package com.mgoulene.mmdb.service.kafka.worker;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import com.mgoulene.config.KafkaProperties;
import com.mgoulene.mmdb.service.kafka.consumer.MMDBConsumerInterface;

public abstract class MMDBKafkaMultiConsumerWorker {
    List<MMDBConsumerInterface> consumers;
    final int nbrOfWorkers;
    final protected String topicName;
    final protected KafkaProperties kafkaProperties;
    ExecutorService executor;

    public MMDBKafkaMultiConsumerWorker(final String topicName, final KafkaProperties kafkaProperties, final int nbrOfWorkers) {
        consumers = new ArrayList<>();
        this.topicName = topicName;
        this.kafkaProperties = kafkaProperties;
        this.nbrOfWorkers = nbrOfWorkers;
        

    }

    @PostConstruct
    public void init() {
        executor = Executors.newFixedThreadPool(nbrOfWorkers);
        for (int i = 0; i < nbrOfWorkers; i++) {
            MMDBConsumerInterface consumer = createConsumer();
            consumers.add(consumer);
            executor.submit(consumer);
            Runtime.getRuntime().addShutdownHook(new Thread(consumer::shutdown));
        }

    }

    @PreDestroy
    public void destroy() {
        for (MMDBConsumerInterface consumer : consumers) {
            consumer.shutdown();
        }
        executor.shutdown();
        try {
            executor.awaitTermination(5000, TimeUnit.MILLISECONDS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public abstract MMDBConsumerInterface createConsumer();

}
