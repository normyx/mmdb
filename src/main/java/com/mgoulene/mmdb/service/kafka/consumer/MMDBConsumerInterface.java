package com.mgoulene.mmdb.service.kafka.consumer;

public interface MMDBConsumerInterface extends Runnable {
    public  void shutdown();
    
}
