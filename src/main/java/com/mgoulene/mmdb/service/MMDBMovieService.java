package com.mgoulene.mmdb.service;

import java.time.LocalDate;
import java.util.Optional;

import com.mgoulene.mmdb.repository.MMDBMovieRepository;
import com.mgoulene.service.MovieService;
import com.mgoulene.service.dto.MovieDTO;
import com.mgoulene.service.mapper.MovieMapper;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class MMDBMovieService extends MovieService {

    private final Logger log = LoggerFactory.getLogger(MMDBMovieService.class);

    private final MMDBMovieRepository mmdbMovieRepository;

    public MMDBMovieService(MovieMapper movieMapper, MMDBMovieRepository mmdbMovieRepository) {
        super(mmdbMovieRepository, movieMapper);
        this.mmdbMovieRepository = mmdbMovieRepository;
    }

    /**
     * Get one movie by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<MovieDTO> findOneWhereTMDBId(String tmdbId) {
        log.debug("Request to get Movie : {}", tmdbId);
        return mmdbMovieRepository.findOneWhereTMDBIdWithEagerRelationships(tmdbId).map(movieMapper::toDto);
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public MovieDTO saveAndCommit(MovieDTO movieDTO) {
        log.debug("Request to save and commit Movie : {}", movieDTO);
        return save(movieDTO);
    }

    @Override
    public MovieDTO save(MovieDTO movieDTO) {
        // TODO Auto-generated method stub
        movieDTO.setLastTMDBUpdate(LocalDate.now());
        return super.save(movieDTO);
    }

}
