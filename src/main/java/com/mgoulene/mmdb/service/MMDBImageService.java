package com.mgoulene.mmdb.service;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import com.mgoulene.domain.Image;
import com.mgoulene.mmdb.repository.MMDBImageRepository;
import com.mgoulene.service.ImageService;
import com.mgoulene.service.dto.ImageDTO;
import com.mgoulene.service.mapper.ImageMapper;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class MMDBImageService extends ImageService {

    private final Logger log = LoggerFactory.getLogger(MMDBImageService.class);

    private final MMDBImageRepository mmdbImageRepository;

    public MMDBImageService(ImageMapper imageMapper, MMDBImageRepository mmdbImageRepository) {
        super(mmdbImageRepository, imageMapper);
        this.mmdbImageRepository = mmdbImageRepository;
    }

    public Optional<ImageDTO> findOneWhereTMDBId(String tmdbId) {
        log.debug("Request to get Picture: {}", tmdbId);
        Optional<ImageDTO> imageDTOOpt = mmdbImageRepository.findOneWhereTMDBId(tmdbId).map(imageMapper::toDto);
        if (imageDTOOpt.isPresent()) {
            return imageDTOOpt;
        }
        return Optional.empty();

    }

    @Override
    public ImageDTO save(ImageDTO imageDTO) {
        imageDTO.setLastTMDBUpdate(LocalDate.now());
        return super.save(imageDTO);
    }

    @Transactional(readOnly = true)
    public Optional<ImageDTO> findPreferedMoviePoster(Long movieId) {
        log.debug("Request to get Poster from movie: {}", movieId);
        List<Image> posters = mmdbImageRepository.findMoviePosters(movieId);
        if (posters != null && posters.size() != 0) {
            return Optional.of(imageMapper.toDto(posters.get(0)));
        } else {
            return Optional.empty();
        }

    }

    @Transactional(readOnly = true)
    public Optional<ImageDTO> findPreferedMovieBackdrop(Long movieId) {
        log.debug("Request to get Backdrop from movie: {}", movieId);
        List<Image> backdrops = mmdbImageRepository.findMovieBacksrops(movieId);
        if (backdrops != null && backdrops.size() != 0) {
            return Optional.of(imageMapper.toDto(backdrops.get(0)));
        } else {
            return Optional.empty();
        }

    }

    @Transactional(readOnly = true)
    public Optional<ImageDTO> findPreferedPersonImage(Long personId) {
        log.debug("Request to get Image from person: {}", personId);
        List<Image> images = mmdbImageRepository.findPersonImages(personId);
        if (images != null && images.size() != 0) {
            return Optional.of(imageMapper.toDto(images.get(0)));
        } else {
            return Optional.empty();
        }

    }

}
