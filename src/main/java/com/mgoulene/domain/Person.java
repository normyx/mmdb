package com.mgoulene.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

/**
 * The Person entity.\n@author A true hipster
 */
@Entity
@Table(name = "person")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Person implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "birthday")
    private LocalDate birthday;

    @Column(name = "deathday")
    private LocalDate deathday;

    @Size(max = 200)
    @Column(name = "name", length = 200)
    private String name;

    @Size(max = 200)
    @Column(name = "aka", length = 200)
    private String aka;

    @Column(name = "gender")
    private Integer gender;

    @Size(max = 4000)
    @Column(name = "biography", length = 4000)
    private String biography;

    @Size(max = 200)
    @Column(name = "place_of_birth", length = 200)
    private String placeOfBirth;

    @Size(max = 200)
    @Column(name = "homepage", length = 200)
    private String homepage;

    @NotNull
    @Column(name = "last_tmdb_update", nullable = false)
    private LocalDate lastTMDBUpdate;

    @NotNull
    @Column(name = "tmdb_id", nullable = false, unique = true)
    private String tmdbId;

    @OneToMany(mappedBy = "person")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    private Set<Image> profiles = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getBirthday() {
        return birthday;
    }

    public Person birthday(LocalDate birthday) {
        this.birthday = birthday;
        return this;
    }

    public void setBirthday(LocalDate birthday) {
        this.birthday = birthday;
    }

    public LocalDate getDeathday() {
        return deathday;
    }

    public Person deathday(LocalDate deathday) {
        this.deathday = deathday;
        return this;
    }

    public void setDeathday(LocalDate deathday) {
        this.deathday = deathday;
    }

    public String getName() {
        return name;
    }

    public Person name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAka() {
        return aka;
    }

    public Person aka(String aka) {
        this.aka = aka;
        return this;
    }

    public void setAka(String aka) {
        this.aka = aka;
    }

    public Integer getGender() {
        return gender;
    }

    public Person gender(Integer gender) {
        this.gender = gender;
        return this;
    }

    public void setGender(Integer gender) {
        this.gender = gender;
    }

    public String getBiography() {
        return biography;
    }

    public Person biography(String biography) {
        this.biography = biography;
        return this;
    }

    public void setBiography(String biography) {
        this.biography = biography;
    }

    public String getPlaceOfBirth() {
        return placeOfBirth;
    }

    public Person placeOfBirth(String placeOfBirth) {
        this.placeOfBirth = placeOfBirth;
        return this;
    }

    public void setPlaceOfBirth(String placeOfBirth) {
        this.placeOfBirth = placeOfBirth;
    }

    public String getHomepage() {
        return homepage;
    }

    public Person homepage(String homepage) {
        this.homepage = homepage;
        return this;
    }

    public void setHomepage(String homepage) {
        this.homepage = homepage;
    }

    public LocalDate getLastTMDBUpdate() {
        return lastTMDBUpdate;
    }

    public Person lastTMDBUpdate(LocalDate lastTMDBUpdate) {
        this.lastTMDBUpdate = lastTMDBUpdate;
        return this;
    }

    public void setLastTMDBUpdate(LocalDate lastTMDBUpdate) {
        this.lastTMDBUpdate = lastTMDBUpdate;
    }

    public String getTmdbId() {
        return tmdbId;
    }

    public Person tmdbId(String tmdbId) {
        this.tmdbId = tmdbId;
        return this;
    }

    public void setTmdbId(String tmdbId) {
        this.tmdbId = tmdbId;
    }

    public Set<Image> getProfiles() {
        return profiles;
    }

    public Person profiles(Set<Image> images) {
        this.profiles = images;
        return this;
    }

    public Person addProfile(Image image) {
        this.profiles.add(image);
        image.setPerson(this);
        return this;
    }

    public Person removeProfile(Image image) {
        this.profiles.remove(image);
        image.setPerson(null);
        return this;
    }

    public void setProfiles(Set<Image> images) {
        this.profiles = images;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Person)) {
            return false;
        }
        return id != null && id.equals(((Person) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Person{" +
            "id=" + getId() +
            ", birthday='" + getBirthday() + "'" +
            ", deathday='" + getDeathday() + "'" +
            ", name='" + getName() + "'" +
            ", aka='" + getAka() + "'" +
            ", gender=" + getGender() +
            ", biography='" + getBiography() + "'" +
            ", placeOfBirth='" + getPlaceOfBirth() + "'" +
            ", homepage='" + getHomepage() + "'" +
            ", lastTMDBUpdate='" + getLastTMDBUpdate() + "'" +
            ", tmdbId='" + getTmdbId() + "'" +
            "}";
    }
}
