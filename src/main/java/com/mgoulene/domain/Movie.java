package com.mgoulene.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

import com.mgoulene.domain.enumeration.MovieStatus;

/**
 * The Movie entity.\n@author A true hipster
 */
@Entity
@Table(name = "movie")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Movie implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Size(max = 200)
    @Column(name = "title", length = 200)
    private String title;

    @Column(name = "for_adult")
    private Boolean forAdult;

    @Column(name = "homepage")
    private String homepage;

    @Column(name = "original_language")
    private String originalLanguage;

    @Column(name = "original_title")
    private String originalTitle;

    @Size(max = 4000)
    @Column(name = "overview", length = 4000)
    private String overview;

    @Column(name = "tagline")
    private String tagline;

    @Enumerated(EnumType.STRING)
    @Column(name = "status")
    private MovieStatus status;

    @DecimalMin(value = "0")
    @DecimalMax(value = "10")
    @Column(name = "vote_average")
    private Float voteAverage;

    @Column(name = "vote_count")
    private Integer voteCount;

    @Column(name = "release_date")
    private LocalDate releaseDate;

    @NotNull
    @Column(name = "last_tmdb_update", nullable = false)
    private LocalDate lastTMDBUpdate;

    @NotNull
    @Column(name = "tmdb_id", nullable = false, unique = true)
    private String tmdbId;

    @Column(name = "runtime")
    private Integer runtime;

    @OneToMany(mappedBy = "movie")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    private Set<Credit> credits = new HashSet<>();

    @OneToMany(mappedBy = "posterMovie")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    private Set<Image> posters = new HashSet<>();

    @OneToMany(mappedBy = "backdropMovie")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    private Set<Image> backdrops = new HashSet<>();

    @ManyToMany
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JoinTable(name = "movie_genre",
               joinColumns = @JoinColumn(name = "movie_id", referencedColumnName = "id"),
               inverseJoinColumns = @JoinColumn(name = "genre_id", referencedColumnName = "id"))
    private Set<Genre> genres = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public Movie title(String title) {
        this.title = title;
        return this;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Boolean isForAdult() {
        return forAdult;
    }

    public Movie forAdult(Boolean forAdult) {
        this.forAdult = forAdult;
        return this;
    }

    public void setForAdult(Boolean forAdult) {
        this.forAdult = forAdult;
    }

    public String getHomepage() {
        return homepage;
    }

    public Movie homepage(String homepage) {
        this.homepage = homepage;
        return this;
    }

    public void setHomepage(String homepage) {
        this.homepage = homepage;
    }

    public String getOriginalLanguage() {
        return originalLanguage;
    }

    public Movie originalLanguage(String originalLanguage) {
        this.originalLanguage = originalLanguage;
        return this;
    }

    public void setOriginalLanguage(String originalLanguage) {
        this.originalLanguage = originalLanguage;
    }

    public String getOriginalTitle() {
        return originalTitle;
    }

    public Movie originalTitle(String originalTitle) {
        this.originalTitle = originalTitle;
        return this;
    }

    public void setOriginalTitle(String originalTitle) {
        this.originalTitle = originalTitle;
    }

    public String getOverview() {
        return overview;
    }

    public Movie overview(String overview) {
        this.overview = overview;
        return this;
    }

    public void setOverview(String overview) {
        this.overview = overview;
    }

    public String getTagline() {
        return tagline;
    }

    public Movie tagline(String tagline) {
        this.tagline = tagline;
        return this;
    }

    public void setTagline(String tagline) {
        this.tagline = tagline;
    }

    public MovieStatus getStatus() {
        return status;
    }

    public Movie status(MovieStatus status) {
        this.status = status;
        return this;
    }

    public void setStatus(MovieStatus status) {
        this.status = status;
    }

    public Float getVoteAverage() {
        return voteAverage;
    }

    public Movie voteAverage(Float voteAverage) {
        this.voteAverage = voteAverage;
        return this;
    }

    public void setVoteAverage(Float voteAverage) {
        this.voteAverage = voteAverage;
    }

    public Integer getVoteCount() {
        return voteCount;
    }

    public Movie voteCount(Integer voteCount) {
        this.voteCount = voteCount;
        return this;
    }

    public void setVoteCount(Integer voteCount) {
        this.voteCount = voteCount;
    }

    public LocalDate getReleaseDate() {
        return releaseDate;
    }

    public Movie releaseDate(LocalDate releaseDate) {
        this.releaseDate = releaseDate;
        return this;
    }

    public void setReleaseDate(LocalDate releaseDate) {
        this.releaseDate = releaseDate;
    }

    public LocalDate getLastTMDBUpdate() {
        return lastTMDBUpdate;
    }

    public Movie lastTMDBUpdate(LocalDate lastTMDBUpdate) {
        this.lastTMDBUpdate = lastTMDBUpdate;
        return this;
    }

    public void setLastTMDBUpdate(LocalDate lastTMDBUpdate) {
        this.lastTMDBUpdate = lastTMDBUpdate;
    }

    public String getTmdbId() {
        return tmdbId;
    }

    public Movie tmdbId(String tmdbId) {
        this.tmdbId = tmdbId;
        return this;
    }

    public void setTmdbId(String tmdbId) {
        this.tmdbId = tmdbId;
    }

    public Integer getRuntime() {
        return runtime;
    }

    public Movie runtime(Integer runtime) {
        this.runtime = runtime;
        return this;
    }

    public void setRuntime(Integer runtime) {
        this.runtime = runtime;
    }

    public Set<Credit> getCredits() {
        return credits;
    }

    public Movie credits(Set<Credit> credits) {
        this.credits = credits;
        return this;
    }

    public Movie addCredits(Credit credit) {
        this.credits.add(credit);
        credit.setMovie(this);
        return this;
    }

    public Movie removeCredits(Credit credit) {
        this.credits.remove(credit);
        credit.setMovie(null);
        return this;
    }

    public void setCredits(Set<Credit> credits) {
        this.credits = credits;
    }

    public Set<Image> getPosters() {
        return posters;
    }

    public Movie posters(Set<Image> images) {
        this.posters = images;
        return this;
    }

    public Movie addPoster(Image image) {
        this.posters.add(image);
        image.setPosterMovie(this);
        return this;
    }

    public Movie removePoster(Image image) {
        this.posters.remove(image);
        image.setPosterMovie(null);
        return this;
    }

    public void setPosters(Set<Image> images) {
        this.posters = images;
    }

    public Set<Image> getBackdrops() {
        return backdrops;
    }

    public Movie backdrops(Set<Image> images) {
        this.backdrops = images;
        return this;
    }

    public Movie addBackdrop(Image image) {
        this.backdrops.add(image);
        image.setBackdropMovie(this);
        return this;
    }

    public Movie removeBackdrop(Image image) {
        this.backdrops.remove(image);
        image.setBackdropMovie(null);
        return this;
    }

    public void setBackdrops(Set<Image> images) {
        this.backdrops = images;
    }

    public Set<Genre> getGenres() {
        return genres;
    }

    public Movie genres(Set<Genre> genres) {
        this.genres = genres;
        return this;
    }

    public Movie addGenre(Genre genre) {
        this.genres.add(genre);
        genre.getMovies().add(this);
        return this;
    }

    public Movie removeGenre(Genre genre) {
        this.genres.remove(genre);
        genre.getMovies().remove(this);
        return this;
    }

    public void setGenres(Set<Genre> genres) {
        this.genres = genres;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Movie)) {
            return false;
        }
        return id != null && id.equals(((Movie) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Movie{" +
            "id=" + getId() +
            ", title='" + getTitle() + "'" +
            ", forAdult='" + isForAdult() + "'" +
            ", homepage='" + getHomepage() + "'" +
            ", originalLanguage='" + getOriginalLanguage() + "'" +
            ", originalTitle='" + getOriginalTitle() + "'" +
            ", overview='" + getOverview() + "'" +
            ", tagline='" + getTagline() + "'" +
            ", status='" + getStatus() + "'" +
            ", voteAverage=" + getVoteAverage() +
            ", voteCount=" + getVoteCount() +
            ", releaseDate='" + getReleaseDate() + "'" +
            ", lastTMDBUpdate='" + getLastTMDBUpdate() + "'" +
            ", tmdbId='" + getTmdbId() + "'" +
            ", runtime=" + getRuntime() +
            "}";
    }
}
