package com.mgoulene.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;

/**
 * A ImageData.
 */
@Entity
@Table(name = "image_data")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class ImageData implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Size(max = 40)
    @Column(name = "image_size", length = 40, nullable = false)
    private String imageSize;

    
    @Lob
    @Column(name = "image_bytes", nullable = false)
    private byte[] imageBytes;

    @Column(name = "image_bytes_content_type", nullable = false)
    private String imageBytesContentType;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties(value = "imageData", allowSetters = true)
    private Image image;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getImageSize() {
        return imageSize;
    }

    public ImageData imageSize(String imageSize) {
        this.imageSize = imageSize;
        return this;
    }

    public void setImageSize(String imageSize) {
        this.imageSize = imageSize;
    }

    public byte[] getImageBytes() {
        return imageBytes;
    }

    public ImageData imageBytes(byte[] imageBytes) {
        this.imageBytes = imageBytes;
        return this;
    }

    public void setImageBytes(byte[] imageBytes) {
        this.imageBytes = imageBytes;
    }

    public String getImageBytesContentType() {
        return imageBytesContentType;
    }

    public ImageData imageBytesContentType(String imageBytesContentType) {
        this.imageBytesContentType = imageBytesContentType;
        return this;
    }

    public void setImageBytesContentType(String imageBytesContentType) {
        this.imageBytesContentType = imageBytesContentType;
    }

    public Image getImage() {
        return image;
    }

    public ImageData image(Image image) {
        this.image = image;
        return this;
    }

    public void setImage(Image image) {
        this.image = image;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ImageData)) {
            return false;
        }
        return id != null && id.equals(((ImageData) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ImageData{" +
            "id=" + getId() +
            ", imageSize='" + getImageSize() + "'" +
            ", imageBytes='" + getImageBytes() + "'" +
            ", imageBytesContentType='" + getImageBytesContentType() + "'" +
            "}";
    }
}
