package com.mgoulene.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.LocalDate;

/**
 * The Image entity.\n@author A true hipster
 */
@Entity
@Table(name = "image")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Image implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "tmdb_id", nullable = false)
    private String tmdbId;

    @Column(name = "locale")
    private String locale;

    @Column(name = "vote_average")
    private Float voteAverage;

    @Column(name = "vote_count")
    private Integer voteCount;

    @NotNull
    @Column(name = "last_tmdb_update", nullable = false)
    private LocalDate lastTMDBUpdate;

    @ManyToOne
    @JsonIgnoreProperties(value = "posters", allowSetters = true)
    private Movie posterMovie;

    @ManyToOne
    @JsonIgnoreProperties(value = "backdrops", allowSetters = true)
    private Movie backdropMovie;

    @ManyToOne
    @JsonIgnoreProperties(value = "profiles", allowSetters = true)
    private Person person;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTmdbId() {
        return tmdbId;
    }

    public Image tmdbId(String tmdbId) {
        this.tmdbId = tmdbId;
        return this;
    }

    public void setTmdbId(String tmdbId) {
        this.tmdbId = tmdbId;
    }

    public String getLocale() {
        return locale;
    }

    public Image locale(String locale) {
        this.locale = locale;
        return this;
    }

    public void setLocale(String locale) {
        this.locale = locale;
    }

    public Float getVoteAverage() {
        return voteAverage;
    }

    public Image voteAverage(Float voteAverage) {
        this.voteAverage = voteAverage;
        return this;
    }

    public void setVoteAverage(Float voteAverage) {
        this.voteAverage = voteAverage;
    }

    public Integer getVoteCount() {
        return voteCount;
    }

    public Image voteCount(Integer voteCount) {
        this.voteCount = voteCount;
        return this;
    }

    public void setVoteCount(Integer voteCount) {
        this.voteCount = voteCount;
    }

    public LocalDate getLastTMDBUpdate() {
        return lastTMDBUpdate;
    }

    public Image lastTMDBUpdate(LocalDate lastTMDBUpdate) {
        this.lastTMDBUpdate = lastTMDBUpdate;
        return this;
    }

    public void setLastTMDBUpdate(LocalDate lastTMDBUpdate) {
        this.lastTMDBUpdate = lastTMDBUpdate;
    }

    public Movie getPosterMovie() {
        return posterMovie;
    }

    public Image posterMovie(Movie movie) {
        this.posterMovie = movie;
        return this;
    }

    public void setPosterMovie(Movie movie) {
        this.posterMovie = movie;
    }

    public Movie getBackdropMovie() {
        return backdropMovie;
    }

    public Image backdropMovie(Movie movie) {
        this.backdropMovie = movie;
        return this;
    }

    public void setBackdropMovie(Movie movie) {
        this.backdropMovie = movie;
    }

    public Person getPerson() {
        return person;
    }

    public Image person(Person person) {
        this.person = person;
        return this;
    }

    public void setPerson(Person person) {
        this.person = person;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Image)) {
            return false;
        }
        return id != null && id.equals(((Image) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Image{" +
            "id=" + getId() +
            ", tmdbId='" + getTmdbId() + "'" +
            ", locale='" + getLocale() + "'" +
            ", voteAverage=" + getVoteAverage() +
            ", voteCount=" + getVoteCount() +
            ", lastTMDBUpdate='" + getLastTMDBUpdate() + "'" +
            "}";
    }
}
