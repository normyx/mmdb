package com.mgoulene.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.LocalDate;

/**
 * The Credit entity.\n@author A true hipster
 */
@Entity
@Table(name = "credit")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Credit implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    /**
     * fieldName
     */
    @NotNull
    @Column(name = "tmdb_id", nullable = false, unique = true)
    private String tmdbId;

    @Size(max = 200)
    @Column(name = "character", length = 200)
    private String character;

    @Column(name = "credit_type")
    private String creditType;

    @Column(name = "department")
    private String department;

    @Column(name = "job")
    private String job;

    @Column(name = "jhi_order")
    private Integer order;

    @NotNull
    @Column(name = "last_tmdb_update", nullable = false)
    private LocalDate lastTMDBUpdate;

    @ManyToOne
    @JsonIgnoreProperties(value = "credits", allowSetters = true)
    private Person person;

    @ManyToOne
    @JsonIgnoreProperties(value = "credits", allowSetters = true)
    private Movie movie;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTmdbId() {
        return tmdbId;
    }

    public Credit tmdbId(String tmdbId) {
        this.tmdbId = tmdbId;
        return this;
    }

    public void setTmdbId(String tmdbId) {
        this.tmdbId = tmdbId;
    }

    public String getCharacter() {
        return character;
    }

    public Credit character(String character) {
        this.character = character;
        return this;
    }

    public void setCharacter(String character) {
        this.character = character;
    }

    public String getCreditType() {
        return creditType;
    }

    public Credit creditType(String creditType) {
        this.creditType = creditType;
        return this;
    }

    public void setCreditType(String creditType) {
        this.creditType = creditType;
    }

    public String getDepartment() {
        return department;
    }

    public Credit department(String department) {
        this.department = department;
        return this;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getJob() {
        return job;
    }

    public Credit job(String job) {
        this.job = job;
        return this;
    }

    public void setJob(String job) {
        this.job = job;
    }

    public Integer getOrder() {
        return order;
    }

    public Credit order(Integer order) {
        this.order = order;
        return this;
    }

    public void setOrder(Integer order) {
        this.order = order;
    }

    public LocalDate getLastTMDBUpdate() {
        return lastTMDBUpdate;
    }

    public Credit lastTMDBUpdate(LocalDate lastTMDBUpdate) {
        this.lastTMDBUpdate = lastTMDBUpdate;
        return this;
    }

    public void setLastTMDBUpdate(LocalDate lastTMDBUpdate) {
        this.lastTMDBUpdate = lastTMDBUpdate;
    }

    public Person getPerson() {
        return person;
    }

    public Credit person(Person person) {
        this.person = person;
        return this;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    public Movie getMovie() {
        return movie;
    }

    public Credit movie(Movie movie) {
        this.movie = movie;
        return this;
    }

    public void setMovie(Movie movie) {
        this.movie = movie;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Credit)) {
            return false;
        }
        return id != null && id.equals(((Credit) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Credit{" +
            "id=" + getId() +
            ", tmdbId='" + getTmdbId() + "'" +
            ", character='" + getCharacter() + "'" +
            ", creditType='" + getCreditType() + "'" +
            ", department='" + getDepartment() + "'" +
            ", job='" + getJob() + "'" +
            ", order=" + getOrder() +
            ", lastTMDBUpdate='" + getLastTMDBUpdate() + "'" +
            "}";
    }
}
