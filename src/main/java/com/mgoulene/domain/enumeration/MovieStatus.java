package com.mgoulene.domain.enumeration;

/**
 * The MovieStatus enumeration.
 */
public enum MovieStatus {
    RUMORED, PLANNED, IN_PRODUCTION, POST_PRODUCTION, RELEASED, CANCELED
}
