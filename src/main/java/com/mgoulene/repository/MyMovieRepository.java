package com.mgoulene.repository;

import com.mgoulene.domain.MyMovie;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Spring Data  repository for the MyMovie entity.
 */
@SuppressWarnings("unused")
@Repository
public interface MyMovieRepository extends JpaRepository<MyMovie, Long>, JpaSpecificationExecutor<MyMovie> {

    @Query("select myMovie from MyMovie myMovie where myMovie.user.login = ?#{principal.username}")
    List<MyMovie> findByUserIsCurrentUser();
}
