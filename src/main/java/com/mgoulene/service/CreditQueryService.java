package com.mgoulene.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.mgoulene.domain.Credit;
import com.mgoulene.domain.*; // for static metamodels
import com.mgoulene.repository.CreditRepository;
import com.mgoulene.service.dto.CreditCriteria;
import com.mgoulene.service.dto.CreditDTO;
import com.mgoulene.service.mapper.CreditMapper;

/**
 * Service for executing complex queries for {@link Credit} entities in the database.
 * The main input is a {@link CreditCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link CreditDTO} or a {@link Page} of {@link CreditDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class CreditQueryService extends QueryService<Credit> {

    private final Logger log = LoggerFactory.getLogger(CreditQueryService.class);

    private final CreditRepository creditRepository;

    private final CreditMapper creditMapper;

    public CreditQueryService(CreditRepository creditRepository, CreditMapper creditMapper) {
        this.creditRepository = creditRepository;
        this.creditMapper = creditMapper;
    }

    /**
     * Return a {@link List} of {@link CreditDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<CreditDTO> findByCriteria(CreditCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<Credit> specification = createSpecification(criteria);
        return creditMapper.toDto(creditRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link CreditDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<CreditDTO> findByCriteria(CreditCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<Credit> specification = createSpecification(criteria);
        return creditRepository.findAll(specification, page)
            .map(creditMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(CreditCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<Credit> specification = createSpecification(criteria);
        return creditRepository.count(specification);
    }

    /**
     * Function to convert {@link CreditCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<Credit> createSpecification(CreditCriteria criteria) {
        Specification<Credit> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), Credit_.id));
            }
            if (criteria.getTmdbId() != null) {
                specification = specification.and(buildStringSpecification(criteria.getTmdbId(), Credit_.tmdbId));
            }
            if (criteria.getCharacter() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCharacter(), Credit_.character));
            }
            if (criteria.getCreditType() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCreditType(), Credit_.creditType));
            }
            if (criteria.getDepartment() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDepartment(), Credit_.department));
            }
            if (criteria.getJob() != null) {
                specification = specification.and(buildStringSpecification(criteria.getJob(), Credit_.job));
            }
            if (criteria.getOrder() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getOrder(), Credit_.order));
            }
            if (criteria.getLastTMDBUpdate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getLastTMDBUpdate(), Credit_.lastTMDBUpdate));
            }
            if (criteria.getPersonId() != null) {
                specification = specification.and(buildSpecification(criteria.getPersonId(),
                    root -> root.join(Credit_.person, JoinType.LEFT).get(Person_.id)));
            }
            if (criteria.getMovieId() != null) {
                specification = specification.and(buildSpecification(criteria.getMovieId(),
                    root -> root.join(Credit_.movie, JoinType.LEFT).get(Movie_.id)));
            }
        }
        return specification;
    }
}
