package com.mgoulene.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.mgoulene.domain.MyMovie;
import com.mgoulene.domain.*; // for static metamodels
import com.mgoulene.repository.MyMovieRepository;
import com.mgoulene.service.dto.MyMovieCriteria;
import com.mgoulene.service.dto.MyMovieDTO;
import com.mgoulene.service.mapper.MyMovieMapper;

/**
 * Service for executing complex queries for {@link MyMovie} entities in the database.
 * The main input is a {@link MyMovieCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link MyMovieDTO} or a {@link Page} of {@link MyMovieDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class MyMovieQueryService extends QueryService<MyMovie> {

    private final Logger log = LoggerFactory.getLogger(MyMovieQueryService.class);

    private final MyMovieRepository myMovieRepository;

    private final MyMovieMapper myMovieMapper;

    public MyMovieQueryService(MyMovieRepository myMovieRepository, MyMovieMapper myMovieMapper) {
        this.myMovieRepository = myMovieRepository;
        this.myMovieMapper = myMovieMapper;
    }

    /**
     * Return a {@link List} of {@link MyMovieDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<MyMovieDTO> findByCriteria(MyMovieCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<MyMovie> specification = createSpecification(criteria);
        return myMovieMapper.toDto(myMovieRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link MyMovieDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<MyMovieDTO> findByCriteria(MyMovieCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<MyMovie> specification = createSpecification(criteria);
        return myMovieRepository.findAll(specification, page)
            .map(myMovieMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(MyMovieCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<MyMovie> specification = createSpecification(criteria);
        return myMovieRepository.count(specification);
    }

    /**
     * Function to convert {@link MyMovieCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<MyMovie> createSpecification(MyMovieCriteria criteria) {
        Specification<MyMovie> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), MyMovie_.id));
            }
            if (criteria.getComments() != null) {
                specification = specification.and(buildStringSpecification(criteria.getComments(), MyMovie_.comments));
            }
            if (criteria.getVote() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getVote(), MyMovie_.vote));
            }
            if (criteria.getViewedDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getViewedDate(), MyMovie_.viewedDate));
            }
            if (criteria.getMovieId() != null) {
                specification = specification.and(buildSpecification(criteria.getMovieId(),
                    root -> root.join(MyMovie_.movie, JoinType.LEFT).get(Movie_.id)));
            }
            if (criteria.getUserId() != null) {
                specification = specification.and(buildSpecification(criteria.getUserId(),
                    root -> root.join(MyMovie_.user, JoinType.LEFT).get(User_.id)));
            }
        }
        return specification;
    }
}
