package com.mgoulene.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.mgoulene.domain.Genre;
import com.mgoulene.domain.*; // for static metamodels
import com.mgoulene.repository.GenreRepository;
import com.mgoulene.service.dto.GenreCriteria;
import com.mgoulene.service.dto.GenreDTO;
import com.mgoulene.service.mapper.GenreMapper;

/**
 * Service for executing complex queries for {@link Genre} entities in the database.
 * The main input is a {@link GenreCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link GenreDTO} or a {@link Page} of {@link GenreDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class GenreQueryService extends QueryService<Genre> {

    private final Logger log = LoggerFactory.getLogger(GenreQueryService.class);

    private final GenreRepository genreRepository;

    private final GenreMapper genreMapper;

    public GenreQueryService(GenreRepository genreRepository, GenreMapper genreMapper) {
        this.genreRepository = genreRepository;
        this.genreMapper = genreMapper;
    }

    /**
     * Return a {@link List} of {@link GenreDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<GenreDTO> findByCriteria(GenreCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<Genre> specification = createSpecification(criteria);
        return genreMapper.toDto(genreRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link GenreDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<GenreDTO> findByCriteria(GenreCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<Genre> specification = createSpecification(criteria);
        return genreRepository.findAll(specification, page)
            .map(genreMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(GenreCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<Genre> specification = createSpecification(criteria);
        return genreRepository.count(specification);
    }

    /**
     * Function to convert {@link GenreCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<Genre> createSpecification(GenreCriteria criteria) {
        Specification<Genre> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), Genre_.id));
            }
            if (criteria.getTmdbId() != null) {
                specification = specification.and(buildStringSpecification(criteria.getTmdbId(), Genre_.tmdbId));
            }
            if (criteria.getName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getName(), Genre_.name));
            }
            if (criteria.getLastTMDBUpdate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getLastTMDBUpdate(), Genre_.lastTMDBUpdate));
            }
            if (criteria.getMovieId() != null) {
                specification = specification.and(buildSpecification(criteria.getMovieId(),
                    root -> root.join(Genre_.movies, JoinType.LEFT).get(Movie_.id)));
            }
        }
        return specification;
    }
}
