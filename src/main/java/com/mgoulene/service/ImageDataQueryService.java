package com.mgoulene.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.mgoulene.domain.ImageData;
import com.mgoulene.domain.*; // for static metamodels
import com.mgoulene.repository.ImageDataRepository;
import com.mgoulene.service.dto.ImageDataCriteria;
import com.mgoulene.service.dto.ImageDataDTO;
import com.mgoulene.service.mapper.ImageDataMapper;

/**
 * Service for executing complex queries for {@link ImageData} entities in the database.
 * The main input is a {@link ImageDataCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link ImageDataDTO} or a {@link Page} of {@link ImageDataDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class ImageDataQueryService extends QueryService<ImageData> {

    private final Logger log = LoggerFactory.getLogger(ImageDataQueryService.class);

    private final ImageDataRepository imageDataRepository;

    private final ImageDataMapper imageDataMapper;

    public ImageDataQueryService(ImageDataRepository imageDataRepository, ImageDataMapper imageDataMapper) {
        this.imageDataRepository = imageDataRepository;
        this.imageDataMapper = imageDataMapper;
    }

    /**
     * Return a {@link List} of {@link ImageDataDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<ImageDataDTO> findByCriteria(ImageDataCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<ImageData> specification = createSpecification(criteria);
        return imageDataMapper.toDto(imageDataRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link ImageDataDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<ImageDataDTO> findByCriteria(ImageDataCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<ImageData> specification = createSpecification(criteria);
        return imageDataRepository.findAll(specification, page)
            .map(imageDataMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(ImageDataCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<ImageData> specification = createSpecification(criteria);
        return imageDataRepository.count(specification);
    }

    /**
     * Function to convert {@link ImageDataCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<ImageData> createSpecification(ImageDataCriteria criteria) {
        Specification<ImageData> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), ImageData_.id));
            }
            if (criteria.getImageSize() != null) {
                specification = specification.and(buildStringSpecification(criteria.getImageSize(), ImageData_.imageSize));
            }
            if (criteria.getImageId() != null) {
                specification = specification.and(buildSpecification(criteria.getImageId(),
                    root -> root.join(ImageData_.image, JoinType.LEFT).get(Image_.id)));
            }
        }
        return specification;
    }
}
