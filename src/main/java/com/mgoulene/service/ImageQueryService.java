package com.mgoulene.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.mgoulene.domain.Image;
import com.mgoulene.domain.*; // for static metamodels
import com.mgoulene.repository.ImageRepository;
import com.mgoulene.service.dto.ImageCriteria;
import com.mgoulene.service.dto.ImageDTO;
import com.mgoulene.service.mapper.ImageMapper;

/**
 * Service for executing complex queries for {@link Image} entities in the database.
 * The main input is a {@link ImageCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link ImageDTO} or a {@link Page} of {@link ImageDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class ImageQueryService extends QueryService<Image> {

    private final Logger log = LoggerFactory.getLogger(ImageQueryService.class);

    private final ImageRepository imageRepository;

    private final ImageMapper imageMapper;

    public ImageQueryService(ImageRepository imageRepository, ImageMapper imageMapper) {
        this.imageRepository = imageRepository;
        this.imageMapper = imageMapper;
    }

    /**
     * Return a {@link List} of {@link ImageDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<ImageDTO> findByCriteria(ImageCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<Image> specification = createSpecification(criteria);
        return imageMapper.toDto(imageRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link ImageDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<ImageDTO> findByCriteria(ImageCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<Image> specification = createSpecification(criteria);
        return imageRepository.findAll(specification, page)
            .map(imageMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(ImageCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<Image> specification = createSpecification(criteria);
        return imageRepository.count(specification);
    }

    /**
     * Function to convert {@link ImageCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<Image> createSpecification(ImageCriteria criteria) {
        Specification<Image> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), Image_.id));
            }
            if (criteria.getTmdbId() != null) {
                specification = specification.and(buildStringSpecification(criteria.getTmdbId(), Image_.tmdbId));
            }
            if (criteria.getLocale() != null) {
                specification = specification.and(buildStringSpecification(criteria.getLocale(), Image_.locale));
            }
            if (criteria.getVoteAverage() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getVoteAverage(), Image_.voteAverage));
            }
            if (criteria.getVoteCount() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getVoteCount(), Image_.voteCount));
            }
            if (criteria.getLastTMDBUpdate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getLastTMDBUpdate(), Image_.lastTMDBUpdate));
            }
            if (criteria.getPosterMovieId() != null) {
                specification = specification.and(buildSpecification(criteria.getPosterMovieId(),
                    root -> root.join(Image_.posterMovie, JoinType.LEFT).get(Movie_.id)));
            }
            if (criteria.getBackdropMovieId() != null) {
                specification = specification.and(buildSpecification(criteria.getBackdropMovieId(),
                    root -> root.join(Image_.backdropMovie, JoinType.LEFT).get(Movie_.id)));
            }
            if (criteria.getPersonId() != null) {
                specification = specification.and(buildSpecification(criteria.getPersonId(),
                    root -> root.join(Image_.person, JoinType.LEFT).get(Person_.id)));
            }
        }
        return specification;
    }
}
