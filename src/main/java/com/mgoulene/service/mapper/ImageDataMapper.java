package com.mgoulene.service.mapper;


import com.mgoulene.domain.*;
import com.mgoulene.service.dto.ImageDataDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link ImageData} and its DTO {@link ImageDataDTO}.
 */
@Mapper(componentModel = "spring", uses = {ImageMapper.class})
public interface ImageDataMapper extends EntityMapper<ImageDataDTO, ImageData> {

    @Mapping(source = "image.id", target = "imageId")
    ImageDataDTO toDto(ImageData imageData);

    @Mapping(source = "imageId", target = "image")
    ImageData toEntity(ImageDataDTO imageDataDTO);

    default ImageData fromId(Long id) {
        if (id == null) {
            return null;
        }
        ImageData imageData = new ImageData();
        imageData.setId(id);
        return imageData;
    }
}
