package com.mgoulene.service.mapper;


import com.mgoulene.domain.*;
import com.mgoulene.service.dto.ImageDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Image} and its DTO {@link ImageDTO}.
 */
@Mapper(componentModel = "spring", uses = {MovieMapper.class, PersonMapper.class})
public interface ImageMapper extends EntityMapper<ImageDTO, Image> {

    @Mapping(source = "posterMovie.id", target = "posterMovieId")
    @Mapping(source = "posterMovie.title", target = "posterMovieTitle")
    @Mapping(source = "backdropMovie.id", target = "backdropMovieId")
    @Mapping(source = "backdropMovie.title", target = "backdropMovieTitle")
    @Mapping(source = "person.id", target = "personId")
    @Mapping(source = "person.name", target = "personName")
    ImageDTO toDto(Image image);

    @Mapping(source = "posterMovieId", target = "posterMovie")
    @Mapping(source = "backdropMovieId", target = "backdropMovie")
    @Mapping(source = "personId", target = "person")
    Image toEntity(ImageDTO imageDTO);

    default Image fromId(Long id) {
        if (id == null) {
            return null;
        }
        Image image = new Image();
        image.setId(id);
        return image;
    }
}
