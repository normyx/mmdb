package com.mgoulene.service.mapper;


import com.mgoulene.domain.*;
import com.mgoulene.service.dto.MovieDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Movie} and its DTO {@link MovieDTO}.
 */
@Mapper(componentModel = "spring", uses = {GenreMapper.class})
public interface MovieMapper extends EntityMapper<MovieDTO, Movie> {


    @Mapping(target = "credits", ignore = true)
    @Mapping(target = "removeCredits", ignore = true)
    @Mapping(target = "posters", ignore = true)
    @Mapping(target = "removePoster", ignore = true)
    @Mapping(target = "backdrops", ignore = true)
    @Mapping(target = "removeBackdrop", ignore = true)
    @Mapping(target = "removeGenre", ignore = true)
    Movie toEntity(MovieDTO movieDTO);

    default Movie fromId(Long id) {
        if (id == null) {
            return null;
        }
        Movie movie = new Movie();
        movie.setId(id);
        return movie;
    }
}
