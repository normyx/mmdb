package com.mgoulene.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.LocalDateFilter;

/**
 * Criteria class for the {@link com.mgoulene.domain.Image} entity. This class is used
 * in {@link com.mgoulene.web.rest.ImageResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /images?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class ImageCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter tmdbId;

    private StringFilter locale;

    private FloatFilter voteAverage;

    private IntegerFilter voteCount;

    private LocalDateFilter lastTMDBUpdate;

    private LongFilter posterMovieId;

    private LongFilter backdropMovieId;

    private LongFilter personId;

    public ImageCriteria() {
    }

    public ImageCriteria(ImageCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.tmdbId = other.tmdbId == null ? null : other.tmdbId.copy();
        this.locale = other.locale == null ? null : other.locale.copy();
        this.voteAverage = other.voteAverage == null ? null : other.voteAverage.copy();
        this.voteCount = other.voteCount == null ? null : other.voteCount.copy();
        this.lastTMDBUpdate = other.lastTMDBUpdate == null ? null : other.lastTMDBUpdate.copy();
        this.posterMovieId = other.posterMovieId == null ? null : other.posterMovieId.copy();
        this.backdropMovieId = other.backdropMovieId == null ? null : other.backdropMovieId.copy();
        this.personId = other.personId == null ? null : other.personId.copy();
    }

    @Override
    public ImageCriteria copy() {
        return new ImageCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getTmdbId() {
        return tmdbId;
    }

    public void setTmdbId(StringFilter tmdbId) {
        this.tmdbId = tmdbId;
    }

    public StringFilter getLocale() {
        return locale;
    }

    public void setLocale(StringFilter locale) {
        this.locale = locale;
    }

    public FloatFilter getVoteAverage() {
        return voteAverage;
    }

    public void setVoteAverage(FloatFilter voteAverage) {
        this.voteAverage = voteAverage;
    }

    public IntegerFilter getVoteCount() {
        return voteCount;
    }

    public void setVoteCount(IntegerFilter voteCount) {
        this.voteCount = voteCount;
    }

    public LocalDateFilter getLastTMDBUpdate() {
        return lastTMDBUpdate;
    }

    public void setLastTMDBUpdate(LocalDateFilter lastTMDBUpdate) {
        this.lastTMDBUpdate = lastTMDBUpdate;
    }

    public LongFilter getPosterMovieId() {
        return posterMovieId;
    }

    public void setPosterMovieId(LongFilter posterMovieId) {
        this.posterMovieId = posterMovieId;
    }

    public LongFilter getBackdropMovieId() {
        return backdropMovieId;
    }

    public void setBackdropMovieId(LongFilter backdropMovieId) {
        this.backdropMovieId = backdropMovieId;
    }

    public LongFilter getPersonId() {
        return personId;
    }

    public void setPersonId(LongFilter personId) {
        this.personId = personId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final ImageCriteria that = (ImageCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(tmdbId, that.tmdbId) &&
            Objects.equals(locale, that.locale) &&
            Objects.equals(voteAverage, that.voteAverage) &&
            Objects.equals(voteCount, that.voteCount) &&
            Objects.equals(lastTMDBUpdate, that.lastTMDBUpdate) &&
            Objects.equals(posterMovieId, that.posterMovieId) &&
            Objects.equals(backdropMovieId, that.backdropMovieId) &&
            Objects.equals(personId, that.personId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        tmdbId,
        locale,
        voteAverage,
        voteCount,
        lastTMDBUpdate,
        posterMovieId,
        backdropMovieId,
        personId
        );
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ImageCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (tmdbId != null ? "tmdbId=" + tmdbId + ", " : "") +
                (locale != null ? "locale=" + locale + ", " : "") +
                (voteAverage != null ? "voteAverage=" + voteAverage + ", " : "") +
                (voteCount != null ? "voteCount=" + voteCount + ", " : "") +
                (lastTMDBUpdate != null ? "lastTMDBUpdate=" + lastTMDBUpdate + ", " : "") +
                (posterMovieId != null ? "posterMovieId=" + posterMovieId + ", " : "") +
                (backdropMovieId != null ? "backdropMovieId=" + backdropMovieId + ", " : "") +
                (personId != null ? "personId=" + personId + ", " : "") +
            "}";
    }

}
