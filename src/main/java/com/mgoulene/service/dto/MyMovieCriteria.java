package com.mgoulene.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.LocalDateFilter;

/**
 * Criteria class for the {@link com.mgoulene.domain.MyMovie} entity. This class is used
 * in {@link com.mgoulene.web.rest.MyMovieResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /my-movies?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class MyMovieCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter comments;

    private IntegerFilter vote;

    private LocalDateFilter viewedDate;

    private LongFilter movieId;

    private LongFilter userId;

    public MyMovieCriteria() {
    }

    public MyMovieCriteria(MyMovieCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.comments = other.comments == null ? null : other.comments.copy();
        this.vote = other.vote == null ? null : other.vote.copy();
        this.viewedDate = other.viewedDate == null ? null : other.viewedDate.copy();
        this.movieId = other.movieId == null ? null : other.movieId.copy();
        this.userId = other.userId == null ? null : other.userId.copy();
    }

    @Override
    public MyMovieCriteria copy() {
        return new MyMovieCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getComments() {
        return comments;
    }

    public void setComments(StringFilter comments) {
        this.comments = comments;
    }

    public IntegerFilter getVote() {
        return vote;
    }

    public void setVote(IntegerFilter vote) {
        this.vote = vote;
    }

    public LocalDateFilter getViewedDate() {
        return viewedDate;
    }

    public void setViewedDate(LocalDateFilter viewedDate) {
        this.viewedDate = viewedDate;
    }

    public LongFilter getMovieId() {
        return movieId;
    }

    public void setMovieId(LongFilter movieId) {
        this.movieId = movieId;
    }

    public LongFilter getUserId() {
        return userId;
    }

    public void setUserId(LongFilter userId) {
        this.userId = userId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final MyMovieCriteria that = (MyMovieCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(comments, that.comments) &&
            Objects.equals(vote, that.vote) &&
            Objects.equals(viewedDate, that.viewedDate) &&
            Objects.equals(movieId, that.movieId) &&
            Objects.equals(userId, that.userId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        comments,
        vote,
        viewedDate,
        movieId,
        userId
        );
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "MyMovieCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (comments != null ? "comments=" + comments + ", " : "") +
                (vote != null ? "vote=" + vote + ", " : "") +
                (viewedDate != null ? "viewedDate=" + viewedDate + ", " : "") +
                (movieId != null ? "movieId=" + movieId + ", " : "") +
                (userId != null ? "userId=" + userId + ", " : "") +
            "}";
    }

}
