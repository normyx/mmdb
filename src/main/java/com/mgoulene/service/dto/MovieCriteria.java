package com.mgoulene.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import com.mgoulene.domain.enumeration.MovieStatus;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.LocalDateFilter;

/**
 * Criteria class for the {@link com.mgoulene.domain.Movie} entity. This class is used
 * in {@link com.mgoulene.web.rest.MovieResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /movies?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class MovieCriteria implements Serializable, Criteria {
    /**
     * Class for filtering MovieStatus
     */
    public static class MovieStatusFilter extends Filter<MovieStatus> {

        public MovieStatusFilter() {
        }

        public MovieStatusFilter(MovieStatusFilter filter) {
            super(filter);
        }

        @Override
        public MovieStatusFilter copy() {
            return new MovieStatusFilter(this);
        }

    }

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter title;

    private BooleanFilter forAdult;

    private StringFilter homepage;

    private StringFilter originalLanguage;

    private StringFilter originalTitle;

    private StringFilter overview;

    private StringFilter tagline;

    private MovieStatusFilter status;

    private FloatFilter voteAverage;

    private IntegerFilter voteCount;

    private LocalDateFilter releaseDate;

    private LocalDateFilter lastTMDBUpdate;

    private StringFilter tmdbId;

    private IntegerFilter runtime;

    private LongFilter creditsId;

    private LongFilter posterId;

    private LongFilter backdropId;

    private LongFilter genreId;

    public MovieCriteria() {
    }

    public MovieCriteria(MovieCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.title = other.title == null ? null : other.title.copy();
        this.forAdult = other.forAdult == null ? null : other.forAdult.copy();
        this.homepage = other.homepage == null ? null : other.homepage.copy();
        this.originalLanguage = other.originalLanguage == null ? null : other.originalLanguage.copy();
        this.originalTitle = other.originalTitle == null ? null : other.originalTitle.copy();
        this.overview = other.overview == null ? null : other.overview.copy();
        this.tagline = other.tagline == null ? null : other.tagline.copy();
        this.status = other.status == null ? null : other.status.copy();
        this.voteAverage = other.voteAverage == null ? null : other.voteAverage.copy();
        this.voteCount = other.voteCount == null ? null : other.voteCount.copy();
        this.releaseDate = other.releaseDate == null ? null : other.releaseDate.copy();
        this.lastTMDBUpdate = other.lastTMDBUpdate == null ? null : other.lastTMDBUpdate.copy();
        this.tmdbId = other.tmdbId == null ? null : other.tmdbId.copy();
        this.runtime = other.runtime == null ? null : other.runtime.copy();
        this.creditsId = other.creditsId == null ? null : other.creditsId.copy();
        this.posterId = other.posterId == null ? null : other.posterId.copy();
        this.backdropId = other.backdropId == null ? null : other.backdropId.copy();
        this.genreId = other.genreId == null ? null : other.genreId.copy();
    }

    @Override
    public MovieCriteria copy() {
        return new MovieCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getTitle() {
        return title;
    }

    public void setTitle(StringFilter title) {
        this.title = title;
    }

    public BooleanFilter getForAdult() {
        return forAdult;
    }

    public void setForAdult(BooleanFilter forAdult) {
        this.forAdult = forAdult;
    }

    public StringFilter getHomepage() {
        return homepage;
    }

    public void setHomepage(StringFilter homepage) {
        this.homepage = homepage;
    }

    public StringFilter getOriginalLanguage() {
        return originalLanguage;
    }

    public void setOriginalLanguage(StringFilter originalLanguage) {
        this.originalLanguage = originalLanguage;
    }

    public StringFilter getOriginalTitle() {
        return originalTitle;
    }

    public void setOriginalTitle(StringFilter originalTitle) {
        this.originalTitle = originalTitle;
    }

    public StringFilter getOverview() {
        return overview;
    }

    public void setOverview(StringFilter overview) {
        this.overview = overview;
    }

    public StringFilter getTagline() {
        return tagline;
    }

    public void setTagline(StringFilter tagline) {
        this.tagline = tagline;
    }

    public MovieStatusFilter getStatus() {
        return status;
    }

    public void setStatus(MovieStatusFilter status) {
        this.status = status;
    }

    public FloatFilter getVoteAverage() {
        return voteAverage;
    }

    public void setVoteAverage(FloatFilter voteAverage) {
        this.voteAverage = voteAverage;
    }

    public IntegerFilter getVoteCount() {
        return voteCount;
    }

    public void setVoteCount(IntegerFilter voteCount) {
        this.voteCount = voteCount;
    }

    public LocalDateFilter getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(LocalDateFilter releaseDate) {
        this.releaseDate = releaseDate;
    }

    public LocalDateFilter getLastTMDBUpdate() {
        return lastTMDBUpdate;
    }

    public void setLastTMDBUpdate(LocalDateFilter lastTMDBUpdate) {
        this.lastTMDBUpdate = lastTMDBUpdate;
    }

    public StringFilter getTmdbId() {
        return tmdbId;
    }

    public void setTmdbId(StringFilter tmdbId) {
        this.tmdbId = tmdbId;
    }

    public IntegerFilter getRuntime() {
        return runtime;
    }

    public void setRuntime(IntegerFilter runtime) {
        this.runtime = runtime;
    }

    public LongFilter getCreditsId() {
        return creditsId;
    }

    public void setCreditsId(LongFilter creditsId) {
        this.creditsId = creditsId;
    }

    public LongFilter getPosterId() {
        return posterId;
    }

    public void setPosterId(LongFilter posterId) {
        this.posterId = posterId;
    }

    public LongFilter getBackdropId() {
        return backdropId;
    }

    public void setBackdropId(LongFilter backdropId) {
        this.backdropId = backdropId;
    }

    public LongFilter getGenreId() {
        return genreId;
    }

    public void setGenreId(LongFilter genreId) {
        this.genreId = genreId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final MovieCriteria that = (MovieCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(title, that.title) &&
            Objects.equals(forAdult, that.forAdult) &&
            Objects.equals(homepage, that.homepage) &&
            Objects.equals(originalLanguage, that.originalLanguage) &&
            Objects.equals(originalTitle, that.originalTitle) &&
            Objects.equals(overview, that.overview) &&
            Objects.equals(tagline, that.tagline) &&
            Objects.equals(status, that.status) &&
            Objects.equals(voteAverage, that.voteAverage) &&
            Objects.equals(voteCount, that.voteCount) &&
            Objects.equals(releaseDate, that.releaseDate) &&
            Objects.equals(lastTMDBUpdate, that.lastTMDBUpdate) &&
            Objects.equals(tmdbId, that.tmdbId) &&
            Objects.equals(runtime, that.runtime) &&
            Objects.equals(creditsId, that.creditsId) &&
            Objects.equals(posterId, that.posterId) &&
            Objects.equals(backdropId, that.backdropId) &&
            Objects.equals(genreId, that.genreId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        title,
        forAdult,
        homepage,
        originalLanguage,
        originalTitle,
        overview,
        tagline,
        status,
        voteAverage,
        voteCount,
        releaseDate,
        lastTMDBUpdate,
        tmdbId,
        runtime,
        creditsId,
        posterId,
        backdropId,
        genreId
        );
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "MovieCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (title != null ? "title=" + title + ", " : "") +
                (forAdult != null ? "forAdult=" + forAdult + ", " : "") +
                (homepage != null ? "homepage=" + homepage + ", " : "") +
                (originalLanguage != null ? "originalLanguage=" + originalLanguage + ", " : "") +
                (originalTitle != null ? "originalTitle=" + originalTitle + ", " : "") +
                (overview != null ? "overview=" + overview + ", " : "") +
                (tagline != null ? "tagline=" + tagline + ", " : "") +
                (status != null ? "status=" + status + ", " : "") +
                (voteAverage != null ? "voteAverage=" + voteAverage + ", " : "") +
                (voteCount != null ? "voteCount=" + voteCount + ", " : "") +
                (releaseDate != null ? "releaseDate=" + releaseDate + ", " : "") +
                (lastTMDBUpdate != null ? "lastTMDBUpdate=" + lastTMDBUpdate + ", " : "") +
                (tmdbId != null ? "tmdbId=" + tmdbId + ", " : "") +
                (runtime != null ? "runtime=" + runtime + ", " : "") +
                (creditsId != null ? "creditsId=" + creditsId + ", " : "") +
                (posterId != null ? "posterId=" + posterId + ", " : "") +
                (backdropId != null ? "backdropId=" + backdropId + ", " : "") +
                (genreId != null ? "genreId=" + genreId + ", " : "") +
            "}";
    }

}
