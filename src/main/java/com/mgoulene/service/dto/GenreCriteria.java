package com.mgoulene.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.LocalDateFilter;

/**
 * Criteria class for the {@link com.mgoulene.domain.Genre} entity. This class is used
 * in {@link com.mgoulene.web.rest.GenreResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /genres?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class GenreCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter tmdbId;

    private StringFilter name;

    private LocalDateFilter lastTMDBUpdate;

    private LongFilter movieId;

    public GenreCriteria() {
    }

    public GenreCriteria(GenreCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.tmdbId = other.tmdbId == null ? null : other.tmdbId.copy();
        this.name = other.name == null ? null : other.name.copy();
        this.lastTMDBUpdate = other.lastTMDBUpdate == null ? null : other.lastTMDBUpdate.copy();
        this.movieId = other.movieId == null ? null : other.movieId.copy();
    }

    @Override
    public GenreCriteria copy() {
        return new GenreCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getTmdbId() {
        return tmdbId;
    }

    public void setTmdbId(StringFilter tmdbId) {
        this.tmdbId = tmdbId;
    }

    public StringFilter getName() {
        return name;
    }

    public void setName(StringFilter name) {
        this.name = name;
    }

    public LocalDateFilter getLastTMDBUpdate() {
        return lastTMDBUpdate;
    }

    public void setLastTMDBUpdate(LocalDateFilter lastTMDBUpdate) {
        this.lastTMDBUpdate = lastTMDBUpdate;
    }

    public LongFilter getMovieId() {
        return movieId;
    }

    public void setMovieId(LongFilter movieId) {
        this.movieId = movieId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final GenreCriteria that = (GenreCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(tmdbId, that.tmdbId) &&
            Objects.equals(name, that.name) &&
            Objects.equals(lastTMDBUpdate, that.lastTMDBUpdate) &&
            Objects.equals(movieId, that.movieId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        tmdbId,
        name,
        lastTMDBUpdate,
        movieId
        );
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "GenreCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (tmdbId != null ? "tmdbId=" + tmdbId + ", " : "") +
                (name != null ? "name=" + name + ", " : "") +
                (lastTMDBUpdate != null ? "lastTMDBUpdate=" + lastTMDBUpdate + ", " : "") +
                (movieId != null ? "movieId=" + movieId + ", " : "") +
            "}";
    }

}
