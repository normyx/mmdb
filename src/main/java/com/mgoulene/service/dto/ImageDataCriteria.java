package com.mgoulene.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link com.mgoulene.domain.ImageData} entity. This class is used
 * in {@link com.mgoulene.web.rest.ImageDataResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /image-data?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class ImageDataCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter imageSize;

    private LongFilter imageId;

    public ImageDataCriteria() {
    }

    public ImageDataCriteria(ImageDataCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.imageSize = other.imageSize == null ? null : other.imageSize.copy();
        this.imageId = other.imageId == null ? null : other.imageId.copy();
    }

    @Override
    public ImageDataCriteria copy() {
        return new ImageDataCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getImageSize() {
        return imageSize;
    }

    public void setImageSize(StringFilter imageSize) {
        this.imageSize = imageSize;
    }

    public LongFilter getImageId() {
        return imageId;
    }

    public void setImageId(LongFilter imageId) {
        this.imageId = imageId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final ImageDataCriteria that = (ImageDataCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(imageSize, that.imageSize) &&
            Objects.equals(imageId, that.imageId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        imageSize,
        imageId
        );
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ImageDataCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (imageSize != null ? "imageSize=" + imageSize + ", " : "") +
                (imageId != null ? "imageId=" + imageId + ", " : "") +
            "}";
    }

}
