package com.mgoulene.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.mgoulene.domain.Movie;
import com.mgoulene.domain.*; // for static metamodels
import com.mgoulene.repository.MovieRepository;
import com.mgoulene.service.dto.MovieCriteria;
import com.mgoulene.service.dto.MovieDTO;
import com.mgoulene.service.mapper.MovieMapper;

/**
 * Service for executing complex queries for {@link Movie} entities in the database.
 * The main input is a {@link MovieCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link MovieDTO} or a {@link Page} of {@link MovieDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class MovieQueryService extends QueryService<Movie> {

    private final Logger log = LoggerFactory.getLogger(MovieQueryService.class);

    private final MovieRepository movieRepository;

    private final MovieMapper movieMapper;

    public MovieQueryService(MovieRepository movieRepository, MovieMapper movieMapper) {
        this.movieRepository = movieRepository;
        this.movieMapper = movieMapper;
    }

    /**
     * Return a {@link List} of {@link MovieDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<MovieDTO> findByCriteria(MovieCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<Movie> specification = createSpecification(criteria);
        return movieMapper.toDto(movieRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link MovieDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<MovieDTO> findByCriteria(MovieCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<Movie> specification = createSpecification(criteria);
        return movieRepository.findAll(specification, page)
            .map(movieMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(MovieCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<Movie> specification = createSpecification(criteria);
        return movieRepository.count(specification);
    }

    /**
     * Function to convert {@link MovieCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<Movie> createSpecification(MovieCriteria criteria) {
        Specification<Movie> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), Movie_.id));
            }
            if (criteria.getTitle() != null) {
                specification = specification.and(buildStringSpecification(criteria.getTitle(), Movie_.title));
            }
            if (criteria.getForAdult() != null) {
                specification = specification.and(buildSpecification(criteria.getForAdult(), Movie_.forAdult));
            }
            if (criteria.getHomepage() != null) {
                specification = specification.and(buildStringSpecification(criteria.getHomepage(), Movie_.homepage));
            }
            if (criteria.getOriginalLanguage() != null) {
                specification = specification.and(buildStringSpecification(criteria.getOriginalLanguage(), Movie_.originalLanguage));
            }
            if (criteria.getOriginalTitle() != null) {
                specification = specification.and(buildStringSpecification(criteria.getOriginalTitle(), Movie_.originalTitle));
            }
            if (criteria.getOverview() != null) {
                specification = specification.and(buildStringSpecification(criteria.getOverview(), Movie_.overview));
            }
            if (criteria.getTagline() != null) {
                specification = specification.and(buildStringSpecification(criteria.getTagline(), Movie_.tagline));
            }
            if (criteria.getStatus() != null) {
                specification = specification.and(buildSpecification(criteria.getStatus(), Movie_.status));
            }
            if (criteria.getVoteAverage() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getVoteAverage(), Movie_.voteAverage));
            }
            if (criteria.getVoteCount() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getVoteCount(), Movie_.voteCount));
            }
            if (criteria.getReleaseDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getReleaseDate(), Movie_.releaseDate));
            }
            if (criteria.getLastTMDBUpdate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getLastTMDBUpdate(), Movie_.lastTMDBUpdate));
            }
            if (criteria.getTmdbId() != null) {
                specification = specification.and(buildStringSpecification(criteria.getTmdbId(), Movie_.tmdbId));
            }
            if (criteria.getRuntime() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getRuntime(), Movie_.runtime));
            }
            if (criteria.getCreditsId() != null) {
                specification = specification.and(buildSpecification(criteria.getCreditsId(),
                    root -> root.join(Movie_.credits, JoinType.LEFT).get(Credit_.id)));
            }
            if (criteria.getPosterId() != null) {
                specification = specification.and(buildSpecification(criteria.getPosterId(),
                    root -> root.join(Movie_.posters, JoinType.LEFT).get(Image_.id)));
            }
            if (criteria.getBackdropId() != null) {
                specification = specification.and(buildSpecification(criteria.getBackdropId(),
                    root -> root.join(Movie_.backdrops, JoinType.LEFT).get(Image_.id)));
            }
            if (criteria.getGenreId() != null) {
                specification = specification.and(buildSpecification(criteria.getGenreId(),
                    root -> root.join(Movie_.genres, JoinType.LEFT).get(Genre_.id)));
            }
        }
        return specification;
    }
}
