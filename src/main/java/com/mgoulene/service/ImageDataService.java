package com.mgoulene.service;

import com.mgoulene.domain.ImageData;
import com.mgoulene.repository.ImageDataRepository;
import com.mgoulene.service.dto.ImageDataDTO;
import com.mgoulene.service.mapper.ImageDataMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link ImageData}.
 */
@Service
@Transactional
public class ImageDataService {

    private final Logger log = LoggerFactory.getLogger(ImageDataService.class);

    private final ImageDataRepository imageDataRepository;

    protected final ImageDataMapper imageDataMapper;

    public ImageDataService(ImageDataRepository imageDataRepository, ImageDataMapper imageDataMapper) {
        this.imageDataRepository = imageDataRepository;
        this.imageDataMapper = imageDataMapper;
    }

    /**
     * Save a imageData.
     *
     * @param imageDataDTO the entity to save.
     * @return the persisted entity.
     */
    public ImageDataDTO save(ImageDataDTO imageDataDTO) {
        log.debug("Request to save ImageData : {}", imageDataDTO);
        ImageData imageData = imageDataMapper.toEntity(imageDataDTO);
        imageData = imageDataRepository.save(imageData);
        return imageDataMapper.toDto(imageData);
    }

    /**
     * Get all the imageData.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<ImageDataDTO> findAll(Pageable pageable) {
        log.debug("Request to get all ImageData");
        return imageDataRepository.findAll(pageable)
            .map(imageDataMapper::toDto);
    }


    /**
     * Get one imageData by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<ImageDataDTO> findOne(Long id) {
        log.debug("Request to get ImageData : {}", id);
        return imageDataRepository.findById(id)
            .map(imageDataMapper::toDto);
    }

    /**
     * Delete the imageData by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete ImageData : {}", id);
        imageDataRepository.deleteById(id);
    }
}
