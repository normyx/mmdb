package com.mgoulene.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.context.properties.ConfigurationProperties;
/**
 * Properties specific to Mmdb.
 * <p>
 * Properties are configured in the {@code application.yml} file.
 * See {@link io.github.jhipster.config.JHipsterProperties} for a good example.
 */
@ConfigurationProperties(prefix = "application", ignoreUnknownFields = false)
public class ApplicationProperties {
        

    public final TMDBProperties tmdb = new TMDBProperties();

    public TMDBProperties getTMDB() {
        return tmdb;
    }

    public static class TMDBProperties {
        private final Logger log = LoggerFactory.getLogger(TMDBProperties.class);
        private String apiKey;

        private String language;

        private boolean usePersistentCache = false;

        public String getApiKey() {
            return apiKey;
        }

        public String getLanguage() {
            return language;
        }

        public void setLanguage(String language) {
            this.language = language;
        }

        public void setApiKey(String apiKey) {
            this.apiKey = apiKey;
        }

        public boolean isUsePersistentCache() {
            return usePersistentCache;
        }

        public void setUsePersistentCache(boolean usePersistentCache) {
            this.usePersistentCache = usePersistentCache;
        }


        public String getURLWithAPIKey(String originalURL) {
            return getURLWithAPIKey(originalURL, true);
        }
        public String getURLWithAPIKey(String originalURL, boolean withLanguage) {
            String language = withLanguage ? (getLanguage() != null ? "&language=" + getLanguage() : "") : "";
            String newURL = originalURL + language + "&api_key=" + getApiKey();
            log.debug("TMDB URL from {} to {}", originalURL, newURL);
            return newURL;
        }

        
    }

}
