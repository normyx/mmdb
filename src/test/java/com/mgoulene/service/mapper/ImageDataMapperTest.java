package com.mgoulene.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class ImageDataMapperTest {

    private ImageDataMapper imageDataMapper;

    @BeforeEach
    public void setUp() {
        imageDataMapper = new ImageDataMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(imageDataMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(imageDataMapper.fromId(null)).isNull();
    }
}
