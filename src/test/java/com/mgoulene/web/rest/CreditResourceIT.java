package com.mgoulene.web.rest;

import com.mgoulene.MmdbApp;
import com.mgoulene.domain.Credit;
import com.mgoulene.domain.Person;
import com.mgoulene.domain.Movie;
import com.mgoulene.repository.CreditRepository;
import com.mgoulene.service.CreditService;
import com.mgoulene.service.dto.CreditDTO;
import com.mgoulene.service.mapper.CreditMapper;
import com.mgoulene.service.dto.CreditCriteria;
import com.mgoulene.service.CreditQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link CreditResource} REST controller.
 */
@SpringBootTest(classes = MmdbApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class CreditResourceIT {

    private static final String DEFAULT_TMDB_ID = "AAAAAAAAAA";
    private static final String UPDATED_TMDB_ID = "BBBBBBBBBB";

    private static final String DEFAULT_CHARACTER = "AAAAAAAAAA";
    private static final String UPDATED_CHARACTER = "BBBBBBBBBB";

    private static final String DEFAULT_CREDIT_TYPE = "AAAAAAAAAA";
    private static final String UPDATED_CREDIT_TYPE = "BBBBBBBBBB";

    private static final String DEFAULT_DEPARTMENT = "AAAAAAAAAA";
    private static final String UPDATED_DEPARTMENT = "BBBBBBBBBB";

    private static final String DEFAULT_JOB = "AAAAAAAAAA";
    private static final String UPDATED_JOB = "BBBBBBBBBB";

    private static final Integer DEFAULT_ORDER = 1;
    private static final Integer UPDATED_ORDER = 2;
    private static final Integer SMALLER_ORDER = 1 - 1;

    private static final LocalDate DEFAULT_LAST_TMDB_UPDATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_LAST_TMDB_UPDATE = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_LAST_TMDB_UPDATE = LocalDate.ofEpochDay(-1L);

    @Autowired
    private CreditRepository creditRepository;

    @Autowired
    private CreditMapper creditMapper;

    @Autowired
    private CreditService creditService;

    @Autowired
    private CreditQueryService creditQueryService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restCreditMockMvc;

    private Credit credit;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Credit createEntity(EntityManager em) {
        Credit credit = new Credit()
            .tmdbId(DEFAULT_TMDB_ID)
            .character(DEFAULT_CHARACTER)
            .creditType(DEFAULT_CREDIT_TYPE)
            .department(DEFAULT_DEPARTMENT)
            .job(DEFAULT_JOB)
            .order(DEFAULT_ORDER)
            .lastTMDBUpdate(DEFAULT_LAST_TMDB_UPDATE);
        return credit;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Credit createUpdatedEntity(EntityManager em) {
        Credit credit = new Credit()
            .tmdbId(UPDATED_TMDB_ID)
            .character(UPDATED_CHARACTER)
            .creditType(UPDATED_CREDIT_TYPE)
            .department(UPDATED_DEPARTMENT)
            .job(UPDATED_JOB)
            .order(UPDATED_ORDER)
            .lastTMDBUpdate(UPDATED_LAST_TMDB_UPDATE);
        return credit;
    }

    @BeforeEach
    public void initTest() {
        credit = createEntity(em);
    }

    @Test
    @Transactional
    public void createCredit() throws Exception {
        int databaseSizeBeforeCreate = creditRepository.findAll().size();
        // Create the Credit
        CreditDTO creditDTO = creditMapper.toDto(credit);
        restCreditMockMvc.perform(post("/api/credits")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(creditDTO)))
            .andExpect(status().isCreated());

        // Validate the Credit in the database
        List<Credit> creditList = creditRepository.findAll();
        assertThat(creditList).hasSize(databaseSizeBeforeCreate + 1);
        Credit testCredit = creditList.get(creditList.size() - 1);
        assertThat(testCredit.getTmdbId()).isEqualTo(DEFAULT_TMDB_ID);
        assertThat(testCredit.getCharacter()).isEqualTo(DEFAULT_CHARACTER);
        assertThat(testCredit.getCreditType()).isEqualTo(DEFAULT_CREDIT_TYPE);
        assertThat(testCredit.getDepartment()).isEqualTo(DEFAULT_DEPARTMENT);
        assertThat(testCredit.getJob()).isEqualTo(DEFAULT_JOB);
        assertThat(testCredit.getOrder()).isEqualTo(DEFAULT_ORDER);
        assertThat(testCredit.getLastTMDBUpdate()).isEqualTo(DEFAULT_LAST_TMDB_UPDATE);
    }

    @Test
    @Transactional
    public void createCreditWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = creditRepository.findAll().size();

        // Create the Credit with an existing ID
        credit.setId(1L);
        CreditDTO creditDTO = creditMapper.toDto(credit);

        // An entity with an existing ID cannot be created, so this API call must fail
        restCreditMockMvc.perform(post("/api/credits")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(creditDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Credit in the database
        List<Credit> creditList = creditRepository.findAll();
        assertThat(creditList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkTmdbIdIsRequired() throws Exception {
        int databaseSizeBeforeTest = creditRepository.findAll().size();
        // set the field null
        credit.setTmdbId(null);

        // Create the Credit, which fails.
        CreditDTO creditDTO = creditMapper.toDto(credit);


        restCreditMockMvc.perform(post("/api/credits")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(creditDTO)))
            .andExpect(status().isBadRequest());

        List<Credit> creditList = creditRepository.findAll();
        assertThat(creditList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkLastTMDBUpdateIsRequired() throws Exception {
        int databaseSizeBeforeTest = creditRepository.findAll().size();
        // set the field null
        credit.setLastTMDBUpdate(null);

        // Create the Credit, which fails.
        CreditDTO creditDTO = creditMapper.toDto(credit);


        restCreditMockMvc.perform(post("/api/credits")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(creditDTO)))
            .andExpect(status().isBadRequest());

        List<Credit> creditList = creditRepository.findAll();
        assertThat(creditList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllCredits() throws Exception {
        // Initialize the database
        creditRepository.saveAndFlush(credit);

        // Get all the creditList
        restCreditMockMvc.perform(get("/api/credits?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(credit.getId().intValue())))
            .andExpect(jsonPath("$.[*].tmdbId").value(hasItem(DEFAULT_TMDB_ID)))
            .andExpect(jsonPath("$.[*].character").value(hasItem(DEFAULT_CHARACTER)))
            .andExpect(jsonPath("$.[*].creditType").value(hasItem(DEFAULT_CREDIT_TYPE)))
            .andExpect(jsonPath("$.[*].department").value(hasItem(DEFAULT_DEPARTMENT)))
            .andExpect(jsonPath("$.[*].job").value(hasItem(DEFAULT_JOB)))
            .andExpect(jsonPath("$.[*].order").value(hasItem(DEFAULT_ORDER)))
            .andExpect(jsonPath("$.[*].lastTMDBUpdate").value(hasItem(DEFAULT_LAST_TMDB_UPDATE.toString())));
    }
    
    @Test
    @Transactional
    public void getCredit() throws Exception {
        // Initialize the database
        creditRepository.saveAndFlush(credit);

        // Get the credit
        restCreditMockMvc.perform(get("/api/credits/{id}", credit.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(credit.getId().intValue()))
            .andExpect(jsonPath("$.tmdbId").value(DEFAULT_TMDB_ID))
            .andExpect(jsonPath("$.character").value(DEFAULT_CHARACTER))
            .andExpect(jsonPath("$.creditType").value(DEFAULT_CREDIT_TYPE))
            .andExpect(jsonPath("$.department").value(DEFAULT_DEPARTMENT))
            .andExpect(jsonPath("$.job").value(DEFAULT_JOB))
            .andExpect(jsonPath("$.order").value(DEFAULT_ORDER))
            .andExpect(jsonPath("$.lastTMDBUpdate").value(DEFAULT_LAST_TMDB_UPDATE.toString()));
    }


    @Test
    @Transactional
    public void getCreditsByIdFiltering() throws Exception {
        // Initialize the database
        creditRepository.saveAndFlush(credit);

        Long id = credit.getId();

        defaultCreditShouldBeFound("id.equals=" + id);
        defaultCreditShouldNotBeFound("id.notEquals=" + id);

        defaultCreditShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultCreditShouldNotBeFound("id.greaterThan=" + id);

        defaultCreditShouldBeFound("id.lessThanOrEqual=" + id);
        defaultCreditShouldNotBeFound("id.lessThan=" + id);
    }


    @Test
    @Transactional
    public void getAllCreditsByTmdbIdIsEqualToSomething() throws Exception {
        // Initialize the database
        creditRepository.saveAndFlush(credit);

        // Get all the creditList where tmdbId equals to DEFAULT_TMDB_ID
        defaultCreditShouldBeFound("tmdbId.equals=" + DEFAULT_TMDB_ID);

        // Get all the creditList where tmdbId equals to UPDATED_TMDB_ID
        defaultCreditShouldNotBeFound("tmdbId.equals=" + UPDATED_TMDB_ID);
    }

    @Test
    @Transactional
    public void getAllCreditsByTmdbIdIsNotEqualToSomething() throws Exception {
        // Initialize the database
        creditRepository.saveAndFlush(credit);

        // Get all the creditList where tmdbId not equals to DEFAULT_TMDB_ID
        defaultCreditShouldNotBeFound("tmdbId.notEquals=" + DEFAULT_TMDB_ID);

        // Get all the creditList where tmdbId not equals to UPDATED_TMDB_ID
        defaultCreditShouldBeFound("tmdbId.notEquals=" + UPDATED_TMDB_ID);
    }

    @Test
    @Transactional
    public void getAllCreditsByTmdbIdIsInShouldWork() throws Exception {
        // Initialize the database
        creditRepository.saveAndFlush(credit);

        // Get all the creditList where tmdbId in DEFAULT_TMDB_ID or UPDATED_TMDB_ID
        defaultCreditShouldBeFound("tmdbId.in=" + DEFAULT_TMDB_ID + "," + UPDATED_TMDB_ID);

        // Get all the creditList where tmdbId equals to UPDATED_TMDB_ID
        defaultCreditShouldNotBeFound("tmdbId.in=" + UPDATED_TMDB_ID);
    }

    @Test
    @Transactional
    public void getAllCreditsByTmdbIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        creditRepository.saveAndFlush(credit);

        // Get all the creditList where tmdbId is not null
        defaultCreditShouldBeFound("tmdbId.specified=true");

        // Get all the creditList where tmdbId is null
        defaultCreditShouldNotBeFound("tmdbId.specified=false");
    }
                @Test
    @Transactional
    public void getAllCreditsByTmdbIdContainsSomething() throws Exception {
        // Initialize the database
        creditRepository.saveAndFlush(credit);

        // Get all the creditList where tmdbId contains DEFAULT_TMDB_ID
        defaultCreditShouldBeFound("tmdbId.contains=" + DEFAULT_TMDB_ID);

        // Get all the creditList where tmdbId contains UPDATED_TMDB_ID
        defaultCreditShouldNotBeFound("tmdbId.contains=" + UPDATED_TMDB_ID);
    }

    @Test
    @Transactional
    public void getAllCreditsByTmdbIdNotContainsSomething() throws Exception {
        // Initialize the database
        creditRepository.saveAndFlush(credit);

        // Get all the creditList where tmdbId does not contain DEFAULT_TMDB_ID
        defaultCreditShouldNotBeFound("tmdbId.doesNotContain=" + DEFAULT_TMDB_ID);

        // Get all the creditList where tmdbId does not contain UPDATED_TMDB_ID
        defaultCreditShouldBeFound("tmdbId.doesNotContain=" + UPDATED_TMDB_ID);
    }


    @Test
    @Transactional
    public void getAllCreditsByCharacterIsEqualToSomething() throws Exception {
        // Initialize the database
        creditRepository.saveAndFlush(credit);

        // Get all the creditList where character equals to DEFAULT_CHARACTER
        defaultCreditShouldBeFound("character.equals=" + DEFAULT_CHARACTER);

        // Get all the creditList where character equals to UPDATED_CHARACTER
        defaultCreditShouldNotBeFound("character.equals=" + UPDATED_CHARACTER);
    }

    @Test
    @Transactional
    public void getAllCreditsByCharacterIsNotEqualToSomething() throws Exception {
        // Initialize the database
        creditRepository.saveAndFlush(credit);

        // Get all the creditList where character not equals to DEFAULT_CHARACTER
        defaultCreditShouldNotBeFound("character.notEquals=" + DEFAULT_CHARACTER);

        // Get all the creditList where character not equals to UPDATED_CHARACTER
        defaultCreditShouldBeFound("character.notEquals=" + UPDATED_CHARACTER);
    }

    @Test
    @Transactional
    public void getAllCreditsByCharacterIsInShouldWork() throws Exception {
        // Initialize the database
        creditRepository.saveAndFlush(credit);

        // Get all the creditList where character in DEFAULT_CHARACTER or UPDATED_CHARACTER
        defaultCreditShouldBeFound("character.in=" + DEFAULT_CHARACTER + "," + UPDATED_CHARACTER);

        // Get all the creditList where character equals to UPDATED_CHARACTER
        defaultCreditShouldNotBeFound("character.in=" + UPDATED_CHARACTER);
    }

    @Test
    @Transactional
    public void getAllCreditsByCharacterIsNullOrNotNull() throws Exception {
        // Initialize the database
        creditRepository.saveAndFlush(credit);

        // Get all the creditList where character is not null
        defaultCreditShouldBeFound("character.specified=true");

        // Get all the creditList where character is null
        defaultCreditShouldNotBeFound("character.specified=false");
    }
                @Test
    @Transactional
    public void getAllCreditsByCharacterContainsSomething() throws Exception {
        // Initialize the database
        creditRepository.saveAndFlush(credit);

        // Get all the creditList where character contains DEFAULT_CHARACTER
        defaultCreditShouldBeFound("character.contains=" + DEFAULT_CHARACTER);

        // Get all the creditList where character contains UPDATED_CHARACTER
        defaultCreditShouldNotBeFound("character.contains=" + UPDATED_CHARACTER);
    }

    @Test
    @Transactional
    public void getAllCreditsByCharacterNotContainsSomething() throws Exception {
        // Initialize the database
        creditRepository.saveAndFlush(credit);

        // Get all the creditList where character does not contain DEFAULT_CHARACTER
        defaultCreditShouldNotBeFound("character.doesNotContain=" + DEFAULT_CHARACTER);

        // Get all the creditList where character does not contain UPDATED_CHARACTER
        defaultCreditShouldBeFound("character.doesNotContain=" + UPDATED_CHARACTER);
    }


    @Test
    @Transactional
    public void getAllCreditsByCreditTypeIsEqualToSomething() throws Exception {
        // Initialize the database
        creditRepository.saveAndFlush(credit);

        // Get all the creditList where creditType equals to DEFAULT_CREDIT_TYPE
        defaultCreditShouldBeFound("creditType.equals=" + DEFAULT_CREDIT_TYPE);

        // Get all the creditList where creditType equals to UPDATED_CREDIT_TYPE
        defaultCreditShouldNotBeFound("creditType.equals=" + UPDATED_CREDIT_TYPE);
    }

    @Test
    @Transactional
    public void getAllCreditsByCreditTypeIsNotEqualToSomething() throws Exception {
        // Initialize the database
        creditRepository.saveAndFlush(credit);

        // Get all the creditList where creditType not equals to DEFAULT_CREDIT_TYPE
        defaultCreditShouldNotBeFound("creditType.notEquals=" + DEFAULT_CREDIT_TYPE);

        // Get all the creditList where creditType not equals to UPDATED_CREDIT_TYPE
        defaultCreditShouldBeFound("creditType.notEquals=" + UPDATED_CREDIT_TYPE);
    }

    @Test
    @Transactional
    public void getAllCreditsByCreditTypeIsInShouldWork() throws Exception {
        // Initialize the database
        creditRepository.saveAndFlush(credit);

        // Get all the creditList where creditType in DEFAULT_CREDIT_TYPE or UPDATED_CREDIT_TYPE
        defaultCreditShouldBeFound("creditType.in=" + DEFAULT_CREDIT_TYPE + "," + UPDATED_CREDIT_TYPE);

        // Get all the creditList where creditType equals to UPDATED_CREDIT_TYPE
        defaultCreditShouldNotBeFound("creditType.in=" + UPDATED_CREDIT_TYPE);
    }

    @Test
    @Transactional
    public void getAllCreditsByCreditTypeIsNullOrNotNull() throws Exception {
        // Initialize the database
        creditRepository.saveAndFlush(credit);

        // Get all the creditList where creditType is not null
        defaultCreditShouldBeFound("creditType.specified=true");

        // Get all the creditList where creditType is null
        defaultCreditShouldNotBeFound("creditType.specified=false");
    }
                @Test
    @Transactional
    public void getAllCreditsByCreditTypeContainsSomething() throws Exception {
        // Initialize the database
        creditRepository.saveAndFlush(credit);

        // Get all the creditList where creditType contains DEFAULT_CREDIT_TYPE
        defaultCreditShouldBeFound("creditType.contains=" + DEFAULT_CREDIT_TYPE);

        // Get all the creditList where creditType contains UPDATED_CREDIT_TYPE
        defaultCreditShouldNotBeFound("creditType.contains=" + UPDATED_CREDIT_TYPE);
    }

    @Test
    @Transactional
    public void getAllCreditsByCreditTypeNotContainsSomething() throws Exception {
        // Initialize the database
        creditRepository.saveAndFlush(credit);

        // Get all the creditList where creditType does not contain DEFAULT_CREDIT_TYPE
        defaultCreditShouldNotBeFound("creditType.doesNotContain=" + DEFAULT_CREDIT_TYPE);

        // Get all the creditList where creditType does not contain UPDATED_CREDIT_TYPE
        defaultCreditShouldBeFound("creditType.doesNotContain=" + UPDATED_CREDIT_TYPE);
    }


    @Test
    @Transactional
    public void getAllCreditsByDepartmentIsEqualToSomething() throws Exception {
        // Initialize the database
        creditRepository.saveAndFlush(credit);

        // Get all the creditList where department equals to DEFAULT_DEPARTMENT
        defaultCreditShouldBeFound("department.equals=" + DEFAULT_DEPARTMENT);

        // Get all the creditList where department equals to UPDATED_DEPARTMENT
        defaultCreditShouldNotBeFound("department.equals=" + UPDATED_DEPARTMENT);
    }

    @Test
    @Transactional
    public void getAllCreditsByDepartmentIsNotEqualToSomething() throws Exception {
        // Initialize the database
        creditRepository.saveAndFlush(credit);

        // Get all the creditList where department not equals to DEFAULT_DEPARTMENT
        defaultCreditShouldNotBeFound("department.notEquals=" + DEFAULT_DEPARTMENT);

        // Get all the creditList where department not equals to UPDATED_DEPARTMENT
        defaultCreditShouldBeFound("department.notEquals=" + UPDATED_DEPARTMENT);
    }

    @Test
    @Transactional
    public void getAllCreditsByDepartmentIsInShouldWork() throws Exception {
        // Initialize the database
        creditRepository.saveAndFlush(credit);

        // Get all the creditList where department in DEFAULT_DEPARTMENT or UPDATED_DEPARTMENT
        defaultCreditShouldBeFound("department.in=" + DEFAULT_DEPARTMENT + "," + UPDATED_DEPARTMENT);

        // Get all the creditList where department equals to UPDATED_DEPARTMENT
        defaultCreditShouldNotBeFound("department.in=" + UPDATED_DEPARTMENT);
    }

    @Test
    @Transactional
    public void getAllCreditsByDepartmentIsNullOrNotNull() throws Exception {
        // Initialize the database
        creditRepository.saveAndFlush(credit);

        // Get all the creditList where department is not null
        defaultCreditShouldBeFound("department.specified=true");

        // Get all the creditList where department is null
        defaultCreditShouldNotBeFound("department.specified=false");
    }
                @Test
    @Transactional
    public void getAllCreditsByDepartmentContainsSomething() throws Exception {
        // Initialize the database
        creditRepository.saveAndFlush(credit);

        // Get all the creditList where department contains DEFAULT_DEPARTMENT
        defaultCreditShouldBeFound("department.contains=" + DEFAULT_DEPARTMENT);

        // Get all the creditList where department contains UPDATED_DEPARTMENT
        defaultCreditShouldNotBeFound("department.contains=" + UPDATED_DEPARTMENT);
    }

    @Test
    @Transactional
    public void getAllCreditsByDepartmentNotContainsSomething() throws Exception {
        // Initialize the database
        creditRepository.saveAndFlush(credit);

        // Get all the creditList where department does not contain DEFAULT_DEPARTMENT
        defaultCreditShouldNotBeFound("department.doesNotContain=" + DEFAULT_DEPARTMENT);

        // Get all the creditList where department does not contain UPDATED_DEPARTMENT
        defaultCreditShouldBeFound("department.doesNotContain=" + UPDATED_DEPARTMENT);
    }


    @Test
    @Transactional
    public void getAllCreditsByJobIsEqualToSomething() throws Exception {
        // Initialize the database
        creditRepository.saveAndFlush(credit);

        // Get all the creditList where job equals to DEFAULT_JOB
        defaultCreditShouldBeFound("job.equals=" + DEFAULT_JOB);

        // Get all the creditList where job equals to UPDATED_JOB
        defaultCreditShouldNotBeFound("job.equals=" + UPDATED_JOB);
    }

    @Test
    @Transactional
    public void getAllCreditsByJobIsNotEqualToSomething() throws Exception {
        // Initialize the database
        creditRepository.saveAndFlush(credit);

        // Get all the creditList where job not equals to DEFAULT_JOB
        defaultCreditShouldNotBeFound("job.notEquals=" + DEFAULT_JOB);

        // Get all the creditList where job not equals to UPDATED_JOB
        defaultCreditShouldBeFound("job.notEquals=" + UPDATED_JOB);
    }

    @Test
    @Transactional
    public void getAllCreditsByJobIsInShouldWork() throws Exception {
        // Initialize the database
        creditRepository.saveAndFlush(credit);

        // Get all the creditList where job in DEFAULT_JOB or UPDATED_JOB
        defaultCreditShouldBeFound("job.in=" + DEFAULT_JOB + "," + UPDATED_JOB);

        // Get all the creditList where job equals to UPDATED_JOB
        defaultCreditShouldNotBeFound("job.in=" + UPDATED_JOB);
    }

    @Test
    @Transactional
    public void getAllCreditsByJobIsNullOrNotNull() throws Exception {
        // Initialize the database
        creditRepository.saveAndFlush(credit);

        // Get all the creditList where job is not null
        defaultCreditShouldBeFound("job.specified=true");

        // Get all the creditList where job is null
        defaultCreditShouldNotBeFound("job.specified=false");
    }
                @Test
    @Transactional
    public void getAllCreditsByJobContainsSomething() throws Exception {
        // Initialize the database
        creditRepository.saveAndFlush(credit);

        // Get all the creditList where job contains DEFAULT_JOB
        defaultCreditShouldBeFound("job.contains=" + DEFAULT_JOB);

        // Get all the creditList where job contains UPDATED_JOB
        defaultCreditShouldNotBeFound("job.contains=" + UPDATED_JOB);
    }

    @Test
    @Transactional
    public void getAllCreditsByJobNotContainsSomething() throws Exception {
        // Initialize the database
        creditRepository.saveAndFlush(credit);

        // Get all the creditList where job does not contain DEFAULT_JOB
        defaultCreditShouldNotBeFound("job.doesNotContain=" + DEFAULT_JOB);

        // Get all the creditList where job does not contain UPDATED_JOB
        defaultCreditShouldBeFound("job.doesNotContain=" + UPDATED_JOB);
    }


    @Test
    @Transactional
    public void getAllCreditsByOrderIsEqualToSomething() throws Exception {
        // Initialize the database
        creditRepository.saveAndFlush(credit);

        // Get all the creditList where order equals to DEFAULT_ORDER
        defaultCreditShouldBeFound("order.equals=" + DEFAULT_ORDER);

        // Get all the creditList where order equals to UPDATED_ORDER
        defaultCreditShouldNotBeFound("order.equals=" + UPDATED_ORDER);
    }

    @Test
    @Transactional
    public void getAllCreditsByOrderIsNotEqualToSomething() throws Exception {
        // Initialize the database
        creditRepository.saveAndFlush(credit);

        // Get all the creditList where order not equals to DEFAULT_ORDER
        defaultCreditShouldNotBeFound("order.notEquals=" + DEFAULT_ORDER);

        // Get all the creditList where order not equals to UPDATED_ORDER
        defaultCreditShouldBeFound("order.notEquals=" + UPDATED_ORDER);
    }

    @Test
    @Transactional
    public void getAllCreditsByOrderIsInShouldWork() throws Exception {
        // Initialize the database
        creditRepository.saveAndFlush(credit);

        // Get all the creditList where order in DEFAULT_ORDER or UPDATED_ORDER
        defaultCreditShouldBeFound("order.in=" + DEFAULT_ORDER + "," + UPDATED_ORDER);

        // Get all the creditList where order equals to UPDATED_ORDER
        defaultCreditShouldNotBeFound("order.in=" + UPDATED_ORDER);
    }

    @Test
    @Transactional
    public void getAllCreditsByOrderIsNullOrNotNull() throws Exception {
        // Initialize the database
        creditRepository.saveAndFlush(credit);

        // Get all the creditList where order is not null
        defaultCreditShouldBeFound("order.specified=true");

        // Get all the creditList where order is null
        defaultCreditShouldNotBeFound("order.specified=false");
    }

    @Test
    @Transactional
    public void getAllCreditsByOrderIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        creditRepository.saveAndFlush(credit);

        // Get all the creditList where order is greater than or equal to DEFAULT_ORDER
        defaultCreditShouldBeFound("order.greaterThanOrEqual=" + DEFAULT_ORDER);

        // Get all the creditList where order is greater than or equal to UPDATED_ORDER
        defaultCreditShouldNotBeFound("order.greaterThanOrEqual=" + UPDATED_ORDER);
    }

    @Test
    @Transactional
    public void getAllCreditsByOrderIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        creditRepository.saveAndFlush(credit);

        // Get all the creditList where order is less than or equal to DEFAULT_ORDER
        defaultCreditShouldBeFound("order.lessThanOrEqual=" + DEFAULT_ORDER);

        // Get all the creditList where order is less than or equal to SMALLER_ORDER
        defaultCreditShouldNotBeFound("order.lessThanOrEqual=" + SMALLER_ORDER);
    }

    @Test
    @Transactional
    public void getAllCreditsByOrderIsLessThanSomething() throws Exception {
        // Initialize the database
        creditRepository.saveAndFlush(credit);

        // Get all the creditList where order is less than DEFAULT_ORDER
        defaultCreditShouldNotBeFound("order.lessThan=" + DEFAULT_ORDER);

        // Get all the creditList where order is less than UPDATED_ORDER
        defaultCreditShouldBeFound("order.lessThan=" + UPDATED_ORDER);
    }

    @Test
    @Transactional
    public void getAllCreditsByOrderIsGreaterThanSomething() throws Exception {
        // Initialize the database
        creditRepository.saveAndFlush(credit);

        // Get all the creditList where order is greater than DEFAULT_ORDER
        defaultCreditShouldNotBeFound("order.greaterThan=" + DEFAULT_ORDER);

        // Get all the creditList where order is greater than SMALLER_ORDER
        defaultCreditShouldBeFound("order.greaterThan=" + SMALLER_ORDER);
    }


    @Test
    @Transactional
    public void getAllCreditsByLastTMDBUpdateIsEqualToSomething() throws Exception {
        // Initialize the database
        creditRepository.saveAndFlush(credit);

        // Get all the creditList where lastTMDBUpdate equals to DEFAULT_LAST_TMDB_UPDATE
        defaultCreditShouldBeFound("lastTMDBUpdate.equals=" + DEFAULT_LAST_TMDB_UPDATE);

        // Get all the creditList where lastTMDBUpdate equals to UPDATED_LAST_TMDB_UPDATE
        defaultCreditShouldNotBeFound("lastTMDBUpdate.equals=" + UPDATED_LAST_TMDB_UPDATE);
    }

    @Test
    @Transactional
    public void getAllCreditsByLastTMDBUpdateIsNotEqualToSomething() throws Exception {
        // Initialize the database
        creditRepository.saveAndFlush(credit);

        // Get all the creditList where lastTMDBUpdate not equals to DEFAULT_LAST_TMDB_UPDATE
        defaultCreditShouldNotBeFound("lastTMDBUpdate.notEquals=" + DEFAULT_LAST_TMDB_UPDATE);

        // Get all the creditList where lastTMDBUpdate not equals to UPDATED_LAST_TMDB_UPDATE
        defaultCreditShouldBeFound("lastTMDBUpdate.notEquals=" + UPDATED_LAST_TMDB_UPDATE);
    }

    @Test
    @Transactional
    public void getAllCreditsByLastTMDBUpdateIsInShouldWork() throws Exception {
        // Initialize the database
        creditRepository.saveAndFlush(credit);

        // Get all the creditList where lastTMDBUpdate in DEFAULT_LAST_TMDB_UPDATE or UPDATED_LAST_TMDB_UPDATE
        defaultCreditShouldBeFound("lastTMDBUpdate.in=" + DEFAULT_LAST_TMDB_UPDATE + "," + UPDATED_LAST_TMDB_UPDATE);

        // Get all the creditList where lastTMDBUpdate equals to UPDATED_LAST_TMDB_UPDATE
        defaultCreditShouldNotBeFound("lastTMDBUpdate.in=" + UPDATED_LAST_TMDB_UPDATE);
    }

    @Test
    @Transactional
    public void getAllCreditsByLastTMDBUpdateIsNullOrNotNull() throws Exception {
        // Initialize the database
        creditRepository.saveAndFlush(credit);

        // Get all the creditList where lastTMDBUpdate is not null
        defaultCreditShouldBeFound("lastTMDBUpdate.specified=true");

        // Get all the creditList where lastTMDBUpdate is null
        defaultCreditShouldNotBeFound("lastTMDBUpdate.specified=false");
    }

    @Test
    @Transactional
    public void getAllCreditsByLastTMDBUpdateIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        creditRepository.saveAndFlush(credit);

        // Get all the creditList where lastTMDBUpdate is greater than or equal to DEFAULT_LAST_TMDB_UPDATE
        defaultCreditShouldBeFound("lastTMDBUpdate.greaterThanOrEqual=" + DEFAULT_LAST_TMDB_UPDATE);

        // Get all the creditList where lastTMDBUpdate is greater than or equal to UPDATED_LAST_TMDB_UPDATE
        defaultCreditShouldNotBeFound("lastTMDBUpdate.greaterThanOrEqual=" + UPDATED_LAST_TMDB_UPDATE);
    }

    @Test
    @Transactional
    public void getAllCreditsByLastTMDBUpdateIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        creditRepository.saveAndFlush(credit);

        // Get all the creditList where lastTMDBUpdate is less than or equal to DEFAULT_LAST_TMDB_UPDATE
        defaultCreditShouldBeFound("lastTMDBUpdate.lessThanOrEqual=" + DEFAULT_LAST_TMDB_UPDATE);

        // Get all the creditList where lastTMDBUpdate is less than or equal to SMALLER_LAST_TMDB_UPDATE
        defaultCreditShouldNotBeFound("lastTMDBUpdate.lessThanOrEqual=" + SMALLER_LAST_TMDB_UPDATE);
    }

    @Test
    @Transactional
    public void getAllCreditsByLastTMDBUpdateIsLessThanSomething() throws Exception {
        // Initialize the database
        creditRepository.saveAndFlush(credit);

        // Get all the creditList where lastTMDBUpdate is less than DEFAULT_LAST_TMDB_UPDATE
        defaultCreditShouldNotBeFound("lastTMDBUpdate.lessThan=" + DEFAULT_LAST_TMDB_UPDATE);

        // Get all the creditList where lastTMDBUpdate is less than UPDATED_LAST_TMDB_UPDATE
        defaultCreditShouldBeFound("lastTMDBUpdate.lessThan=" + UPDATED_LAST_TMDB_UPDATE);
    }

    @Test
    @Transactional
    public void getAllCreditsByLastTMDBUpdateIsGreaterThanSomething() throws Exception {
        // Initialize the database
        creditRepository.saveAndFlush(credit);

        // Get all the creditList where lastTMDBUpdate is greater than DEFAULT_LAST_TMDB_UPDATE
        defaultCreditShouldNotBeFound("lastTMDBUpdate.greaterThan=" + DEFAULT_LAST_TMDB_UPDATE);

        // Get all the creditList where lastTMDBUpdate is greater than SMALLER_LAST_TMDB_UPDATE
        defaultCreditShouldBeFound("lastTMDBUpdate.greaterThan=" + SMALLER_LAST_TMDB_UPDATE);
    }


    @Test
    @Transactional
    public void getAllCreditsByPersonIsEqualToSomething() throws Exception {
        // Initialize the database
        creditRepository.saveAndFlush(credit);
        Person person = PersonResourceIT.createEntity(em);
        em.persist(person);
        em.flush();
        credit.setPerson(person);
        creditRepository.saveAndFlush(credit);
        Long personId = person.getId();

        // Get all the creditList where person equals to personId
        defaultCreditShouldBeFound("personId.equals=" + personId);

        // Get all the creditList where person equals to personId + 1
        defaultCreditShouldNotBeFound("personId.equals=" + (personId + 1));
    }


    @Test
    @Transactional
    public void getAllCreditsByMovieIsEqualToSomething() throws Exception {
        // Initialize the database
        creditRepository.saveAndFlush(credit);
        Movie movie = MovieResourceIT.createEntity(em);
        em.persist(movie);
        em.flush();
        credit.setMovie(movie);
        creditRepository.saveAndFlush(credit);
        Long movieId = movie.getId();

        // Get all the creditList where movie equals to movieId
        defaultCreditShouldBeFound("movieId.equals=" + movieId);

        // Get all the creditList where movie equals to movieId + 1
        defaultCreditShouldNotBeFound("movieId.equals=" + (movieId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultCreditShouldBeFound(String filter) throws Exception {
        restCreditMockMvc.perform(get("/api/credits?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(credit.getId().intValue())))
            .andExpect(jsonPath("$.[*].tmdbId").value(hasItem(DEFAULT_TMDB_ID)))
            .andExpect(jsonPath("$.[*].character").value(hasItem(DEFAULT_CHARACTER)))
            .andExpect(jsonPath("$.[*].creditType").value(hasItem(DEFAULT_CREDIT_TYPE)))
            .andExpect(jsonPath("$.[*].department").value(hasItem(DEFAULT_DEPARTMENT)))
            .andExpect(jsonPath("$.[*].job").value(hasItem(DEFAULT_JOB)))
            .andExpect(jsonPath("$.[*].order").value(hasItem(DEFAULT_ORDER)))
            .andExpect(jsonPath("$.[*].lastTMDBUpdate").value(hasItem(DEFAULT_LAST_TMDB_UPDATE.toString())));

        // Check, that the count call also returns 1
        restCreditMockMvc.perform(get("/api/credits/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultCreditShouldNotBeFound(String filter) throws Exception {
        restCreditMockMvc.perform(get("/api/credits?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restCreditMockMvc.perform(get("/api/credits/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    public void getNonExistingCredit() throws Exception {
        // Get the credit
        restCreditMockMvc.perform(get("/api/credits/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateCredit() throws Exception {
        // Initialize the database
        creditRepository.saveAndFlush(credit);

        int databaseSizeBeforeUpdate = creditRepository.findAll().size();

        // Update the credit
        Credit updatedCredit = creditRepository.findById(credit.getId()).get();
        // Disconnect from session so that the updates on updatedCredit are not directly saved in db
        em.detach(updatedCredit);
        updatedCredit
            .tmdbId(UPDATED_TMDB_ID)
            .character(UPDATED_CHARACTER)
            .creditType(UPDATED_CREDIT_TYPE)
            .department(UPDATED_DEPARTMENT)
            .job(UPDATED_JOB)
            .order(UPDATED_ORDER)
            .lastTMDBUpdate(UPDATED_LAST_TMDB_UPDATE);
        CreditDTO creditDTO = creditMapper.toDto(updatedCredit);

        restCreditMockMvc.perform(put("/api/credits")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(creditDTO)))
            .andExpect(status().isOk());

        // Validate the Credit in the database
        List<Credit> creditList = creditRepository.findAll();
        assertThat(creditList).hasSize(databaseSizeBeforeUpdate);
        Credit testCredit = creditList.get(creditList.size() - 1);
        assertThat(testCredit.getTmdbId()).isEqualTo(UPDATED_TMDB_ID);
        assertThat(testCredit.getCharacter()).isEqualTo(UPDATED_CHARACTER);
        assertThat(testCredit.getCreditType()).isEqualTo(UPDATED_CREDIT_TYPE);
        assertThat(testCredit.getDepartment()).isEqualTo(UPDATED_DEPARTMENT);
        assertThat(testCredit.getJob()).isEqualTo(UPDATED_JOB);
        assertThat(testCredit.getOrder()).isEqualTo(UPDATED_ORDER);
        assertThat(testCredit.getLastTMDBUpdate()).isEqualTo(UPDATED_LAST_TMDB_UPDATE);
    }

    @Test
    @Transactional
    public void updateNonExistingCredit() throws Exception {
        int databaseSizeBeforeUpdate = creditRepository.findAll().size();

        // Create the Credit
        CreditDTO creditDTO = creditMapper.toDto(credit);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restCreditMockMvc.perform(put("/api/credits")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(creditDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Credit in the database
        List<Credit> creditList = creditRepository.findAll();
        assertThat(creditList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteCredit() throws Exception {
        // Initialize the database
        creditRepository.saveAndFlush(credit);

        int databaseSizeBeforeDelete = creditRepository.findAll().size();

        // Delete the credit
        restCreditMockMvc.perform(delete("/api/credits/{id}", credit.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Credit> creditList = creditRepository.findAll();
        assertThat(creditList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
