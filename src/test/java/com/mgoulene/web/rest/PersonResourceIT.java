package com.mgoulene.web.rest;

import com.mgoulene.MmdbApp;
import com.mgoulene.domain.Person;
import com.mgoulene.domain.Image;
import com.mgoulene.repository.PersonRepository;
import com.mgoulene.service.PersonService;
import com.mgoulene.service.dto.PersonDTO;
import com.mgoulene.service.mapper.PersonMapper;
import com.mgoulene.service.dto.PersonCriteria;
import com.mgoulene.service.PersonQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link PersonResource} REST controller.
 */
@SpringBootTest(classes = MmdbApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class PersonResourceIT {

    private static final LocalDate DEFAULT_BIRTHDAY = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_BIRTHDAY = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_BIRTHDAY = LocalDate.ofEpochDay(-1L);

    private static final LocalDate DEFAULT_DEATHDAY = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DEATHDAY = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DEATHDAY = LocalDate.ofEpochDay(-1L);

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_AKA = "AAAAAAAAAA";
    private static final String UPDATED_AKA = "BBBBBBBBBB";

    private static final Integer DEFAULT_GENDER = 1;
    private static final Integer UPDATED_GENDER = 2;
    private static final Integer SMALLER_GENDER = 1 - 1;

    private static final String DEFAULT_BIOGRAPHY = "AAAAAAAAAA";
    private static final String UPDATED_BIOGRAPHY = "BBBBBBBBBB";

    private static final String DEFAULT_PLACE_OF_BIRTH = "AAAAAAAAAA";
    private static final String UPDATED_PLACE_OF_BIRTH = "BBBBBBBBBB";

    private static final String DEFAULT_HOMEPAGE = "AAAAAAAAAA";
    private static final String UPDATED_HOMEPAGE = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_LAST_TMDB_UPDATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_LAST_TMDB_UPDATE = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_LAST_TMDB_UPDATE = LocalDate.ofEpochDay(-1L);

    private static final String DEFAULT_TMDB_ID = "AAAAAAAAAA";
    private static final String UPDATED_TMDB_ID = "BBBBBBBBBB";

    @Autowired
    private PersonRepository personRepository;

    @Autowired
    private PersonMapper personMapper;

    @Autowired
    private PersonService personService;

    @Autowired
    private PersonQueryService personQueryService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restPersonMockMvc;

    private Person person;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Person createEntity(EntityManager em) {
        Person person = new Person()
            .birthday(DEFAULT_BIRTHDAY)
            .deathday(DEFAULT_DEATHDAY)
            .name(DEFAULT_NAME)
            .aka(DEFAULT_AKA)
            .gender(DEFAULT_GENDER)
            .biography(DEFAULT_BIOGRAPHY)
            .placeOfBirth(DEFAULT_PLACE_OF_BIRTH)
            .homepage(DEFAULT_HOMEPAGE)
            .lastTMDBUpdate(DEFAULT_LAST_TMDB_UPDATE)
            .tmdbId(DEFAULT_TMDB_ID);
        return person;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Person createUpdatedEntity(EntityManager em) {
        Person person = new Person()
            .birthday(UPDATED_BIRTHDAY)
            .deathday(UPDATED_DEATHDAY)
            .name(UPDATED_NAME)
            .aka(UPDATED_AKA)
            .gender(UPDATED_GENDER)
            .biography(UPDATED_BIOGRAPHY)
            .placeOfBirth(UPDATED_PLACE_OF_BIRTH)
            .homepage(UPDATED_HOMEPAGE)
            .lastTMDBUpdate(UPDATED_LAST_TMDB_UPDATE)
            .tmdbId(UPDATED_TMDB_ID);
        return person;
    }

    @BeforeEach
    public void initTest() {
        person = createEntity(em);
    }

    @Test
    @Transactional
    public void createPerson() throws Exception {
        int databaseSizeBeforeCreate = personRepository.findAll().size();
        // Create the Person
        PersonDTO personDTO = personMapper.toDto(person);
        restPersonMockMvc.perform(post("/api/people")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(personDTO)))
            .andExpect(status().isCreated());

        // Validate the Person in the database
        List<Person> personList = personRepository.findAll();
        assertThat(personList).hasSize(databaseSizeBeforeCreate + 1);
        Person testPerson = personList.get(personList.size() - 1);
        assertThat(testPerson.getBirthday()).isEqualTo(DEFAULT_BIRTHDAY);
        assertThat(testPerson.getDeathday()).isEqualTo(DEFAULT_DEATHDAY);
        assertThat(testPerson.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testPerson.getAka()).isEqualTo(DEFAULT_AKA);
        assertThat(testPerson.getGender()).isEqualTo(DEFAULT_GENDER);
        assertThat(testPerson.getBiography()).isEqualTo(DEFAULT_BIOGRAPHY);
        assertThat(testPerson.getPlaceOfBirth()).isEqualTo(DEFAULT_PLACE_OF_BIRTH);
        assertThat(testPerson.getHomepage()).isEqualTo(DEFAULT_HOMEPAGE);
        assertThat(testPerson.getLastTMDBUpdate()).isEqualTo(DEFAULT_LAST_TMDB_UPDATE);
        assertThat(testPerson.getTmdbId()).isEqualTo(DEFAULT_TMDB_ID);
    }

    @Test
    @Transactional
    public void createPersonWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = personRepository.findAll().size();

        // Create the Person with an existing ID
        person.setId(1L);
        PersonDTO personDTO = personMapper.toDto(person);

        // An entity with an existing ID cannot be created, so this API call must fail
        restPersonMockMvc.perform(post("/api/people")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(personDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Person in the database
        List<Person> personList = personRepository.findAll();
        assertThat(personList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkLastTMDBUpdateIsRequired() throws Exception {
        int databaseSizeBeforeTest = personRepository.findAll().size();
        // set the field null
        person.setLastTMDBUpdate(null);

        // Create the Person, which fails.
        PersonDTO personDTO = personMapper.toDto(person);


        restPersonMockMvc.perform(post("/api/people")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(personDTO)))
            .andExpect(status().isBadRequest());

        List<Person> personList = personRepository.findAll();
        assertThat(personList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkTmdbIdIsRequired() throws Exception {
        int databaseSizeBeforeTest = personRepository.findAll().size();
        // set the field null
        person.setTmdbId(null);

        // Create the Person, which fails.
        PersonDTO personDTO = personMapper.toDto(person);


        restPersonMockMvc.perform(post("/api/people")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(personDTO)))
            .andExpect(status().isBadRequest());

        List<Person> personList = personRepository.findAll();
        assertThat(personList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllPeople() throws Exception {
        // Initialize the database
        personRepository.saveAndFlush(person);

        // Get all the personList
        restPersonMockMvc.perform(get("/api/people?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(person.getId().intValue())))
            .andExpect(jsonPath("$.[*].birthday").value(hasItem(DEFAULT_BIRTHDAY.toString())))
            .andExpect(jsonPath("$.[*].deathday").value(hasItem(DEFAULT_DEATHDAY.toString())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].aka").value(hasItem(DEFAULT_AKA)))
            .andExpect(jsonPath("$.[*].gender").value(hasItem(DEFAULT_GENDER)))
            .andExpect(jsonPath("$.[*].biography").value(hasItem(DEFAULT_BIOGRAPHY)))
            .andExpect(jsonPath("$.[*].placeOfBirth").value(hasItem(DEFAULT_PLACE_OF_BIRTH)))
            .andExpect(jsonPath("$.[*].homepage").value(hasItem(DEFAULT_HOMEPAGE)))
            .andExpect(jsonPath("$.[*].lastTMDBUpdate").value(hasItem(DEFAULT_LAST_TMDB_UPDATE.toString())))
            .andExpect(jsonPath("$.[*].tmdbId").value(hasItem(DEFAULT_TMDB_ID)));
    }
    
    @Test
    @Transactional
    public void getPerson() throws Exception {
        // Initialize the database
        personRepository.saveAndFlush(person);

        // Get the person
        restPersonMockMvc.perform(get("/api/people/{id}", person.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(person.getId().intValue()))
            .andExpect(jsonPath("$.birthday").value(DEFAULT_BIRTHDAY.toString()))
            .andExpect(jsonPath("$.deathday").value(DEFAULT_DEATHDAY.toString()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME))
            .andExpect(jsonPath("$.aka").value(DEFAULT_AKA))
            .andExpect(jsonPath("$.gender").value(DEFAULT_GENDER))
            .andExpect(jsonPath("$.biography").value(DEFAULT_BIOGRAPHY))
            .andExpect(jsonPath("$.placeOfBirth").value(DEFAULT_PLACE_OF_BIRTH))
            .andExpect(jsonPath("$.homepage").value(DEFAULT_HOMEPAGE))
            .andExpect(jsonPath("$.lastTMDBUpdate").value(DEFAULT_LAST_TMDB_UPDATE.toString()))
            .andExpect(jsonPath("$.tmdbId").value(DEFAULT_TMDB_ID));
    }


    @Test
    @Transactional
    public void getPeopleByIdFiltering() throws Exception {
        // Initialize the database
        personRepository.saveAndFlush(person);

        Long id = person.getId();

        defaultPersonShouldBeFound("id.equals=" + id);
        defaultPersonShouldNotBeFound("id.notEquals=" + id);

        defaultPersonShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultPersonShouldNotBeFound("id.greaterThan=" + id);

        defaultPersonShouldBeFound("id.lessThanOrEqual=" + id);
        defaultPersonShouldNotBeFound("id.lessThan=" + id);
    }


    @Test
    @Transactional
    public void getAllPeopleByBirthdayIsEqualToSomething() throws Exception {
        // Initialize the database
        personRepository.saveAndFlush(person);

        // Get all the personList where birthday equals to DEFAULT_BIRTHDAY
        defaultPersonShouldBeFound("birthday.equals=" + DEFAULT_BIRTHDAY);

        // Get all the personList where birthday equals to UPDATED_BIRTHDAY
        defaultPersonShouldNotBeFound("birthday.equals=" + UPDATED_BIRTHDAY);
    }

    @Test
    @Transactional
    public void getAllPeopleByBirthdayIsNotEqualToSomething() throws Exception {
        // Initialize the database
        personRepository.saveAndFlush(person);

        // Get all the personList where birthday not equals to DEFAULT_BIRTHDAY
        defaultPersonShouldNotBeFound("birthday.notEquals=" + DEFAULT_BIRTHDAY);

        // Get all the personList where birthday not equals to UPDATED_BIRTHDAY
        defaultPersonShouldBeFound("birthday.notEquals=" + UPDATED_BIRTHDAY);
    }

    @Test
    @Transactional
    public void getAllPeopleByBirthdayIsInShouldWork() throws Exception {
        // Initialize the database
        personRepository.saveAndFlush(person);

        // Get all the personList where birthday in DEFAULT_BIRTHDAY or UPDATED_BIRTHDAY
        defaultPersonShouldBeFound("birthday.in=" + DEFAULT_BIRTHDAY + "," + UPDATED_BIRTHDAY);

        // Get all the personList where birthday equals to UPDATED_BIRTHDAY
        defaultPersonShouldNotBeFound("birthday.in=" + UPDATED_BIRTHDAY);
    }

    @Test
    @Transactional
    public void getAllPeopleByBirthdayIsNullOrNotNull() throws Exception {
        // Initialize the database
        personRepository.saveAndFlush(person);

        // Get all the personList where birthday is not null
        defaultPersonShouldBeFound("birthday.specified=true");

        // Get all the personList where birthday is null
        defaultPersonShouldNotBeFound("birthday.specified=false");
    }

    @Test
    @Transactional
    public void getAllPeopleByBirthdayIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        personRepository.saveAndFlush(person);

        // Get all the personList where birthday is greater than or equal to DEFAULT_BIRTHDAY
        defaultPersonShouldBeFound("birthday.greaterThanOrEqual=" + DEFAULT_BIRTHDAY);

        // Get all the personList where birthday is greater than or equal to UPDATED_BIRTHDAY
        defaultPersonShouldNotBeFound("birthday.greaterThanOrEqual=" + UPDATED_BIRTHDAY);
    }

    @Test
    @Transactional
    public void getAllPeopleByBirthdayIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        personRepository.saveAndFlush(person);

        // Get all the personList where birthday is less than or equal to DEFAULT_BIRTHDAY
        defaultPersonShouldBeFound("birthday.lessThanOrEqual=" + DEFAULT_BIRTHDAY);

        // Get all the personList where birthday is less than or equal to SMALLER_BIRTHDAY
        defaultPersonShouldNotBeFound("birthday.lessThanOrEqual=" + SMALLER_BIRTHDAY);
    }

    @Test
    @Transactional
    public void getAllPeopleByBirthdayIsLessThanSomething() throws Exception {
        // Initialize the database
        personRepository.saveAndFlush(person);

        // Get all the personList where birthday is less than DEFAULT_BIRTHDAY
        defaultPersonShouldNotBeFound("birthday.lessThan=" + DEFAULT_BIRTHDAY);

        // Get all the personList where birthday is less than UPDATED_BIRTHDAY
        defaultPersonShouldBeFound("birthday.lessThan=" + UPDATED_BIRTHDAY);
    }

    @Test
    @Transactional
    public void getAllPeopleByBirthdayIsGreaterThanSomething() throws Exception {
        // Initialize the database
        personRepository.saveAndFlush(person);

        // Get all the personList where birthday is greater than DEFAULT_BIRTHDAY
        defaultPersonShouldNotBeFound("birthday.greaterThan=" + DEFAULT_BIRTHDAY);

        // Get all the personList where birthday is greater than SMALLER_BIRTHDAY
        defaultPersonShouldBeFound("birthday.greaterThan=" + SMALLER_BIRTHDAY);
    }


    @Test
    @Transactional
    public void getAllPeopleByDeathdayIsEqualToSomething() throws Exception {
        // Initialize the database
        personRepository.saveAndFlush(person);

        // Get all the personList where deathday equals to DEFAULT_DEATHDAY
        defaultPersonShouldBeFound("deathday.equals=" + DEFAULT_DEATHDAY);

        // Get all the personList where deathday equals to UPDATED_DEATHDAY
        defaultPersonShouldNotBeFound("deathday.equals=" + UPDATED_DEATHDAY);
    }

    @Test
    @Transactional
    public void getAllPeopleByDeathdayIsNotEqualToSomething() throws Exception {
        // Initialize the database
        personRepository.saveAndFlush(person);

        // Get all the personList where deathday not equals to DEFAULT_DEATHDAY
        defaultPersonShouldNotBeFound("deathday.notEquals=" + DEFAULT_DEATHDAY);

        // Get all the personList where deathday not equals to UPDATED_DEATHDAY
        defaultPersonShouldBeFound("deathday.notEquals=" + UPDATED_DEATHDAY);
    }

    @Test
    @Transactional
    public void getAllPeopleByDeathdayIsInShouldWork() throws Exception {
        // Initialize the database
        personRepository.saveAndFlush(person);

        // Get all the personList where deathday in DEFAULT_DEATHDAY or UPDATED_DEATHDAY
        defaultPersonShouldBeFound("deathday.in=" + DEFAULT_DEATHDAY + "," + UPDATED_DEATHDAY);

        // Get all the personList where deathday equals to UPDATED_DEATHDAY
        defaultPersonShouldNotBeFound("deathday.in=" + UPDATED_DEATHDAY);
    }

    @Test
    @Transactional
    public void getAllPeopleByDeathdayIsNullOrNotNull() throws Exception {
        // Initialize the database
        personRepository.saveAndFlush(person);

        // Get all the personList where deathday is not null
        defaultPersonShouldBeFound("deathday.specified=true");

        // Get all the personList where deathday is null
        defaultPersonShouldNotBeFound("deathday.specified=false");
    }

    @Test
    @Transactional
    public void getAllPeopleByDeathdayIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        personRepository.saveAndFlush(person);

        // Get all the personList where deathday is greater than or equal to DEFAULT_DEATHDAY
        defaultPersonShouldBeFound("deathday.greaterThanOrEqual=" + DEFAULT_DEATHDAY);

        // Get all the personList where deathday is greater than or equal to UPDATED_DEATHDAY
        defaultPersonShouldNotBeFound("deathday.greaterThanOrEqual=" + UPDATED_DEATHDAY);
    }

    @Test
    @Transactional
    public void getAllPeopleByDeathdayIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        personRepository.saveAndFlush(person);

        // Get all the personList where deathday is less than or equal to DEFAULT_DEATHDAY
        defaultPersonShouldBeFound("deathday.lessThanOrEqual=" + DEFAULT_DEATHDAY);

        // Get all the personList where deathday is less than or equal to SMALLER_DEATHDAY
        defaultPersonShouldNotBeFound("deathday.lessThanOrEqual=" + SMALLER_DEATHDAY);
    }

    @Test
    @Transactional
    public void getAllPeopleByDeathdayIsLessThanSomething() throws Exception {
        // Initialize the database
        personRepository.saveAndFlush(person);

        // Get all the personList where deathday is less than DEFAULT_DEATHDAY
        defaultPersonShouldNotBeFound("deathday.lessThan=" + DEFAULT_DEATHDAY);

        // Get all the personList where deathday is less than UPDATED_DEATHDAY
        defaultPersonShouldBeFound("deathday.lessThan=" + UPDATED_DEATHDAY);
    }

    @Test
    @Transactional
    public void getAllPeopleByDeathdayIsGreaterThanSomething() throws Exception {
        // Initialize the database
        personRepository.saveAndFlush(person);

        // Get all the personList where deathday is greater than DEFAULT_DEATHDAY
        defaultPersonShouldNotBeFound("deathday.greaterThan=" + DEFAULT_DEATHDAY);

        // Get all the personList where deathday is greater than SMALLER_DEATHDAY
        defaultPersonShouldBeFound("deathday.greaterThan=" + SMALLER_DEATHDAY);
    }


    @Test
    @Transactional
    public void getAllPeopleByNameIsEqualToSomething() throws Exception {
        // Initialize the database
        personRepository.saveAndFlush(person);

        // Get all the personList where name equals to DEFAULT_NAME
        defaultPersonShouldBeFound("name.equals=" + DEFAULT_NAME);

        // Get all the personList where name equals to UPDATED_NAME
        defaultPersonShouldNotBeFound("name.equals=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    public void getAllPeopleByNameIsNotEqualToSomething() throws Exception {
        // Initialize the database
        personRepository.saveAndFlush(person);

        // Get all the personList where name not equals to DEFAULT_NAME
        defaultPersonShouldNotBeFound("name.notEquals=" + DEFAULT_NAME);

        // Get all the personList where name not equals to UPDATED_NAME
        defaultPersonShouldBeFound("name.notEquals=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    public void getAllPeopleByNameIsInShouldWork() throws Exception {
        // Initialize the database
        personRepository.saveAndFlush(person);

        // Get all the personList where name in DEFAULT_NAME or UPDATED_NAME
        defaultPersonShouldBeFound("name.in=" + DEFAULT_NAME + "," + UPDATED_NAME);

        // Get all the personList where name equals to UPDATED_NAME
        defaultPersonShouldNotBeFound("name.in=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    public void getAllPeopleByNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        personRepository.saveAndFlush(person);

        // Get all the personList where name is not null
        defaultPersonShouldBeFound("name.specified=true");

        // Get all the personList where name is null
        defaultPersonShouldNotBeFound("name.specified=false");
    }
                @Test
    @Transactional
    public void getAllPeopleByNameContainsSomething() throws Exception {
        // Initialize the database
        personRepository.saveAndFlush(person);

        // Get all the personList where name contains DEFAULT_NAME
        defaultPersonShouldBeFound("name.contains=" + DEFAULT_NAME);

        // Get all the personList where name contains UPDATED_NAME
        defaultPersonShouldNotBeFound("name.contains=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    public void getAllPeopleByNameNotContainsSomething() throws Exception {
        // Initialize the database
        personRepository.saveAndFlush(person);

        // Get all the personList where name does not contain DEFAULT_NAME
        defaultPersonShouldNotBeFound("name.doesNotContain=" + DEFAULT_NAME);

        // Get all the personList where name does not contain UPDATED_NAME
        defaultPersonShouldBeFound("name.doesNotContain=" + UPDATED_NAME);
    }


    @Test
    @Transactional
    public void getAllPeopleByAkaIsEqualToSomething() throws Exception {
        // Initialize the database
        personRepository.saveAndFlush(person);

        // Get all the personList where aka equals to DEFAULT_AKA
        defaultPersonShouldBeFound("aka.equals=" + DEFAULT_AKA);

        // Get all the personList where aka equals to UPDATED_AKA
        defaultPersonShouldNotBeFound("aka.equals=" + UPDATED_AKA);
    }

    @Test
    @Transactional
    public void getAllPeopleByAkaIsNotEqualToSomething() throws Exception {
        // Initialize the database
        personRepository.saveAndFlush(person);

        // Get all the personList where aka not equals to DEFAULT_AKA
        defaultPersonShouldNotBeFound("aka.notEquals=" + DEFAULT_AKA);

        // Get all the personList where aka not equals to UPDATED_AKA
        defaultPersonShouldBeFound("aka.notEquals=" + UPDATED_AKA);
    }

    @Test
    @Transactional
    public void getAllPeopleByAkaIsInShouldWork() throws Exception {
        // Initialize the database
        personRepository.saveAndFlush(person);

        // Get all the personList where aka in DEFAULT_AKA or UPDATED_AKA
        defaultPersonShouldBeFound("aka.in=" + DEFAULT_AKA + "," + UPDATED_AKA);

        // Get all the personList where aka equals to UPDATED_AKA
        defaultPersonShouldNotBeFound("aka.in=" + UPDATED_AKA);
    }

    @Test
    @Transactional
    public void getAllPeopleByAkaIsNullOrNotNull() throws Exception {
        // Initialize the database
        personRepository.saveAndFlush(person);

        // Get all the personList where aka is not null
        defaultPersonShouldBeFound("aka.specified=true");

        // Get all the personList where aka is null
        defaultPersonShouldNotBeFound("aka.specified=false");
    }
                @Test
    @Transactional
    public void getAllPeopleByAkaContainsSomething() throws Exception {
        // Initialize the database
        personRepository.saveAndFlush(person);

        // Get all the personList where aka contains DEFAULT_AKA
        defaultPersonShouldBeFound("aka.contains=" + DEFAULT_AKA);

        // Get all the personList where aka contains UPDATED_AKA
        defaultPersonShouldNotBeFound("aka.contains=" + UPDATED_AKA);
    }

    @Test
    @Transactional
    public void getAllPeopleByAkaNotContainsSomething() throws Exception {
        // Initialize the database
        personRepository.saveAndFlush(person);

        // Get all the personList where aka does not contain DEFAULT_AKA
        defaultPersonShouldNotBeFound("aka.doesNotContain=" + DEFAULT_AKA);

        // Get all the personList where aka does not contain UPDATED_AKA
        defaultPersonShouldBeFound("aka.doesNotContain=" + UPDATED_AKA);
    }


    @Test
    @Transactional
    public void getAllPeopleByGenderIsEqualToSomething() throws Exception {
        // Initialize the database
        personRepository.saveAndFlush(person);

        // Get all the personList where gender equals to DEFAULT_GENDER
        defaultPersonShouldBeFound("gender.equals=" + DEFAULT_GENDER);

        // Get all the personList where gender equals to UPDATED_GENDER
        defaultPersonShouldNotBeFound("gender.equals=" + UPDATED_GENDER);
    }

    @Test
    @Transactional
    public void getAllPeopleByGenderIsNotEqualToSomething() throws Exception {
        // Initialize the database
        personRepository.saveAndFlush(person);

        // Get all the personList where gender not equals to DEFAULT_GENDER
        defaultPersonShouldNotBeFound("gender.notEquals=" + DEFAULT_GENDER);

        // Get all the personList where gender not equals to UPDATED_GENDER
        defaultPersonShouldBeFound("gender.notEquals=" + UPDATED_GENDER);
    }

    @Test
    @Transactional
    public void getAllPeopleByGenderIsInShouldWork() throws Exception {
        // Initialize the database
        personRepository.saveAndFlush(person);

        // Get all the personList where gender in DEFAULT_GENDER or UPDATED_GENDER
        defaultPersonShouldBeFound("gender.in=" + DEFAULT_GENDER + "," + UPDATED_GENDER);

        // Get all the personList where gender equals to UPDATED_GENDER
        defaultPersonShouldNotBeFound("gender.in=" + UPDATED_GENDER);
    }

    @Test
    @Transactional
    public void getAllPeopleByGenderIsNullOrNotNull() throws Exception {
        // Initialize the database
        personRepository.saveAndFlush(person);

        // Get all the personList where gender is not null
        defaultPersonShouldBeFound("gender.specified=true");

        // Get all the personList where gender is null
        defaultPersonShouldNotBeFound("gender.specified=false");
    }

    @Test
    @Transactional
    public void getAllPeopleByGenderIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        personRepository.saveAndFlush(person);

        // Get all the personList where gender is greater than or equal to DEFAULT_GENDER
        defaultPersonShouldBeFound("gender.greaterThanOrEqual=" + DEFAULT_GENDER);

        // Get all the personList where gender is greater than or equal to UPDATED_GENDER
        defaultPersonShouldNotBeFound("gender.greaterThanOrEqual=" + UPDATED_GENDER);
    }

    @Test
    @Transactional
    public void getAllPeopleByGenderIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        personRepository.saveAndFlush(person);

        // Get all the personList where gender is less than or equal to DEFAULT_GENDER
        defaultPersonShouldBeFound("gender.lessThanOrEqual=" + DEFAULT_GENDER);

        // Get all the personList where gender is less than or equal to SMALLER_GENDER
        defaultPersonShouldNotBeFound("gender.lessThanOrEqual=" + SMALLER_GENDER);
    }

    @Test
    @Transactional
    public void getAllPeopleByGenderIsLessThanSomething() throws Exception {
        // Initialize the database
        personRepository.saveAndFlush(person);

        // Get all the personList where gender is less than DEFAULT_GENDER
        defaultPersonShouldNotBeFound("gender.lessThan=" + DEFAULT_GENDER);

        // Get all the personList where gender is less than UPDATED_GENDER
        defaultPersonShouldBeFound("gender.lessThan=" + UPDATED_GENDER);
    }

    @Test
    @Transactional
    public void getAllPeopleByGenderIsGreaterThanSomething() throws Exception {
        // Initialize the database
        personRepository.saveAndFlush(person);

        // Get all the personList where gender is greater than DEFAULT_GENDER
        defaultPersonShouldNotBeFound("gender.greaterThan=" + DEFAULT_GENDER);

        // Get all the personList where gender is greater than SMALLER_GENDER
        defaultPersonShouldBeFound("gender.greaterThan=" + SMALLER_GENDER);
    }


    @Test
    @Transactional
    public void getAllPeopleByBiographyIsEqualToSomething() throws Exception {
        // Initialize the database
        personRepository.saveAndFlush(person);

        // Get all the personList where biography equals to DEFAULT_BIOGRAPHY
        defaultPersonShouldBeFound("biography.equals=" + DEFAULT_BIOGRAPHY);

        // Get all the personList where biography equals to UPDATED_BIOGRAPHY
        defaultPersonShouldNotBeFound("biography.equals=" + UPDATED_BIOGRAPHY);
    }

    @Test
    @Transactional
    public void getAllPeopleByBiographyIsNotEqualToSomething() throws Exception {
        // Initialize the database
        personRepository.saveAndFlush(person);

        // Get all the personList where biography not equals to DEFAULT_BIOGRAPHY
        defaultPersonShouldNotBeFound("biography.notEquals=" + DEFAULT_BIOGRAPHY);

        // Get all the personList where biography not equals to UPDATED_BIOGRAPHY
        defaultPersonShouldBeFound("biography.notEquals=" + UPDATED_BIOGRAPHY);
    }

    @Test
    @Transactional
    public void getAllPeopleByBiographyIsInShouldWork() throws Exception {
        // Initialize the database
        personRepository.saveAndFlush(person);

        // Get all the personList where biography in DEFAULT_BIOGRAPHY or UPDATED_BIOGRAPHY
        defaultPersonShouldBeFound("biography.in=" + DEFAULT_BIOGRAPHY + "," + UPDATED_BIOGRAPHY);

        // Get all the personList where biography equals to UPDATED_BIOGRAPHY
        defaultPersonShouldNotBeFound("biography.in=" + UPDATED_BIOGRAPHY);
    }

    @Test
    @Transactional
    public void getAllPeopleByBiographyIsNullOrNotNull() throws Exception {
        // Initialize the database
        personRepository.saveAndFlush(person);

        // Get all the personList where biography is not null
        defaultPersonShouldBeFound("biography.specified=true");

        // Get all the personList where biography is null
        defaultPersonShouldNotBeFound("biography.specified=false");
    }
                @Test
    @Transactional
    public void getAllPeopleByBiographyContainsSomething() throws Exception {
        // Initialize the database
        personRepository.saveAndFlush(person);

        // Get all the personList where biography contains DEFAULT_BIOGRAPHY
        defaultPersonShouldBeFound("biography.contains=" + DEFAULT_BIOGRAPHY);

        // Get all the personList where biography contains UPDATED_BIOGRAPHY
        defaultPersonShouldNotBeFound("biography.contains=" + UPDATED_BIOGRAPHY);
    }

    @Test
    @Transactional
    public void getAllPeopleByBiographyNotContainsSomething() throws Exception {
        // Initialize the database
        personRepository.saveAndFlush(person);

        // Get all the personList where biography does not contain DEFAULT_BIOGRAPHY
        defaultPersonShouldNotBeFound("biography.doesNotContain=" + DEFAULT_BIOGRAPHY);

        // Get all the personList where biography does not contain UPDATED_BIOGRAPHY
        defaultPersonShouldBeFound("biography.doesNotContain=" + UPDATED_BIOGRAPHY);
    }


    @Test
    @Transactional
    public void getAllPeopleByPlaceOfBirthIsEqualToSomething() throws Exception {
        // Initialize the database
        personRepository.saveAndFlush(person);

        // Get all the personList where placeOfBirth equals to DEFAULT_PLACE_OF_BIRTH
        defaultPersonShouldBeFound("placeOfBirth.equals=" + DEFAULT_PLACE_OF_BIRTH);

        // Get all the personList where placeOfBirth equals to UPDATED_PLACE_OF_BIRTH
        defaultPersonShouldNotBeFound("placeOfBirth.equals=" + UPDATED_PLACE_OF_BIRTH);
    }

    @Test
    @Transactional
    public void getAllPeopleByPlaceOfBirthIsNotEqualToSomething() throws Exception {
        // Initialize the database
        personRepository.saveAndFlush(person);

        // Get all the personList where placeOfBirth not equals to DEFAULT_PLACE_OF_BIRTH
        defaultPersonShouldNotBeFound("placeOfBirth.notEquals=" + DEFAULT_PLACE_OF_BIRTH);

        // Get all the personList where placeOfBirth not equals to UPDATED_PLACE_OF_BIRTH
        defaultPersonShouldBeFound("placeOfBirth.notEquals=" + UPDATED_PLACE_OF_BIRTH);
    }

    @Test
    @Transactional
    public void getAllPeopleByPlaceOfBirthIsInShouldWork() throws Exception {
        // Initialize the database
        personRepository.saveAndFlush(person);

        // Get all the personList where placeOfBirth in DEFAULT_PLACE_OF_BIRTH or UPDATED_PLACE_OF_BIRTH
        defaultPersonShouldBeFound("placeOfBirth.in=" + DEFAULT_PLACE_OF_BIRTH + "," + UPDATED_PLACE_OF_BIRTH);

        // Get all the personList where placeOfBirth equals to UPDATED_PLACE_OF_BIRTH
        defaultPersonShouldNotBeFound("placeOfBirth.in=" + UPDATED_PLACE_OF_BIRTH);
    }

    @Test
    @Transactional
    public void getAllPeopleByPlaceOfBirthIsNullOrNotNull() throws Exception {
        // Initialize the database
        personRepository.saveAndFlush(person);

        // Get all the personList where placeOfBirth is not null
        defaultPersonShouldBeFound("placeOfBirth.specified=true");

        // Get all the personList where placeOfBirth is null
        defaultPersonShouldNotBeFound("placeOfBirth.specified=false");
    }
                @Test
    @Transactional
    public void getAllPeopleByPlaceOfBirthContainsSomething() throws Exception {
        // Initialize the database
        personRepository.saveAndFlush(person);

        // Get all the personList where placeOfBirth contains DEFAULT_PLACE_OF_BIRTH
        defaultPersonShouldBeFound("placeOfBirth.contains=" + DEFAULT_PLACE_OF_BIRTH);

        // Get all the personList where placeOfBirth contains UPDATED_PLACE_OF_BIRTH
        defaultPersonShouldNotBeFound("placeOfBirth.contains=" + UPDATED_PLACE_OF_BIRTH);
    }

    @Test
    @Transactional
    public void getAllPeopleByPlaceOfBirthNotContainsSomething() throws Exception {
        // Initialize the database
        personRepository.saveAndFlush(person);

        // Get all the personList where placeOfBirth does not contain DEFAULT_PLACE_OF_BIRTH
        defaultPersonShouldNotBeFound("placeOfBirth.doesNotContain=" + DEFAULT_PLACE_OF_BIRTH);

        // Get all the personList where placeOfBirth does not contain UPDATED_PLACE_OF_BIRTH
        defaultPersonShouldBeFound("placeOfBirth.doesNotContain=" + UPDATED_PLACE_OF_BIRTH);
    }


    @Test
    @Transactional
    public void getAllPeopleByHomepageIsEqualToSomething() throws Exception {
        // Initialize the database
        personRepository.saveAndFlush(person);

        // Get all the personList where homepage equals to DEFAULT_HOMEPAGE
        defaultPersonShouldBeFound("homepage.equals=" + DEFAULT_HOMEPAGE);

        // Get all the personList where homepage equals to UPDATED_HOMEPAGE
        defaultPersonShouldNotBeFound("homepage.equals=" + UPDATED_HOMEPAGE);
    }

    @Test
    @Transactional
    public void getAllPeopleByHomepageIsNotEqualToSomething() throws Exception {
        // Initialize the database
        personRepository.saveAndFlush(person);

        // Get all the personList where homepage not equals to DEFAULT_HOMEPAGE
        defaultPersonShouldNotBeFound("homepage.notEquals=" + DEFAULT_HOMEPAGE);

        // Get all the personList where homepage not equals to UPDATED_HOMEPAGE
        defaultPersonShouldBeFound("homepage.notEquals=" + UPDATED_HOMEPAGE);
    }

    @Test
    @Transactional
    public void getAllPeopleByHomepageIsInShouldWork() throws Exception {
        // Initialize the database
        personRepository.saveAndFlush(person);

        // Get all the personList where homepage in DEFAULT_HOMEPAGE or UPDATED_HOMEPAGE
        defaultPersonShouldBeFound("homepage.in=" + DEFAULT_HOMEPAGE + "," + UPDATED_HOMEPAGE);

        // Get all the personList where homepage equals to UPDATED_HOMEPAGE
        defaultPersonShouldNotBeFound("homepage.in=" + UPDATED_HOMEPAGE);
    }

    @Test
    @Transactional
    public void getAllPeopleByHomepageIsNullOrNotNull() throws Exception {
        // Initialize the database
        personRepository.saveAndFlush(person);

        // Get all the personList where homepage is not null
        defaultPersonShouldBeFound("homepage.specified=true");

        // Get all the personList where homepage is null
        defaultPersonShouldNotBeFound("homepage.specified=false");
    }
                @Test
    @Transactional
    public void getAllPeopleByHomepageContainsSomething() throws Exception {
        // Initialize the database
        personRepository.saveAndFlush(person);

        // Get all the personList where homepage contains DEFAULT_HOMEPAGE
        defaultPersonShouldBeFound("homepage.contains=" + DEFAULT_HOMEPAGE);

        // Get all the personList where homepage contains UPDATED_HOMEPAGE
        defaultPersonShouldNotBeFound("homepage.contains=" + UPDATED_HOMEPAGE);
    }

    @Test
    @Transactional
    public void getAllPeopleByHomepageNotContainsSomething() throws Exception {
        // Initialize the database
        personRepository.saveAndFlush(person);

        // Get all the personList where homepage does not contain DEFAULT_HOMEPAGE
        defaultPersonShouldNotBeFound("homepage.doesNotContain=" + DEFAULT_HOMEPAGE);

        // Get all the personList where homepage does not contain UPDATED_HOMEPAGE
        defaultPersonShouldBeFound("homepage.doesNotContain=" + UPDATED_HOMEPAGE);
    }


    @Test
    @Transactional
    public void getAllPeopleByLastTMDBUpdateIsEqualToSomething() throws Exception {
        // Initialize the database
        personRepository.saveAndFlush(person);

        // Get all the personList where lastTMDBUpdate equals to DEFAULT_LAST_TMDB_UPDATE
        defaultPersonShouldBeFound("lastTMDBUpdate.equals=" + DEFAULT_LAST_TMDB_UPDATE);

        // Get all the personList where lastTMDBUpdate equals to UPDATED_LAST_TMDB_UPDATE
        defaultPersonShouldNotBeFound("lastTMDBUpdate.equals=" + UPDATED_LAST_TMDB_UPDATE);
    }

    @Test
    @Transactional
    public void getAllPeopleByLastTMDBUpdateIsNotEqualToSomething() throws Exception {
        // Initialize the database
        personRepository.saveAndFlush(person);

        // Get all the personList where lastTMDBUpdate not equals to DEFAULT_LAST_TMDB_UPDATE
        defaultPersonShouldNotBeFound("lastTMDBUpdate.notEquals=" + DEFAULT_LAST_TMDB_UPDATE);

        // Get all the personList where lastTMDBUpdate not equals to UPDATED_LAST_TMDB_UPDATE
        defaultPersonShouldBeFound("lastTMDBUpdate.notEquals=" + UPDATED_LAST_TMDB_UPDATE);
    }

    @Test
    @Transactional
    public void getAllPeopleByLastTMDBUpdateIsInShouldWork() throws Exception {
        // Initialize the database
        personRepository.saveAndFlush(person);

        // Get all the personList where lastTMDBUpdate in DEFAULT_LAST_TMDB_UPDATE or UPDATED_LAST_TMDB_UPDATE
        defaultPersonShouldBeFound("lastTMDBUpdate.in=" + DEFAULT_LAST_TMDB_UPDATE + "," + UPDATED_LAST_TMDB_UPDATE);

        // Get all the personList where lastTMDBUpdate equals to UPDATED_LAST_TMDB_UPDATE
        defaultPersonShouldNotBeFound("lastTMDBUpdate.in=" + UPDATED_LAST_TMDB_UPDATE);
    }

    @Test
    @Transactional
    public void getAllPeopleByLastTMDBUpdateIsNullOrNotNull() throws Exception {
        // Initialize the database
        personRepository.saveAndFlush(person);

        // Get all the personList where lastTMDBUpdate is not null
        defaultPersonShouldBeFound("lastTMDBUpdate.specified=true");

        // Get all the personList where lastTMDBUpdate is null
        defaultPersonShouldNotBeFound("lastTMDBUpdate.specified=false");
    }

    @Test
    @Transactional
    public void getAllPeopleByLastTMDBUpdateIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        personRepository.saveAndFlush(person);

        // Get all the personList where lastTMDBUpdate is greater than or equal to DEFAULT_LAST_TMDB_UPDATE
        defaultPersonShouldBeFound("lastTMDBUpdate.greaterThanOrEqual=" + DEFAULT_LAST_TMDB_UPDATE);

        // Get all the personList where lastTMDBUpdate is greater than or equal to UPDATED_LAST_TMDB_UPDATE
        defaultPersonShouldNotBeFound("lastTMDBUpdate.greaterThanOrEqual=" + UPDATED_LAST_TMDB_UPDATE);
    }

    @Test
    @Transactional
    public void getAllPeopleByLastTMDBUpdateIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        personRepository.saveAndFlush(person);

        // Get all the personList where lastTMDBUpdate is less than or equal to DEFAULT_LAST_TMDB_UPDATE
        defaultPersonShouldBeFound("lastTMDBUpdate.lessThanOrEqual=" + DEFAULT_LAST_TMDB_UPDATE);

        // Get all the personList where lastTMDBUpdate is less than or equal to SMALLER_LAST_TMDB_UPDATE
        defaultPersonShouldNotBeFound("lastTMDBUpdate.lessThanOrEqual=" + SMALLER_LAST_TMDB_UPDATE);
    }

    @Test
    @Transactional
    public void getAllPeopleByLastTMDBUpdateIsLessThanSomething() throws Exception {
        // Initialize the database
        personRepository.saveAndFlush(person);

        // Get all the personList where lastTMDBUpdate is less than DEFAULT_LAST_TMDB_UPDATE
        defaultPersonShouldNotBeFound("lastTMDBUpdate.lessThan=" + DEFAULT_LAST_TMDB_UPDATE);

        // Get all the personList where lastTMDBUpdate is less than UPDATED_LAST_TMDB_UPDATE
        defaultPersonShouldBeFound("lastTMDBUpdate.lessThan=" + UPDATED_LAST_TMDB_UPDATE);
    }

    @Test
    @Transactional
    public void getAllPeopleByLastTMDBUpdateIsGreaterThanSomething() throws Exception {
        // Initialize the database
        personRepository.saveAndFlush(person);

        // Get all the personList where lastTMDBUpdate is greater than DEFAULT_LAST_TMDB_UPDATE
        defaultPersonShouldNotBeFound("lastTMDBUpdate.greaterThan=" + DEFAULT_LAST_TMDB_UPDATE);

        // Get all the personList where lastTMDBUpdate is greater than SMALLER_LAST_TMDB_UPDATE
        defaultPersonShouldBeFound("lastTMDBUpdate.greaterThan=" + SMALLER_LAST_TMDB_UPDATE);
    }


    @Test
    @Transactional
    public void getAllPeopleByTmdbIdIsEqualToSomething() throws Exception {
        // Initialize the database
        personRepository.saveAndFlush(person);

        // Get all the personList where tmdbId equals to DEFAULT_TMDB_ID
        defaultPersonShouldBeFound("tmdbId.equals=" + DEFAULT_TMDB_ID);

        // Get all the personList where tmdbId equals to UPDATED_TMDB_ID
        defaultPersonShouldNotBeFound("tmdbId.equals=" + UPDATED_TMDB_ID);
    }

    @Test
    @Transactional
    public void getAllPeopleByTmdbIdIsNotEqualToSomething() throws Exception {
        // Initialize the database
        personRepository.saveAndFlush(person);

        // Get all the personList where tmdbId not equals to DEFAULT_TMDB_ID
        defaultPersonShouldNotBeFound("tmdbId.notEquals=" + DEFAULT_TMDB_ID);

        // Get all the personList where tmdbId not equals to UPDATED_TMDB_ID
        defaultPersonShouldBeFound("tmdbId.notEquals=" + UPDATED_TMDB_ID);
    }

    @Test
    @Transactional
    public void getAllPeopleByTmdbIdIsInShouldWork() throws Exception {
        // Initialize the database
        personRepository.saveAndFlush(person);

        // Get all the personList where tmdbId in DEFAULT_TMDB_ID or UPDATED_TMDB_ID
        defaultPersonShouldBeFound("tmdbId.in=" + DEFAULT_TMDB_ID + "," + UPDATED_TMDB_ID);

        // Get all the personList where tmdbId equals to UPDATED_TMDB_ID
        defaultPersonShouldNotBeFound("tmdbId.in=" + UPDATED_TMDB_ID);
    }

    @Test
    @Transactional
    public void getAllPeopleByTmdbIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        personRepository.saveAndFlush(person);

        // Get all the personList where tmdbId is not null
        defaultPersonShouldBeFound("tmdbId.specified=true");

        // Get all the personList where tmdbId is null
        defaultPersonShouldNotBeFound("tmdbId.specified=false");
    }
                @Test
    @Transactional
    public void getAllPeopleByTmdbIdContainsSomething() throws Exception {
        // Initialize the database
        personRepository.saveAndFlush(person);

        // Get all the personList where tmdbId contains DEFAULT_TMDB_ID
        defaultPersonShouldBeFound("tmdbId.contains=" + DEFAULT_TMDB_ID);

        // Get all the personList where tmdbId contains UPDATED_TMDB_ID
        defaultPersonShouldNotBeFound("tmdbId.contains=" + UPDATED_TMDB_ID);
    }

    @Test
    @Transactional
    public void getAllPeopleByTmdbIdNotContainsSomething() throws Exception {
        // Initialize the database
        personRepository.saveAndFlush(person);

        // Get all the personList where tmdbId does not contain DEFAULT_TMDB_ID
        defaultPersonShouldNotBeFound("tmdbId.doesNotContain=" + DEFAULT_TMDB_ID);

        // Get all the personList where tmdbId does not contain UPDATED_TMDB_ID
        defaultPersonShouldBeFound("tmdbId.doesNotContain=" + UPDATED_TMDB_ID);
    }


    @Test
    @Transactional
    public void getAllPeopleByProfileIsEqualToSomething() throws Exception {
        // Initialize the database
        personRepository.saveAndFlush(person);
        Image profile = ImageResourceIT.createEntity(em);
        em.persist(profile);
        em.flush();
        person.addProfile(profile);
        personRepository.saveAndFlush(person);
        Long profileId = profile.getId();

        // Get all the personList where profile equals to profileId
        defaultPersonShouldBeFound("profileId.equals=" + profileId);

        // Get all the personList where profile equals to profileId + 1
        defaultPersonShouldNotBeFound("profileId.equals=" + (profileId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultPersonShouldBeFound(String filter) throws Exception {
        restPersonMockMvc.perform(get("/api/people?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(person.getId().intValue())))
            .andExpect(jsonPath("$.[*].birthday").value(hasItem(DEFAULT_BIRTHDAY.toString())))
            .andExpect(jsonPath("$.[*].deathday").value(hasItem(DEFAULT_DEATHDAY.toString())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].aka").value(hasItem(DEFAULT_AKA)))
            .andExpect(jsonPath("$.[*].gender").value(hasItem(DEFAULT_GENDER)))
            .andExpect(jsonPath("$.[*].biography").value(hasItem(DEFAULT_BIOGRAPHY)))
            .andExpect(jsonPath("$.[*].placeOfBirth").value(hasItem(DEFAULT_PLACE_OF_BIRTH)))
            .andExpect(jsonPath("$.[*].homepage").value(hasItem(DEFAULT_HOMEPAGE)))
            .andExpect(jsonPath("$.[*].lastTMDBUpdate").value(hasItem(DEFAULT_LAST_TMDB_UPDATE.toString())))
            .andExpect(jsonPath("$.[*].tmdbId").value(hasItem(DEFAULT_TMDB_ID)));

        // Check, that the count call also returns 1
        restPersonMockMvc.perform(get("/api/people/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultPersonShouldNotBeFound(String filter) throws Exception {
        restPersonMockMvc.perform(get("/api/people?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restPersonMockMvc.perform(get("/api/people/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    public void getNonExistingPerson() throws Exception {
        // Get the person
        restPersonMockMvc.perform(get("/api/people/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updatePerson() throws Exception {
        // Initialize the database
        personRepository.saveAndFlush(person);

        int databaseSizeBeforeUpdate = personRepository.findAll().size();

        // Update the person
        Person updatedPerson = personRepository.findById(person.getId()).get();
        // Disconnect from session so that the updates on updatedPerson are not directly saved in db
        em.detach(updatedPerson);
        updatedPerson
            .birthday(UPDATED_BIRTHDAY)
            .deathday(UPDATED_DEATHDAY)
            .name(UPDATED_NAME)
            .aka(UPDATED_AKA)
            .gender(UPDATED_GENDER)
            .biography(UPDATED_BIOGRAPHY)
            .placeOfBirth(UPDATED_PLACE_OF_BIRTH)
            .homepage(UPDATED_HOMEPAGE)
            .lastTMDBUpdate(UPDATED_LAST_TMDB_UPDATE)
            .tmdbId(UPDATED_TMDB_ID);
        PersonDTO personDTO = personMapper.toDto(updatedPerson);

        restPersonMockMvc.perform(put("/api/people")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(personDTO)))
            .andExpect(status().isOk());

        // Validate the Person in the database
        List<Person> personList = personRepository.findAll();
        assertThat(personList).hasSize(databaseSizeBeforeUpdate);
        Person testPerson = personList.get(personList.size() - 1);
        assertThat(testPerson.getBirthday()).isEqualTo(UPDATED_BIRTHDAY);
        assertThat(testPerson.getDeathday()).isEqualTo(UPDATED_DEATHDAY);
        assertThat(testPerson.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testPerson.getAka()).isEqualTo(UPDATED_AKA);
        assertThat(testPerson.getGender()).isEqualTo(UPDATED_GENDER);
        assertThat(testPerson.getBiography()).isEqualTo(UPDATED_BIOGRAPHY);
        assertThat(testPerson.getPlaceOfBirth()).isEqualTo(UPDATED_PLACE_OF_BIRTH);
        assertThat(testPerson.getHomepage()).isEqualTo(UPDATED_HOMEPAGE);
        assertThat(testPerson.getLastTMDBUpdate()).isEqualTo(UPDATED_LAST_TMDB_UPDATE);
        assertThat(testPerson.getTmdbId()).isEqualTo(UPDATED_TMDB_ID);
    }

    @Test
    @Transactional
    public void updateNonExistingPerson() throws Exception {
        int databaseSizeBeforeUpdate = personRepository.findAll().size();

        // Create the Person
        PersonDTO personDTO = personMapper.toDto(person);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restPersonMockMvc.perform(put("/api/people")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(personDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Person in the database
        List<Person> personList = personRepository.findAll();
        assertThat(personList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deletePerson() throws Exception {
        // Initialize the database
        personRepository.saveAndFlush(person);

        int databaseSizeBeforeDelete = personRepository.findAll().size();

        // Delete the person
        restPersonMockMvc.perform(delete("/api/people/{id}", person.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Person> personList = personRepository.findAll();
        assertThat(personList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
