package com.mgoulene.web.rest;

import com.mgoulene.MmdbApp;
import com.mgoulene.domain.Genre;
import com.mgoulene.domain.Movie;
import com.mgoulene.repository.GenreRepository;
import com.mgoulene.service.GenreService;
import com.mgoulene.service.dto.GenreDTO;
import com.mgoulene.service.mapper.GenreMapper;
import com.mgoulene.service.dto.GenreCriteria;
import com.mgoulene.service.GenreQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link GenreResource} REST controller.
 */
@SpringBootTest(classes = MmdbApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class GenreResourceIT {

    private static final String DEFAULT_TMDB_ID = "AAAAAAAAAA";
    private static final String UPDATED_TMDB_ID = "BBBBBBBBBB";

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_LAST_TMDB_UPDATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_LAST_TMDB_UPDATE = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_LAST_TMDB_UPDATE = LocalDate.ofEpochDay(-1L);

    @Autowired
    private GenreRepository genreRepository;

    @Autowired
    private GenreMapper genreMapper;

    @Autowired
    private GenreService genreService;

    @Autowired
    private GenreQueryService genreQueryService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restGenreMockMvc;

    private Genre genre;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Genre createEntity(EntityManager em) {
        Genre genre = new Genre()
            .tmdbId(DEFAULT_TMDB_ID)
            .name(DEFAULT_NAME)
            .lastTMDBUpdate(DEFAULT_LAST_TMDB_UPDATE);
        return genre;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Genre createUpdatedEntity(EntityManager em) {
        Genre genre = new Genre()
            .tmdbId(UPDATED_TMDB_ID)
            .name(UPDATED_NAME)
            .lastTMDBUpdate(UPDATED_LAST_TMDB_UPDATE);
        return genre;
    }

    @BeforeEach
    public void initTest() {
        genre = createEntity(em);
    }

    @Test
    @Transactional
    public void createGenre() throws Exception {
        int databaseSizeBeforeCreate = genreRepository.findAll().size();
        // Create the Genre
        GenreDTO genreDTO = genreMapper.toDto(genre);
        restGenreMockMvc.perform(post("/api/genres")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(genreDTO)))
            .andExpect(status().isCreated());

        // Validate the Genre in the database
        List<Genre> genreList = genreRepository.findAll();
        assertThat(genreList).hasSize(databaseSizeBeforeCreate + 1);
        Genre testGenre = genreList.get(genreList.size() - 1);
        assertThat(testGenre.getTmdbId()).isEqualTo(DEFAULT_TMDB_ID);
        assertThat(testGenre.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testGenre.getLastTMDBUpdate()).isEqualTo(DEFAULT_LAST_TMDB_UPDATE);
    }

    @Test
    @Transactional
    public void createGenreWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = genreRepository.findAll().size();

        // Create the Genre with an existing ID
        genre.setId(1L);
        GenreDTO genreDTO = genreMapper.toDto(genre);

        // An entity with an existing ID cannot be created, so this API call must fail
        restGenreMockMvc.perform(post("/api/genres")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(genreDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Genre in the database
        List<Genre> genreList = genreRepository.findAll();
        assertThat(genreList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkTmdbIdIsRequired() throws Exception {
        int databaseSizeBeforeTest = genreRepository.findAll().size();
        // set the field null
        genre.setTmdbId(null);

        // Create the Genre, which fails.
        GenreDTO genreDTO = genreMapper.toDto(genre);


        restGenreMockMvc.perform(post("/api/genres")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(genreDTO)))
            .andExpect(status().isBadRequest());

        List<Genre> genreList = genreRepository.findAll();
        assertThat(genreList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = genreRepository.findAll().size();
        // set the field null
        genre.setName(null);

        // Create the Genre, which fails.
        GenreDTO genreDTO = genreMapper.toDto(genre);


        restGenreMockMvc.perform(post("/api/genres")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(genreDTO)))
            .andExpect(status().isBadRequest());

        List<Genre> genreList = genreRepository.findAll();
        assertThat(genreList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkLastTMDBUpdateIsRequired() throws Exception {
        int databaseSizeBeforeTest = genreRepository.findAll().size();
        // set the field null
        genre.setLastTMDBUpdate(null);

        // Create the Genre, which fails.
        GenreDTO genreDTO = genreMapper.toDto(genre);


        restGenreMockMvc.perform(post("/api/genres")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(genreDTO)))
            .andExpect(status().isBadRequest());

        List<Genre> genreList = genreRepository.findAll();
        assertThat(genreList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllGenres() throws Exception {
        // Initialize the database
        genreRepository.saveAndFlush(genre);

        // Get all the genreList
        restGenreMockMvc.perform(get("/api/genres?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(genre.getId().intValue())))
            .andExpect(jsonPath("$.[*].tmdbId").value(hasItem(DEFAULT_TMDB_ID)))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].lastTMDBUpdate").value(hasItem(DEFAULT_LAST_TMDB_UPDATE.toString())));
    }
    
    @Test
    @Transactional
    public void getGenre() throws Exception {
        // Initialize the database
        genreRepository.saveAndFlush(genre);

        // Get the genre
        restGenreMockMvc.perform(get("/api/genres/{id}", genre.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(genre.getId().intValue()))
            .andExpect(jsonPath("$.tmdbId").value(DEFAULT_TMDB_ID))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME))
            .andExpect(jsonPath("$.lastTMDBUpdate").value(DEFAULT_LAST_TMDB_UPDATE.toString()));
    }


    @Test
    @Transactional
    public void getGenresByIdFiltering() throws Exception {
        // Initialize the database
        genreRepository.saveAndFlush(genre);

        Long id = genre.getId();

        defaultGenreShouldBeFound("id.equals=" + id);
        defaultGenreShouldNotBeFound("id.notEquals=" + id);

        defaultGenreShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultGenreShouldNotBeFound("id.greaterThan=" + id);

        defaultGenreShouldBeFound("id.lessThanOrEqual=" + id);
        defaultGenreShouldNotBeFound("id.lessThan=" + id);
    }


    @Test
    @Transactional
    public void getAllGenresByTmdbIdIsEqualToSomething() throws Exception {
        // Initialize the database
        genreRepository.saveAndFlush(genre);

        // Get all the genreList where tmdbId equals to DEFAULT_TMDB_ID
        defaultGenreShouldBeFound("tmdbId.equals=" + DEFAULT_TMDB_ID);

        // Get all the genreList where tmdbId equals to UPDATED_TMDB_ID
        defaultGenreShouldNotBeFound("tmdbId.equals=" + UPDATED_TMDB_ID);
    }

    @Test
    @Transactional
    public void getAllGenresByTmdbIdIsNotEqualToSomething() throws Exception {
        // Initialize the database
        genreRepository.saveAndFlush(genre);

        // Get all the genreList where tmdbId not equals to DEFAULT_TMDB_ID
        defaultGenreShouldNotBeFound("tmdbId.notEquals=" + DEFAULT_TMDB_ID);

        // Get all the genreList where tmdbId not equals to UPDATED_TMDB_ID
        defaultGenreShouldBeFound("tmdbId.notEquals=" + UPDATED_TMDB_ID);
    }

    @Test
    @Transactional
    public void getAllGenresByTmdbIdIsInShouldWork() throws Exception {
        // Initialize the database
        genreRepository.saveAndFlush(genre);

        // Get all the genreList where tmdbId in DEFAULT_TMDB_ID or UPDATED_TMDB_ID
        defaultGenreShouldBeFound("tmdbId.in=" + DEFAULT_TMDB_ID + "," + UPDATED_TMDB_ID);

        // Get all the genreList where tmdbId equals to UPDATED_TMDB_ID
        defaultGenreShouldNotBeFound("tmdbId.in=" + UPDATED_TMDB_ID);
    }

    @Test
    @Transactional
    public void getAllGenresByTmdbIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        genreRepository.saveAndFlush(genre);

        // Get all the genreList where tmdbId is not null
        defaultGenreShouldBeFound("tmdbId.specified=true");

        // Get all the genreList where tmdbId is null
        defaultGenreShouldNotBeFound("tmdbId.specified=false");
    }
                @Test
    @Transactional
    public void getAllGenresByTmdbIdContainsSomething() throws Exception {
        // Initialize the database
        genreRepository.saveAndFlush(genre);

        // Get all the genreList where tmdbId contains DEFAULT_TMDB_ID
        defaultGenreShouldBeFound("tmdbId.contains=" + DEFAULT_TMDB_ID);

        // Get all the genreList where tmdbId contains UPDATED_TMDB_ID
        defaultGenreShouldNotBeFound("tmdbId.contains=" + UPDATED_TMDB_ID);
    }

    @Test
    @Transactional
    public void getAllGenresByTmdbIdNotContainsSomething() throws Exception {
        // Initialize the database
        genreRepository.saveAndFlush(genre);

        // Get all the genreList where tmdbId does not contain DEFAULT_TMDB_ID
        defaultGenreShouldNotBeFound("tmdbId.doesNotContain=" + DEFAULT_TMDB_ID);

        // Get all the genreList where tmdbId does not contain UPDATED_TMDB_ID
        defaultGenreShouldBeFound("tmdbId.doesNotContain=" + UPDATED_TMDB_ID);
    }


    @Test
    @Transactional
    public void getAllGenresByNameIsEqualToSomething() throws Exception {
        // Initialize the database
        genreRepository.saveAndFlush(genre);

        // Get all the genreList where name equals to DEFAULT_NAME
        defaultGenreShouldBeFound("name.equals=" + DEFAULT_NAME);

        // Get all the genreList where name equals to UPDATED_NAME
        defaultGenreShouldNotBeFound("name.equals=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    public void getAllGenresByNameIsNotEqualToSomething() throws Exception {
        // Initialize the database
        genreRepository.saveAndFlush(genre);

        // Get all the genreList where name not equals to DEFAULT_NAME
        defaultGenreShouldNotBeFound("name.notEquals=" + DEFAULT_NAME);

        // Get all the genreList where name not equals to UPDATED_NAME
        defaultGenreShouldBeFound("name.notEquals=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    public void getAllGenresByNameIsInShouldWork() throws Exception {
        // Initialize the database
        genreRepository.saveAndFlush(genre);

        // Get all the genreList where name in DEFAULT_NAME or UPDATED_NAME
        defaultGenreShouldBeFound("name.in=" + DEFAULT_NAME + "," + UPDATED_NAME);

        // Get all the genreList where name equals to UPDATED_NAME
        defaultGenreShouldNotBeFound("name.in=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    public void getAllGenresByNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        genreRepository.saveAndFlush(genre);

        // Get all the genreList where name is not null
        defaultGenreShouldBeFound("name.specified=true");

        // Get all the genreList where name is null
        defaultGenreShouldNotBeFound("name.specified=false");
    }
                @Test
    @Transactional
    public void getAllGenresByNameContainsSomething() throws Exception {
        // Initialize the database
        genreRepository.saveAndFlush(genre);

        // Get all the genreList where name contains DEFAULT_NAME
        defaultGenreShouldBeFound("name.contains=" + DEFAULT_NAME);

        // Get all the genreList where name contains UPDATED_NAME
        defaultGenreShouldNotBeFound("name.contains=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    public void getAllGenresByNameNotContainsSomething() throws Exception {
        // Initialize the database
        genreRepository.saveAndFlush(genre);

        // Get all the genreList where name does not contain DEFAULT_NAME
        defaultGenreShouldNotBeFound("name.doesNotContain=" + DEFAULT_NAME);

        // Get all the genreList where name does not contain UPDATED_NAME
        defaultGenreShouldBeFound("name.doesNotContain=" + UPDATED_NAME);
    }


    @Test
    @Transactional
    public void getAllGenresByLastTMDBUpdateIsEqualToSomething() throws Exception {
        // Initialize the database
        genreRepository.saveAndFlush(genre);

        // Get all the genreList where lastTMDBUpdate equals to DEFAULT_LAST_TMDB_UPDATE
        defaultGenreShouldBeFound("lastTMDBUpdate.equals=" + DEFAULT_LAST_TMDB_UPDATE);

        // Get all the genreList where lastTMDBUpdate equals to UPDATED_LAST_TMDB_UPDATE
        defaultGenreShouldNotBeFound("lastTMDBUpdate.equals=" + UPDATED_LAST_TMDB_UPDATE);
    }

    @Test
    @Transactional
    public void getAllGenresByLastTMDBUpdateIsNotEqualToSomething() throws Exception {
        // Initialize the database
        genreRepository.saveAndFlush(genre);

        // Get all the genreList where lastTMDBUpdate not equals to DEFAULT_LAST_TMDB_UPDATE
        defaultGenreShouldNotBeFound("lastTMDBUpdate.notEquals=" + DEFAULT_LAST_TMDB_UPDATE);

        // Get all the genreList where lastTMDBUpdate not equals to UPDATED_LAST_TMDB_UPDATE
        defaultGenreShouldBeFound("lastTMDBUpdate.notEquals=" + UPDATED_LAST_TMDB_UPDATE);
    }

    @Test
    @Transactional
    public void getAllGenresByLastTMDBUpdateIsInShouldWork() throws Exception {
        // Initialize the database
        genreRepository.saveAndFlush(genre);

        // Get all the genreList where lastTMDBUpdate in DEFAULT_LAST_TMDB_UPDATE or UPDATED_LAST_TMDB_UPDATE
        defaultGenreShouldBeFound("lastTMDBUpdate.in=" + DEFAULT_LAST_TMDB_UPDATE + "," + UPDATED_LAST_TMDB_UPDATE);

        // Get all the genreList where lastTMDBUpdate equals to UPDATED_LAST_TMDB_UPDATE
        defaultGenreShouldNotBeFound("lastTMDBUpdate.in=" + UPDATED_LAST_TMDB_UPDATE);
    }

    @Test
    @Transactional
    public void getAllGenresByLastTMDBUpdateIsNullOrNotNull() throws Exception {
        // Initialize the database
        genreRepository.saveAndFlush(genre);

        // Get all the genreList where lastTMDBUpdate is not null
        defaultGenreShouldBeFound("lastTMDBUpdate.specified=true");

        // Get all the genreList where lastTMDBUpdate is null
        defaultGenreShouldNotBeFound("lastTMDBUpdate.specified=false");
    }

    @Test
    @Transactional
    public void getAllGenresByLastTMDBUpdateIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        genreRepository.saveAndFlush(genre);

        // Get all the genreList where lastTMDBUpdate is greater than or equal to DEFAULT_LAST_TMDB_UPDATE
        defaultGenreShouldBeFound("lastTMDBUpdate.greaterThanOrEqual=" + DEFAULT_LAST_TMDB_UPDATE);

        // Get all the genreList where lastTMDBUpdate is greater than or equal to UPDATED_LAST_TMDB_UPDATE
        defaultGenreShouldNotBeFound("lastTMDBUpdate.greaterThanOrEqual=" + UPDATED_LAST_TMDB_UPDATE);
    }

    @Test
    @Transactional
    public void getAllGenresByLastTMDBUpdateIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        genreRepository.saveAndFlush(genre);

        // Get all the genreList where lastTMDBUpdate is less than or equal to DEFAULT_LAST_TMDB_UPDATE
        defaultGenreShouldBeFound("lastTMDBUpdate.lessThanOrEqual=" + DEFAULT_LAST_TMDB_UPDATE);

        // Get all the genreList where lastTMDBUpdate is less than or equal to SMALLER_LAST_TMDB_UPDATE
        defaultGenreShouldNotBeFound("lastTMDBUpdate.lessThanOrEqual=" + SMALLER_LAST_TMDB_UPDATE);
    }

    @Test
    @Transactional
    public void getAllGenresByLastTMDBUpdateIsLessThanSomething() throws Exception {
        // Initialize the database
        genreRepository.saveAndFlush(genre);

        // Get all the genreList where lastTMDBUpdate is less than DEFAULT_LAST_TMDB_UPDATE
        defaultGenreShouldNotBeFound("lastTMDBUpdate.lessThan=" + DEFAULT_LAST_TMDB_UPDATE);

        // Get all the genreList where lastTMDBUpdate is less than UPDATED_LAST_TMDB_UPDATE
        defaultGenreShouldBeFound("lastTMDBUpdate.lessThan=" + UPDATED_LAST_TMDB_UPDATE);
    }

    @Test
    @Transactional
    public void getAllGenresByLastTMDBUpdateIsGreaterThanSomething() throws Exception {
        // Initialize the database
        genreRepository.saveAndFlush(genre);

        // Get all the genreList where lastTMDBUpdate is greater than DEFAULT_LAST_TMDB_UPDATE
        defaultGenreShouldNotBeFound("lastTMDBUpdate.greaterThan=" + DEFAULT_LAST_TMDB_UPDATE);

        // Get all the genreList where lastTMDBUpdate is greater than SMALLER_LAST_TMDB_UPDATE
        defaultGenreShouldBeFound("lastTMDBUpdate.greaterThan=" + SMALLER_LAST_TMDB_UPDATE);
    }


    @Test
    @Transactional
    public void getAllGenresByMovieIsEqualToSomething() throws Exception {
        // Initialize the database
        genreRepository.saveAndFlush(genre);
        Movie movie = MovieResourceIT.createEntity(em);
        em.persist(movie);
        em.flush();
        genre.addMovie(movie);
        genreRepository.saveAndFlush(genre);
        Long movieId = movie.getId();

        // Get all the genreList where movie equals to movieId
        defaultGenreShouldBeFound("movieId.equals=" + movieId);

        // Get all the genreList where movie equals to movieId + 1
        defaultGenreShouldNotBeFound("movieId.equals=" + (movieId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultGenreShouldBeFound(String filter) throws Exception {
        restGenreMockMvc.perform(get("/api/genres?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(genre.getId().intValue())))
            .andExpect(jsonPath("$.[*].tmdbId").value(hasItem(DEFAULT_TMDB_ID)))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].lastTMDBUpdate").value(hasItem(DEFAULT_LAST_TMDB_UPDATE.toString())));

        // Check, that the count call also returns 1
        restGenreMockMvc.perform(get("/api/genres/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultGenreShouldNotBeFound(String filter) throws Exception {
        restGenreMockMvc.perform(get("/api/genres?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restGenreMockMvc.perform(get("/api/genres/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    public void getNonExistingGenre() throws Exception {
        // Get the genre
        restGenreMockMvc.perform(get("/api/genres/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateGenre() throws Exception {
        // Initialize the database
        genreRepository.saveAndFlush(genre);

        int databaseSizeBeforeUpdate = genreRepository.findAll().size();

        // Update the genre
        Genre updatedGenre = genreRepository.findById(genre.getId()).get();
        // Disconnect from session so that the updates on updatedGenre are not directly saved in db
        em.detach(updatedGenre);
        updatedGenre
            .tmdbId(UPDATED_TMDB_ID)
            .name(UPDATED_NAME)
            .lastTMDBUpdate(UPDATED_LAST_TMDB_UPDATE);
        GenreDTO genreDTO = genreMapper.toDto(updatedGenre);

        restGenreMockMvc.perform(put("/api/genres")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(genreDTO)))
            .andExpect(status().isOk());

        // Validate the Genre in the database
        List<Genre> genreList = genreRepository.findAll();
        assertThat(genreList).hasSize(databaseSizeBeforeUpdate);
        Genre testGenre = genreList.get(genreList.size() - 1);
        assertThat(testGenre.getTmdbId()).isEqualTo(UPDATED_TMDB_ID);
        assertThat(testGenre.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testGenre.getLastTMDBUpdate()).isEqualTo(UPDATED_LAST_TMDB_UPDATE);
    }

    @Test
    @Transactional
    public void updateNonExistingGenre() throws Exception {
        int databaseSizeBeforeUpdate = genreRepository.findAll().size();

        // Create the Genre
        GenreDTO genreDTO = genreMapper.toDto(genre);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restGenreMockMvc.perform(put("/api/genres")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(genreDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Genre in the database
        List<Genre> genreList = genreRepository.findAll();
        assertThat(genreList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteGenre() throws Exception {
        // Initialize the database
        genreRepository.saveAndFlush(genre);

        int databaseSizeBeforeDelete = genreRepository.findAll().size();

        // Delete the genre
        restGenreMockMvc.perform(delete("/api/genres/{id}", genre.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Genre> genreList = genreRepository.findAll();
        assertThat(genreList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
