package com.mgoulene.web.rest;

import com.mgoulene.MmdbApp;
import com.mgoulene.domain.Image;
import com.mgoulene.domain.Movie;
import com.mgoulene.domain.Person;
import com.mgoulene.repository.ImageRepository;
import com.mgoulene.service.ImageService;
import com.mgoulene.service.dto.ImageDTO;
import com.mgoulene.service.mapper.ImageMapper;
import com.mgoulene.service.dto.ImageCriteria;
import com.mgoulene.service.ImageQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link ImageResource} REST controller.
 */
@SpringBootTest(classes = MmdbApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class ImageResourceIT {

    private static final String DEFAULT_TMDB_ID = "AAAAAAAAAA";
    private static final String UPDATED_TMDB_ID = "BBBBBBBBBB";

    private static final String DEFAULT_LOCALE = "AAAAAAAAAA";
    private static final String UPDATED_LOCALE = "BBBBBBBBBB";

    private static final Float DEFAULT_VOTE_AVERAGE = 1F;
    private static final Float UPDATED_VOTE_AVERAGE = 2F;
    private static final Float SMALLER_VOTE_AVERAGE = 1F - 1F;

    private static final Integer DEFAULT_VOTE_COUNT = 1;
    private static final Integer UPDATED_VOTE_COUNT = 2;
    private static final Integer SMALLER_VOTE_COUNT = 1 - 1;

    private static final LocalDate DEFAULT_LAST_TMDB_UPDATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_LAST_TMDB_UPDATE = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_LAST_TMDB_UPDATE = LocalDate.ofEpochDay(-1L);

    @Autowired
    private ImageRepository imageRepository;

    @Autowired
    private ImageMapper imageMapper;

    @Autowired
    private ImageService imageService;

    @Autowired
    private ImageQueryService imageQueryService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restImageMockMvc;

    private Image image;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Image createEntity(EntityManager em) {
        Image image = new Image()
            .tmdbId(DEFAULT_TMDB_ID)
            .locale(DEFAULT_LOCALE)
            .voteAverage(DEFAULT_VOTE_AVERAGE)
            .voteCount(DEFAULT_VOTE_COUNT)
            .lastTMDBUpdate(DEFAULT_LAST_TMDB_UPDATE);
        return image;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Image createUpdatedEntity(EntityManager em) {
        Image image = new Image()
            .tmdbId(UPDATED_TMDB_ID)
            .locale(UPDATED_LOCALE)
            .voteAverage(UPDATED_VOTE_AVERAGE)
            .voteCount(UPDATED_VOTE_COUNT)
            .lastTMDBUpdate(UPDATED_LAST_TMDB_UPDATE);
        return image;
    }

    @BeforeEach
    public void initTest() {
        image = createEntity(em);
    }

    @Test
    @Transactional
    public void createImage() throws Exception {
        int databaseSizeBeforeCreate = imageRepository.findAll().size();
        // Create the Image
        ImageDTO imageDTO = imageMapper.toDto(image);
        restImageMockMvc.perform(post("/api/images")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(imageDTO)))
            .andExpect(status().isCreated());

        // Validate the Image in the database
        List<Image> imageList = imageRepository.findAll();
        assertThat(imageList).hasSize(databaseSizeBeforeCreate + 1);
        Image testImage = imageList.get(imageList.size() - 1);
        assertThat(testImage.getTmdbId()).isEqualTo(DEFAULT_TMDB_ID);
        assertThat(testImage.getLocale()).isEqualTo(DEFAULT_LOCALE);
        assertThat(testImage.getVoteAverage()).isEqualTo(DEFAULT_VOTE_AVERAGE);
        assertThat(testImage.getVoteCount()).isEqualTo(DEFAULT_VOTE_COUNT);
        assertThat(testImage.getLastTMDBUpdate()).isEqualTo(DEFAULT_LAST_TMDB_UPDATE);
    }

    @Test
    @Transactional
    public void createImageWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = imageRepository.findAll().size();

        // Create the Image with an existing ID
        image.setId(1L);
        ImageDTO imageDTO = imageMapper.toDto(image);

        // An entity with an existing ID cannot be created, so this API call must fail
        restImageMockMvc.perform(post("/api/images")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(imageDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Image in the database
        List<Image> imageList = imageRepository.findAll();
        assertThat(imageList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkTmdbIdIsRequired() throws Exception {
        int databaseSizeBeforeTest = imageRepository.findAll().size();
        // set the field null
        image.setTmdbId(null);

        // Create the Image, which fails.
        ImageDTO imageDTO = imageMapper.toDto(image);


        restImageMockMvc.perform(post("/api/images")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(imageDTO)))
            .andExpect(status().isBadRequest());

        List<Image> imageList = imageRepository.findAll();
        assertThat(imageList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkLastTMDBUpdateIsRequired() throws Exception {
        int databaseSizeBeforeTest = imageRepository.findAll().size();
        // set the field null
        image.setLastTMDBUpdate(null);

        // Create the Image, which fails.
        ImageDTO imageDTO = imageMapper.toDto(image);


        restImageMockMvc.perform(post("/api/images")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(imageDTO)))
            .andExpect(status().isBadRequest());

        List<Image> imageList = imageRepository.findAll();
        assertThat(imageList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllImages() throws Exception {
        // Initialize the database
        imageRepository.saveAndFlush(image);

        // Get all the imageList
        restImageMockMvc.perform(get("/api/images?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(image.getId().intValue())))
            .andExpect(jsonPath("$.[*].tmdbId").value(hasItem(DEFAULT_TMDB_ID)))
            .andExpect(jsonPath("$.[*].locale").value(hasItem(DEFAULT_LOCALE)))
            .andExpect(jsonPath("$.[*].voteAverage").value(hasItem(DEFAULT_VOTE_AVERAGE.doubleValue())))
            .andExpect(jsonPath("$.[*].voteCount").value(hasItem(DEFAULT_VOTE_COUNT)))
            .andExpect(jsonPath("$.[*].lastTMDBUpdate").value(hasItem(DEFAULT_LAST_TMDB_UPDATE.toString())));
    }
    
    @Test
    @Transactional
    public void getImage() throws Exception {
        // Initialize the database
        imageRepository.saveAndFlush(image);

        // Get the image
        restImageMockMvc.perform(get("/api/images/{id}", image.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(image.getId().intValue()))
            .andExpect(jsonPath("$.tmdbId").value(DEFAULT_TMDB_ID))
            .andExpect(jsonPath("$.locale").value(DEFAULT_LOCALE))
            .andExpect(jsonPath("$.voteAverage").value(DEFAULT_VOTE_AVERAGE.doubleValue()))
            .andExpect(jsonPath("$.voteCount").value(DEFAULT_VOTE_COUNT))
            .andExpect(jsonPath("$.lastTMDBUpdate").value(DEFAULT_LAST_TMDB_UPDATE.toString()));
    }


    @Test
    @Transactional
    public void getImagesByIdFiltering() throws Exception {
        // Initialize the database
        imageRepository.saveAndFlush(image);

        Long id = image.getId();

        defaultImageShouldBeFound("id.equals=" + id);
        defaultImageShouldNotBeFound("id.notEquals=" + id);

        defaultImageShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultImageShouldNotBeFound("id.greaterThan=" + id);

        defaultImageShouldBeFound("id.lessThanOrEqual=" + id);
        defaultImageShouldNotBeFound("id.lessThan=" + id);
    }


    @Test
    @Transactional
    public void getAllImagesByTmdbIdIsEqualToSomething() throws Exception {
        // Initialize the database
        imageRepository.saveAndFlush(image);

        // Get all the imageList where tmdbId equals to DEFAULT_TMDB_ID
        defaultImageShouldBeFound("tmdbId.equals=" + DEFAULT_TMDB_ID);

        // Get all the imageList where tmdbId equals to UPDATED_TMDB_ID
        defaultImageShouldNotBeFound("tmdbId.equals=" + UPDATED_TMDB_ID);
    }

    @Test
    @Transactional
    public void getAllImagesByTmdbIdIsNotEqualToSomething() throws Exception {
        // Initialize the database
        imageRepository.saveAndFlush(image);

        // Get all the imageList where tmdbId not equals to DEFAULT_TMDB_ID
        defaultImageShouldNotBeFound("tmdbId.notEquals=" + DEFAULT_TMDB_ID);

        // Get all the imageList where tmdbId not equals to UPDATED_TMDB_ID
        defaultImageShouldBeFound("tmdbId.notEquals=" + UPDATED_TMDB_ID);
    }

    @Test
    @Transactional
    public void getAllImagesByTmdbIdIsInShouldWork() throws Exception {
        // Initialize the database
        imageRepository.saveAndFlush(image);

        // Get all the imageList where tmdbId in DEFAULT_TMDB_ID or UPDATED_TMDB_ID
        defaultImageShouldBeFound("tmdbId.in=" + DEFAULT_TMDB_ID + "," + UPDATED_TMDB_ID);

        // Get all the imageList where tmdbId equals to UPDATED_TMDB_ID
        defaultImageShouldNotBeFound("tmdbId.in=" + UPDATED_TMDB_ID);
    }

    @Test
    @Transactional
    public void getAllImagesByTmdbIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        imageRepository.saveAndFlush(image);

        // Get all the imageList where tmdbId is not null
        defaultImageShouldBeFound("tmdbId.specified=true");

        // Get all the imageList where tmdbId is null
        defaultImageShouldNotBeFound("tmdbId.specified=false");
    }
                @Test
    @Transactional
    public void getAllImagesByTmdbIdContainsSomething() throws Exception {
        // Initialize the database
        imageRepository.saveAndFlush(image);

        // Get all the imageList where tmdbId contains DEFAULT_TMDB_ID
        defaultImageShouldBeFound("tmdbId.contains=" + DEFAULT_TMDB_ID);

        // Get all the imageList where tmdbId contains UPDATED_TMDB_ID
        defaultImageShouldNotBeFound("tmdbId.contains=" + UPDATED_TMDB_ID);
    }

    @Test
    @Transactional
    public void getAllImagesByTmdbIdNotContainsSomething() throws Exception {
        // Initialize the database
        imageRepository.saveAndFlush(image);

        // Get all the imageList where tmdbId does not contain DEFAULT_TMDB_ID
        defaultImageShouldNotBeFound("tmdbId.doesNotContain=" + DEFAULT_TMDB_ID);

        // Get all the imageList where tmdbId does not contain UPDATED_TMDB_ID
        defaultImageShouldBeFound("tmdbId.doesNotContain=" + UPDATED_TMDB_ID);
    }


    @Test
    @Transactional
    public void getAllImagesByLocaleIsEqualToSomething() throws Exception {
        // Initialize the database
        imageRepository.saveAndFlush(image);

        // Get all the imageList where locale equals to DEFAULT_LOCALE
        defaultImageShouldBeFound("locale.equals=" + DEFAULT_LOCALE);

        // Get all the imageList where locale equals to UPDATED_LOCALE
        defaultImageShouldNotBeFound("locale.equals=" + UPDATED_LOCALE);
    }

    @Test
    @Transactional
    public void getAllImagesByLocaleIsNotEqualToSomething() throws Exception {
        // Initialize the database
        imageRepository.saveAndFlush(image);

        // Get all the imageList where locale not equals to DEFAULT_LOCALE
        defaultImageShouldNotBeFound("locale.notEquals=" + DEFAULT_LOCALE);

        // Get all the imageList where locale not equals to UPDATED_LOCALE
        defaultImageShouldBeFound("locale.notEquals=" + UPDATED_LOCALE);
    }

    @Test
    @Transactional
    public void getAllImagesByLocaleIsInShouldWork() throws Exception {
        // Initialize the database
        imageRepository.saveAndFlush(image);

        // Get all the imageList where locale in DEFAULT_LOCALE or UPDATED_LOCALE
        defaultImageShouldBeFound("locale.in=" + DEFAULT_LOCALE + "," + UPDATED_LOCALE);

        // Get all the imageList where locale equals to UPDATED_LOCALE
        defaultImageShouldNotBeFound("locale.in=" + UPDATED_LOCALE);
    }

    @Test
    @Transactional
    public void getAllImagesByLocaleIsNullOrNotNull() throws Exception {
        // Initialize the database
        imageRepository.saveAndFlush(image);

        // Get all the imageList where locale is not null
        defaultImageShouldBeFound("locale.specified=true");

        // Get all the imageList where locale is null
        defaultImageShouldNotBeFound("locale.specified=false");
    }
                @Test
    @Transactional
    public void getAllImagesByLocaleContainsSomething() throws Exception {
        // Initialize the database
        imageRepository.saveAndFlush(image);

        // Get all the imageList where locale contains DEFAULT_LOCALE
        defaultImageShouldBeFound("locale.contains=" + DEFAULT_LOCALE);

        // Get all the imageList where locale contains UPDATED_LOCALE
        defaultImageShouldNotBeFound("locale.contains=" + UPDATED_LOCALE);
    }

    @Test
    @Transactional
    public void getAllImagesByLocaleNotContainsSomething() throws Exception {
        // Initialize the database
        imageRepository.saveAndFlush(image);

        // Get all the imageList where locale does not contain DEFAULT_LOCALE
        defaultImageShouldNotBeFound("locale.doesNotContain=" + DEFAULT_LOCALE);

        // Get all the imageList where locale does not contain UPDATED_LOCALE
        defaultImageShouldBeFound("locale.doesNotContain=" + UPDATED_LOCALE);
    }


    @Test
    @Transactional
    public void getAllImagesByVoteAverageIsEqualToSomething() throws Exception {
        // Initialize the database
        imageRepository.saveAndFlush(image);

        // Get all the imageList where voteAverage equals to DEFAULT_VOTE_AVERAGE
        defaultImageShouldBeFound("voteAverage.equals=" + DEFAULT_VOTE_AVERAGE);

        // Get all the imageList where voteAverage equals to UPDATED_VOTE_AVERAGE
        defaultImageShouldNotBeFound("voteAverage.equals=" + UPDATED_VOTE_AVERAGE);
    }

    @Test
    @Transactional
    public void getAllImagesByVoteAverageIsNotEqualToSomething() throws Exception {
        // Initialize the database
        imageRepository.saveAndFlush(image);

        // Get all the imageList where voteAverage not equals to DEFAULT_VOTE_AVERAGE
        defaultImageShouldNotBeFound("voteAverage.notEquals=" + DEFAULT_VOTE_AVERAGE);

        // Get all the imageList where voteAverage not equals to UPDATED_VOTE_AVERAGE
        defaultImageShouldBeFound("voteAverage.notEquals=" + UPDATED_VOTE_AVERAGE);
    }

    @Test
    @Transactional
    public void getAllImagesByVoteAverageIsInShouldWork() throws Exception {
        // Initialize the database
        imageRepository.saveAndFlush(image);

        // Get all the imageList where voteAverage in DEFAULT_VOTE_AVERAGE or UPDATED_VOTE_AVERAGE
        defaultImageShouldBeFound("voteAverage.in=" + DEFAULT_VOTE_AVERAGE + "," + UPDATED_VOTE_AVERAGE);

        // Get all the imageList where voteAverage equals to UPDATED_VOTE_AVERAGE
        defaultImageShouldNotBeFound("voteAverage.in=" + UPDATED_VOTE_AVERAGE);
    }

    @Test
    @Transactional
    public void getAllImagesByVoteAverageIsNullOrNotNull() throws Exception {
        // Initialize the database
        imageRepository.saveAndFlush(image);

        // Get all the imageList where voteAverage is not null
        defaultImageShouldBeFound("voteAverage.specified=true");

        // Get all the imageList where voteAverage is null
        defaultImageShouldNotBeFound("voteAverage.specified=false");
    }

    @Test
    @Transactional
    public void getAllImagesByVoteAverageIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        imageRepository.saveAndFlush(image);

        // Get all the imageList where voteAverage is greater than or equal to DEFAULT_VOTE_AVERAGE
        defaultImageShouldBeFound("voteAverage.greaterThanOrEqual=" + DEFAULT_VOTE_AVERAGE);

        // Get all the imageList where voteAverage is greater than or equal to UPDATED_VOTE_AVERAGE
        defaultImageShouldNotBeFound("voteAverage.greaterThanOrEqual=" + UPDATED_VOTE_AVERAGE);
    }

    @Test
    @Transactional
    public void getAllImagesByVoteAverageIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        imageRepository.saveAndFlush(image);

        // Get all the imageList where voteAverage is less than or equal to DEFAULT_VOTE_AVERAGE
        defaultImageShouldBeFound("voteAverage.lessThanOrEqual=" + DEFAULT_VOTE_AVERAGE);

        // Get all the imageList where voteAverage is less than or equal to SMALLER_VOTE_AVERAGE
        defaultImageShouldNotBeFound("voteAverage.lessThanOrEqual=" + SMALLER_VOTE_AVERAGE);
    }

    @Test
    @Transactional
    public void getAllImagesByVoteAverageIsLessThanSomething() throws Exception {
        // Initialize the database
        imageRepository.saveAndFlush(image);

        // Get all the imageList where voteAverage is less than DEFAULT_VOTE_AVERAGE
        defaultImageShouldNotBeFound("voteAverage.lessThan=" + DEFAULT_VOTE_AVERAGE);

        // Get all the imageList where voteAverage is less than UPDATED_VOTE_AVERAGE
        defaultImageShouldBeFound("voteAverage.lessThan=" + UPDATED_VOTE_AVERAGE);
    }

    @Test
    @Transactional
    public void getAllImagesByVoteAverageIsGreaterThanSomething() throws Exception {
        // Initialize the database
        imageRepository.saveAndFlush(image);

        // Get all the imageList where voteAverage is greater than DEFAULT_VOTE_AVERAGE
        defaultImageShouldNotBeFound("voteAverage.greaterThan=" + DEFAULT_VOTE_AVERAGE);

        // Get all the imageList where voteAverage is greater than SMALLER_VOTE_AVERAGE
        defaultImageShouldBeFound("voteAverage.greaterThan=" + SMALLER_VOTE_AVERAGE);
    }


    @Test
    @Transactional
    public void getAllImagesByVoteCountIsEqualToSomething() throws Exception {
        // Initialize the database
        imageRepository.saveAndFlush(image);

        // Get all the imageList where voteCount equals to DEFAULT_VOTE_COUNT
        defaultImageShouldBeFound("voteCount.equals=" + DEFAULT_VOTE_COUNT);

        // Get all the imageList where voteCount equals to UPDATED_VOTE_COUNT
        defaultImageShouldNotBeFound("voteCount.equals=" + UPDATED_VOTE_COUNT);
    }

    @Test
    @Transactional
    public void getAllImagesByVoteCountIsNotEqualToSomething() throws Exception {
        // Initialize the database
        imageRepository.saveAndFlush(image);

        // Get all the imageList where voteCount not equals to DEFAULT_VOTE_COUNT
        defaultImageShouldNotBeFound("voteCount.notEquals=" + DEFAULT_VOTE_COUNT);

        // Get all the imageList where voteCount not equals to UPDATED_VOTE_COUNT
        defaultImageShouldBeFound("voteCount.notEquals=" + UPDATED_VOTE_COUNT);
    }

    @Test
    @Transactional
    public void getAllImagesByVoteCountIsInShouldWork() throws Exception {
        // Initialize the database
        imageRepository.saveAndFlush(image);

        // Get all the imageList where voteCount in DEFAULT_VOTE_COUNT or UPDATED_VOTE_COUNT
        defaultImageShouldBeFound("voteCount.in=" + DEFAULT_VOTE_COUNT + "," + UPDATED_VOTE_COUNT);

        // Get all the imageList where voteCount equals to UPDATED_VOTE_COUNT
        defaultImageShouldNotBeFound("voteCount.in=" + UPDATED_VOTE_COUNT);
    }

    @Test
    @Transactional
    public void getAllImagesByVoteCountIsNullOrNotNull() throws Exception {
        // Initialize the database
        imageRepository.saveAndFlush(image);

        // Get all the imageList where voteCount is not null
        defaultImageShouldBeFound("voteCount.specified=true");

        // Get all the imageList where voteCount is null
        defaultImageShouldNotBeFound("voteCount.specified=false");
    }

    @Test
    @Transactional
    public void getAllImagesByVoteCountIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        imageRepository.saveAndFlush(image);

        // Get all the imageList where voteCount is greater than or equal to DEFAULT_VOTE_COUNT
        defaultImageShouldBeFound("voteCount.greaterThanOrEqual=" + DEFAULT_VOTE_COUNT);

        // Get all the imageList where voteCount is greater than or equal to UPDATED_VOTE_COUNT
        defaultImageShouldNotBeFound("voteCount.greaterThanOrEqual=" + UPDATED_VOTE_COUNT);
    }

    @Test
    @Transactional
    public void getAllImagesByVoteCountIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        imageRepository.saveAndFlush(image);

        // Get all the imageList where voteCount is less than or equal to DEFAULT_VOTE_COUNT
        defaultImageShouldBeFound("voteCount.lessThanOrEqual=" + DEFAULT_VOTE_COUNT);

        // Get all the imageList where voteCount is less than or equal to SMALLER_VOTE_COUNT
        defaultImageShouldNotBeFound("voteCount.lessThanOrEqual=" + SMALLER_VOTE_COUNT);
    }

    @Test
    @Transactional
    public void getAllImagesByVoteCountIsLessThanSomething() throws Exception {
        // Initialize the database
        imageRepository.saveAndFlush(image);

        // Get all the imageList where voteCount is less than DEFAULT_VOTE_COUNT
        defaultImageShouldNotBeFound("voteCount.lessThan=" + DEFAULT_VOTE_COUNT);

        // Get all the imageList where voteCount is less than UPDATED_VOTE_COUNT
        defaultImageShouldBeFound("voteCount.lessThan=" + UPDATED_VOTE_COUNT);
    }

    @Test
    @Transactional
    public void getAllImagesByVoteCountIsGreaterThanSomething() throws Exception {
        // Initialize the database
        imageRepository.saveAndFlush(image);

        // Get all the imageList where voteCount is greater than DEFAULT_VOTE_COUNT
        defaultImageShouldNotBeFound("voteCount.greaterThan=" + DEFAULT_VOTE_COUNT);

        // Get all the imageList where voteCount is greater than SMALLER_VOTE_COUNT
        defaultImageShouldBeFound("voteCount.greaterThan=" + SMALLER_VOTE_COUNT);
    }


    @Test
    @Transactional
    public void getAllImagesByLastTMDBUpdateIsEqualToSomething() throws Exception {
        // Initialize the database
        imageRepository.saveAndFlush(image);

        // Get all the imageList where lastTMDBUpdate equals to DEFAULT_LAST_TMDB_UPDATE
        defaultImageShouldBeFound("lastTMDBUpdate.equals=" + DEFAULT_LAST_TMDB_UPDATE);

        // Get all the imageList where lastTMDBUpdate equals to UPDATED_LAST_TMDB_UPDATE
        defaultImageShouldNotBeFound("lastTMDBUpdate.equals=" + UPDATED_LAST_TMDB_UPDATE);
    }

    @Test
    @Transactional
    public void getAllImagesByLastTMDBUpdateIsNotEqualToSomething() throws Exception {
        // Initialize the database
        imageRepository.saveAndFlush(image);

        // Get all the imageList where lastTMDBUpdate not equals to DEFAULT_LAST_TMDB_UPDATE
        defaultImageShouldNotBeFound("lastTMDBUpdate.notEquals=" + DEFAULT_LAST_TMDB_UPDATE);

        // Get all the imageList where lastTMDBUpdate not equals to UPDATED_LAST_TMDB_UPDATE
        defaultImageShouldBeFound("lastTMDBUpdate.notEquals=" + UPDATED_LAST_TMDB_UPDATE);
    }

    @Test
    @Transactional
    public void getAllImagesByLastTMDBUpdateIsInShouldWork() throws Exception {
        // Initialize the database
        imageRepository.saveAndFlush(image);

        // Get all the imageList where lastTMDBUpdate in DEFAULT_LAST_TMDB_UPDATE or UPDATED_LAST_TMDB_UPDATE
        defaultImageShouldBeFound("lastTMDBUpdate.in=" + DEFAULT_LAST_TMDB_UPDATE + "," + UPDATED_LAST_TMDB_UPDATE);

        // Get all the imageList where lastTMDBUpdate equals to UPDATED_LAST_TMDB_UPDATE
        defaultImageShouldNotBeFound("lastTMDBUpdate.in=" + UPDATED_LAST_TMDB_UPDATE);
    }

    @Test
    @Transactional
    public void getAllImagesByLastTMDBUpdateIsNullOrNotNull() throws Exception {
        // Initialize the database
        imageRepository.saveAndFlush(image);

        // Get all the imageList where lastTMDBUpdate is not null
        defaultImageShouldBeFound("lastTMDBUpdate.specified=true");

        // Get all the imageList where lastTMDBUpdate is null
        defaultImageShouldNotBeFound("lastTMDBUpdate.specified=false");
    }

    @Test
    @Transactional
    public void getAllImagesByLastTMDBUpdateIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        imageRepository.saveAndFlush(image);

        // Get all the imageList where lastTMDBUpdate is greater than or equal to DEFAULT_LAST_TMDB_UPDATE
        defaultImageShouldBeFound("lastTMDBUpdate.greaterThanOrEqual=" + DEFAULT_LAST_TMDB_UPDATE);

        // Get all the imageList where lastTMDBUpdate is greater than or equal to UPDATED_LAST_TMDB_UPDATE
        defaultImageShouldNotBeFound("lastTMDBUpdate.greaterThanOrEqual=" + UPDATED_LAST_TMDB_UPDATE);
    }

    @Test
    @Transactional
    public void getAllImagesByLastTMDBUpdateIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        imageRepository.saveAndFlush(image);

        // Get all the imageList where lastTMDBUpdate is less than or equal to DEFAULT_LAST_TMDB_UPDATE
        defaultImageShouldBeFound("lastTMDBUpdate.lessThanOrEqual=" + DEFAULT_LAST_TMDB_UPDATE);

        // Get all the imageList where lastTMDBUpdate is less than or equal to SMALLER_LAST_TMDB_UPDATE
        defaultImageShouldNotBeFound("lastTMDBUpdate.lessThanOrEqual=" + SMALLER_LAST_TMDB_UPDATE);
    }

    @Test
    @Transactional
    public void getAllImagesByLastTMDBUpdateIsLessThanSomething() throws Exception {
        // Initialize the database
        imageRepository.saveAndFlush(image);

        // Get all the imageList where lastTMDBUpdate is less than DEFAULT_LAST_TMDB_UPDATE
        defaultImageShouldNotBeFound("lastTMDBUpdate.lessThan=" + DEFAULT_LAST_TMDB_UPDATE);

        // Get all the imageList where lastTMDBUpdate is less than UPDATED_LAST_TMDB_UPDATE
        defaultImageShouldBeFound("lastTMDBUpdate.lessThan=" + UPDATED_LAST_TMDB_UPDATE);
    }

    @Test
    @Transactional
    public void getAllImagesByLastTMDBUpdateIsGreaterThanSomething() throws Exception {
        // Initialize the database
        imageRepository.saveAndFlush(image);

        // Get all the imageList where lastTMDBUpdate is greater than DEFAULT_LAST_TMDB_UPDATE
        defaultImageShouldNotBeFound("lastTMDBUpdate.greaterThan=" + DEFAULT_LAST_TMDB_UPDATE);

        // Get all the imageList where lastTMDBUpdate is greater than SMALLER_LAST_TMDB_UPDATE
        defaultImageShouldBeFound("lastTMDBUpdate.greaterThan=" + SMALLER_LAST_TMDB_UPDATE);
    }


    @Test
    @Transactional
    public void getAllImagesByPosterMovieIsEqualToSomething() throws Exception {
        // Initialize the database
        imageRepository.saveAndFlush(image);
        Movie posterMovie = MovieResourceIT.createEntity(em);
        em.persist(posterMovie);
        em.flush();
        image.setPosterMovie(posterMovie);
        imageRepository.saveAndFlush(image);
        Long posterMovieId = posterMovie.getId();

        // Get all the imageList where posterMovie equals to posterMovieId
        defaultImageShouldBeFound("posterMovieId.equals=" + posterMovieId);

        // Get all the imageList where posterMovie equals to posterMovieId + 1
        defaultImageShouldNotBeFound("posterMovieId.equals=" + (posterMovieId + 1));
    }


    @Test
    @Transactional
    public void getAllImagesByBackdropMovieIsEqualToSomething() throws Exception {
        // Initialize the database
        imageRepository.saveAndFlush(image);
        Movie backdropMovie = MovieResourceIT.createEntity(em);
        em.persist(backdropMovie);
        em.flush();
        image.setBackdropMovie(backdropMovie);
        imageRepository.saveAndFlush(image);
        Long backdropMovieId = backdropMovie.getId();

        // Get all the imageList where backdropMovie equals to backdropMovieId
        defaultImageShouldBeFound("backdropMovieId.equals=" + backdropMovieId);

        // Get all the imageList where backdropMovie equals to backdropMovieId + 1
        defaultImageShouldNotBeFound("backdropMovieId.equals=" + (backdropMovieId + 1));
    }


    @Test
    @Transactional
    public void getAllImagesByPersonIsEqualToSomething() throws Exception {
        // Initialize the database
        imageRepository.saveAndFlush(image);
        Person person = PersonResourceIT.createEntity(em);
        em.persist(person);
        em.flush();
        image.setPerson(person);
        imageRepository.saveAndFlush(image);
        Long personId = person.getId();

        // Get all the imageList where person equals to personId
        defaultImageShouldBeFound("personId.equals=" + personId);

        // Get all the imageList where person equals to personId + 1
        defaultImageShouldNotBeFound("personId.equals=" + (personId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultImageShouldBeFound(String filter) throws Exception {
        restImageMockMvc.perform(get("/api/images?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(image.getId().intValue())))
            .andExpect(jsonPath("$.[*].tmdbId").value(hasItem(DEFAULT_TMDB_ID)))
            .andExpect(jsonPath("$.[*].locale").value(hasItem(DEFAULT_LOCALE)))
            .andExpect(jsonPath("$.[*].voteAverage").value(hasItem(DEFAULT_VOTE_AVERAGE.doubleValue())))
            .andExpect(jsonPath("$.[*].voteCount").value(hasItem(DEFAULT_VOTE_COUNT)))
            .andExpect(jsonPath("$.[*].lastTMDBUpdate").value(hasItem(DEFAULT_LAST_TMDB_UPDATE.toString())));

        // Check, that the count call also returns 1
        restImageMockMvc.perform(get("/api/images/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultImageShouldNotBeFound(String filter) throws Exception {
        restImageMockMvc.perform(get("/api/images?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restImageMockMvc.perform(get("/api/images/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    public void getNonExistingImage() throws Exception {
        // Get the image
        restImageMockMvc.perform(get("/api/images/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateImage() throws Exception {
        // Initialize the database
        imageRepository.saveAndFlush(image);

        int databaseSizeBeforeUpdate = imageRepository.findAll().size();

        // Update the image
        Image updatedImage = imageRepository.findById(image.getId()).get();
        // Disconnect from session so that the updates on updatedImage are not directly saved in db
        em.detach(updatedImage);
        updatedImage
            .tmdbId(UPDATED_TMDB_ID)
            .locale(UPDATED_LOCALE)
            .voteAverage(UPDATED_VOTE_AVERAGE)
            .voteCount(UPDATED_VOTE_COUNT)
            .lastTMDBUpdate(UPDATED_LAST_TMDB_UPDATE);
        ImageDTO imageDTO = imageMapper.toDto(updatedImage);

        restImageMockMvc.perform(put("/api/images")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(imageDTO)))
            .andExpect(status().isOk());

        // Validate the Image in the database
        List<Image> imageList = imageRepository.findAll();
        assertThat(imageList).hasSize(databaseSizeBeforeUpdate);
        Image testImage = imageList.get(imageList.size() - 1);
        assertThat(testImage.getTmdbId()).isEqualTo(UPDATED_TMDB_ID);
        assertThat(testImage.getLocale()).isEqualTo(UPDATED_LOCALE);
        assertThat(testImage.getVoteAverage()).isEqualTo(UPDATED_VOTE_AVERAGE);
        assertThat(testImage.getVoteCount()).isEqualTo(UPDATED_VOTE_COUNT);
        assertThat(testImage.getLastTMDBUpdate()).isEqualTo(UPDATED_LAST_TMDB_UPDATE);
    }

    @Test
    @Transactional
    public void updateNonExistingImage() throws Exception {
        int databaseSizeBeforeUpdate = imageRepository.findAll().size();

        // Create the Image
        ImageDTO imageDTO = imageMapper.toDto(image);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restImageMockMvc.perform(put("/api/images")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(imageDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Image in the database
        List<Image> imageList = imageRepository.findAll();
        assertThat(imageList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteImage() throws Exception {
        // Initialize the database
        imageRepository.saveAndFlush(image);

        int databaseSizeBeforeDelete = imageRepository.findAll().size();

        // Delete the image
        restImageMockMvc.perform(delete("/api/images/{id}", image.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Image> imageList = imageRepository.findAll();
        assertThat(imageList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
