package com.mgoulene.web.rest;

import com.mgoulene.MmdbApp;
import com.mgoulene.domain.Movie;
import com.mgoulene.domain.Credit;
import com.mgoulene.domain.Image;
import com.mgoulene.domain.Genre;
import com.mgoulene.repository.MovieRepository;
import com.mgoulene.service.MovieService;
import com.mgoulene.service.dto.MovieDTO;
import com.mgoulene.service.mapper.MovieMapper;
import com.mgoulene.service.dto.MovieCriteria;
import com.mgoulene.service.MovieQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.mgoulene.domain.enumeration.MovieStatus;
/**
 * Integration tests for the {@link MovieResource} REST controller.
 */
@SpringBootTest(classes = MmdbApp.class)
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@WithMockUser
public class MovieResourceIT {

    private static final String DEFAULT_TITLE = "AAAAAAAAAA";
    private static final String UPDATED_TITLE = "BBBBBBBBBB";

    private static final Boolean DEFAULT_FOR_ADULT = false;
    private static final Boolean UPDATED_FOR_ADULT = true;

    private static final String DEFAULT_HOMEPAGE = "AAAAAAAAAA";
    private static final String UPDATED_HOMEPAGE = "BBBBBBBBBB";

    private static final String DEFAULT_ORIGINAL_LANGUAGE = "AAAAAAAAAA";
    private static final String UPDATED_ORIGINAL_LANGUAGE = "BBBBBBBBBB";

    private static final String DEFAULT_ORIGINAL_TITLE = "AAAAAAAAAA";
    private static final String UPDATED_ORIGINAL_TITLE = "BBBBBBBBBB";

    private static final String DEFAULT_OVERVIEW = "AAAAAAAAAA";
    private static final String UPDATED_OVERVIEW = "BBBBBBBBBB";

    private static final String DEFAULT_TAGLINE = "AAAAAAAAAA";
    private static final String UPDATED_TAGLINE = "BBBBBBBBBB";

    private static final MovieStatus DEFAULT_STATUS = MovieStatus.RUMORED;
    private static final MovieStatus UPDATED_STATUS = MovieStatus.PLANNED;

    private static final Float DEFAULT_VOTE_AVERAGE = 0F;
    private static final Float UPDATED_VOTE_AVERAGE = 1F;
    private static final Float SMALLER_VOTE_AVERAGE = 0F - 1F;

    private static final Integer DEFAULT_VOTE_COUNT = 1;
    private static final Integer UPDATED_VOTE_COUNT = 2;
    private static final Integer SMALLER_VOTE_COUNT = 1 - 1;

    private static final LocalDate DEFAULT_RELEASE_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_RELEASE_DATE = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_RELEASE_DATE = LocalDate.ofEpochDay(-1L);

    private static final LocalDate DEFAULT_LAST_TMDB_UPDATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_LAST_TMDB_UPDATE = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_LAST_TMDB_UPDATE = LocalDate.ofEpochDay(-1L);

    private static final String DEFAULT_TMDB_ID = "AAAAAAAAAA";
    private static final String UPDATED_TMDB_ID = "BBBBBBBBBB";

    private static final Integer DEFAULT_RUNTIME = 1;
    private static final Integer UPDATED_RUNTIME = 2;
    private static final Integer SMALLER_RUNTIME = 1 - 1;

    @Autowired
    private MovieRepository movieRepository;

    @Mock
    private MovieRepository movieRepositoryMock;

    @Autowired
    private MovieMapper movieMapper;

    @Mock
    private MovieService movieServiceMock;

    @Autowired
    private MovieService movieService;

    @Autowired
    private MovieQueryService movieQueryService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restMovieMockMvc;

    private Movie movie;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Movie createEntity(EntityManager em) {
        Movie movie = new Movie()
            .title(DEFAULT_TITLE)
            .forAdult(DEFAULT_FOR_ADULT)
            .homepage(DEFAULT_HOMEPAGE)
            .originalLanguage(DEFAULT_ORIGINAL_LANGUAGE)
            .originalTitle(DEFAULT_ORIGINAL_TITLE)
            .overview(DEFAULT_OVERVIEW)
            .tagline(DEFAULT_TAGLINE)
            .status(DEFAULT_STATUS)
            .voteAverage(DEFAULT_VOTE_AVERAGE)
            .voteCount(DEFAULT_VOTE_COUNT)
            .releaseDate(DEFAULT_RELEASE_DATE)
            .lastTMDBUpdate(DEFAULT_LAST_TMDB_UPDATE)
            .tmdbId(DEFAULT_TMDB_ID)
            .runtime(DEFAULT_RUNTIME);
        return movie;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Movie createUpdatedEntity(EntityManager em) {
        Movie movie = new Movie()
            .title(UPDATED_TITLE)
            .forAdult(UPDATED_FOR_ADULT)
            .homepage(UPDATED_HOMEPAGE)
            .originalLanguage(UPDATED_ORIGINAL_LANGUAGE)
            .originalTitle(UPDATED_ORIGINAL_TITLE)
            .overview(UPDATED_OVERVIEW)
            .tagline(UPDATED_TAGLINE)
            .status(UPDATED_STATUS)
            .voteAverage(UPDATED_VOTE_AVERAGE)
            .voteCount(UPDATED_VOTE_COUNT)
            .releaseDate(UPDATED_RELEASE_DATE)
            .lastTMDBUpdate(UPDATED_LAST_TMDB_UPDATE)
            .tmdbId(UPDATED_TMDB_ID)
            .runtime(UPDATED_RUNTIME);
        return movie;
    }

    @BeforeEach
    public void initTest() {
        movie = createEntity(em);
    }

    @Test
    @Transactional
    public void createMovie() throws Exception {
        int databaseSizeBeforeCreate = movieRepository.findAll().size();
        // Create the Movie
        MovieDTO movieDTO = movieMapper.toDto(movie);
        restMovieMockMvc.perform(post("/api/movies")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(movieDTO)))
            .andExpect(status().isCreated());

        // Validate the Movie in the database
        List<Movie> movieList = movieRepository.findAll();
        assertThat(movieList).hasSize(databaseSizeBeforeCreate + 1);
        Movie testMovie = movieList.get(movieList.size() - 1);
        assertThat(testMovie.getTitle()).isEqualTo(DEFAULT_TITLE);
        assertThat(testMovie.isForAdult()).isEqualTo(DEFAULT_FOR_ADULT);
        assertThat(testMovie.getHomepage()).isEqualTo(DEFAULT_HOMEPAGE);
        assertThat(testMovie.getOriginalLanguage()).isEqualTo(DEFAULT_ORIGINAL_LANGUAGE);
        assertThat(testMovie.getOriginalTitle()).isEqualTo(DEFAULT_ORIGINAL_TITLE);
        assertThat(testMovie.getOverview()).isEqualTo(DEFAULT_OVERVIEW);
        assertThat(testMovie.getTagline()).isEqualTo(DEFAULT_TAGLINE);
        assertThat(testMovie.getStatus()).isEqualTo(DEFAULT_STATUS);
        assertThat(testMovie.getVoteAverage()).isEqualTo(DEFAULT_VOTE_AVERAGE);
        assertThat(testMovie.getVoteCount()).isEqualTo(DEFAULT_VOTE_COUNT);
        assertThat(testMovie.getReleaseDate()).isEqualTo(DEFAULT_RELEASE_DATE);
        assertThat(testMovie.getLastTMDBUpdate()).isEqualTo(DEFAULT_LAST_TMDB_UPDATE);
        assertThat(testMovie.getTmdbId()).isEqualTo(DEFAULT_TMDB_ID);
        assertThat(testMovie.getRuntime()).isEqualTo(DEFAULT_RUNTIME);
    }

    @Test
    @Transactional
    public void createMovieWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = movieRepository.findAll().size();

        // Create the Movie with an existing ID
        movie.setId(1L);
        MovieDTO movieDTO = movieMapper.toDto(movie);

        // An entity with an existing ID cannot be created, so this API call must fail
        restMovieMockMvc.perform(post("/api/movies")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(movieDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Movie in the database
        List<Movie> movieList = movieRepository.findAll();
        assertThat(movieList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkLastTMDBUpdateIsRequired() throws Exception {
        int databaseSizeBeforeTest = movieRepository.findAll().size();
        // set the field null
        movie.setLastTMDBUpdate(null);

        // Create the Movie, which fails.
        MovieDTO movieDTO = movieMapper.toDto(movie);


        restMovieMockMvc.perform(post("/api/movies")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(movieDTO)))
            .andExpect(status().isBadRequest());

        List<Movie> movieList = movieRepository.findAll();
        assertThat(movieList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkTmdbIdIsRequired() throws Exception {
        int databaseSizeBeforeTest = movieRepository.findAll().size();
        // set the field null
        movie.setTmdbId(null);

        // Create the Movie, which fails.
        MovieDTO movieDTO = movieMapper.toDto(movie);


        restMovieMockMvc.perform(post("/api/movies")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(movieDTO)))
            .andExpect(status().isBadRequest());

        List<Movie> movieList = movieRepository.findAll();
        assertThat(movieList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllMovies() throws Exception {
        // Initialize the database
        movieRepository.saveAndFlush(movie);

        // Get all the movieList
        restMovieMockMvc.perform(get("/api/movies?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(movie.getId().intValue())))
            .andExpect(jsonPath("$.[*].title").value(hasItem(DEFAULT_TITLE)))
            .andExpect(jsonPath("$.[*].forAdult").value(hasItem(DEFAULT_FOR_ADULT.booleanValue())))
            .andExpect(jsonPath("$.[*].homepage").value(hasItem(DEFAULT_HOMEPAGE)))
            .andExpect(jsonPath("$.[*].originalLanguage").value(hasItem(DEFAULT_ORIGINAL_LANGUAGE)))
            .andExpect(jsonPath("$.[*].originalTitle").value(hasItem(DEFAULT_ORIGINAL_TITLE)))
            .andExpect(jsonPath("$.[*].overview").value(hasItem(DEFAULT_OVERVIEW)))
            .andExpect(jsonPath("$.[*].tagline").value(hasItem(DEFAULT_TAGLINE)))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.toString())))
            .andExpect(jsonPath("$.[*].voteAverage").value(hasItem(DEFAULT_VOTE_AVERAGE.doubleValue())))
            .andExpect(jsonPath("$.[*].voteCount").value(hasItem(DEFAULT_VOTE_COUNT)))
            .andExpect(jsonPath("$.[*].releaseDate").value(hasItem(DEFAULT_RELEASE_DATE.toString())))
            .andExpect(jsonPath("$.[*].lastTMDBUpdate").value(hasItem(DEFAULT_LAST_TMDB_UPDATE.toString())))
            .andExpect(jsonPath("$.[*].tmdbId").value(hasItem(DEFAULT_TMDB_ID)))
            .andExpect(jsonPath("$.[*].runtime").value(hasItem(DEFAULT_RUNTIME)));
    }
    
    @SuppressWarnings({"unchecked"})
    public void getAllMoviesWithEagerRelationshipsIsEnabled() throws Exception {
        when(movieServiceMock.findAllWithEagerRelationships(any())).thenReturn(new PageImpl(new ArrayList<>()));

        restMovieMockMvc.perform(get("/api/movies?eagerload=true"))
            .andExpect(status().isOk());

        verify(movieServiceMock, times(1)).findAllWithEagerRelationships(any());
    }

    @SuppressWarnings({"unchecked"})
    public void getAllMoviesWithEagerRelationshipsIsNotEnabled() throws Exception {
        when(movieServiceMock.findAllWithEagerRelationships(any())).thenReturn(new PageImpl(new ArrayList<>()));

        restMovieMockMvc.perform(get("/api/movies?eagerload=true"))
            .andExpect(status().isOk());

        verify(movieServiceMock, times(1)).findAllWithEagerRelationships(any());
    }

    @Test
    @Transactional
    public void getMovie() throws Exception {
        // Initialize the database
        movieRepository.saveAndFlush(movie);

        // Get the movie
        restMovieMockMvc.perform(get("/api/movies/{id}", movie.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(movie.getId().intValue()))
            .andExpect(jsonPath("$.title").value(DEFAULT_TITLE))
            .andExpect(jsonPath("$.forAdult").value(DEFAULT_FOR_ADULT.booleanValue()))
            .andExpect(jsonPath("$.homepage").value(DEFAULT_HOMEPAGE))
            .andExpect(jsonPath("$.originalLanguage").value(DEFAULT_ORIGINAL_LANGUAGE))
            .andExpect(jsonPath("$.originalTitle").value(DEFAULT_ORIGINAL_TITLE))
            .andExpect(jsonPath("$.overview").value(DEFAULT_OVERVIEW))
            .andExpect(jsonPath("$.tagline").value(DEFAULT_TAGLINE))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS.toString()))
            .andExpect(jsonPath("$.voteAverage").value(DEFAULT_VOTE_AVERAGE.doubleValue()))
            .andExpect(jsonPath("$.voteCount").value(DEFAULT_VOTE_COUNT))
            .andExpect(jsonPath("$.releaseDate").value(DEFAULT_RELEASE_DATE.toString()))
            .andExpect(jsonPath("$.lastTMDBUpdate").value(DEFAULT_LAST_TMDB_UPDATE.toString()))
            .andExpect(jsonPath("$.tmdbId").value(DEFAULT_TMDB_ID))
            .andExpect(jsonPath("$.runtime").value(DEFAULT_RUNTIME));
    }


    @Test
    @Transactional
    public void getMoviesByIdFiltering() throws Exception {
        // Initialize the database
        movieRepository.saveAndFlush(movie);

        Long id = movie.getId();

        defaultMovieShouldBeFound("id.equals=" + id);
        defaultMovieShouldNotBeFound("id.notEquals=" + id);

        defaultMovieShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultMovieShouldNotBeFound("id.greaterThan=" + id);

        defaultMovieShouldBeFound("id.lessThanOrEqual=" + id);
        defaultMovieShouldNotBeFound("id.lessThan=" + id);
    }


    @Test
    @Transactional
    public void getAllMoviesByTitleIsEqualToSomething() throws Exception {
        // Initialize the database
        movieRepository.saveAndFlush(movie);

        // Get all the movieList where title equals to DEFAULT_TITLE
        defaultMovieShouldBeFound("title.equals=" + DEFAULT_TITLE);

        // Get all the movieList where title equals to UPDATED_TITLE
        defaultMovieShouldNotBeFound("title.equals=" + UPDATED_TITLE);
    }

    @Test
    @Transactional
    public void getAllMoviesByTitleIsNotEqualToSomething() throws Exception {
        // Initialize the database
        movieRepository.saveAndFlush(movie);

        // Get all the movieList where title not equals to DEFAULT_TITLE
        defaultMovieShouldNotBeFound("title.notEquals=" + DEFAULT_TITLE);

        // Get all the movieList where title not equals to UPDATED_TITLE
        defaultMovieShouldBeFound("title.notEquals=" + UPDATED_TITLE);
    }

    @Test
    @Transactional
    public void getAllMoviesByTitleIsInShouldWork() throws Exception {
        // Initialize the database
        movieRepository.saveAndFlush(movie);

        // Get all the movieList where title in DEFAULT_TITLE or UPDATED_TITLE
        defaultMovieShouldBeFound("title.in=" + DEFAULT_TITLE + "," + UPDATED_TITLE);

        // Get all the movieList where title equals to UPDATED_TITLE
        defaultMovieShouldNotBeFound("title.in=" + UPDATED_TITLE);
    }

    @Test
    @Transactional
    public void getAllMoviesByTitleIsNullOrNotNull() throws Exception {
        // Initialize the database
        movieRepository.saveAndFlush(movie);

        // Get all the movieList where title is not null
        defaultMovieShouldBeFound("title.specified=true");

        // Get all the movieList where title is null
        defaultMovieShouldNotBeFound("title.specified=false");
    }
                @Test
    @Transactional
    public void getAllMoviesByTitleContainsSomething() throws Exception {
        // Initialize the database
        movieRepository.saveAndFlush(movie);

        // Get all the movieList where title contains DEFAULT_TITLE
        defaultMovieShouldBeFound("title.contains=" + DEFAULT_TITLE);

        // Get all the movieList where title contains UPDATED_TITLE
        defaultMovieShouldNotBeFound("title.contains=" + UPDATED_TITLE);
    }

    @Test
    @Transactional
    public void getAllMoviesByTitleNotContainsSomething() throws Exception {
        // Initialize the database
        movieRepository.saveAndFlush(movie);

        // Get all the movieList where title does not contain DEFAULT_TITLE
        defaultMovieShouldNotBeFound("title.doesNotContain=" + DEFAULT_TITLE);

        // Get all the movieList where title does not contain UPDATED_TITLE
        defaultMovieShouldBeFound("title.doesNotContain=" + UPDATED_TITLE);
    }


    @Test
    @Transactional
    public void getAllMoviesByForAdultIsEqualToSomething() throws Exception {
        // Initialize the database
        movieRepository.saveAndFlush(movie);

        // Get all the movieList where forAdult equals to DEFAULT_FOR_ADULT
        defaultMovieShouldBeFound("forAdult.equals=" + DEFAULT_FOR_ADULT);

        // Get all the movieList where forAdult equals to UPDATED_FOR_ADULT
        defaultMovieShouldNotBeFound("forAdult.equals=" + UPDATED_FOR_ADULT);
    }

    @Test
    @Transactional
    public void getAllMoviesByForAdultIsNotEqualToSomething() throws Exception {
        // Initialize the database
        movieRepository.saveAndFlush(movie);

        // Get all the movieList where forAdult not equals to DEFAULT_FOR_ADULT
        defaultMovieShouldNotBeFound("forAdult.notEquals=" + DEFAULT_FOR_ADULT);

        // Get all the movieList where forAdult not equals to UPDATED_FOR_ADULT
        defaultMovieShouldBeFound("forAdult.notEquals=" + UPDATED_FOR_ADULT);
    }

    @Test
    @Transactional
    public void getAllMoviesByForAdultIsInShouldWork() throws Exception {
        // Initialize the database
        movieRepository.saveAndFlush(movie);

        // Get all the movieList where forAdult in DEFAULT_FOR_ADULT or UPDATED_FOR_ADULT
        defaultMovieShouldBeFound("forAdult.in=" + DEFAULT_FOR_ADULT + "," + UPDATED_FOR_ADULT);

        // Get all the movieList where forAdult equals to UPDATED_FOR_ADULT
        defaultMovieShouldNotBeFound("forAdult.in=" + UPDATED_FOR_ADULT);
    }

    @Test
    @Transactional
    public void getAllMoviesByForAdultIsNullOrNotNull() throws Exception {
        // Initialize the database
        movieRepository.saveAndFlush(movie);

        // Get all the movieList where forAdult is not null
        defaultMovieShouldBeFound("forAdult.specified=true");

        // Get all the movieList where forAdult is null
        defaultMovieShouldNotBeFound("forAdult.specified=false");
    }

    @Test
    @Transactional
    public void getAllMoviesByHomepageIsEqualToSomething() throws Exception {
        // Initialize the database
        movieRepository.saveAndFlush(movie);

        // Get all the movieList where homepage equals to DEFAULT_HOMEPAGE
        defaultMovieShouldBeFound("homepage.equals=" + DEFAULT_HOMEPAGE);

        // Get all the movieList where homepage equals to UPDATED_HOMEPAGE
        defaultMovieShouldNotBeFound("homepage.equals=" + UPDATED_HOMEPAGE);
    }

    @Test
    @Transactional
    public void getAllMoviesByHomepageIsNotEqualToSomething() throws Exception {
        // Initialize the database
        movieRepository.saveAndFlush(movie);

        // Get all the movieList where homepage not equals to DEFAULT_HOMEPAGE
        defaultMovieShouldNotBeFound("homepage.notEquals=" + DEFAULT_HOMEPAGE);

        // Get all the movieList where homepage not equals to UPDATED_HOMEPAGE
        defaultMovieShouldBeFound("homepage.notEquals=" + UPDATED_HOMEPAGE);
    }

    @Test
    @Transactional
    public void getAllMoviesByHomepageIsInShouldWork() throws Exception {
        // Initialize the database
        movieRepository.saveAndFlush(movie);

        // Get all the movieList where homepage in DEFAULT_HOMEPAGE or UPDATED_HOMEPAGE
        defaultMovieShouldBeFound("homepage.in=" + DEFAULT_HOMEPAGE + "," + UPDATED_HOMEPAGE);

        // Get all the movieList where homepage equals to UPDATED_HOMEPAGE
        defaultMovieShouldNotBeFound("homepage.in=" + UPDATED_HOMEPAGE);
    }

    @Test
    @Transactional
    public void getAllMoviesByHomepageIsNullOrNotNull() throws Exception {
        // Initialize the database
        movieRepository.saveAndFlush(movie);

        // Get all the movieList where homepage is not null
        defaultMovieShouldBeFound("homepage.specified=true");

        // Get all the movieList where homepage is null
        defaultMovieShouldNotBeFound("homepage.specified=false");
    }
                @Test
    @Transactional
    public void getAllMoviesByHomepageContainsSomething() throws Exception {
        // Initialize the database
        movieRepository.saveAndFlush(movie);

        // Get all the movieList where homepage contains DEFAULT_HOMEPAGE
        defaultMovieShouldBeFound("homepage.contains=" + DEFAULT_HOMEPAGE);

        // Get all the movieList where homepage contains UPDATED_HOMEPAGE
        defaultMovieShouldNotBeFound("homepage.contains=" + UPDATED_HOMEPAGE);
    }

    @Test
    @Transactional
    public void getAllMoviesByHomepageNotContainsSomething() throws Exception {
        // Initialize the database
        movieRepository.saveAndFlush(movie);

        // Get all the movieList where homepage does not contain DEFAULT_HOMEPAGE
        defaultMovieShouldNotBeFound("homepage.doesNotContain=" + DEFAULT_HOMEPAGE);

        // Get all the movieList where homepage does not contain UPDATED_HOMEPAGE
        defaultMovieShouldBeFound("homepage.doesNotContain=" + UPDATED_HOMEPAGE);
    }


    @Test
    @Transactional
    public void getAllMoviesByOriginalLanguageIsEqualToSomething() throws Exception {
        // Initialize the database
        movieRepository.saveAndFlush(movie);

        // Get all the movieList where originalLanguage equals to DEFAULT_ORIGINAL_LANGUAGE
        defaultMovieShouldBeFound("originalLanguage.equals=" + DEFAULT_ORIGINAL_LANGUAGE);

        // Get all the movieList where originalLanguage equals to UPDATED_ORIGINAL_LANGUAGE
        defaultMovieShouldNotBeFound("originalLanguage.equals=" + UPDATED_ORIGINAL_LANGUAGE);
    }

    @Test
    @Transactional
    public void getAllMoviesByOriginalLanguageIsNotEqualToSomething() throws Exception {
        // Initialize the database
        movieRepository.saveAndFlush(movie);

        // Get all the movieList where originalLanguage not equals to DEFAULT_ORIGINAL_LANGUAGE
        defaultMovieShouldNotBeFound("originalLanguage.notEquals=" + DEFAULT_ORIGINAL_LANGUAGE);

        // Get all the movieList where originalLanguage not equals to UPDATED_ORIGINAL_LANGUAGE
        defaultMovieShouldBeFound("originalLanguage.notEquals=" + UPDATED_ORIGINAL_LANGUAGE);
    }

    @Test
    @Transactional
    public void getAllMoviesByOriginalLanguageIsInShouldWork() throws Exception {
        // Initialize the database
        movieRepository.saveAndFlush(movie);

        // Get all the movieList where originalLanguage in DEFAULT_ORIGINAL_LANGUAGE or UPDATED_ORIGINAL_LANGUAGE
        defaultMovieShouldBeFound("originalLanguage.in=" + DEFAULT_ORIGINAL_LANGUAGE + "," + UPDATED_ORIGINAL_LANGUAGE);

        // Get all the movieList where originalLanguage equals to UPDATED_ORIGINAL_LANGUAGE
        defaultMovieShouldNotBeFound("originalLanguage.in=" + UPDATED_ORIGINAL_LANGUAGE);
    }

    @Test
    @Transactional
    public void getAllMoviesByOriginalLanguageIsNullOrNotNull() throws Exception {
        // Initialize the database
        movieRepository.saveAndFlush(movie);

        // Get all the movieList where originalLanguage is not null
        defaultMovieShouldBeFound("originalLanguage.specified=true");

        // Get all the movieList where originalLanguage is null
        defaultMovieShouldNotBeFound("originalLanguage.specified=false");
    }
                @Test
    @Transactional
    public void getAllMoviesByOriginalLanguageContainsSomething() throws Exception {
        // Initialize the database
        movieRepository.saveAndFlush(movie);

        // Get all the movieList where originalLanguage contains DEFAULT_ORIGINAL_LANGUAGE
        defaultMovieShouldBeFound("originalLanguage.contains=" + DEFAULT_ORIGINAL_LANGUAGE);

        // Get all the movieList where originalLanguage contains UPDATED_ORIGINAL_LANGUAGE
        defaultMovieShouldNotBeFound("originalLanguage.contains=" + UPDATED_ORIGINAL_LANGUAGE);
    }

    @Test
    @Transactional
    public void getAllMoviesByOriginalLanguageNotContainsSomething() throws Exception {
        // Initialize the database
        movieRepository.saveAndFlush(movie);

        // Get all the movieList where originalLanguage does not contain DEFAULT_ORIGINAL_LANGUAGE
        defaultMovieShouldNotBeFound("originalLanguage.doesNotContain=" + DEFAULT_ORIGINAL_LANGUAGE);

        // Get all the movieList where originalLanguage does not contain UPDATED_ORIGINAL_LANGUAGE
        defaultMovieShouldBeFound("originalLanguage.doesNotContain=" + UPDATED_ORIGINAL_LANGUAGE);
    }


    @Test
    @Transactional
    public void getAllMoviesByOriginalTitleIsEqualToSomething() throws Exception {
        // Initialize the database
        movieRepository.saveAndFlush(movie);

        // Get all the movieList where originalTitle equals to DEFAULT_ORIGINAL_TITLE
        defaultMovieShouldBeFound("originalTitle.equals=" + DEFAULT_ORIGINAL_TITLE);

        // Get all the movieList where originalTitle equals to UPDATED_ORIGINAL_TITLE
        defaultMovieShouldNotBeFound("originalTitle.equals=" + UPDATED_ORIGINAL_TITLE);
    }

    @Test
    @Transactional
    public void getAllMoviesByOriginalTitleIsNotEqualToSomething() throws Exception {
        // Initialize the database
        movieRepository.saveAndFlush(movie);

        // Get all the movieList where originalTitle not equals to DEFAULT_ORIGINAL_TITLE
        defaultMovieShouldNotBeFound("originalTitle.notEquals=" + DEFAULT_ORIGINAL_TITLE);

        // Get all the movieList where originalTitle not equals to UPDATED_ORIGINAL_TITLE
        defaultMovieShouldBeFound("originalTitle.notEquals=" + UPDATED_ORIGINAL_TITLE);
    }

    @Test
    @Transactional
    public void getAllMoviesByOriginalTitleIsInShouldWork() throws Exception {
        // Initialize the database
        movieRepository.saveAndFlush(movie);

        // Get all the movieList where originalTitle in DEFAULT_ORIGINAL_TITLE or UPDATED_ORIGINAL_TITLE
        defaultMovieShouldBeFound("originalTitle.in=" + DEFAULT_ORIGINAL_TITLE + "," + UPDATED_ORIGINAL_TITLE);

        // Get all the movieList where originalTitle equals to UPDATED_ORIGINAL_TITLE
        defaultMovieShouldNotBeFound("originalTitle.in=" + UPDATED_ORIGINAL_TITLE);
    }

    @Test
    @Transactional
    public void getAllMoviesByOriginalTitleIsNullOrNotNull() throws Exception {
        // Initialize the database
        movieRepository.saveAndFlush(movie);

        // Get all the movieList where originalTitle is not null
        defaultMovieShouldBeFound("originalTitle.specified=true");

        // Get all the movieList where originalTitle is null
        defaultMovieShouldNotBeFound("originalTitle.specified=false");
    }
                @Test
    @Transactional
    public void getAllMoviesByOriginalTitleContainsSomething() throws Exception {
        // Initialize the database
        movieRepository.saveAndFlush(movie);

        // Get all the movieList where originalTitle contains DEFAULT_ORIGINAL_TITLE
        defaultMovieShouldBeFound("originalTitle.contains=" + DEFAULT_ORIGINAL_TITLE);

        // Get all the movieList where originalTitle contains UPDATED_ORIGINAL_TITLE
        defaultMovieShouldNotBeFound("originalTitle.contains=" + UPDATED_ORIGINAL_TITLE);
    }

    @Test
    @Transactional
    public void getAllMoviesByOriginalTitleNotContainsSomething() throws Exception {
        // Initialize the database
        movieRepository.saveAndFlush(movie);

        // Get all the movieList where originalTitle does not contain DEFAULT_ORIGINAL_TITLE
        defaultMovieShouldNotBeFound("originalTitle.doesNotContain=" + DEFAULT_ORIGINAL_TITLE);

        // Get all the movieList where originalTitle does not contain UPDATED_ORIGINAL_TITLE
        defaultMovieShouldBeFound("originalTitle.doesNotContain=" + UPDATED_ORIGINAL_TITLE);
    }


    @Test
    @Transactional
    public void getAllMoviesByOverviewIsEqualToSomething() throws Exception {
        // Initialize the database
        movieRepository.saveAndFlush(movie);

        // Get all the movieList where overview equals to DEFAULT_OVERVIEW
        defaultMovieShouldBeFound("overview.equals=" + DEFAULT_OVERVIEW);

        // Get all the movieList where overview equals to UPDATED_OVERVIEW
        defaultMovieShouldNotBeFound("overview.equals=" + UPDATED_OVERVIEW);
    }

    @Test
    @Transactional
    public void getAllMoviesByOverviewIsNotEqualToSomething() throws Exception {
        // Initialize the database
        movieRepository.saveAndFlush(movie);

        // Get all the movieList where overview not equals to DEFAULT_OVERVIEW
        defaultMovieShouldNotBeFound("overview.notEquals=" + DEFAULT_OVERVIEW);

        // Get all the movieList where overview not equals to UPDATED_OVERVIEW
        defaultMovieShouldBeFound("overview.notEquals=" + UPDATED_OVERVIEW);
    }

    @Test
    @Transactional
    public void getAllMoviesByOverviewIsInShouldWork() throws Exception {
        // Initialize the database
        movieRepository.saveAndFlush(movie);

        // Get all the movieList where overview in DEFAULT_OVERVIEW or UPDATED_OVERVIEW
        defaultMovieShouldBeFound("overview.in=" + DEFAULT_OVERVIEW + "," + UPDATED_OVERVIEW);

        // Get all the movieList where overview equals to UPDATED_OVERVIEW
        defaultMovieShouldNotBeFound("overview.in=" + UPDATED_OVERVIEW);
    }

    @Test
    @Transactional
    public void getAllMoviesByOverviewIsNullOrNotNull() throws Exception {
        // Initialize the database
        movieRepository.saveAndFlush(movie);

        // Get all the movieList where overview is not null
        defaultMovieShouldBeFound("overview.specified=true");

        // Get all the movieList where overview is null
        defaultMovieShouldNotBeFound("overview.specified=false");
    }
                @Test
    @Transactional
    public void getAllMoviesByOverviewContainsSomething() throws Exception {
        // Initialize the database
        movieRepository.saveAndFlush(movie);

        // Get all the movieList where overview contains DEFAULT_OVERVIEW
        defaultMovieShouldBeFound("overview.contains=" + DEFAULT_OVERVIEW);

        // Get all the movieList where overview contains UPDATED_OVERVIEW
        defaultMovieShouldNotBeFound("overview.contains=" + UPDATED_OVERVIEW);
    }

    @Test
    @Transactional
    public void getAllMoviesByOverviewNotContainsSomething() throws Exception {
        // Initialize the database
        movieRepository.saveAndFlush(movie);

        // Get all the movieList where overview does not contain DEFAULT_OVERVIEW
        defaultMovieShouldNotBeFound("overview.doesNotContain=" + DEFAULT_OVERVIEW);

        // Get all the movieList where overview does not contain UPDATED_OVERVIEW
        defaultMovieShouldBeFound("overview.doesNotContain=" + UPDATED_OVERVIEW);
    }


    @Test
    @Transactional
    public void getAllMoviesByTaglineIsEqualToSomething() throws Exception {
        // Initialize the database
        movieRepository.saveAndFlush(movie);

        // Get all the movieList where tagline equals to DEFAULT_TAGLINE
        defaultMovieShouldBeFound("tagline.equals=" + DEFAULT_TAGLINE);

        // Get all the movieList where tagline equals to UPDATED_TAGLINE
        defaultMovieShouldNotBeFound("tagline.equals=" + UPDATED_TAGLINE);
    }

    @Test
    @Transactional
    public void getAllMoviesByTaglineIsNotEqualToSomething() throws Exception {
        // Initialize the database
        movieRepository.saveAndFlush(movie);

        // Get all the movieList where tagline not equals to DEFAULT_TAGLINE
        defaultMovieShouldNotBeFound("tagline.notEquals=" + DEFAULT_TAGLINE);

        // Get all the movieList where tagline not equals to UPDATED_TAGLINE
        defaultMovieShouldBeFound("tagline.notEquals=" + UPDATED_TAGLINE);
    }

    @Test
    @Transactional
    public void getAllMoviesByTaglineIsInShouldWork() throws Exception {
        // Initialize the database
        movieRepository.saveAndFlush(movie);

        // Get all the movieList where tagline in DEFAULT_TAGLINE or UPDATED_TAGLINE
        defaultMovieShouldBeFound("tagline.in=" + DEFAULT_TAGLINE + "," + UPDATED_TAGLINE);

        // Get all the movieList where tagline equals to UPDATED_TAGLINE
        defaultMovieShouldNotBeFound("tagline.in=" + UPDATED_TAGLINE);
    }

    @Test
    @Transactional
    public void getAllMoviesByTaglineIsNullOrNotNull() throws Exception {
        // Initialize the database
        movieRepository.saveAndFlush(movie);

        // Get all the movieList where tagline is not null
        defaultMovieShouldBeFound("tagline.specified=true");

        // Get all the movieList where tagline is null
        defaultMovieShouldNotBeFound("tagline.specified=false");
    }
                @Test
    @Transactional
    public void getAllMoviesByTaglineContainsSomething() throws Exception {
        // Initialize the database
        movieRepository.saveAndFlush(movie);

        // Get all the movieList where tagline contains DEFAULT_TAGLINE
        defaultMovieShouldBeFound("tagline.contains=" + DEFAULT_TAGLINE);

        // Get all the movieList where tagline contains UPDATED_TAGLINE
        defaultMovieShouldNotBeFound("tagline.contains=" + UPDATED_TAGLINE);
    }

    @Test
    @Transactional
    public void getAllMoviesByTaglineNotContainsSomething() throws Exception {
        // Initialize the database
        movieRepository.saveAndFlush(movie);

        // Get all the movieList where tagline does not contain DEFAULT_TAGLINE
        defaultMovieShouldNotBeFound("tagline.doesNotContain=" + DEFAULT_TAGLINE);

        // Get all the movieList where tagline does not contain UPDATED_TAGLINE
        defaultMovieShouldBeFound("tagline.doesNotContain=" + UPDATED_TAGLINE);
    }


    @Test
    @Transactional
    public void getAllMoviesByStatusIsEqualToSomething() throws Exception {
        // Initialize the database
        movieRepository.saveAndFlush(movie);

        // Get all the movieList where status equals to DEFAULT_STATUS
        defaultMovieShouldBeFound("status.equals=" + DEFAULT_STATUS);

        // Get all the movieList where status equals to UPDATED_STATUS
        defaultMovieShouldNotBeFound("status.equals=" + UPDATED_STATUS);
    }

    @Test
    @Transactional
    public void getAllMoviesByStatusIsNotEqualToSomething() throws Exception {
        // Initialize the database
        movieRepository.saveAndFlush(movie);

        // Get all the movieList where status not equals to DEFAULT_STATUS
        defaultMovieShouldNotBeFound("status.notEquals=" + DEFAULT_STATUS);

        // Get all the movieList where status not equals to UPDATED_STATUS
        defaultMovieShouldBeFound("status.notEquals=" + UPDATED_STATUS);
    }

    @Test
    @Transactional
    public void getAllMoviesByStatusIsInShouldWork() throws Exception {
        // Initialize the database
        movieRepository.saveAndFlush(movie);

        // Get all the movieList where status in DEFAULT_STATUS or UPDATED_STATUS
        defaultMovieShouldBeFound("status.in=" + DEFAULT_STATUS + "," + UPDATED_STATUS);

        // Get all the movieList where status equals to UPDATED_STATUS
        defaultMovieShouldNotBeFound("status.in=" + UPDATED_STATUS);
    }

    @Test
    @Transactional
    public void getAllMoviesByStatusIsNullOrNotNull() throws Exception {
        // Initialize the database
        movieRepository.saveAndFlush(movie);

        // Get all the movieList where status is not null
        defaultMovieShouldBeFound("status.specified=true");

        // Get all the movieList where status is null
        defaultMovieShouldNotBeFound("status.specified=false");
    }

    @Test
    @Transactional
    public void getAllMoviesByVoteAverageIsEqualToSomething() throws Exception {
        // Initialize the database
        movieRepository.saveAndFlush(movie);

        // Get all the movieList where voteAverage equals to DEFAULT_VOTE_AVERAGE
        defaultMovieShouldBeFound("voteAverage.equals=" + DEFAULT_VOTE_AVERAGE);

        // Get all the movieList where voteAverage equals to UPDATED_VOTE_AVERAGE
        defaultMovieShouldNotBeFound("voteAverage.equals=" + UPDATED_VOTE_AVERAGE);
    }

    @Test
    @Transactional
    public void getAllMoviesByVoteAverageIsNotEqualToSomething() throws Exception {
        // Initialize the database
        movieRepository.saveAndFlush(movie);

        // Get all the movieList where voteAverage not equals to DEFAULT_VOTE_AVERAGE
        defaultMovieShouldNotBeFound("voteAverage.notEquals=" + DEFAULT_VOTE_AVERAGE);

        // Get all the movieList where voteAverage not equals to UPDATED_VOTE_AVERAGE
        defaultMovieShouldBeFound("voteAverage.notEquals=" + UPDATED_VOTE_AVERAGE);
    }

    @Test
    @Transactional
    public void getAllMoviesByVoteAverageIsInShouldWork() throws Exception {
        // Initialize the database
        movieRepository.saveAndFlush(movie);

        // Get all the movieList where voteAverage in DEFAULT_VOTE_AVERAGE or UPDATED_VOTE_AVERAGE
        defaultMovieShouldBeFound("voteAverage.in=" + DEFAULT_VOTE_AVERAGE + "," + UPDATED_VOTE_AVERAGE);

        // Get all the movieList where voteAverage equals to UPDATED_VOTE_AVERAGE
        defaultMovieShouldNotBeFound("voteAverage.in=" + UPDATED_VOTE_AVERAGE);
    }

    @Test
    @Transactional
    public void getAllMoviesByVoteAverageIsNullOrNotNull() throws Exception {
        // Initialize the database
        movieRepository.saveAndFlush(movie);

        // Get all the movieList where voteAverage is not null
        defaultMovieShouldBeFound("voteAverage.specified=true");

        // Get all the movieList where voteAverage is null
        defaultMovieShouldNotBeFound("voteAverage.specified=false");
    }

    @Test
    @Transactional
    public void getAllMoviesByVoteAverageIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        movieRepository.saveAndFlush(movie);

        // Get all the movieList where voteAverage is greater than or equal to DEFAULT_VOTE_AVERAGE
        defaultMovieShouldBeFound("voteAverage.greaterThanOrEqual=" + DEFAULT_VOTE_AVERAGE);

        // Get all the movieList where voteAverage is greater than or equal to (DEFAULT_VOTE_AVERAGE + 1)
        defaultMovieShouldNotBeFound("voteAverage.greaterThanOrEqual=" + (DEFAULT_VOTE_AVERAGE + 1));
    }

    @Test
    @Transactional
    public void getAllMoviesByVoteAverageIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        movieRepository.saveAndFlush(movie);

        // Get all the movieList where voteAverage is less than or equal to DEFAULT_VOTE_AVERAGE
        defaultMovieShouldBeFound("voteAverage.lessThanOrEqual=" + DEFAULT_VOTE_AVERAGE);

        // Get all the movieList where voteAverage is less than or equal to SMALLER_VOTE_AVERAGE
        defaultMovieShouldNotBeFound("voteAverage.lessThanOrEqual=" + SMALLER_VOTE_AVERAGE);
    }

    @Test
    @Transactional
    public void getAllMoviesByVoteAverageIsLessThanSomething() throws Exception {
        // Initialize the database
        movieRepository.saveAndFlush(movie);

        // Get all the movieList where voteAverage is less than DEFAULT_VOTE_AVERAGE
        defaultMovieShouldNotBeFound("voteAverage.lessThan=" + DEFAULT_VOTE_AVERAGE);

        // Get all the movieList where voteAverage is less than (DEFAULT_VOTE_AVERAGE + 1)
        defaultMovieShouldBeFound("voteAverage.lessThan=" + (DEFAULT_VOTE_AVERAGE + 1));
    }

    @Test
    @Transactional
    public void getAllMoviesByVoteAverageIsGreaterThanSomething() throws Exception {
        // Initialize the database
        movieRepository.saveAndFlush(movie);

        // Get all the movieList where voteAverage is greater than DEFAULT_VOTE_AVERAGE
        defaultMovieShouldNotBeFound("voteAverage.greaterThan=" + DEFAULT_VOTE_AVERAGE);

        // Get all the movieList where voteAverage is greater than SMALLER_VOTE_AVERAGE
        defaultMovieShouldBeFound("voteAverage.greaterThan=" + SMALLER_VOTE_AVERAGE);
    }


    @Test
    @Transactional
    public void getAllMoviesByVoteCountIsEqualToSomething() throws Exception {
        // Initialize the database
        movieRepository.saveAndFlush(movie);

        // Get all the movieList where voteCount equals to DEFAULT_VOTE_COUNT
        defaultMovieShouldBeFound("voteCount.equals=" + DEFAULT_VOTE_COUNT);

        // Get all the movieList where voteCount equals to UPDATED_VOTE_COUNT
        defaultMovieShouldNotBeFound("voteCount.equals=" + UPDATED_VOTE_COUNT);
    }

    @Test
    @Transactional
    public void getAllMoviesByVoteCountIsNotEqualToSomething() throws Exception {
        // Initialize the database
        movieRepository.saveAndFlush(movie);

        // Get all the movieList where voteCount not equals to DEFAULT_VOTE_COUNT
        defaultMovieShouldNotBeFound("voteCount.notEquals=" + DEFAULT_VOTE_COUNT);

        // Get all the movieList where voteCount not equals to UPDATED_VOTE_COUNT
        defaultMovieShouldBeFound("voteCount.notEquals=" + UPDATED_VOTE_COUNT);
    }

    @Test
    @Transactional
    public void getAllMoviesByVoteCountIsInShouldWork() throws Exception {
        // Initialize the database
        movieRepository.saveAndFlush(movie);

        // Get all the movieList where voteCount in DEFAULT_VOTE_COUNT or UPDATED_VOTE_COUNT
        defaultMovieShouldBeFound("voteCount.in=" + DEFAULT_VOTE_COUNT + "," + UPDATED_VOTE_COUNT);

        // Get all the movieList where voteCount equals to UPDATED_VOTE_COUNT
        defaultMovieShouldNotBeFound("voteCount.in=" + UPDATED_VOTE_COUNT);
    }

    @Test
    @Transactional
    public void getAllMoviesByVoteCountIsNullOrNotNull() throws Exception {
        // Initialize the database
        movieRepository.saveAndFlush(movie);

        // Get all the movieList where voteCount is not null
        defaultMovieShouldBeFound("voteCount.specified=true");

        // Get all the movieList where voteCount is null
        defaultMovieShouldNotBeFound("voteCount.specified=false");
    }

    @Test
    @Transactional
    public void getAllMoviesByVoteCountIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        movieRepository.saveAndFlush(movie);

        // Get all the movieList where voteCount is greater than or equal to DEFAULT_VOTE_COUNT
        defaultMovieShouldBeFound("voteCount.greaterThanOrEqual=" + DEFAULT_VOTE_COUNT);

        // Get all the movieList where voteCount is greater than or equal to UPDATED_VOTE_COUNT
        defaultMovieShouldNotBeFound("voteCount.greaterThanOrEqual=" + UPDATED_VOTE_COUNT);
    }

    @Test
    @Transactional
    public void getAllMoviesByVoteCountIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        movieRepository.saveAndFlush(movie);

        // Get all the movieList where voteCount is less than or equal to DEFAULT_VOTE_COUNT
        defaultMovieShouldBeFound("voteCount.lessThanOrEqual=" + DEFAULT_VOTE_COUNT);

        // Get all the movieList where voteCount is less than or equal to SMALLER_VOTE_COUNT
        defaultMovieShouldNotBeFound("voteCount.lessThanOrEqual=" + SMALLER_VOTE_COUNT);
    }

    @Test
    @Transactional
    public void getAllMoviesByVoteCountIsLessThanSomething() throws Exception {
        // Initialize the database
        movieRepository.saveAndFlush(movie);

        // Get all the movieList where voteCount is less than DEFAULT_VOTE_COUNT
        defaultMovieShouldNotBeFound("voteCount.lessThan=" + DEFAULT_VOTE_COUNT);

        // Get all the movieList where voteCount is less than UPDATED_VOTE_COUNT
        defaultMovieShouldBeFound("voteCount.lessThan=" + UPDATED_VOTE_COUNT);
    }

    @Test
    @Transactional
    public void getAllMoviesByVoteCountIsGreaterThanSomething() throws Exception {
        // Initialize the database
        movieRepository.saveAndFlush(movie);

        // Get all the movieList where voteCount is greater than DEFAULT_VOTE_COUNT
        defaultMovieShouldNotBeFound("voteCount.greaterThan=" + DEFAULT_VOTE_COUNT);

        // Get all the movieList where voteCount is greater than SMALLER_VOTE_COUNT
        defaultMovieShouldBeFound("voteCount.greaterThan=" + SMALLER_VOTE_COUNT);
    }


    @Test
    @Transactional
    public void getAllMoviesByReleaseDateIsEqualToSomething() throws Exception {
        // Initialize the database
        movieRepository.saveAndFlush(movie);

        // Get all the movieList where releaseDate equals to DEFAULT_RELEASE_DATE
        defaultMovieShouldBeFound("releaseDate.equals=" + DEFAULT_RELEASE_DATE);

        // Get all the movieList where releaseDate equals to UPDATED_RELEASE_DATE
        defaultMovieShouldNotBeFound("releaseDate.equals=" + UPDATED_RELEASE_DATE);
    }

    @Test
    @Transactional
    public void getAllMoviesByReleaseDateIsNotEqualToSomething() throws Exception {
        // Initialize the database
        movieRepository.saveAndFlush(movie);

        // Get all the movieList where releaseDate not equals to DEFAULT_RELEASE_DATE
        defaultMovieShouldNotBeFound("releaseDate.notEquals=" + DEFAULT_RELEASE_DATE);

        // Get all the movieList where releaseDate not equals to UPDATED_RELEASE_DATE
        defaultMovieShouldBeFound("releaseDate.notEquals=" + UPDATED_RELEASE_DATE);
    }

    @Test
    @Transactional
    public void getAllMoviesByReleaseDateIsInShouldWork() throws Exception {
        // Initialize the database
        movieRepository.saveAndFlush(movie);

        // Get all the movieList where releaseDate in DEFAULT_RELEASE_DATE or UPDATED_RELEASE_DATE
        defaultMovieShouldBeFound("releaseDate.in=" + DEFAULT_RELEASE_DATE + "," + UPDATED_RELEASE_DATE);

        // Get all the movieList where releaseDate equals to UPDATED_RELEASE_DATE
        defaultMovieShouldNotBeFound("releaseDate.in=" + UPDATED_RELEASE_DATE);
    }

    @Test
    @Transactional
    public void getAllMoviesByReleaseDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        movieRepository.saveAndFlush(movie);

        // Get all the movieList where releaseDate is not null
        defaultMovieShouldBeFound("releaseDate.specified=true");

        // Get all the movieList where releaseDate is null
        defaultMovieShouldNotBeFound("releaseDate.specified=false");
    }

    @Test
    @Transactional
    public void getAllMoviesByReleaseDateIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        movieRepository.saveAndFlush(movie);

        // Get all the movieList where releaseDate is greater than or equal to DEFAULT_RELEASE_DATE
        defaultMovieShouldBeFound("releaseDate.greaterThanOrEqual=" + DEFAULT_RELEASE_DATE);

        // Get all the movieList where releaseDate is greater than or equal to UPDATED_RELEASE_DATE
        defaultMovieShouldNotBeFound("releaseDate.greaterThanOrEqual=" + UPDATED_RELEASE_DATE);
    }

    @Test
    @Transactional
    public void getAllMoviesByReleaseDateIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        movieRepository.saveAndFlush(movie);

        // Get all the movieList where releaseDate is less than or equal to DEFAULT_RELEASE_DATE
        defaultMovieShouldBeFound("releaseDate.lessThanOrEqual=" + DEFAULT_RELEASE_DATE);

        // Get all the movieList where releaseDate is less than or equal to SMALLER_RELEASE_DATE
        defaultMovieShouldNotBeFound("releaseDate.lessThanOrEqual=" + SMALLER_RELEASE_DATE);
    }

    @Test
    @Transactional
    public void getAllMoviesByReleaseDateIsLessThanSomething() throws Exception {
        // Initialize the database
        movieRepository.saveAndFlush(movie);

        // Get all the movieList where releaseDate is less than DEFAULT_RELEASE_DATE
        defaultMovieShouldNotBeFound("releaseDate.lessThan=" + DEFAULT_RELEASE_DATE);

        // Get all the movieList where releaseDate is less than UPDATED_RELEASE_DATE
        defaultMovieShouldBeFound("releaseDate.lessThan=" + UPDATED_RELEASE_DATE);
    }

    @Test
    @Transactional
    public void getAllMoviesByReleaseDateIsGreaterThanSomething() throws Exception {
        // Initialize the database
        movieRepository.saveAndFlush(movie);

        // Get all the movieList where releaseDate is greater than DEFAULT_RELEASE_DATE
        defaultMovieShouldNotBeFound("releaseDate.greaterThan=" + DEFAULT_RELEASE_DATE);

        // Get all the movieList where releaseDate is greater than SMALLER_RELEASE_DATE
        defaultMovieShouldBeFound("releaseDate.greaterThan=" + SMALLER_RELEASE_DATE);
    }


    @Test
    @Transactional
    public void getAllMoviesByLastTMDBUpdateIsEqualToSomething() throws Exception {
        // Initialize the database
        movieRepository.saveAndFlush(movie);

        // Get all the movieList where lastTMDBUpdate equals to DEFAULT_LAST_TMDB_UPDATE
        defaultMovieShouldBeFound("lastTMDBUpdate.equals=" + DEFAULT_LAST_TMDB_UPDATE);

        // Get all the movieList where lastTMDBUpdate equals to UPDATED_LAST_TMDB_UPDATE
        defaultMovieShouldNotBeFound("lastTMDBUpdate.equals=" + UPDATED_LAST_TMDB_UPDATE);
    }

    @Test
    @Transactional
    public void getAllMoviesByLastTMDBUpdateIsNotEqualToSomething() throws Exception {
        // Initialize the database
        movieRepository.saveAndFlush(movie);

        // Get all the movieList where lastTMDBUpdate not equals to DEFAULT_LAST_TMDB_UPDATE
        defaultMovieShouldNotBeFound("lastTMDBUpdate.notEquals=" + DEFAULT_LAST_TMDB_UPDATE);

        // Get all the movieList where lastTMDBUpdate not equals to UPDATED_LAST_TMDB_UPDATE
        defaultMovieShouldBeFound("lastTMDBUpdate.notEquals=" + UPDATED_LAST_TMDB_UPDATE);
    }

    @Test
    @Transactional
    public void getAllMoviesByLastTMDBUpdateIsInShouldWork() throws Exception {
        // Initialize the database
        movieRepository.saveAndFlush(movie);

        // Get all the movieList where lastTMDBUpdate in DEFAULT_LAST_TMDB_UPDATE or UPDATED_LAST_TMDB_UPDATE
        defaultMovieShouldBeFound("lastTMDBUpdate.in=" + DEFAULT_LAST_TMDB_UPDATE + "," + UPDATED_LAST_TMDB_UPDATE);

        // Get all the movieList where lastTMDBUpdate equals to UPDATED_LAST_TMDB_UPDATE
        defaultMovieShouldNotBeFound("lastTMDBUpdate.in=" + UPDATED_LAST_TMDB_UPDATE);
    }

    @Test
    @Transactional
    public void getAllMoviesByLastTMDBUpdateIsNullOrNotNull() throws Exception {
        // Initialize the database
        movieRepository.saveAndFlush(movie);

        // Get all the movieList where lastTMDBUpdate is not null
        defaultMovieShouldBeFound("lastTMDBUpdate.specified=true");

        // Get all the movieList where lastTMDBUpdate is null
        defaultMovieShouldNotBeFound("lastTMDBUpdate.specified=false");
    }

    @Test
    @Transactional
    public void getAllMoviesByLastTMDBUpdateIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        movieRepository.saveAndFlush(movie);

        // Get all the movieList where lastTMDBUpdate is greater than or equal to DEFAULT_LAST_TMDB_UPDATE
        defaultMovieShouldBeFound("lastTMDBUpdate.greaterThanOrEqual=" + DEFAULT_LAST_TMDB_UPDATE);

        // Get all the movieList where lastTMDBUpdate is greater than or equal to UPDATED_LAST_TMDB_UPDATE
        defaultMovieShouldNotBeFound("lastTMDBUpdate.greaterThanOrEqual=" + UPDATED_LAST_TMDB_UPDATE);
    }

    @Test
    @Transactional
    public void getAllMoviesByLastTMDBUpdateIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        movieRepository.saveAndFlush(movie);

        // Get all the movieList where lastTMDBUpdate is less than or equal to DEFAULT_LAST_TMDB_UPDATE
        defaultMovieShouldBeFound("lastTMDBUpdate.lessThanOrEqual=" + DEFAULT_LAST_TMDB_UPDATE);

        // Get all the movieList where lastTMDBUpdate is less than or equal to SMALLER_LAST_TMDB_UPDATE
        defaultMovieShouldNotBeFound("lastTMDBUpdate.lessThanOrEqual=" + SMALLER_LAST_TMDB_UPDATE);
    }

    @Test
    @Transactional
    public void getAllMoviesByLastTMDBUpdateIsLessThanSomething() throws Exception {
        // Initialize the database
        movieRepository.saveAndFlush(movie);

        // Get all the movieList where lastTMDBUpdate is less than DEFAULT_LAST_TMDB_UPDATE
        defaultMovieShouldNotBeFound("lastTMDBUpdate.lessThan=" + DEFAULT_LAST_TMDB_UPDATE);

        // Get all the movieList where lastTMDBUpdate is less than UPDATED_LAST_TMDB_UPDATE
        defaultMovieShouldBeFound("lastTMDBUpdate.lessThan=" + UPDATED_LAST_TMDB_UPDATE);
    }

    @Test
    @Transactional
    public void getAllMoviesByLastTMDBUpdateIsGreaterThanSomething() throws Exception {
        // Initialize the database
        movieRepository.saveAndFlush(movie);

        // Get all the movieList where lastTMDBUpdate is greater than DEFAULT_LAST_TMDB_UPDATE
        defaultMovieShouldNotBeFound("lastTMDBUpdate.greaterThan=" + DEFAULT_LAST_TMDB_UPDATE);

        // Get all the movieList where lastTMDBUpdate is greater than SMALLER_LAST_TMDB_UPDATE
        defaultMovieShouldBeFound("lastTMDBUpdate.greaterThan=" + SMALLER_LAST_TMDB_UPDATE);
    }


    @Test
    @Transactional
    public void getAllMoviesByTmdbIdIsEqualToSomething() throws Exception {
        // Initialize the database
        movieRepository.saveAndFlush(movie);

        // Get all the movieList where tmdbId equals to DEFAULT_TMDB_ID
        defaultMovieShouldBeFound("tmdbId.equals=" + DEFAULT_TMDB_ID);

        // Get all the movieList where tmdbId equals to UPDATED_TMDB_ID
        defaultMovieShouldNotBeFound("tmdbId.equals=" + UPDATED_TMDB_ID);
    }

    @Test
    @Transactional
    public void getAllMoviesByTmdbIdIsNotEqualToSomething() throws Exception {
        // Initialize the database
        movieRepository.saveAndFlush(movie);

        // Get all the movieList where tmdbId not equals to DEFAULT_TMDB_ID
        defaultMovieShouldNotBeFound("tmdbId.notEquals=" + DEFAULT_TMDB_ID);

        // Get all the movieList where tmdbId not equals to UPDATED_TMDB_ID
        defaultMovieShouldBeFound("tmdbId.notEquals=" + UPDATED_TMDB_ID);
    }

    @Test
    @Transactional
    public void getAllMoviesByTmdbIdIsInShouldWork() throws Exception {
        // Initialize the database
        movieRepository.saveAndFlush(movie);

        // Get all the movieList where tmdbId in DEFAULT_TMDB_ID or UPDATED_TMDB_ID
        defaultMovieShouldBeFound("tmdbId.in=" + DEFAULT_TMDB_ID + "," + UPDATED_TMDB_ID);

        // Get all the movieList where tmdbId equals to UPDATED_TMDB_ID
        defaultMovieShouldNotBeFound("tmdbId.in=" + UPDATED_TMDB_ID);
    }

    @Test
    @Transactional
    public void getAllMoviesByTmdbIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        movieRepository.saveAndFlush(movie);

        // Get all the movieList where tmdbId is not null
        defaultMovieShouldBeFound("tmdbId.specified=true");

        // Get all the movieList where tmdbId is null
        defaultMovieShouldNotBeFound("tmdbId.specified=false");
    }
                @Test
    @Transactional
    public void getAllMoviesByTmdbIdContainsSomething() throws Exception {
        // Initialize the database
        movieRepository.saveAndFlush(movie);

        // Get all the movieList where tmdbId contains DEFAULT_TMDB_ID
        defaultMovieShouldBeFound("tmdbId.contains=" + DEFAULT_TMDB_ID);

        // Get all the movieList where tmdbId contains UPDATED_TMDB_ID
        defaultMovieShouldNotBeFound("tmdbId.contains=" + UPDATED_TMDB_ID);
    }

    @Test
    @Transactional
    public void getAllMoviesByTmdbIdNotContainsSomething() throws Exception {
        // Initialize the database
        movieRepository.saveAndFlush(movie);

        // Get all the movieList where tmdbId does not contain DEFAULT_TMDB_ID
        defaultMovieShouldNotBeFound("tmdbId.doesNotContain=" + DEFAULT_TMDB_ID);

        // Get all the movieList where tmdbId does not contain UPDATED_TMDB_ID
        defaultMovieShouldBeFound("tmdbId.doesNotContain=" + UPDATED_TMDB_ID);
    }


    @Test
    @Transactional
    public void getAllMoviesByRuntimeIsEqualToSomething() throws Exception {
        // Initialize the database
        movieRepository.saveAndFlush(movie);

        // Get all the movieList where runtime equals to DEFAULT_RUNTIME
        defaultMovieShouldBeFound("runtime.equals=" + DEFAULT_RUNTIME);

        // Get all the movieList where runtime equals to UPDATED_RUNTIME
        defaultMovieShouldNotBeFound("runtime.equals=" + UPDATED_RUNTIME);
    }

    @Test
    @Transactional
    public void getAllMoviesByRuntimeIsNotEqualToSomething() throws Exception {
        // Initialize the database
        movieRepository.saveAndFlush(movie);

        // Get all the movieList where runtime not equals to DEFAULT_RUNTIME
        defaultMovieShouldNotBeFound("runtime.notEquals=" + DEFAULT_RUNTIME);

        // Get all the movieList where runtime not equals to UPDATED_RUNTIME
        defaultMovieShouldBeFound("runtime.notEquals=" + UPDATED_RUNTIME);
    }

    @Test
    @Transactional
    public void getAllMoviesByRuntimeIsInShouldWork() throws Exception {
        // Initialize the database
        movieRepository.saveAndFlush(movie);

        // Get all the movieList where runtime in DEFAULT_RUNTIME or UPDATED_RUNTIME
        defaultMovieShouldBeFound("runtime.in=" + DEFAULT_RUNTIME + "," + UPDATED_RUNTIME);

        // Get all the movieList where runtime equals to UPDATED_RUNTIME
        defaultMovieShouldNotBeFound("runtime.in=" + UPDATED_RUNTIME);
    }

    @Test
    @Transactional
    public void getAllMoviesByRuntimeIsNullOrNotNull() throws Exception {
        // Initialize the database
        movieRepository.saveAndFlush(movie);

        // Get all the movieList where runtime is not null
        defaultMovieShouldBeFound("runtime.specified=true");

        // Get all the movieList where runtime is null
        defaultMovieShouldNotBeFound("runtime.specified=false");
    }

    @Test
    @Transactional
    public void getAllMoviesByRuntimeIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        movieRepository.saveAndFlush(movie);

        // Get all the movieList where runtime is greater than or equal to DEFAULT_RUNTIME
        defaultMovieShouldBeFound("runtime.greaterThanOrEqual=" + DEFAULT_RUNTIME);

        // Get all the movieList where runtime is greater than or equal to UPDATED_RUNTIME
        defaultMovieShouldNotBeFound("runtime.greaterThanOrEqual=" + UPDATED_RUNTIME);
    }

    @Test
    @Transactional
    public void getAllMoviesByRuntimeIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        movieRepository.saveAndFlush(movie);

        // Get all the movieList where runtime is less than or equal to DEFAULT_RUNTIME
        defaultMovieShouldBeFound("runtime.lessThanOrEqual=" + DEFAULT_RUNTIME);

        // Get all the movieList where runtime is less than or equal to SMALLER_RUNTIME
        defaultMovieShouldNotBeFound("runtime.lessThanOrEqual=" + SMALLER_RUNTIME);
    }

    @Test
    @Transactional
    public void getAllMoviesByRuntimeIsLessThanSomething() throws Exception {
        // Initialize the database
        movieRepository.saveAndFlush(movie);

        // Get all the movieList where runtime is less than DEFAULT_RUNTIME
        defaultMovieShouldNotBeFound("runtime.lessThan=" + DEFAULT_RUNTIME);

        // Get all the movieList where runtime is less than UPDATED_RUNTIME
        defaultMovieShouldBeFound("runtime.lessThan=" + UPDATED_RUNTIME);
    }

    @Test
    @Transactional
    public void getAllMoviesByRuntimeIsGreaterThanSomething() throws Exception {
        // Initialize the database
        movieRepository.saveAndFlush(movie);

        // Get all the movieList where runtime is greater than DEFAULT_RUNTIME
        defaultMovieShouldNotBeFound("runtime.greaterThan=" + DEFAULT_RUNTIME);

        // Get all the movieList where runtime is greater than SMALLER_RUNTIME
        defaultMovieShouldBeFound("runtime.greaterThan=" + SMALLER_RUNTIME);
    }


    @Test
    @Transactional
    public void getAllMoviesByCreditsIsEqualToSomething() throws Exception {
        // Initialize the database
        movieRepository.saveAndFlush(movie);
        Credit credits = CreditResourceIT.createEntity(em);
        em.persist(credits);
        em.flush();
        movie.addCredits(credits);
        movieRepository.saveAndFlush(movie);
        Long creditsId = credits.getId();

        // Get all the movieList where credits equals to creditsId
        defaultMovieShouldBeFound("creditsId.equals=" + creditsId);

        // Get all the movieList where credits equals to creditsId + 1
        defaultMovieShouldNotBeFound("creditsId.equals=" + (creditsId + 1));
    }


    @Test
    @Transactional
    public void getAllMoviesByPosterIsEqualToSomething() throws Exception {
        // Initialize the database
        movieRepository.saveAndFlush(movie);
        Image poster = ImageResourceIT.createEntity(em);
        em.persist(poster);
        em.flush();
        movie.addPoster(poster);
        movieRepository.saveAndFlush(movie);
        Long posterId = poster.getId();

        // Get all the movieList where poster equals to posterId
        defaultMovieShouldBeFound("posterId.equals=" + posterId);

        // Get all the movieList where poster equals to posterId + 1
        defaultMovieShouldNotBeFound("posterId.equals=" + (posterId + 1));
    }


    @Test
    @Transactional
    public void getAllMoviesByBackdropIsEqualToSomething() throws Exception {
        // Initialize the database
        movieRepository.saveAndFlush(movie);
        Image backdrop = ImageResourceIT.createEntity(em);
        em.persist(backdrop);
        em.flush();
        movie.addBackdrop(backdrop);
        movieRepository.saveAndFlush(movie);
        Long backdropId = backdrop.getId();

        // Get all the movieList where backdrop equals to backdropId
        defaultMovieShouldBeFound("backdropId.equals=" + backdropId);

        // Get all the movieList where backdrop equals to backdropId + 1
        defaultMovieShouldNotBeFound("backdropId.equals=" + (backdropId + 1));
    }


    @Test
    @Transactional
    public void getAllMoviesByGenreIsEqualToSomething() throws Exception {
        // Initialize the database
        movieRepository.saveAndFlush(movie);
        Genre genre = GenreResourceIT.createEntity(em);
        em.persist(genre);
        em.flush();
        movie.addGenre(genre);
        movieRepository.saveAndFlush(movie);
        Long genreId = genre.getId();

        // Get all the movieList where genre equals to genreId
        defaultMovieShouldBeFound("genreId.equals=" + genreId);

        // Get all the movieList where genre equals to genreId + 1
        defaultMovieShouldNotBeFound("genreId.equals=" + (genreId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultMovieShouldBeFound(String filter) throws Exception {
        restMovieMockMvc.perform(get("/api/movies?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(movie.getId().intValue())))
            .andExpect(jsonPath("$.[*].title").value(hasItem(DEFAULT_TITLE)))
            .andExpect(jsonPath("$.[*].forAdult").value(hasItem(DEFAULT_FOR_ADULT.booleanValue())))
            .andExpect(jsonPath("$.[*].homepage").value(hasItem(DEFAULT_HOMEPAGE)))
            .andExpect(jsonPath("$.[*].originalLanguage").value(hasItem(DEFAULT_ORIGINAL_LANGUAGE)))
            .andExpect(jsonPath("$.[*].originalTitle").value(hasItem(DEFAULT_ORIGINAL_TITLE)))
            .andExpect(jsonPath("$.[*].overview").value(hasItem(DEFAULT_OVERVIEW)))
            .andExpect(jsonPath("$.[*].tagline").value(hasItem(DEFAULT_TAGLINE)))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.toString())))
            .andExpect(jsonPath("$.[*].voteAverage").value(hasItem(DEFAULT_VOTE_AVERAGE.doubleValue())))
            .andExpect(jsonPath("$.[*].voteCount").value(hasItem(DEFAULT_VOTE_COUNT)))
            .andExpect(jsonPath("$.[*].releaseDate").value(hasItem(DEFAULT_RELEASE_DATE.toString())))
            .andExpect(jsonPath("$.[*].lastTMDBUpdate").value(hasItem(DEFAULT_LAST_TMDB_UPDATE.toString())))
            .andExpect(jsonPath("$.[*].tmdbId").value(hasItem(DEFAULT_TMDB_ID)))
            .andExpect(jsonPath("$.[*].runtime").value(hasItem(DEFAULT_RUNTIME)));

        // Check, that the count call also returns 1
        restMovieMockMvc.perform(get("/api/movies/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultMovieShouldNotBeFound(String filter) throws Exception {
        restMovieMockMvc.perform(get("/api/movies?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restMovieMockMvc.perform(get("/api/movies/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    public void getNonExistingMovie() throws Exception {
        // Get the movie
        restMovieMockMvc.perform(get("/api/movies/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateMovie() throws Exception {
        // Initialize the database
        movieRepository.saveAndFlush(movie);

        int databaseSizeBeforeUpdate = movieRepository.findAll().size();

        // Update the movie
        Movie updatedMovie = movieRepository.findById(movie.getId()).get();
        // Disconnect from session so that the updates on updatedMovie are not directly saved in db
        em.detach(updatedMovie);
        updatedMovie
            .title(UPDATED_TITLE)
            .forAdult(UPDATED_FOR_ADULT)
            .homepage(UPDATED_HOMEPAGE)
            .originalLanguage(UPDATED_ORIGINAL_LANGUAGE)
            .originalTitle(UPDATED_ORIGINAL_TITLE)
            .overview(UPDATED_OVERVIEW)
            .tagline(UPDATED_TAGLINE)
            .status(UPDATED_STATUS)
            .voteAverage(UPDATED_VOTE_AVERAGE)
            .voteCount(UPDATED_VOTE_COUNT)
            .releaseDate(UPDATED_RELEASE_DATE)
            .lastTMDBUpdate(UPDATED_LAST_TMDB_UPDATE)
            .tmdbId(UPDATED_TMDB_ID)
            .runtime(UPDATED_RUNTIME);
        MovieDTO movieDTO = movieMapper.toDto(updatedMovie);

        restMovieMockMvc.perform(put("/api/movies")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(movieDTO)))
            .andExpect(status().isOk());

        // Validate the Movie in the database
        List<Movie> movieList = movieRepository.findAll();
        assertThat(movieList).hasSize(databaseSizeBeforeUpdate);
        Movie testMovie = movieList.get(movieList.size() - 1);
        assertThat(testMovie.getTitle()).isEqualTo(UPDATED_TITLE);
        assertThat(testMovie.isForAdult()).isEqualTo(UPDATED_FOR_ADULT);
        assertThat(testMovie.getHomepage()).isEqualTo(UPDATED_HOMEPAGE);
        assertThat(testMovie.getOriginalLanguage()).isEqualTo(UPDATED_ORIGINAL_LANGUAGE);
        assertThat(testMovie.getOriginalTitle()).isEqualTo(UPDATED_ORIGINAL_TITLE);
        assertThat(testMovie.getOverview()).isEqualTo(UPDATED_OVERVIEW);
        assertThat(testMovie.getTagline()).isEqualTo(UPDATED_TAGLINE);
        assertThat(testMovie.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testMovie.getVoteAverage()).isEqualTo(UPDATED_VOTE_AVERAGE);
        assertThat(testMovie.getVoteCount()).isEqualTo(UPDATED_VOTE_COUNT);
        assertThat(testMovie.getReleaseDate()).isEqualTo(UPDATED_RELEASE_DATE);
        assertThat(testMovie.getLastTMDBUpdate()).isEqualTo(UPDATED_LAST_TMDB_UPDATE);
        assertThat(testMovie.getTmdbId()).isEqualTo(UPDATED_TMDB_ID);
        assertThat(testMovie.getRuntime()).isEqualTo(UPDATED_RUNTIME);
    }

    @Test
    @Transactional
    public void updateNonExistingMovie() throws Exception {
        int databaseSizeBeforeUpdate = movieRepository.findAll().size();

        // Create the Movie
        MovieDTO movieDTO = movieMapper.toDto(movie);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restMovieMockMvc.perform(put("/api/movies")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(movieDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Movie in the database
        List<Movie> movieList = movieRepository.findAll();
        assertThat(movieList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteMovie() throws Exception {
        // Initialize the database
        movieRepository.saveAndFlush(movie);

        int databaseSizeBeforeDelete = movieRepository.findAll().size();

        // Delete the movie
        restMovieMockMvc.perform(delete("/api/movies/{id}", movie.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Movie> movieList = movieRepository.findAll();
        assertThat(movieList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
