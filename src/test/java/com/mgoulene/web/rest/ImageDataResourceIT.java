package com.mgoulene.web.rest;

import com.mgoulene.MmdbApp;
import com.mgoulene.domain.ImageData;
import com.mgoulene.domain.Image;
import com.mgoulene.repository.ImageDataRepository;
import com.mgoulene.service.ImageDataService;
import com.mgoulene.service.dto.ImageDataDTO;
import com.mgoulene.service.mapper.ImageDataMapper;
import com.mgoulene.service.dto.ImageDataCriteria;
import com.mgoulene.service.ImageDataQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link ImageDataResource} REST controller.
 */
@SpringBootTest(classes = MmdbApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class ImageDataResourceIT {

    private static final String DEFAULT_IMAGE_SIZE = "AAAAAAAAAA";
    private static final String UPDATED_IMAGE_SIZE = "BBBBBBBBBB";

    private static final byte[] DEFAULT_IMAGE_BYTES = TestUtil.createByteArray(1, "0");
    private static final byte[] UPDATED_IMAGE_BYTES = TestUtil.createByteArray(1, "1");
    private static final String DEFAULT_IMAGE_BYTES_CONTENT_TYPE = "image/jpg";
    private static final String UPDATED_IMAGE_BYTES_CONTENT_TYPE = "image/png";

    @Autowired
    private ImageDataRepository imageDataRepository;

    @Autowired
    private ImageDataMapper imageDataMapper;

    @Autowired
    private ImageDataService imageDataService;

    @Autowired
    private ImageDataQueryService imageDataQueryService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restImageDataMockMvc;

    private ImageData imageData;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ImageData createEntity(EntityManager em) {
        ImageData imageData = new ImageData()
            .imageSize(DEFAULT_IMAGE_SIZE)
            .imageBytes(DEFAULT_IMAGE_BYTES)
            .imageBytesContentType(DEFAULT_IMAGE_BYTES_CONTENT_TYPE);
        // Add required entity
        Image image;
        if (TestUtil.findAll(em, Image.class).isEmpty()) {
            image = ImageResourceIT.createEntity(em);
            em.persist(image);
            em.flush();
        } else {
            image = TestUtil.findAll(em, Image.class).get(0);
        }
        imageData.setImage(image);
        return imageData;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ImageData createUpdatedEntity(EntityManager em) {
        ImageData imageData = new ImageData()
            .imageSize(UPDATED_IMAGE_SIZE)
            .imageBytes(UPDATED_IMAGE_BYTES)
            .imageBytesContentType(UPDATED_IMAGE_BYTES_CONTENT_TYPE);
        // Add required entity
        Image image;
        if (TestUtil.findAll(em, Image.class).isEmpty()) {
            image = ImageResourceIT.createUpdatedEntity(em);
            em.persist(image);
            em.flush();
        } else {
            image = TestUtil.findAll(em, Image.class).get(0);
        }
        imageData.setImage(image);
        return imageData;
    }

    @BeforeEach
    public void initTest() {
        imageData = createEntity(em);
    }

    @Test
    @Transactional
    public void createImageData() throws Exception {
        int databaseSizeBeforeCreate = imageDataRepository.findAll().size();
        // Create the ImageData
        ImageDataDTO imageDataDTO = imageDataMapper.toDto(imageData);
        restImageDataMockMvc.perform(post("/api/image-data")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(imageDataDTO)))
            .andExpect(status().isCreated());

        // Validate the ImageData in the database
        List<ImageData> imageDataList = imageDataRepository.findAll();
        assertThat(imageDataList).hasSize(databaseSizeBeforeCreate + 1);
        ImageData testImageData = imageDataList.get(imageDataList.size() - 1);
        assertThat(testImageData.getImageSize()).isEqualTo(DEFAULT_IMAGE_SIZE);
        assertThat(testImageData.getImageBytes()).isEqualTo(DEFAULT_IMAGE_BYTES);
        assertThat(testImageData.getImageBytesContentType()).isEqualTo(DEFAULT_IMAGE_BYTES_CONTENT_TYPE);
    }

    @Test
    @Transactional
    public void createImageDataWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = imageDataRepository.findAll().size();

        // Create the ImageData with an existing ID
        imageData.setId(1L);
        ImageDataDTO imageDataDTO = imageDataMapper.toDto(imageData);

        // An entity with an existing ID cannot be created, so this API call must fail
        restImageDataMockMvc.perform(post("/api/image-data")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(imageDataDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ImageData in the database
        List<ImageData> imageDataList = imageDataRepository.findAll();
        assertThat(imageDataList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkImageSizeIsRequired() throws Exception {
        int databaseSizeBeforeTest = imageDataRepository.findAll().size();
        // set the field null
        imageData.setImageSize(null);

        // Create the ImageData, which fails.
        ImageDataDTO imageDataDTO = imageDataMapper.toDto(imageData);


        restImageDataMockMvc.perform(post("/api/image-data")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(imageDataDTO)))
            .andExpect(status().isBadRequest());

        List<ImageData> imageDataList = imageDataRepository.findAll();
        assertThat(imageDataList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllImageData() throws Exception {
        // Initialize the database
        imageDataRepository.saveAndFlush(imageData);

        // Get all the imageDataList
        restImageDataMockMvc.perform(get("/api/image-data?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(imageData.getId().intValue())))
            .andExpect(jsonPath("$.[*].imageSize").value(hasItem(DEFAULT_IMAGE_SIZE)))
            .andExpect(jsonPath("$.[*].imageBytesContentType").value(hasItem(DEFAULT_IMAGE_BYTES_CONTENT_TYPE)))
            .andExpect(jsonPath("$.[*].imageBytes").value(hasItem(Base64Utils.encodeToString(DEFAULT_IMAGE_BYTES))));
    }
    
    @Test
    @Transactional
    public void getImageData() throws Exception {
        // Initialize the database
        imageDataRepository.saveAndFlush(imageData);

        // Get the imageData
        restImageDataMockMvc.perform(get("/api/image-data/{id}", imageData.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(imageData.getId().intValue()))
            .andExpect(jsonPath("$.imageSize").value(DEFAULT_IMAGE_SIZE))
            .andExpect(jsonPath("$.imageBytesContentType").value(DEFAULT_IMAGE_BYTES_CONTENT_TYPE))
            .andExpect(jsonPath("$.imageBytes").value(Base64Utils.encodeToString(DEFAULT_IMAGE_BYTES)));
    }


    @Test
    @Transactional
    public void getImageDataByIdFiltering() throws Exception {
        // Initialize the database
        imageDataRepository.saveAndFlush(imageData);

        Long id = imageData.getId();

        defaultImageDataShouldBeFound("id.equals=" + id);
        defaultImageDataShouldNotBeFound("id.notEquals=" + id);

        defaultImageDataShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultImageDataShouldNotBeFound("id.greaterThan=" + id);

        defaultImageDataShouldBeFound("id.lessThanOrEqual=" + id);
        defaultImageDataShouldNotBeFound("id.lessThan=" + id);
    }


    @Test
    @Transactional
    public void getAllImageDataByImageSizeIsEqualToSomething() throws Exception {
        // Initialize the database
        imageDataRepository.saveAndFlush(imageData);

        // Get all the imageDataList where imageSize equals to DEFAULT_IMAGE_SIZE
        defaultImageDataShouldBeFound("imageSize.equals=" + DEFAULT_IMAGE_SIZE);

        // Get all the imageDataList where imageSize equals to UPDATED_IMAGE_SIZE
        defaultImageDataShouldNotBeFound("imageSize.equals=" + UPDATED_IMAGE_SIZE);
    }

    @Test
    @Transactional
    public void getAllImageDataByImageSizeIsNotEqualToSomething() throws Exception {
        // Initialize the database
        imageDataRepository.saveAndFlush(imageData);

        // Get all the imageDataList where imageSize not equals to DEFAULT_IMAGE_SIZE
        defaultImageDataShouldNotBeFound("imageSize.notEquals=" + DEFAULT_IMAGE_SIZE);

        // Get all the imageDataList where imageSize not equals to UPDATED_IMAGE_SIZE
        defaultImageDataShouldBeFound("imageSize.notEquals=" + UPDATED_IMAGE_SIZE);
    }

    @Test
    @Transactional
    public void getAllImageDataByImageSizeIsInShouldWork() throws Exception {
        // Initialize the database
        imageDataRepository.saveAndFlush(imageData);

        // Get all the imageDataList where imageSize in DEFAULT_IMAGE_SIZE or UPDATED_IMAGE_SIZE
        defaultImageDataShouldBeFound("imageSize.in=" + DEFAULT_IMAGE_SIZE + "," + UPDATED_IMAGE_SIZE);

        // Get all the imageDataList where imageSize equals to UPDATED_IMAGE_SIZE
        defaultImageDataShouldNotBeFound("imageSize.in=" + UPDATED_IMAGE_SIZE);
    }

    @Test
    @Transactional
    public void getAllImageDataByImageSizeIsNullOrNotNull() throws Exception {
        // Initialize the database
        imageDataRepository.saveAndFlush(imageData);

        // Get all the imageDataList where imageSize is not null
        defaultImageDataShouldBeFound("imageSize.specified=true");

        // Get all the imageDataList where imageSize is null
        defaultImageDataShouldNotBeFound("imageSize.specified=false");
    }
                @Test
    @Transactional
    public void getAllImageDataByImageSizeContainsSomething() throws Exception {
        // Initialize the database
        imageDataRepository.saveAndFlush(imageData);

        // Get all the imageDataList where imageSize contains DEFAULT_IMAGE_SIZE
        defaultImageDataShouldBeFound("imageSize.contains=" + DEFAULT_IMAGE_SIZE);

        // Get all the imageDataList where imageSize contains UPDATED_IMAGE_SIZE
        defaultImageDataShouldNotBeFound("imageSize.contains=" + UPDATED_IMAGE_SIZE);
    }

    @Test
    @Transactional
    public void getAllImageDataByImageSizeNotContainsSomething() throws Exception {
        // Initialize the database
        imageDataRepository.saveAndFlush(imageData);

        // Get all the imageDataList where imageSize does not contain DEFAULT_IMAGE_SIZE
        defaultImageDataShouldNotBeFound("imageSize.doesNotContain=" + DEFAULT_IMAGE_SIZE);

        // Get all the imageDataList where imageSize does not contain UPDATED_IMAGE_SIZE
        defaultImageDataShouldBeFound("imageSize.doesNotContain=" + UPDATED_IMAGE_SIZE);
    }


    @Test
    @Transactional
    public void getAllImageDataByImageIsEqualToSomething() throws Exception {
        // Get already existing entity
        Image image = imageData.getImage();
        imageDataRepository.saveAndFlush(imageData);
        Long imageId = image.getId();

        // Get all the imageDataList where image equals to imageId
        defaultImageDataShouldBeFound("imageId.equals=" + imageId);

        // Get all the imageDataList where image equals to imageId + 1
        defaultImageDataShouldNotBeFound("imageId.equals=" + (imageId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultImageDataShouldBeFound(String filter) throws Exception {
        restImageDataMockMvc.perform(get("/api/image-data?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(imageData.getId().intValue())))
            .andExpect(jsonPath("$.[*].imageSize").value(hasItem(DEFAULT_IMAGE_SIZE)))
            .andExpect(jsonPath("$.[*].imageBytesContentType").value(hasItem(DEFAULT_IMAGE_BYTES_CONTENT_TYPE)))
            .andExpect(jsonPath("$.[*].imageBytes").value(hasItem(Base64Utils.encodeToString(DEFAULT_IMAGE_BYTES))));

        // Check, that the count call also returns 1
        restImageDataMockMvc.perform(get("/api/image-data/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultImageDataShouldNotBeFound(String filter) throws Exception {
        restImageDataMockMvc.perform(get("/api/image-data?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restImageDataMockMvc.perform(get("/api/image-data/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    public void getNonExistingImageData() throws Exception {
        // Get the imageData
        restImageDataMockMvc.perform(get("/api/image-data/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateImageData() throws Exception {
        // Initialize the database
        imageDataRepository.saveAndFlush(imageData);

        int databaseSizeBeforeUpdate = imageDataRepository.findAll().size();

        // Update the imageData
        ImageData updatedImageData = imageDataRepository.findById(imageData.getId()).get();
        // Disconnect from session so that the updates on updatedImageData are not directly saved in db
        em.detach(updatedImageData);
        updatedImageData
            .imageSize(UPDATED_IMAGE_SIZE)
            .imageBytes(UPDATED_IMAGE_BYTES)
            .imageBytesContentType(UPDATED_IMAGE_BYTES_CONTENT_TYPE);
        ImageDataDTO imageDataDTO = imageDataMapper.toDto(updatedImageData);

        restImageDataMockMvc.perform(put("/api/image-data")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(imageDataDTO)))
            .andExpect(status().isOk());

        // Validate the ImageData in the database
        List<ImageData> imageDataList = imageDataRepository.findAll();
        assertThat(imageDataList).hasSize(databaseSizeBeforeUpdate);
        ImageData testImageData = imageDataList.get(imageDataList.size() - 1);
        assertThat(testImageData.getImageSize()).isEqualTo(UPDATED_IMAGE_SIZE);
        assertThat(testImageData.getImageBytes()).isEqualTo(UPDATED_IMAGE_BYTES);
        assertThat(testImageData.getImageBytesContentType()).isEqualTo(UPDATED_IMAGE_BYTES_CONTENT_TYPE);
    }

    @Test
    @Transactional
    public void updateNonExistingImageData() throws Exception {
        int databaseSizeBeforeUpdate = imageDataRepository.findAll().size();

        // Create the ImageData
        ImageDataDTO imageDataDTO = imageDataMapper.toDto(imageData);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restImageDataMockMvc.perform(put("/api/image-data")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(imageDataDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ImageData in the database
        List<ImageData> imageDataList = imageDataRepository.findAll();
        assertThat(imageDataList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteImageData() throws Exception {
        // Initialize the database
        imageDataRepository.saveAndFlush(imageData);

        int databaseSizeBeforeDelete = imageDataRepository.findAll().size();

        // Delete the imageData
        restImageDataMockMvc.perform(delete("/api/image-data/{id}", imageData.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<ImageData> imageDataList = imageDataRepository.findAll();
        assertThat(imageDataList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
