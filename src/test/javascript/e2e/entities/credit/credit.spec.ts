import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { CreditComponentsPage, CreditDeleteDialog, CreditUpdatePage } from './credit.page-object';

const expect = chai.expect;

describe('Credit e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let creditComponentsPage: CreditComponentsPage;
  let creditUpdatePage: CreditUpdatePage;
  let creditDeleteDialog: CreditDeleteDialog;

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing('admin', 'admin');
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load Credits', async () => {
    await navBarPage.goToEntity('credit');
    creditComponentsPage = new CreditComponentsPage();
    await browser.wait(ec.visibilityOf(creditComponentsPage.title), 5000);
    expect(await creditComponentsPage.getTitle()).to.eq('mmdbApp.credit.home.title');
    await browser.wait(ec.or(ec.visibilityOf(creditComponentsPage.entities), ec.visibilityOf(creditComponentsPage.noResult)), 1000);
  });

  it('should load create Credit page', async () => {
    await creditComponentsPage.clickOnCreateButton();
    creditUpdatePage = new CreditUpdatePage();
    expect(await creditUpdatePage.getPageTitle()).to.eq('mmdbApp.credit.home.createOrEditLabel');
    await creditUpdatePage.cancel();
  });

  it('should create and save Credits', async () => {
    const nbButtonsBeforeCreate = await creditComponentsPage.countDeleteButtons();

    await creditComponentsPage.clickOnCreateButton();

    await promise.all([
      creditUpdatePage.setTmdbIdInput('tmdbId'),
      creditUpdatePage.setCharacterInput('character'),
      creditUpdatePage.setCreditTypeInput('creditType'),
      creditUpdatePage.setDepartmentInput('department'),
      creditUpdatePage.setJobInput('job'),
      creditUpdatePage.setOrderInput('5'),
      creditUpdatePage.setLastTMDBUpdateInput('2000-12-31'),
      creditUpdatePage.personSelectLastOption(),
      creditUpdatePage.movieSelectLastOption(),
    ]);

    expect(await creditUpdatePage.getTmdbIdInput()).to.eq('tmdbId', 'Expected TmdbId value to be equals to tmdbId');
    expect(await creditUpdatePage.getCharacterInput()).to.eq('character', 'Expected Character value to be equals to character');
    expect(await creditUpdatePage.getCreditTypeInput()).to.eq('creditType', 'Expected CreditType value to be equals to creditType');
    expect(await creditUpdatePage.getDepartmentInput()).to.eq('department', 'Expected Department value to be equals to department');
    expect(await creditUpdatePage.getJobInput()).to.eq('job', 'Expected Job value to be equals to job');
    expect(await creditUpdatePage.getOrderInput()).to.eq('5', 'Expected order value to be equals to 5');
    expect(await creditUpdatePage.getLastTMDBUpdateInput()).to.eq('2000-12-31', 'Expected lastTMDBUpdate value to be equals to 2000-12-31');

    await creditUpdatePage.save();
    expect(await creditUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

    expect(await creditComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1, 'Expected one more entry in the table');
  });

  it('should delete last Credit', async () => {
    const nbButtonsBeforeDelete = await creditComponentsPage.countDeleteButtons();
    await creditComponentsPage.clickOnLastDeleteButton();

    creditDeleteDialog = new CreditDeleteDialog();
    expect(await creditDeleteDialog.getDialogTitle()).to.eq('mmdbApp.credit.delete.question');
    await creditDeleteDialog.clickOnConfirmButton();

    expect(await creditComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
