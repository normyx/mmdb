import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { PersonComponentsPage, PersonDeleteDialog, PersonUpdatePage } from './person.page-object';

const expect = chai.expect;

describe('Person e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let personComponentsPage: PersonComponentsPage;
  let personUpdatePage: PersonUpdatePage;
  let personDeleteDialog: PersonDeleteDialog;

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing('admin', 'admin');
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load People', async () => {
    await navBarPage.goToEntity('person');
    personComponentsPage = new PersonComponentsPage();
    await browser.wait(ec.visibilityOf(personComponentsPage.title), 5000);
    expect(await personComponentsPage.getTitle()).to.eq('mmdbApp.person.home.title');
    await browser.wait(ec.or(ec.visibilityOf(personComponentsPage.entities), ec.visibilityOf(personComponentsPage.noResult)), 1000);
  });

  it('should load create Person page', async () => {
    await personComponentsPage.clickOnCreateButton();
    personUpdatePage = new PersonUpdatePage();
    expect(await personUpdatePage.getPageTitle()).to.eq('mmdbApp.person.home.createOrEditLabel');
    await personUpdatePage.cancel();
  });

  it('should create and save People', async () => {
    const nbButtonsBeforeCreate = await personComponentsPage.countDeleteButtons();

    await personComponentsPage.clickOnCreateButton();

    await promise.all([
      personUpdatePage.setBirthdayInput('2000-12-31'),
      personUpdatePage.setDeathdayInput('2000-12-31'),
      personUpdatePage.setNameInput('name'),
      personUpdatePage.setAkaInput('aka'),
      personUpdatePage.setGenderInput('5'),
      personUpdatePage.setBiographyInput('biography'),
      personUpdatePage.setPlaceOfBirthInput('placeOfBirth'),
      personUpdatePage.setHomepageInput('homepage'),
      personUpdatePage.setLastTMDBUpdateInput('2000-12-31'),
      personUpdatePage.setTmdbIdInput('tmdbId'),
    ]);

    expect(await personUpdatePage.getBirthdayInput()).to.eq('2000-12-31', 'Expected birthday value to be equals to 2000-12-31');
    expect(await personUpdatePage.getDeathdayInput()).to.eq('2000-12-31', 'Expected deathday value to be equals to 2000-12-31');
    expect(await personUpdatePage.getNameInput()).to.eq('name', 'Expected Name value to be equals to name');
    expect(await personUpdatePage.getAkaInput()).to.eq('aka', 'Expected Aka value to be equals to aka');
    expect(await personUpdatePage.getGenderInput()).to.eq('5', 'Expected gender value to be equals to 5');
    expect(await personUpdatePage.getBiographyInput()).to.eq('biography', 'Expected Biography value to be equals to biography');
    expect(await personUpdatePage.getPlaceOfBirthInput()).to.eq('placeOfBirth', 'Expected PlaceOfBirth value to be equals to placeOfBirth');
    expect(await personUpdatePage.getHomepageInput()).to.eq('homepage', 'Expected Homepage value to be equals to homepage');
    expect(await personUpdatePage.getLastTMDBUpdateInput()).to.eq('2000-12-31', 'Expected lastTMDBUpdate value to be equals to 2000-12-31');
    expect(await personUpdatePage.getTmdbIdInput()).to.eq('tmdbId', 'Expected TmdbId value to be equals to tmdbId');

    await personUpdatePage.save();
    expect(await personUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

    expect(await personComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1, 'Expected one more entry in the table');
  });

  it('should delete last Person', async () => {
    const nbButtonsBeforeDelete = await personComponentsPage.countDeleteButtons();
    await personComponentsPage.clickOnLastDeleteButton();

    personDeleteDialog = new PersonDeleteDialog();
    expect(await personDeleteDialog.getDialogTitle()).to.eq('mmdbApp.person.delete.question');
    await personDeleteDialog.clickOnConfirmButton();

    expect(await personComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
