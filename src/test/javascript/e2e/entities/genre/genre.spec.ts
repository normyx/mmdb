import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { GenreComponentsPage, GenreDeleteDialog, GenreUpdatePage } from './genre.page-object';

const expect = chai.expect;

describe('Genre e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let genreComponentsPage: GenreComponentsPage;
  let genreUpdatePage: GenreUpdatePage;
  let genreDeleteDialog: GenreDeleteDialog;

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing('admin', 'admin');
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load Genres', async () => {
    await navBarPage.goToEntity('genre');
    genreComponentsPage = new GenreComponentsPage();
    await browser.wait(ec.visibilityOf(genreComponentsPage.title), 5000);
    expect(await genreComponentsPage.getTitle()).to.eq('mmdbApp.genre.home.title');
    await browser.wait(ec.or(ec.visibilityOf(genreComponentsPage.entities), ec.visibilityOf(genreComponentsPage.noResult)), 1000);
  });

  it('should load create Genre page', async () => {
    await genreComponentsPage.clickOnCreateButton();
    genreUpdatePage = new GenreUpdatePage();
    expect(await genreUpdatePage.getPageTitle()).to.eq('mmdbApp.genre.home.createOrEditLabel');
    await genreUpdatePage.cancel();
  });

  it('should create and save Genres', async () => {
    const nbButtonsBeforeCreate = await genreComponentsPage.countDeleteButtons();

    await genreComponentsPage.clickOnCreateButton();

    await promise.all([
      genreUpdatePage.setTmdbIdInput('tmdbId'),
      genreUpdatePage.setNameInput('name'),
      genreUpdatePage.setLastTMDBUpdateInput('2000-12-31'),
    ]);

    expect(await genreUpdatePage.getTmdbIdInput()).to.eq('tmdbId', 'Expected TmdbId value to be equals to tmdbId');
    expect(await genreUpdatePage.getNameInput()).to.eq('name', 'Expected Name value to be equals to name');
    expect(await genreUpdatePage.getLastTMDBUpdateInput()).to.eq('2000-12-31', 'Expected lastTMDBUpdate value to be equals to 2000-12-31');

    await genreUpdatePage.save();
    expect(await genreUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

    expect(await genreComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1, 'Expected one more entry in the table');
  });

  it('should delete last Genre', async () => {
    const nbButtonsBeforeDelete = await genreComponentsPage.countDeleteButtons();
    await genreComponentsPage.clickOnLastDeleteButton();

    genreDeleteDialog = new GenreDeleteDialog();
    expect(await genreDeleteDialog.getDialogTitle()).to.eq('mmdbApp.genre.delete.question');
    await genreDeleteDialog.clickOnConfirmButton();

    expect(await genreComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
