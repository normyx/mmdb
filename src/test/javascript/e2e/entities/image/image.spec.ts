import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { ImageComponentsPage, ImageDeleteDialog, ImageUpdatePage } from './image.page-object';

const expect = chai.expect;

describe('Image e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let imageComponentsPage: ImageComponentsPage;
  let imageUpdatePage: ImageUpdatePage;
  let imageDeleteDialog: ImageDeleteDialog;

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing('admin', 'admin');
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load Images', async () => {
    await navBarPage.goToEntity('image');
    imageComponentsPage = new ImageComponentsPage();
    await browser.wait(ec.visibilityOf(imageComponentsPage.title), 5000);
    expect(await imageComponentsPage.getTitle()).to.eq('mmdbApp.image.home.title');
    await browser.wait(ec.or(ec.visibilityOf(imageComponentsPage.entities), ec.visibilityOf(imageComponentsPage.noResult)), 1000);
  });

  it('should load create Image page', async () => {
    await imageComponentsPage.clickOnCreateButton();
    imageUpdatePage = new ImageUpdatePage();
    expect(await imageUpdatePage.getPageTitle()).to.eq('mmdbApp.image.home.createOrEditLabel');
    await imageUpdatePage.cancel();
  });

  it('should create and save Images', async () => {
    const nbButtonsBeforeCreate = await imageComponentsPage.countDeleteButtons();

    await imageComponentsPage.clickOnCreateButton();

    await promise.all([
      imageUpdatePage.setTmdbIdInput('tmdbId'),
      imageUpdatePage.setLocaleInput('locale'),
      imageUpdatePage.setVoteAverageInput('5'),
      imageUpdatePage.setVoteCountInput('5'),
      imageUpdatePage.setLastTMDBUpdateInput('2000-12-31'),
      imageUpdatePage.posterMovieSelectLastOption(),
      imageUpdatePage.backdropMovieSelectLastOption(),
      imageUpdatePage.personSelectLastOption(),
    ]);

    expect(await imageUpdatePage.getTmdbIdInput()).to.eq('tmdbId', 'Expected TmdbId value to be equals to tmdbId');
    expect(await imageUpdatePage.getLocaleInput()).to.eq('locale', 'Expected Locale value to be equals to locale');
    expect(await imageUpdatePage.getVoteAverageInput()).to.eq('5', 'Expected voteAverage value to be equals to 5');
    expect(await imageUpdatePage.getVoteCountInput()).to.eq('5', 'Expected voteCount value to be equals to 5');
    expect(await imageUpdatePage.getLastTMDBUpdateInput()).to.eq('2000-12-31', 'Expected lastTMDBUpdate value to be equals to 2000-12-31');

    await imageUpdatePage.save();
    expect(await imageUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

    expect(await imageComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1, 'Expected one more entry in the table');
  });

  it('should delete last Image', async () => {
    const nbButtonsBeforeDelete = await imageComponentsPage.countDeleteButtons();
    await imageComponentsPage.clickOnLastDeleteButton();

    imageDeleteDialog = new ImageDeleteDialog();
    expect(await imageDeleteDialog.getDialogTitle()).to.eq('mmdbApp.image.delete.question');
    await imageDeleteDialog.clickOnConfirmButton();

    expect(await imageComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
