import { element, by, ElementFinder } from 'protractor';

export class ImageComponentsPage {
  createButton = element(by.id('jh-create-entity'));
  deleteButtons = element.all(by.css('jhi-image div table .btn-danger'));
  title = element.all(by.css('jhi-image div h2#page-heading span')).first();
  noResult = element(by.id('no-result'));
  entities = element(by.id('entities'));

  async clickOnCreateButton(): Promise<void> {
    await this.createButton.click();
  }

  async clickOnLastDeleteButton(): Promise<void> {
    await this.deleteButtons.last().click();
  }

  async countDeleteButtons(): Promise<number> {
    return this.deleteButtons.count();
  }

  async getTitle(): Promise<string> {
    return this.title.getAttribute('jhiTranslate');
  }
}

export class ImageUpdatePage {
  pageTitle = element(by.id('jhi-image-heading'));
  saveButton = element(by.id('save-entity'));
  cancelButton = element(by.id('cancel-save'));

  tmdbIdInput = element(by.id('field_tmdbId'));
  localeInput = element(by.id('field_locale'));
  voteAverageInput = element(by.id('field_voteAverage'));
  voteCountInput = element(by.id('field_voteCount'));
  lastTMDBUpdateInput = element(by.id('field_lastTMDBUpdate'));

  posterMovieSelect = element(by.id('field_posterMovie'));
  backdropMovieSelect = element(by.id('field_backdropMovie'));
  personSelect = element(by.id('field_person'));

  async getPageTitle(): Promise<string> {
    return this.pageTitle.getAttribute('jhiTranslate');
  }

  async setTmdbIdInput(tmdbId: string): Promise<void> {
    await this.tmdbIdInput.sendKeys(tmdbId);
  }

  async getTmdbIdInput(): Promise<string> {
    return await this.tmdbIdInput.getAttribute('value');
  }

  async setLocaleInput(locale: string): Promise<void> {
    await this.localeInput.sendKeys(locale);
  }

  async getLocaleInput(): Promise<string> {
    return await this.localeInput.getAttribute('value');
  }

  async setVoteAverageInput(voteAverage: string): Promise<void> {
    await this.voteAverageInput.sendKeys(voteAverage);
  }

  async getVoteAverageInput(): Promise<string> {
    return await this.voteAverageInput.getAttribute('value');
  }

  async setVoteCountInput(voteCount: string): Promise<void> {
    await this.voteCountInput.sendKeys(voteCount);
  }

  async getVoteCountInput(): Promise<string> {
    return await this.voteCountInput.getAttribute('value');
  }

  async setLastTMDBUpdateInput(lastTMDBUpdate: string): Promise<void> {
    await this.lastTMDBUpdateInput.sendKeys(lastTMDBUpdate);
  }

  async getLastTMDBUpdateInput(): Promise<string> {
    return await this.lastTMDBUpdateInput.getAttribute('value');
  }

  async posterMovieSelectLastOption(): Promise<void> {
    await this.posterMovieSelect.all(by.tagName('option')).last().click();
  }

  async posterMovieSelectOption(option: string): Promise<void> {
    await this.posterMovieSelect.sendKeys(option);
  }

  getPosterMovieSelect(): ElementFinder {
    return this.posterMovieSelect;
  }

  async getPosterMovieSelectedOption(): Promise<string> {
    return await this.posterMovieSelect.element(by.css('option:checked')).getText();
  }

  async backdropMovieSelectLastOption(): Promise<void> {
    await this.backdropMovieSelect.all(by.tagName('option')).last().click();
  }

  async backdropMovieSelectOption(option: string): Promise<void> {
    await this.backdropMovieSelect.sendKeys(option);
  }

  getBackdropMovieSelect(): ElementFinder {
    return this.backdropMovieSelect;
  }

  async getBackdropMovieSelectedOption(): Promise<string> {
    return await this.backdropMovieSelect.element(by.css('option:checked')).getText();
  }

  async personSelectLastOption(): Promise<void> {
    await this.personSelect.all(by.tagName('option')).last().click();
  }

  async personSelectOption(option: string): Promise<void> {
    await this.personSelect.sendKeys(option);
  }

  getPersonSelect(): ElementFinder {
    return this.personSelect;
  }

  async getPersonSelectedOption(): Promise<string> {
    return await this.personSelect.element(by.css('option:checked')).getText();
  }

  async save(): Promise<void> {
    await this.saveButton.click();
  }

  async cancel(): Promise<void> {
    await this.cancelButton.click();
  }

  getSaveButton(): ElementFinder {
    return this.saveButton;
  }
}

export class ImageDeleteDialog {
  private dialogTitle = element(by.id('jhi-delete-image-heading'));
  private confirmButton = element(by.id('jhi-confirm-delete-image'));

  async getDialogTitle(): Promise<string> {
    return this.dialogTitle.getAttribute('jhiTranslate');
  }

  async clickOnConfirmButton(): Promise<void> {
    await this.confirmButton.click();
  }
}
