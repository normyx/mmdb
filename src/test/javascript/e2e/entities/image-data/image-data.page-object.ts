import { element, by, ElementFinder } from 'protractor';

export class ImageDataComponentsPage {
  createButton = element(by.id('jh-create-entity'));
  deleteButtons = element.all(by.css('jhi-image-data div table .btn-danger'));
  title = element.all(by.css('jhi-image-data div h2#page-heading span')).first();
  noResult = element(by.id('no-result'));
  entities = element(by.id('entities'));

  async clickOnCreateButton(): Promise<void> {
    await this.createButton.click();
  }

  async clickOnLastDeleteButton(): Promise<void> {
    await this.deleteButtons.last().click();
  }

  async countDeleteButtons(): Promise<number> {
    return this.deleteButtons.count();
  }

  async getTitle(): Promise<string> {
    return this.title.getAttribute('jhiTranslate');
  }
}

export class ImageDataUpdatePage {
  pageTitle = element(by.id('jhi-image-data-heading'));
  saveButton = element(by.id('save-entity'));
  cancelButton = element(by.id('cancel-save'));

  imageSizeInput = element(by.id('field_imageSize'));
  imageBytesInput = element(by.id('file_imageBytes'));

  imageSelect = element(by.id('field_image'));

  async getPageTitle(): Promise<string> {
    return this.pageTitle.getAttribute('jhiTranslate');
  }

  async setImageSizeInput(imageSize: string): Promise<void> {
    await this.imageSizeInput.sendKeys(imageSize);
  }

  async getImageSizeInput(): Promise<string> {
    return await this.imageSizeInput.getAttribute('value');
  }

  async setImageBytesInput(imageBytes: string): Promise<void> {
    await this.imageBytesInput.sendKeys(imageBytes);
  }

  async getImageBytesInput(): Promise<string> {
    return await this.imageBytesInput.getAttribute('value');
  }

  async imageSelectLastOption(): Promise<void> {
    await this.imageSelect.all(by.tagName('option')).last().click();
  }

  async imageSelectOption(option: string): Promise<void> {
    await this.imageSelect.sendKeys(option);
  }

  getImageSelect(): ElementFinder {
    return this.imageSelect;
  }

  async getImageSelectedOption(): Promise<string> {
    return await this.imageSelect.element(by.css('option:checked')).getText();
  }

  async save(): Promise<void> {
    await this.saveButton.click();
  }

  async cancel(): Promise<void> {
    await this.cancelButton.click();
  }

  getSaveButton(): ElementFinder {
    return this.saveButton;
  }
}

export class ImageDataDeleteDialog {
  private dialogTitle = element(by.id('jhi-delete-imageData-heading'));
  private confirmButton = element(by.id('jhi-confirm-delete-imageData'));

  async getDialogTitle(): Promise<string> {
    return this.dialogTitle.getAttribute('jhiTranslate');
  }

  async clickOnConfirmButton(): Promise<void> {
    await this.confirmButton.click();
  }
}
