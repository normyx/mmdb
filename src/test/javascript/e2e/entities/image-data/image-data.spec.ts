import { browser, ExpectedConditions as ec /* , promise */ } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import {
  ImageDataComponentsPage,
  /* ImageDataDeleteDialog, */
  ImageDataUpdatePage,
} from './image-data.page-object';
import * as path from 'path';

const expect = chai.expect;

describe('ImageData e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let imageDataComponentsPage: ImageDataComponentsPage;
  let imageDataUpdatePage: ImageDataUpdatePage;
  /* let imageDataDeleteDialog: ImageDataDeleteDialog; */
  const fileNameToUpload = 'logo-jhipster.png';
  const fileToUpload = '../../../../../../src/main/webapp/content/images/' + fileNameToUpload;
  const absolutePath = path.resolve(__dirname, fileToUpload);

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing('admin', 'admin');
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load ImageData', async () => {
    await navBarPage.goToEntity('image-data');
    imageDataComponentsPage = new ImageDataComponentsPage();
    await browser.wait(ec.visibilityOf(imageDataComponentsPage.title), 5000);
    expect(await imageDataComponentsPage.getTitle()).to.eq('mmdbApp.imageData.home.title');
    await browser.wait(ec.or(ec.visibilityOf(imageDataComponentsPage.entities), ec.visibilityOf(imageDataComponentsPage.noResult)), 1000);
  });

  it('should load create ImageData page', async () => {
    await imageDataComponentsPage.clickOnCreateButton();
    imageDataUpdatePage = new ImageDataUpdatePage();
    expect(await imageDataUpdatePage.getPageTitle()).to.eq('mmdbApp.imageData.home.createOrEditLabel');
    await imageDataUpdatePage.cancel();
  });

  /* it('should create and save ImageData', async () => {
        const nbButtonsBeforeCreate = await imageDataComponentsPage.countDeleteButtons();

        await imageDataComponentsPage.clickOnCreateButton();

        await promise.all([
            imageDataUpdatePage.setImageSizeInput('imageSize'),
            imageDataUpdatePage.setImageBytesInput(absolutePath),
            imageDataUpdatePage.imageSelectLastOption(),
        ]);

        expect(await imageDataUpdatePage.getImageSizeInput()).to.eq('imageSize', 'Expected ImageSize value to be equals to imageSize');
        expect(await imageDataUpdatePage.getImageBytesInput()).to.endsWith(fileNameToUpload, 'Expected ImageBytes value to be end with ' + fileNameToUpload);

        await imageDataUpdatePage.save();
        expect(await imageDataUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

        expect(await imageDataComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1, 'Expected one more entry in the table');
    }); */

  /* it('should delete last ImageData', async () => {
        const nbButtonsBeforeDelete = await imageDataComponentsPage.countDeleteButtons();
        await imageDataComponentsPage.clickOnLastDeleteButton();

        imageDataDeleteDialog = new ImageDataDeleteDialog();
        expect(await imageDataDeleteDialog.getDialogTitle())
            .to.eq('mmdbApp.imageData.delete.question');
        await imageDataDeleteDialog.clickOnConfirmButton();

        expect(await imageDataComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
    }); */

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
