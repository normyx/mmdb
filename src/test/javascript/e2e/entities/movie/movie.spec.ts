import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { MovieComponentsPage, MovieDeleteDialog, MovieUpdatePage } from './movie.page-object';

const expect = chai.expect;

describe('Movie e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let movieComponentsPage: MovieComponentsPage;
  let movieUpdatePage: MovieUpdatePage;
  let movieDeleteDialog: MovieDeleteDialog;

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing('admin', 'admin');
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load Movies', async () => {
    await navBarPage.goToEntity('movie');
    movieComponentsPage = new MovieComponentsPage();
    await browser.wait(ec.visibilityOf(movieComponentsPage.title), 5000);
    expect(await movieComponentsPage.getTitle()).to.eq('mmdbApp.movie.home.title');
    await browser.wait(ec.or(ec.visibilityOf(movieComponentsPage.entities), ec.visibilityOf(movieComponentsPage.noResult)), 1000);
  });

  it('should load create Movie page', async () => {
    await movieComponentsPage.clickOnCreateButton();
    movieUpdatePage = new MovieUpdatePage();
    expect(await movieUpdatePage.getPageTitle()).to.eq('mmdbApp.movie.home.createOrEditLabel');
    await movieUpdatePage.cancel();
  });

  it('should create and save Movies', async () => {
    const nbButtonsBeforeCreate = await movieComponentsPage.countDeleteButtons();

    await movieComponentsPage.clickOnCreateButton();

    await promise.all([
      movieUpdatePage.setTitleInput('title'),
      movieUpdatePage.setHomepageInput('homepage'),
      movieUpdatePage.setOriginalLanguageInput('originalLanguage'),
      movieUpdatePage.setOriginalTitleInput('originalTitle'),
      movieUpdatePage.setOverviewInput('overview'),
      movieUpdatePage.setTaglineInput('tagline'),
      movieUpdatePage.statusSelectLastOption(),
      movieUpdatePage.setVoteAverageInput('5'),
      movieUpdatePage.setVoteCountInput('5'),
      movieUpdatePage.setReleaseDateInput('2000-12-31'),
      movieUpdatePage.setLastTMDBUpdateInput('2000-12-31'),
      movieUpdatePage.setTmdbIdInput('tmdbId'),
      movieUpdatePage.setRuntimeInput('5'),
      // movieUpdatePage.genreSelectLastOption(),
    ]);

    expect(await movieUpdatePage.getTitleInput()).to.eq('title', 'Expected Title value to be equals to title');
    const selectedForAdult = movieUpdatePage.getForAdultInput();
    if (await selectedForAdult.isSelected()) {
      await movieUpdatePage.getForAdultInput().click();
      expect(await movieUpdatePage.getForAdultInput().isSelected(), 'Expected forAdult not to be selected').to.be.false;
    } else {
      await movieUpdatePage.getForAdultInput().click();
      expect(await movieUpdatePage.getForAdultInput().isSelected(), 'Expected forAdult to be selected').to.be.true;
    }
    expect(await movieUpdatePage.getHomepageInput()).to.eq('homepage', 'Expected Homepage value to be equals to homepage');
    expect(await movieUpdatePage.getOriginalLanguageInput()).to.eq(
      'originalLanguage',
      'Expected OriginalLanguage value to be equals to originalLanguage'
    );
    expect(await movieUpdatePage.getOriginalTitleInput()).to.eq(
      'originalTitle',
      'Expected OriginalTitle value to be equals to originalTitle'
    );
    expect(await movieUpdatePage.getOverviewInput()).to.eq('overview', 'Expected Overview value to be equals to overview');
    expect(await movieUpdatePage.getTaglineInput()).to.eq('tagline', 'Expected Tagline value to be equals to tagline');
    expect(await movieUpdatePage.getVoteAverageInput()).to.eq('5', 'Expected voteAverage value to be equals to 5');
    expect(await movieUpdatePage.getVoteCountInput()).to.eq('5', 'Expected voteCount value to be equals to 5');
    expect(await movieUpdatePage.getReleaseDateInput()).to.eq('2000-12-31', 'Expected releaseDate value to be equals to 2000-12-31');
    expect(await movieUpdatePage.getLastTMDBUpdateInput()).to.eq('2000-12-31', 'Expected lastTMDBUpdate value to be equals to 2000-12-31');
    expect(await movieUpdatePage.getTmdbIdInput()).to.eq('tmdbId', 'Expected TmdbId value to be equals to tmdbId');
    expect(await movieUpdatePage.getRuntimeInput()).to.eq('5', 'Expected runtime value to be equals to 5');

    await movieUpdatePage.save();
    expect(await movieUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

    expect(await movieComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1, 'Expected one more entry in the table');
  });

  it('should delete last Movie', async () => {
    const nbButtonsBeforeDelete = await movieComponentsPage.countDeleteButtons();
    await movieComponentsPage.clickOnLastDeleteButton();

    movieDeleteDialog = new MovieDeleteDialog();
    expect(await movieDeleteDialog.getDialogTitle()).to.eq('mmdbApp.movie.delete.question');
    await movieDeleteDialog.clickOnConfirmButton();

    expect(await movieComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
