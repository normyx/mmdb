import { element, by, ElementFinder } from 'protractor';

export class MovieComponentsPage {
  createButton = element(by.id('jh-create-entity'));
  deleteButtons = element.all(by.css('jhi-movie div table .btn-danger'));
  title = element.all(by.css('jhi-movie div h2#page-heading span')).first();
  noResult = element(by.id('no-result'));
  entities = element(by.id('entities'));

  async clickOnCreateButton(): Promise<void> {
    await this.createButton.click();
  }

  async clickOnLastDeleteButton(): Promise<void> {
    await this.deleteButtons.last().click();
  }

  async countDeleteButtons(): Promise<number> {
    return this.deleteButtons.count();
  }

  async getTitle(): Promise<string> {
    return this.title.getAttribute('jhiTranslate');
  }
}

export class MovieUpdatePage {
  pageTitle = element(by.id('jhi-movie-heading'));
  saveButton = element(by.id('save-entity'));
  cancelButton = element(by.id('cancel-save'));

  titleInput = element(by.id('field_title'));
  forAdultInput = element(by.id('field_forAdult'));
  homepageInput = element(by.id('field_homepage'));
  originalLanguageInput = element(by.id('field_originalLanguage'));
  originalTitleInput = element(by.id('field_originalTitle'));
  overviewInput = element(by.id('field_overview'));
  taglineInput = element(by.id('field_tagline'));
  statusSelect = element(by.id('field_status'));
  voteAverageInput = element(by.id('field_voteAverage'));
  voteCountInput = element(by.id('field_voteCount'));
  releaseDateInput = element(by.id('field_releaseDate'));
  lastTMDBUpdateInput = element(by.id('field_lastTMDBUpdate'));
  tmdbIdInput = element(by.id('field_tmdbId'));
  runtimeInput = element(by.id('field_runtime'));

  genreSelect = element(by.id('field_genre'));

  async getPageTitle(): Promise<string> {
    return this.pageTitle.getAttribute('jhiTranslate');
  }

  async setTitleInput(title: string): Promise<void> {
    await this.titleInput.sendKeys(title);
  }

  async getTitleInput(): Promise<string> {
    return await this.titleInput.getAttribute('value');
  }

  getForAdultInput(): ElementFinder {
    return this.forAdultInput;
  }

  async setHomepageInput(homepage: string): Promise<void> {
    await this.homepageInput.sendKeys(homepage);
  }

  async getHomepageInput(): Promise<string> {
    return await this.homepageInput.getAttribute('value');
  }

  async setOriginalLanguageInput(originalLanguage: string): Promise<void> {
    await this.originalLanguageInput.sendKeys(originalLanguage);
  }

  async getOriginalLanguageInput(): Promise<string> {
    return await this.originalLanguageInput.getAttribute('value');
  }

  async setOriginalTitleInput(originalTitle: string): Promise<void> {
    await this.originalTitleInput.sendKeys(originalTitle);
  }

  async getOriginalTitleInput(): Promise<string> {
    return await this.originalTitleInput.getAttribute('value');
  }

  async setOverviewInput(overview: string): Promise<void> {
    await this.overviewInput.sendKeys(overview);
  }

  async getOverviewInput(): Promise<string> {
    return await this.overviewInput.getAttribute('value');
  }

  async setTaglineInput(tagline: string): Promise<void> {
    await this.taglineInput.sendKeys(tagline);
  }

  async getTaglineInput(): Promise<string> {
    return await this.taglineInput.getAttribute('value');
  }

  async setStatusSelect(status: string): Promise<void> {
    await this.statusSelect.sendKeys(status);
  }

  async getStatusSelect(): Promise<string> {
    return await this.statusSelect.element(by.css('option:checked')).getText();
  }

  async statusSelectLastOption(): Promise<void> {
    await this.statusSelect.all(by.tagName('option')).last().click();
  }

  async setVoteAverageInput(voteAverage: string): Promise<void> {
    await this.voteAverageInput.sendKeys(voteAverage);
  }

  async getVoteAverageInput(): Promise<string> {
    return await this.voteAverageInput.getAttribute('value');
  }

  async setVoteCountInput(voteCount: string): Promise<void> {
    await this.voteCountInput.sendKeys(voteCount);
  }

  async getVoteCountInput(): Promise<string> {
    return await this.voteCountInput.getAttribute('value');
  }

  async setReleaseDateInput(releaseDate: string): Promise<void> {
    await this.releaseDateInput.sendKeys(releaseDate);
  }

  async getReleaseDateInput(): Promise<string> {
    return await this.releaseDateInput.getAttribute('value');
  }

  async setLastTMDBUpdateInput(lastTMDBUpdate: string): Promise<void> {
    await this.lastTMDBUpdateInput.sendKeys(lastTMDBUpdate);
  }

  async getLastTMDBUpdateInput(): Promise<string> {
    return await this.lastTMDBUpdateInput.getAttribute('value');
  }

  async setTmdbIdInput(tmdbId: string): Promise<void> {
    await this.tmdbIdInput.sendKeys(tmdbId);
  }

  async getTmdbIdInput(): Promise<string> {
    return await this.tmdbIdInput.getAttribute('value');
  }

  async setRuntimeInput(runtime: string): Promise<void> {
    await this.runtimeInput.sendKeys(runtime);
  }

  async getRuntimeInput(): Promise<string> {
    return await this.runtimeInput.getAttribute('value');
  }

  async genreSelectLastOption(): Promise<void> {
    await this.genreSelect.all(by.tagName('option')).last().click();
  }

  async genreSelectOption(option: string): Promise<void> {
    await this.genreSelect.sendKeys(option);
  }

  getGenreSelect(): ElementFinder {
    return this.genreSelect;
  }

  async getGenreSelectedOption(): Promise<string> {
    return await this.genreSelect.element(by.css('option:checked')).getText();
  }

  async save(): Promise<void> {
    await this.saveButton.click();
  }

  async cancel(): Promise<void> {
    await this.cancelButton.click();
  }

  getSaveButton(): ElementFinder {
    return this.saveButton;
  }
}

export class MovieDeleteDialog {
  private dialogTitle = element(by.id('jhi-delete-movie-heading'));
  private confirmButton = element(by.id('jhi-confirm-delete-movie'));

  async getDialogTitle(): Promise<string> {
    return this.dialogTitle.getAttribute('jhiTranslate');
  }

  async clickOnConfirmButton(): Promise<void> {
    await this.confirmButton.click();
  }
}
