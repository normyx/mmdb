import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { MmdbTestModule } from '../../../test.module';
import { CreditDetailComponent } from 'app/entities/credit/credit-detail.component';
import { Credit } from 'app/shared/model/credit.model';

describe('Component Tests', () => {
  describe('Credit Management Detail Component', () => {
    let comp: CreditDetailComponent;
    let fixture: ComponentFixture<CreditDetailComponent>;
    const route = ({ data: of({ credit: new Credit(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MmdbTestModule],
        declarations: [CreditDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }],
      })
        .overrideTemplate(CreditDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(CreditDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load credit on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.credit).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
