import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';
import { JhiDataUtils } from 'ng-jhipster';

import { MmdbTestModule } from '../../../test.module';
import { ImageDataDetailComponent } from 'app/entities/image-data/image-data-detail.component';
import { ImageData } from 'app/shared/model/image-data.model';

describe('Component Tests', () => {
  describe('ImageData Management Detail Component', () => {
    let comp: ImageDataDetailComponent;
    let fixture: ComponentFixture<ImageDataDetailComponent>;
    let dataUtils: JhiDataUtils;
    const route = ({ data: of({ imageData: new ImageData(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MmdbTestModule],
        declarations: [ImageDataDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }],
      })
        .overrideTemplate(ImageDataDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(ImageDataDetailComponent);
      comp = fixture.componentInstance;
      dataUtils = fixture.debugElement.injector.get(JhiDataUtils);
    });

    describe('OnInit', () => {
      it('Should load imageData on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.imageData).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });

    describe('byteSize', () => {
      it('Should call byteSize from JhiDataUtils', () => {
        // GIVEN
        spyOn(dataUtils, 'byteSize');
        const fakeBase64 = 'fake base64';

        // WHEN
        comp.byteSize(fakeBase64);

        // THEN
        expect(dataUtils.byteSize).toBeCalledWith(fakeBase64);
      });
    });

    describe('openFile', () => {
      it('Should call openFile from JhiDataUtils', () => {
        // GIVEN
        spyOn(dataUtils, 'openFile');
        const fakeContentType = 'fake content type';
        const fakeBase64 = 'fake base64';

        // WHEN
        comp.openFile(fakeContentType, fakeBase64);

        // THEN
        expect(dataUtils.openFile).toBeCalledWith(fakeContentType, fakeBase64);
      });
    });
  });
});
